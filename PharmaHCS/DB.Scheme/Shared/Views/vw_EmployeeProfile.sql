﻿






CREATE view [Shared].[vw_EmployeeProfile]
as
select  a.EmployeeProfileId	
	,	a.EmployeeId
	,	u1.EmployeeCode +' | '+ u1.EmployeeName EmployeeName 
	,	a.ProfileId
	,	tp.ProfileCode +' | '+ tp.ProfileName ProfileName  
	,	a.FromDate
	,	a.ToDate
	,	u.CountryId
	,	c.CountryCode
	, ISNULL(a.LastModifiedDate, a.CreationDate) LastUpdate
	, ISNULL(a.LastModifiedById, a.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.EmployeeProfile a
	left join Shared.TeamProfile tp on tp.ProfileId = a.ProfileId
	LEFT JOIN HR.Employee u1 ON u1.EmployeeId = a.EmployeeId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(a.LastModifiedById, a.CreatedById)
	left join Shared.Country c on u.CountryId = c.CountryId