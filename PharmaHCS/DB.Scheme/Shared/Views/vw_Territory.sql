﻿
create view [Shared].[vw_Territory]
as
select  e.TerritoryId
	,	e.TerritoryCode
	,	e.TerritoryName
	,	e.ParentTerritoryId
	, isnull(acc.TerritoryCount,0) TerritoryCount
	, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.Territory e
	left join (select count(TerritoryId) TerritoryCount, TerritoryId from shared.Territory group by TerritoryId) acc
		on e.TerritoryId = acc.TerritoryId
	left join Shared.Country c on e.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)