﻿Create view [shared].[vw_Country]
as
select  c.CountryId
	, c.CountryCode
	, c.CountryName
	, ISNULL(c.LastModifiedDate, c.CreationDate) LastUpdate
	, ISNULL(c.LastModifiedById, c.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.Country c
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(c.LastModifiedById, c.CreatedById)