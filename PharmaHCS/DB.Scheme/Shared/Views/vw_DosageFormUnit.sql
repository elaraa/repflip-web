﻿



create view [Shared].[vw_DosageFormUnit]
as
SELECT      e.UnitId 
		,	e.UnitCode
		,	e.UnitName
		,	tp.DosageFormCount
		, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
			from Shared.DosageFormUnit e
			left join (select UnitId , count(*) DosageFormCount  from Shared.ProductDosageForm group by UnitId) tp
			on tp.UnitId = e.UnitId
				LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId