﻿



create view [Shared].[vw_TimeDefinition]
as
SELECT      e.TimeDefinitionId 
		,	e.PeriodName
		,	e.FromDate
		,	e.ToDate
		,	e.QuarterName
		,	e.[Year]
		, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
			from Shared.TimeDefinition e
				LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId