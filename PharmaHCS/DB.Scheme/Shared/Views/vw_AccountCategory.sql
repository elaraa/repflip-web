﻿


CREATE view [Shared].[vw_AccountCategory]
as
select  e.CategoryId
	, e.CategoryCode
	, e.CategoryName
	, e.ParentCategoryId
	, isnull(acc.AccountsCount,0) AccountsCount
	, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.AccountCategory e
	left join (select count(AccountId) AccountsCount, CategoryId from shared.Account group by CategoryId) acc
		on e.CategoryId = acc.CategoryId
	left join Shared.Country c on e.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)