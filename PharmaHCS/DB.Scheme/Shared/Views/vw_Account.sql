﻿




CREATE view [Shared].[vw_Account]
as
select  a.AccountId
	,	a.AccountCode
	,	a.AccountName
	,	a.AccountTypeId
	,	act.AccountTypeCode +' | '+ act.AccountTypeName AccountTypeName 
	,	a.Address
	,	a.TerritoryId
	,	terr.TerritoryCode +' | '+ terr.TerritoryName TerritoryName
	,	a.PotentialId
	,	p.PotentialCode +' | '+ p.PotentialName PotentialName
	,	a.Active
	,	a.Approved
	,	a.CategoryId
	,	ca.CategoryCode +' | '+ ca.CategoryName CategoryName 
	,	a.CountryId
	,	c.CountryCode
	, ISNULL(a.LastModifiedDate, a.CreationDate) LastUpdate
	, ISNULL(a.LastModifiedById, a.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Shared.Account a
	JOIN Shared.AccountType act on a.AccountTypeId = act.AccountTypeId
	JOIN Shared.Territory terr on a.TerritoryId = terr.TerritoryId
	LEFT JOIN CRM.Potential p on a.PotentialId = p.PotentialId
	join Shared.AccountCategory ca on ca.CategoryId = a.CategoryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(a.LastModifiedById, a.CreatedById)
	left join Shared.Country c on a.CountryId = c.CountryId