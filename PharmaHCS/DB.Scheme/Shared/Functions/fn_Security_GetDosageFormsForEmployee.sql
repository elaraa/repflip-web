﻿

-- =============================================
-- Author:		Alaa Elsayed
-- Create date: 26/07/2020
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_Security_GetDosageFormsForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	 select distinct  pdf.DosageFormId,pdf.DosageFormName  from Sales.TeamSalesProduct p join
	 Shared.Product pr on p.ProductId=pr.ProductId join
	 Shared.ProductDosageForm pdf on pr.ProductId=pdf.ProductId join
	 Shared.TeamProfile tp on tp.TeamId = p.TeamId join
	 Shared.EmployeeProfile ep on ep.ProfileId = tp.ProfileId
	  where Shared.fnCompareDates(@date, @date, ep.FromDate, ep.ToDate) = 1
	  and exists(select top 1 1 from hr.fn_Security_GetEmployeesForEmployee(@employeeId, @date) se where ep.EmployeeId = se.EmployeeId)
)