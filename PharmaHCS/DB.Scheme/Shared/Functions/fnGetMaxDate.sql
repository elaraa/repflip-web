﻿
-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 18Sep19
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fnGetMaxDate] 
(
	-- Add the parameters for the function here
	@d1 date,
	@d2 date
)
RETURNS date
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result date

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = case when isnull(@d1,getdate()) < isnull(@d2,getdate()) then isnull(@d2,getdate()) else isnull(@d1,getdate()) end

	-- Return the result of the function
	RETURN @Result

END