﻿

-- =============================================
-- Author:		Alaa Elsayed
-- Create date: 26/07/2020
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_Security_GetTerritoriesForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select distinct pt.TerritoryId,t.TerritoryName,t.TerritoryCode  from  Sales.ProfileTerritory pt join
	   Shared.Territory t on t.TerritoryId=pt.TerritoryId join
	    Shared.EmployeeProfile ep on ep.ProfileId=pt.ProfileId  join
	   Shared.TeamProfile tp on pt.ProfileId = tp.ProfileId 

	where Shared.fnCompareDates(@date, @date, ep.FromDate, ep.ToDate) = 1
	and exists(select top 1 1 from hr.fn_Security_GetEmployeesForEmployee(@employeeId, @date) se where ep.EmployeeId = se.EmployeeId)

	)