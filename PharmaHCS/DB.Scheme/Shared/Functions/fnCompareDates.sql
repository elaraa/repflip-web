﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 18Sep19
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fnCompareDates] 
(
	-- Add the parameters for the function here
	@dFrom1 date,
	@dTo1 date,
	@dFrom2 date,
	@dTo2 date

)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = 
		case 
			when @dFrom1 between @dFrom2 and isnull(@dTo2,getdate()) then 1
			when isnull(@dTo1,getdate()) between @dFrom2 and isnull(@dTo2,getdate()) then 1
			when @dFrom1 < @dFrom2 and isnull(@dTo1,getdate()) > isnull(@dTo2,getdate()) then 1
			when @dTo1 is not null and @dTo2 is null and @dTo1> getdate() then 1
			else 0
			end

	-- Return the result of the function
	RETURN @Result

END