﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 2 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_AccountCategory_GetParentLevelForLeaf] 
(
	-- Add the parameters for the function here
	@Level int , 
	@countryId int = null
)
RETURNS TABLE 
AS
RETURN 
(
	WITH AccountCategory_Cte
	AS
	(
		SELECT O.CategoryId, O.CategoryName,  O.ParentCategoryId, Depth AS Lvl,
			-- #2
			o.CategoryId as [RootCategoryId], O.CategoryName RootCategoryName
		FROM Shared.AccountCategory O
		-- #1
		where (o.CountryId = @countryId or @countryId is null)and Depth = @Level
		UNION ALL
		SELECT OG.CategoryId,OG.CategoryName, OG.ParentCategoryId, Lvl+1 AS Lvl,
			-- #2
			oi.RootCategoryId, oi.CategoryName
		FROM Shared.AccountCategory OG INNER JOIN AccountCategory_Cte OI
		ON OI.CategoryId = OG.ParentCategoryId
			and (OG.CountryId = @countryId or @countryId is null)
	)
	SELECT CategoryId,CategoryName, RootCategoryId,RootCategoryName
	FROM AccountCategory_Cte
)