﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 29 Sep 19
-- Description:	
-- =============================================
CREATE FUNCTION Shared.fn_GetLeafTerritories
(	
	-- Add the parameters for the function here
	@countryId int
)
RETURNS TABLE 
AS
RETURN 
(
	select *
	from Shared.Territory 
	where TerritoryId not in (select distinct ParentTerritoryId from Shared.Territory where ParentTerritoryId is not null)
	and CountryId = @countryId
)