﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 29 Sep 19
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_Security_GetProductsForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select distinct h.TeamId, tsp.ProductId
	from hr.fn_Security_GetEmployeesForEmployee(@employeeId,@date) e
		join hr.EmployeeHistory h on e.EmployeeId = h.EmployeeId
			and Shared.fnCompareDates(@date, @date , h.FromDate, h.ToDate) =1
		join sales.TeamSalesProduct tsp on tsp.TeamId = h.TeamId 
			and Shared.fnCompareDates(@date, @date, tsp.FromDate, tsp.ToDate) =1
	----OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 
)