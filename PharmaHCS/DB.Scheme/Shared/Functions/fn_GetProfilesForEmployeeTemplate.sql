﻿
-- =============================================
-- Author:		Alaa Elsayed
-- Create date: 23/06/2020
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_GetProfilesForEmployeeTemplate]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select  eh.EmployeeHistoryId,eh.EmployeeCode,eh.Employee,eh.NationalIDNumber,eh.Department,eh.ManagerCode,eh.CanVisit,eh.JobTitle,eh.Team ,tp.ProfileCode,tp.ProfileName,ep.EmployeeId, ep.ProfileId, tp.TeamId
	from Shared.EmployeeProfile ep
		join Shared.TeamProfile tp on ep.ProfileId = tp.ProfileId
		join HR.vw_EmployeeHistory eh on ep.EmployeeId=eh.EmployeeId and tp.TeamId=eh.TeamId
	where Shared.fnCompareDates(@date, @date, ep.FromDate, ep.ToDate) = 1
	and exists(select top 1 1 from hr.fn_Security_GetEmployeesForEmployee(@employeeId, @date) se where ep.EmployeeId = se.EmployeeId)
)