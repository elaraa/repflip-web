﻿
-- =============================================
-- Author:		
-- Create date: 26/07/2020
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_Security_GetTeamsForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select distinct tp.TeamId,t.TeamName
	from Shared.EmployeeProfile ep join 
		Shared.TeamProfile tp on ep.ProfileId = tp.ProfileId join
		Shared.Team t on tp.TeamId=t.TeamId 
	where Shared.fnCompareDates(@date, @date, ep.FromDate, ep.ToDate) = 1
	and exists(select top 1 1 from hr.fn_Security_GetEmployeesForEmployee(@employeeId, @date) se where ep.EmployeeId = se.EmployeeId)
)