﻿

-- =============================================
-- Author:		Alaa Elsayed
-- Create date: 26/07/2020
-- Description:	
-- =============================================
CREATE FUNCTION [Shared].[fn_GetEmployeesForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	select distinct f.EmployeeId,e.EmployeeName
	from HR.Employee e join
	HR.fn_Security_GetEmployeesForEmployee(@employeeId,@date) f on f.EmployeeId=e.EmployeeId
)