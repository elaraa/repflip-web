﻿CREATE TABLE [Shared].[ProductGroup] (
    [GroupId]          INT            IDENTITY (1, 1) NOT NULL,
    [GroupName]        NVARCHAR (MAX) NOT NULL,
    [GroupCode]        NVARCHAR (50)  NULL,
    [ParentGroupId]    INT            NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [DF_ProductGroup_CreationDate] DEFAULT (getdate()) NOT NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_ProductGroup] PRIMARY KEY CLUSTERED ([GroupId] ASC),
    CONSTRAINT [FK_ProductGroup_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_ProductGroup_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductGroup_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductGroup_ParentProductGroup] FOREIGN KEY ([ParentGroupId]) REFERENCES [Shared].[ProductGroup] ([GroupId])
);

