﻿CREATE TABLE [Shared].[Product] (
    [ProductId]         INT            IDENTITY (1, 1) NOT NULL,
    [ProductCode]       NVARCHAR (50)  NULL,
    [ProductName]       NVARCHAR (50)  NOT NULL,
    [Indications]       NVARCHAR (MAX) NULL,
    [Contraindications] NVARCHAR (MAX) NULL,
    [SideEffects]       NVARCHAR (MAX) NULL,
    [Storage]           NVARCHAR (MAX) NULL,
    [Active]            BIT            CONSTRAINT [DF_Product_Active_1] DEFAULT ((1)) NOT NULL,
    [ParentProductId]   INT            NULL,
    [GroupId]           INT            NULL,
    [CountryId]         INT            NULL,
    [CreationDate]      DATETIME       CONSTRAINT [Product_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]  DATETIME       NULL,
    [CreatedById]       INT            NULL,
    [LastModifiedById]  INT            NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC),
    CONSTRAINT [FK_Product_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Product_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Product_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Product_ParentProduct] FOREIGN KEY ([ParentProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_Product_ProductGroup] FOREIGN KEY ([GroupId]) REFERENCES [Shared].[ProductGroup] ([GroupId])
);

