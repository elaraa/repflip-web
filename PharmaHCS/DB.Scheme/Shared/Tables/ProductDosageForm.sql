﻿CREATE TABLE [Shared].[ProductDosageForm] (
    [DosageFormId]     INT             IDENTITY (1, 1) NOT NULL,
    [DosageFormCode]   NVARCHAR (50)   NOT NULL,
    [DosageFormName]   NVARCHAR (MAX)  NOT NULL,
    [ProductId]        INT             NOT NULL,
    [Size]             INT             NULL,
    [UnitId]           INT             NULL,
    [PackFactor]       DECIMAL (18, 6) CONSTRAINT [DF_ProductDosageForm_PackFactor] DEFAULT ((1)) NULL,
    [Active]           BIT             CONSTRAINT [DF_ProductDosageForm_Active] DEFAULT ((1)) NOT NULL,
    [CountryId]        INT             NULL,
    [CreationDate]     DATETIME        CONSTRAINT [ProductDosageForm_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME        NULL,
    [CreatedById]      INT             NULL,
    [LastModifiedById] INT             NULL,
    CONSTRAINT [PK_ProductDosageForm] PRIMARY KEY CLUSTERED ([DosageFormId] ASC),
    CONSTRAINT [FK_ProductDosageForm_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_ProductDosageForm_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductDosageForm_DosageFormUnit] FOREIGN KEY ([UnitId]) REFERENCES [Shared].[DosageFormUnit] ([UnitId]),
    CONSTRAINT [FK_ProductDosageForm_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProductDosageForm_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId])
);

