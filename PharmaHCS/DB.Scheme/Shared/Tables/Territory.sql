﻿CREATE TABLE [Shared].[Territory] (
    [TerritoryId]       INT            IDENTITY (1, 1) NOT NULL,
    [TerritoryCode]     NVARCHAR (50)  NOT NULL,
    [TerritoryName]     NVARCHAR (MAX) NOT NULL,
    [ParentTerritoryId] INT            NULL,
    [Depth]             INT            CONSTRAINT [DF_Territory_Depth] DEFAULT ((1)) NOT NULL,
    [CountryId]         INT            NOT NULL,
    [CreationDate]      DATETIME       CONSTRAINT [Territory_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]  DATETIME       NULL,
    [CreatedById]       INT            NULL,
    [LastModifiedById]  INT            NULL,
    CONSTRAINT [PK_Territory] PRIMARY KEY CLUSTERED ([TerritoryId] ASC),
    CONSTRAINT [FK_Territory_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Territory_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Territory_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Territory_ParentTerritory] FOREIGN KEY ([ParentTerritoryId]) REFERENCES [Shared].[Territory] ([TerritoryId])
);

