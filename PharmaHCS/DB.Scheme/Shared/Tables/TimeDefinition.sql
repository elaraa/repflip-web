﻿CREATE TABLE [Shared].[TimeDefinition] (
    [TimeDefinitionId] INT            IDENTITY (1, 1) NOT NULL,
    [PeriodName]       NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NOT NULL,
    [FromDate]         DATE           NOT NULL,
    [ToDate]           DATE           NOT NULL,
    [QuarterName]      NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NULL,
    [Year]             INT            NOT NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [TimeDefinition_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_TimeDefinition] PRIMARY KEY CLUSTERED ([TimeDefinitionId] ASC),
    CONSTRAINT [FK_TimeDefinition_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_TimeDefinition_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TimeDefinition_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

