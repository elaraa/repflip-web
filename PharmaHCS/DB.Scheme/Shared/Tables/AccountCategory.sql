﻿CREATE TABLE [Shared].[AccountCategory] (
    [CategoryId]       INT            IDENTITY (1, 1) NOT NULL,
    [CategoryName]     NVARCHAR (MAX) NOT NULL,
    [CategoryCode]     NVARCHAR (50)  NULL,
    [ParentCategoryId] INT            NULL,
    [Depth]            INT            CONSTRAINT [DF_AccountCategory_Depth] DEFAULT ((1)) NOT NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [AccountCategory_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_AccountCategory] PRIMARY KEY CLUSTERED ([CategoryId] ASC),
    CONSTRAINT [FK_AccountCategory_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_AccountCategory_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AccountCategory_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_AccountCategory_ParentAccountCategory] FOREIGN KEY ([ParentCategoryId]) REFERENCES [Shared].[AccountCategory] ([CategoryId])
);

