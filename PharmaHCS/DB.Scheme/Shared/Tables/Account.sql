﻿CREATE TABLE [Shared].[Account] (
    [AccountId]        INT              IDENTITY (1, 1) NOT NULL,
    [AccountName]      NVARCHAR (MAX)   NOT NULL,
    [AccountCode]      NVARCHAR (50)    NULL,
    [AccountTypeId]    INT              NOT NULL,
    [Address]          NVARCHAR (MAX)   NULL,
    [TerritoryId]      INT              NOT NULL,
    [PotentialId]      INT              NULL,
    [Active]           BIT              NOT NULL,
    [Approved]         BIT              NOT NULL,
    [CategoryId]       INT              NOT NULL,
    [Latitude]         DECIMAL (18, 10) NULL,
    [Longitude]        DECIMAL (18, 10) NULL,
    [CountryId]        INT              NOT NULL,
    [CreationDate]     DATETIME         CONSTRAINT [Account_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME         NULL,
    [CreatedById]      INT              NULL,
    [LastModifiedById] INT              NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([AccountId] ASC)
);

