﻿CREATE TABLE [Shared].[DosageFormUnit] (
    [UnitId]           INT            IDENTITY (1, 1) NOT NULL,
    [UnitName]         NVARCHAR (MAX) NOT NULL,
    [UnitCode]         NVARCHAR (50)  NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [DosageFormUnit_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_DosageFormUnit] PRIMARY KEY CLUSTERED ([UnitId] ASC),
    CONSTRAINT [FK_DosageFormUnit_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_DosageFormUnit_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_DosageFormUnit_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

