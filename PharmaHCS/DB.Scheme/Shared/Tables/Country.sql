﻿CREATE TABLE [Shared].[Country] (
    [CountryId]        INT            IDENTITY (1, 1) NOT NULL,
    [CountryName]      NVARCHAR (MAX) NOT NULL,
    [CountryCode]      NVARCHAR (50)  NULL,
    [CreationDate]     DATETIME       CONSTRAINT [Country_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC),
    CONSTRAINT [FK_Country_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Country_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

