﻿CREATE TABLE [Sales].[TargetType] (
    [TargetTypeId]     INT            NOT NULL,
    [TargetTypeName]   NVARCHAR (MAX) NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [TargetType_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_TargetType] PRIMARY KEY CLUSTERED ([TargetTypeId] ASC),
    CONSTRAINT [FK_TargetType_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TargetType_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

