﻿CREATE TABLE [Sales].[InMarketSalesByEmployeeByProfile] (
    [TeamId]              INT              NOT NULL,
    [EmployeeId]          INT              NOT NULL,
    [ProfileId]           INT              NOT NULL,
    [Perccentage]         DECIMAL (18, 6)  NOT NULL,
    [AccountId]           INT              NOT NULL,
    [DosageFormId]        INT              NOT NULL,
    [CountryId]           INT              NOT NULL,
    [DistributorId]       INT              NOT NULL,
    [DistributorBranchId] INT              NULL,
    [FileId]              INT              NULL,
    [InvoiceNumber]       NVARCHAR (MAX)   NULL,
    [Qty]                 DECIMAL (37, 10) NULL,
    [FileAmount]          DECIMAL (18, 4)  NULL,
    [SalesDate]           DATE             NULL,
    [TimeDefinitionId]    INT              NULL
);

