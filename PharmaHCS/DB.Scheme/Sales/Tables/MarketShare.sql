﻿CREATE TABLE [Sales].[MarketShare] (
    [MarketShareId]    INT      IDENTITY (1, 1) NOT NULL,
    [Year]             INT      NOT NULL,
    [CountryId]        INT      NOT NULL,
    [CreationDate]     DATETIME CONSTRAINT [DF_MarketShare_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_MarketShare] PRIMARY KEY CLUSTERED ([MarketShareId] ASC),
    CONSTRAINT [FK_MarketShare_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_MarketShare_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_MarketShare_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

