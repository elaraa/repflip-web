﻿CREATE TABLE [Sales].[InMarketSales] (
    [InMarketSalesId]     INT             IDENTITY (1, 1) NOT NULL,
    [FileId]              INT             NULL,
    [CountryId]           INT             NOT NULL,
    [DistributorId]       INT             NOT NULL,
    [DistributorBranchId] INT             NULL,
    [AccountId]           INT             NULL,
    [DosageFormId]        INT             NOT NULL,
    [Qty]                 DECIMAL (18, 4) NOT NULL,
    [Amount]              DECIMAL (18, 4) NULL,
    [SalesDate]           DATE            NULL,
    [InvoiceNumber]       NVARCHAR (MAX)  NULL,
    [TimeDefinitionId]    INT             NULL,
    [CreationDate]        DATETIME        CONSTRAINT [InMarketSales_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]    DATETIME        NULL,
    [CreatedById]         INT             NULL,
    [LastModifiedById]    INT             NULL,
    CONSTRAINT [PK_InMarketSales] PRIMARY KEY CLUSTERED ([InMarketSalesId] ASC),
    CONSTRAINT [FK_InMarketSales_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_InMarketSales_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_InMarketSales_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_InMarketSales_Distributor] FOREIGN KEY ([DistributorId]) REFERENCES [Sales].[Distributor] ([DistributorId]),
    CONSTRAINT [FK_InMarketSales_DistributorBranch] FOREIGN KEY ([DistributorBranchId]) REFERENCES [Sales].[DistributorBranch] ([BranchId]),
    CONSTRAINT [FK_InMarketSales_ImportedFile] FOREIGN KEY ([FileId]) REFERENCES [Sales].[ImportedFile] ([FileId]),
    CONSTRAINT [FK_InMarketSales_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_InMarketSales_ProductDosageForm] FOREIGN KEY ([DosageFormId]) REFERENCES [Shared].[ProductDosageForm] ([DosageFormId]),
    CONSTRAINT [FK_InMarketSales_TimeDefinition] FOREIGN KEY ([TimeDefinitionId]) REFERENCES [Shared].[TimeDefinition] ([TimeDefinitionId]),
    CONSTRAINT [FK_InMarketSalesBonus_Distributor] FOREIGN KEY ([DistributorId]) REFERENCES [Sales].[Distributor] ([DistributorId])
);




GO
CREATE NONCLUSTERED INDEX [IX_InMarketSales_TimeDefinitionId]
    ON [Sales].[InMarketSales]([TimeDefinitionId] ASC)
    INCLUDE([CountryId], [AccountId], [DosageFormId], [Qty]);


GO
CREATE NONCLUSTERED INDEX [IX_InMarketSales_CountryId_DosageFormId]
    ON [Sales].[InMarketSales]([CountryId] ASC, [DosageFormId] ASC)
    INCLUDE([AccountId], [Qty], [Amount], [SalesDate]);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [Sales].[InMarketSales]([AccountId] ASC)
    INCLUDE([CountryId], [DosageFormId], [Qty], [Amount], [SalesDate]);

