﻿CREATE TABLE [Sales].[Tier] (
    [TierId]   INT           IDENTITY (1, 1) NOT NULL,
    [TierName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Tier] PRIMARY KEY CLUSTERED ([TierId] ASC)
);



