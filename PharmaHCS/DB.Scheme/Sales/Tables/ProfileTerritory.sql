﻿CREATE TABLE [Sales].[ProfileTerritory] (
    [ProfileTerritoryId] INT             IDENTITY (1, 1) NOT NULL,
    [ProfileId]          INT             NOT NULL,
    [TerritoryId]        INT             NOT NULL,
    [Percentage]         DECIMAL (18, 6) NOT NULL,
    [FromDate]           DATE            NOT NULL,
    [ToDate]             DATE            NULL,
    [CreationDate]       DATETIME        CONSTRAINT [ProfileTerritory_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]   DATETIME        NULL,
    [CreatedById]        INT             NULL,
    [LastModifiedById]   INT             NULL,
    CONSTRAINT [PK_ProfileTerritory_1] PRIMARY KEY CLUSTERED ([ProfileTerritoryId] ASC),
    CONSTRAINT [FK_ProfileTerritory_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTerritory_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTerritory_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId]),
    CONSTRAINT [FK_ProfileTerritory_Territory] FOREIGN KEY ([TerritoryId]) REFERENCES [Shared].[Territory] ([TerritoryId])
);

