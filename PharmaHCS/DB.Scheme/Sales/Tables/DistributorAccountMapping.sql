﻿CREATE TABLE [Sales].[DistributorAccountMapping] (
    [DistributorAccountMappingId] INT            IDENTITY (1, 1) NOT NULL,
    [DistributorId]               INT            NOT NULL,
    [AccountId]                   INT            NOT NULL,
    [AccountCode]                 NVARCHAR (MAX) NOT NULL,
    [AccountName]                 NVARCHAR (MAX) NULL,
    [AccountAddress]              NVARCHAR (MAX) NULL,
    [AccountPhone]                NVARCHAR (MAX) NULL,
    [FileId]                      INT            NULL,
    [CreationDate]                DATETIME       CONSTRAINT [DistributorAccountMapping_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]            DATETIME       NULL,
    [CreatedById]                 INT            NULL,
    [LastModifiedById]            INT            NULL,
    CONSTRAINT [PK_DistributorAccountMapping] PRIMARY KEY CLUSTERED ([DistributorAccountMappingId] ASC),
    CONSTRAINT [FK_DistributorAccountMapping_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_DistributorAccountMapping_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_DistributorAccountMapping_Distributor] FOREIGN KEY ([DistributorId]) REFERENCES [Sales].[Distributor] ([DistributorId]),
    CONSTRAINT [FK_DistributorAccountMapping_ImportedFile] FOREIGN KEY ([FileId]) REFERENCES [Sales].[ImportedFile] ([FileId]),
    CONSTRAINT [FK_DistributorAccountMapping_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

