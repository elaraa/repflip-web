﻿CREATE TABLE [Sales].[Distributor] (
    [DistributorId]    INT            IDENTITY (1, 1) NOT NULL,
    [DistributorName]  NVARCHAR (MAX) NOT NULL,
    [DistributorCode]  NVARCHAR (50)  NULL,
    [SalesPluginId]    INT            NULL,
    [CountryId]        INT            NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [Distributor_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_Distributor] PRIMARY KEY CLUSTERED ([DistributorId] ASC),
    CONSTRAINT [FK_Distributor_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Distributor_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Distributor_ImportPlugin] FOREIGN KEY ([SalesPluginId]) REFERENCES [Sales].[ImportPlugin] ([PluginId]),
    CONSTRAINT [FK_Distributor_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

