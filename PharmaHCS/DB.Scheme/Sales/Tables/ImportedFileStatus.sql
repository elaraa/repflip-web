﻿CREATE TABLE [Sales].[ImportedFileStatus] (
    [StatusId]         INT            NOT NULL,
    [StatusName]       NVARCHAR (MAX) NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [ImportedFileStatus_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_ImportedFileStatus] PRIMARY KEY CLUSTERED ([StatusId] ASC),
    CONSTRAINT [FK_ImportedFileStatus_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ImportedFileStatus_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

