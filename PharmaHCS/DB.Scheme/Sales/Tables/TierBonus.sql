﻿CREATE TABLE [Sales].[TierBonus] (
    [TierBonusId] INT          IDENTITY (1, 1) NOT NULL,
    [ProductId]   INT          NOT NULL,
    [TierId]      INT          NOT NULL,
    [SalesQty]    DECIMAL (18) NULL,
    [BonusQty]    DECIMAL (18) NULL,
    [FromPeriod]  DATE         NOT NULL,
    [ToPeriod]    DATE         NULL,
    CONSTRAINT [PK_TierBonus] PRIMARY KEY CLUSTERED ([TierBonusId] ASC),
    CONSTRAINT [FK_TierBonus_Tier] FOREIGN KEY ([TierId]) REFERENCES [Sales].[Tier] ([TierId]),
    CONSTRAINT [FK_TierBonus_TierBonus] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId])
);







