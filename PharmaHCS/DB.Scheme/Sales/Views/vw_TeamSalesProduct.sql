﻿



CREATE view [Sales].[vw_TeamSalesProduct]
as
SELECT      e.TeamSalesProductId 
		,	e.TeamId
		,	e.ProductId
		,	p.ProductCode +' | '+ p.ProductName ProductName
		,	e.FromDate
		,	e.ToDate		
		, t.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.TeamSalesProduct e
			join Shared.Team t on t.TeamId = e.TeamId
			join Shared.Product p on p.ProductId = e.ProductId
				LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on t.CountryId = c.CountryId