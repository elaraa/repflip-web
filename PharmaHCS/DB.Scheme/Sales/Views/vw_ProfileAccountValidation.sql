﻿CREATE view Sales.vw_ProfileAccountValidation
as
select 
		pa.ProfileAccountId
	,	pa.ProfileId
	,	a.AccountCode + ' | ' + a.AccountName AccountName
	,	ac.CategoryId
	,	ac.RootCategoryName CategoryName
	,	terr.TerritoryId
	,	terr.TerritoryCode +' | '+ terr.TerritoryName TerritoryName
	,	pa.Perccentage
	,	pa.FromDate, pa.ToDate
	,	tpt.ProfileId MainProfileId
	,	tpt.ProfileCode MainProfileCode
from Sales.ProfileAccount pa
	join Shared.TeamProfile tp on pa.ProfileId = tp.ProfileId
	join Shared.Account a on pa.AccountId = a.AccountId
	join Shared.fn_AccountCategory_GetParentLevelForLeaf(1,null) ac on a.CategoryId = ac.CategoryId
		join Shared.Territory terr on a.TerritoryId = terr.TerritoryId
	left join sales.ProfileTerritory pt on pt.ProfileId <> pa.ProfileId
		and a.TerritoryId = pt.TerritoryId
		and Shared.fnCompareDates(pa.FromDate, pa.ToDate, pt.FromDate, pt.ToDate) = 1
	left join Shared.TeamProfile tpt on pt.ProfileId = tpt.ProfileId
		and tpt.TeamId = tp.TeamId