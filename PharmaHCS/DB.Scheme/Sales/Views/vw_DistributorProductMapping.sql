﻿



CREATE view [Sales].[vw_DistributorProductMapping]
as
select  d.DistributorProductMappingId
	,	d.DistributorId
	,	d.DosageFormId
	,	f.DosageFormCode +' | '+ f.DosageFormName DosageFormName
	,	d.DosageFormCode +' | '+ d.DosageFormName  DosageFormNameFile
	,	d.FileId
	,	f.CountryId
	,	c.CountryCode
	,	ISNULL(d.LastModifiedDate, d.CreationDate) LastUpdate
	,	ISNULL(d.LastModifiedById, d.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Sales.DistributorProductMapping d
				join Shared.ProductDosageForm f on f.DosageFormId = d.DosageFormId
				join Sales.Distributor r on r.DistributorId = d.DistributorId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(d.LastModifiedById, d.CreatedById)
	left join Shared.Country c on f.CountryId = c.CountryId