﻿

Create view [Sales].[vw_DistributorBranch]
as
select  d.BranchId
	,	d.DistributorId
	,	d.BranchCode
	,	d.BranchName
	,	r.CountryId
	,	c.CountryCode
	,	ISNULL(d.LastModifiedDate, d.CreationDate) LastUpdate
	,	ISNULL(d.LastModifiedById, d.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Sales.DistributorBranch d
				join Sales.Distributor r on r.DistributorId = d.DistributorId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(d.LastModifiedById, d.CreatedById)
	left join Shared.Country c on r.CountryId = c.CountryId