﻿CREATE VIEW Sales.vw_ImportedFile
AS
SELECT        CASE WHEN s.FileId IS NULL THEN 0 ELSE 1 END AS IsSaved, e.FileId, e.DistributorId, Sales.Distributor.DistributorCode + ' | ' + Sales.Distributor.DistributorName AS DistributorName, e.StatusId, 
                         Sales.ImportedFileStatus.StatusName, e.TimeDefinitionId, Shared.TimeDefinition.PeriodName, e.ImportMode, e.CountryId, c.CountryCode, ISNULL(e.LastModifiedDate, e.CreationDate) AS LastUpdate, 
                         ISNULL(e.LastModifiedById, e.CreatedById) AS UpdatedBy, u.EmployeeName AS UpdatedByLogonName
FROM            Sales.ImportedFile AS e INNER JOIN
                         Sales.Distributor ON e.DistributorId = Sales.Distributor.DistributorId INNER JOIN
                         Sales.ImportedFileStatus ON e.StatusId = Sales.ImportedFileStatus.StatusId LEFT OUTER JOIN
                         Shared.Country AS c ON e.CountryId = c.CountryId LEFT OUTER JOIN
                         Shared.TimeDefinition ON e.TimeDefinitionId = Shared.TimeDefinition.TimeDefinitionId LEFT OUTER JOIN
                         HR.Employee AS u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById) LEFT OUTER JOIN
                             (SELECT DISTINCT FileId
                                FROM            Sales.InMarketSales
                                WHERE        (FileId IS NOT NULL)) AS s ON e.FileId = s.FileId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_ImportedFile';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_ImportedFile';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Distributor (Sales)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ImportedFileStatus (Sales)"
            Begin Extent = 
               Top = 138
               Left = 258
               Bottom = 268
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TimeDefinition (Shared)"
            Begin Extent = 
               Top = 270
               Left = 258
               Bottom = 400
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 300
               Bottom = 85
               Right = 470
            End
          ', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_ImportedFile';

