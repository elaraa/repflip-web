﻿
/*
get unmapped accounts -> that will be created
*/
Create view [Sales].[vw_ImportedFile_AccountsToCreate]
as
select isnull(max(FileRecordId),-1) RecordId,FileId, CustomerCode, CustomerName, CustomerAddress, CustomerTerritory
	, null Territory
	, null AccountType
	, null Category
from sales.ImportedFileRecord
where (CustomerMapped = 0 or AccountId is null) and AccountToBeCreated = 1
group by FileId, CustomerCode, CustomerName, CustomerAddress, CustomerTerritory