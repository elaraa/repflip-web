﻿


Create view [Sales].[vw_EmployeeTerritory]
as
SELECT      e.ProfileTerritoryId
		,	p.ProfileId
		,	t.ProfileCode +' | '+ t.ProfileName ProfileName
		,	p.EmployeeId
		,	e.TerritoryId 
		,	r.TerritoryCode  + ' | '+ r.TerritoryName TerritoryName
		,	p.FromDate
		,	p.ToDate
		, r.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Sales.ProfileTerritory e
				join Shared.TeamProfile t on t.ProfileId = e.ProfileId
				join Shared.EmployeeProfile p on p.ProfileId = t.ProfileId	
			join Shared.Territory r on r.TerritoryId = e.TerritoryId
			LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on r.CountryId = c.CountryId