﻿CREATE VIEW [Sales].[vw_TargetProfile_Details]
as
select TargetId, t.CountryId, t.Year
	, t.ProfileId, tp.ProfileCode
	, e.EmployeeCode+' | '+e.EmployeeName EmployeeName
	, tm.TeamId, tm.TeamName
	, t.ProductId, p.ProductCode+' | '+p.ProductName ProductName 
	, t.TerritoryId, ter.TerritoryCode+' | '+ter.TerritoryName TerritoryName
	, t.TimeDefinitionId, td.PeriodName
	, Qty, Amount
	, c.CountryCode
	,ISNULL(t.LastModifiedDate, t.CreationDate) LastUpdate
	,ISNULL(t.LastModifiedById, t.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName

from sales.Target t
	join Shared.Product p on t.ProductId = p.ProductId
	join Shared.TimeDefinition td on t.TimeDefinitionId = td.TimeDefinitionId
	join Shared.Territory ter on t.TerritoryId = ter.TerritoryId
	join Shared.TeamProfile tp on t.ProfileId = tp.ProfileId
	join Shared.Team tm on tp.TeamId = tm.TeamId
	left join Shared.EmployeeProfile ep on ep.ProfileId = t.ProfileId
		and Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
	left join HR.Employee e on e.EmployeeId = ep.EmployeeId

	join Shared.Country c on t.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(t.LastModifiedById, t.CreatedById)
where TargetTypeId = 6