﻿

/*
get unmapped products
*/
CREATE view [Sales].[vw_ImportedFile_UnmappedProduct]
as
select isnull(max(FileRecordId),-1) RecordId,FileId, DosageFormCode, DosageFormName, count(*) AffectedRecords, null ProductId
from sales.ImportedFileRecord
where ProductMapped = 0 or DosageFormId is null
group by FileId, DosageFormCode, DosageFormName