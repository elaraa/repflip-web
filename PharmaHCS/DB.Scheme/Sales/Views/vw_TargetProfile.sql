﻿create view sales.vw_TargetProfile
as
select isnull(Min(TargetId),-1) TargetId, Year, t.CountryId, c.CountryCode, sum(Qty) Qty, sum(Amount) Amount
from sales.Target t
	join Shared.Country c on t.CountryId = c.CountryId
where TargetTypeId = 6
group by Year, t.CountryId, c.CountryCode