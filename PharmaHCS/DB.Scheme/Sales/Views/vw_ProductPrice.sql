﻿





CREATE view [Sales].[vw_ProductPrice]
as
select  a.ProductPriceId	
	,	a.ProductId
	,	t.ProductCode +' | '+ t.ProductName ProductName 
	, case 
		when a.DosageFormId is null and a.DistributorId is null and a.TerritoryId  is null and a.AccountId is null and a.AccountCategoryId is null then 'Product'
		when a.DosageFormId is not null and a.DistributorId is null and a.TerritoryId  is null and a.AccountId is null and a.AccountCategoryId is null then 'Dosage'
		when a.DistributorId is not null and a.TerritoryId  is null and a.AccountId is null and a.AccountCategoryId is null then 'Distributor'
		when a.TerritoryId  is not null and a.AccountId is null and a.AccountCategoryId is null then 'Territory'
		when a.AccountCategoryId is not null and a.AccountId is null then 'Account Category'
		when a.AccountId is not null then 'Account'
		end PriceType
	, case 
		when a.DosageFormId is null and a.DistributorId is null and a.TerritoryId  is null and a.AccountId is null and a.AccountCategoryId is null then null
		when a.DosageFormId is not null and a.DistributorId is null and a.TerritoryId  is null and a.AccountId is null and a.AccountCategoryId is null then pdf.DosageFormCode +' | '+ pdf.DosageFormName
		when a.DistributorId is not null and a.TerritoryId  is null and a.AccountId is null and a.AccountCategoryId is null then d.DistributorCode +' | '+ d.DistributorName
		when a.TerritoryId  is not null and a.AccountId is null and a.AccountCategoryId is null then r.TerritoryCode +' | '+ r.TerritoryName
		when a.AccountCategoryId is not null and a.AccountId is null then ac.CategoryCode +' | '+ ac.CategoryName
		when a.AccountId is not null then o.AccountCode +' | '+ o.AccountName
		end PriceTypeName
	,	a.DosageFormId
	,	pdf.DosageFormCode +' | '+ pdf.DosageFormName DosageFormName
	,	a.DistributorId
	,	d.DistributorCode +' | '+ d.DistributorName DistributorName
	,	a.TerritoryId 
	,	r.TerritoryCode +' | '+ r.TerritoryName TerritoryName
	,	a.AccountCategoryId 
	,	ac.CategoryCode +' | '+ ac.CategoryName CategoryName
	,	a.AccountId
	,	o.AccountCode +' | '+ o.AccountName AccountName
	,	a.FromDate
	,	a.ToDate
	,	a.Price
	,	t.CountryId
	,	c.CountryCode
	, ISNULL(a.LastModifiedDate, a.CreationDate) LastUpdate
	, ISNULL(a.LastModifiedById, a.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Sales.ProductPrice a
	 JOIN Shared.Product t on a.ProductId = t.ProductId
	left join Shared.ProductDosageForm pdf on pdf.DosageFormId = a.DosageFormId
	left join Sales.Distributor d on d.DistributorId = a.DistributorId
	left join Shared.Territory r on r.TerritoryId = a.TerritoryId
	left join Shared.Account o on o.AccountId = a.AccountId
	left join Shared.AccountCategory ac on ac.CategoryId = a.AccountCategoryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(a.LastModifiedById, a.CreatedById)
	left join Shared.Country c on t.CountryId = c.CountryId