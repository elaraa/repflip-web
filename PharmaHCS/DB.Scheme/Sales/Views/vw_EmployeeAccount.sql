﻿



CREATE view [Sales].[vw_EmployeeAccount]
as
SELECT      pa.ProfileAccountId 
		,	p.ProfileId
		,	t.ProfileCode +' | '+ t.ProfileName ProfileName
		,	e.EmployeeId
		,	pa.AccountId
		,	a.AccountCode + ' | '+ a.AccountName AccountName
		,	a.TerritoryId 
		,	r.TerritoryCode  + ' | '+ r.TerritoryName TerritoryName
		,	a.CategoryId
		,	ac.CategoryCode  + ' | '+ ac.CategoryName CategoryName
		,	Shared.fnGetMaxDate( pa.FromDate, p.FromDate) FromDate
		,	Shared.fnGetMinDate( pa.ToDate, p.ToDate) ToDate 
		, a.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from HR.Employee e
				join Shared.EmployeeProfile p on p.EmployeeId = e.EmployeeId
				join Shared.TeamProfile t on t.ProfileId = p.ProfileId
			join sales.ProfileAccount pa on pa.ProfileId = t.ProfileId			
			join Shared.Account a on a.AccountId = pa.AccountId
			join Shared.Territory r on r.TerritoryId = a.TerritoryId
			join Shared.AccountCategory ac on ac.CategoryId = a.CategoryId
			LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on a.CountryId = c.CountryId