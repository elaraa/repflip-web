﻿CREATE VIEW Sales.vw_ProfileAccount
AS
SELECT        e.ProfileAccountId, e.ProfileId, tp.ProfileCode, tp.TeamId AS Team, e.AccountId, a.AccountCode, a.AccountCode + ' | ' + a.AccountName AS AccountName, ac.CategoryId, ac.RootCategoryName AS CategoryName, terr.TerritoryId, 
                         terr.TerritoryCode + ' | ' + terr.TerritoryName AS TerritoryName, e.Perccentage, e.ProductId, p.ProductCode, p.ProductCode + ' | ' + p.ProductName AS ProductName, e.DosageFromId, df.DosageFormCode, e.FromDate, e.ToDate, 
                         a.CountryId, c.CountryCode, ISNULL(e.LastModifiedDate, e.CreationDate) AS LastUpdate, ISNULL(e.LastModifiedById, e.CreatedById) AS UpdatedBy, u.EmployeeName AS UpdatedByLogonName
FROM            Sales.ProfileAccount AS e LEFT OUTER JOIN
                         Shared.ProductDosageForm AS df ON e.DosageFromId = df.DosageFormId LEFT OUTER JOIN
                         Shared.Product AS p ON p.ProductId = e.ProductId INNER JOIN
                         Shared.Account AS a ON a.AccountId = e.AccountId INNER JOIN
                         Shared.TeamProfile AS tp ON tp.ProfileId = e.ProfileId INNER JOIN
                         Shared.fn_AccountCategory_GetParentLevelForLeaf(1, NULL) AS ac ON a.CategoryId = ac.CategoryId INNER JOIN
                         Shared.Territory AS terr ON a.TerritoryId = terr.TerritoryId LEFT OUTER JOIN
                         HR.Employee AS u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById) LEFT OUTER JOIN
                         Shared.Country AS c ON a.CountryId = c.CountryId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_ProfileAccount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'nd
         Begin Table = "c"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "df"
            Begin Extent = 
               Top = 250
               Left = 490
               Bottom = 380
               Right = 678
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_ProfileAccount';






GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 258
               Bottom = 136
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "a"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tp"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ac"
            Begin Extent = 
               Top = 138
               Left = 258
               Bottom = 268
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "terr"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 402
               Left = 258
               Bottom = 532
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         E', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_ProfileAccount';





