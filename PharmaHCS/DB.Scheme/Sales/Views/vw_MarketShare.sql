﻿create view Sales.vw_MarketShare
as
select 	s.MarketShareId
	,	s.[Year]
	,	s.CountryId
	,	c.CountryCode
	, ISNULL(s.LastModifiedDate, s.CreationDate) LastUpdate
	, ISNULL(s.LastModifiedById, s.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.MarketShare s
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(s.LastModifiedById, s.CreatedById)
	left join Shared.Country c on s.CountryId = c.CountryId