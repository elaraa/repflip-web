﻿
CREATE VIEW [Sales].[vw_target_ProductPrice]
as
select ProductId, td.Year year, MAX(Price) Price
from sales.ProductPrice pp
	join Shared.TimeDefinition td on Shared.fnCompareDates(td.FromDate, td.ToDate,pp.FromDate, pp.ToDate) = 1
where AccountId is null and TerritoryId is null and DistributorId is null and DosageFormId is null and AccountCategoryId is null
group by ProductId, td.Year
having MAX(Price) <> 0