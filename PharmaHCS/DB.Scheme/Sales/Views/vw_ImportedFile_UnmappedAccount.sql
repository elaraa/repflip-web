﻿


/*
get unmapped accounts
*/
CREATE view [Sales].[vw_ImportedFile_UnmappedAccount]
as
select isnull(max(FileRecordId),-1) RecordId,FileId, CustomerCode, CustomerName, CustomerAddress, CustomerTerritory, count(*) AffectedRecords, null TerritoryId, null AccountId
from sales.ImportedFileRecord
where (CustomerMapped = 0 or AccountId is null) and AccountToBeCreated = 0
group by FileId, CustomerCode, CustomerName, CustomerAddress, CustomerTerritory