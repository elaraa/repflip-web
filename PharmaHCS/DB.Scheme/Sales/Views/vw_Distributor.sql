﻿




CREATE view [Sales].[vw_Distributor]
as
select  d.DistributorId
	,	d.DistributorCode
	,	d.DistributorName
	,	cast(isnull(d.SalesPluginId,0) as bit) HasPlugin
	,	isnull(acc.BranchesCount,0) BranchesCount
	,	d.CountryId
	,	c.CountryCode
	,	ISNULL(d.LastModifiedDate, d.CreationDate) LastUpdate
	,	ISNULL(d.LastModifiedById, d.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Sales.Distributor d
	left join (select count(BranchId) BranchesCount, DistributorId from Sales.DistributorBranch group by DistributorId) acc
		on d.DistributorId = acc.DistributorId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(d.LastModifiedById, d.CreatedById)
	left join Shared.Country c on d.CountryId = c.CountryId