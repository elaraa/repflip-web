﻿

CREATE view [Sales].[vw_targetPhasing_Details]
as
select t.TargetId
	, t.Year
	, t.ProductId
	, p.ProductCode +' | '+ p.ProductName ProductName
	, td.TimeDefinitionId
	,td.PeriodName
	, t.Percentage
	, case when tl.Percentage <> 0 then t.Percentage/tl.Percentage else null end  PercentageGrowth
	, t.CountryId
	, c.CountryCode
	, ISNULL(t.LastModifiedDate, t.CreationDate) LastUpdate
	, ISNULL(t.LastModifiedById, t.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.Target t
	left join Sales.Target tl on t.TargetTypeId = tl.TargetTypeId and t.CountryId = tl.CountryId
		and t.Year = tl.Year+1 and t.ProductId = tl.ProductId
	join Shared.Product p on t.ProductId = p.ProductId and t.CountryId = p.CountryId
	join Shared.TimeDefinition td on t.TimeDefinitionId = td.TimeDefinitionId and td.CountryId = t.CountryId
	join Shared.Country c on t.CountryId = c.CountryId
	left join hr.Employee u on u.EmployeeId = ISNULL(t.LastModifiedById, t.CreatedById)
where t.TargetTypeId = 3 /*phasing*/