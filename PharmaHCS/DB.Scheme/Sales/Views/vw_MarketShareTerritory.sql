﻿create view Sales.vw_MarketShareTerritory
as
select 	s.MarketShareTerritoryId
	, s.MarketShareId
	,	s.TerritoryId
	,	c.TerritoryCode +' | '+c.TerritoryName TerritoryName
	,	s.MarketShare
	, ISNULL(s.LastModifiedDate, s.CreationDate) LastUpdate
	, ISNULL(s.LastModifiedById, s.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.MarketShareTerritory s
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(s.LastModifiedById, s.CreatedById)
	left join Shared.Territory c on s.TerritoryId = c.TerritoryId