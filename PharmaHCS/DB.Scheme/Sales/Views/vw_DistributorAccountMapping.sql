﻿

Create view [Sales].[vw_DistributorAccountMapping]
as
select  d.DistributorAccountMappingId
	,	d.DistributorId
	,	d.AccountId
	,	f.AccountCode +' | '+ f.AccountName AccountName
	,	d.AccountCode +' | '+ d.AccountName  AccountNameFile
	,	d.AccountAddress
	,	d.AccountPhone
	,	d.FileId
	,	f.CountryId
	,	c.CountryCode
	,	ISNULL(d.LastModifiedDate, d.CreationDate) LastUpdate
	,	ISNULL(d.LastModifiedById, d.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from Sales.DistributorAccountMapping d
				join Shared.Account f on f.AccountId = d.AccountId
				join Sales.Distributor r on r.DistributorId = d.DistributorId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(d.LastModifiedById, d.CreatedById)
	left join Shared.Country c on f.CountryId = c.CountryId