﻿




CREATE view [Sales].[vw_TargetProduct]
as
select  isnull(min(tc.TargetId),-1) TargetId, tc.Year, sum(tc.Qty) Qty, sum(tc.Amount) Amount
	, (sum(tc.Qty)/sum(tl.Qty))-1 QtyGrowth
	, (sum(tc.Amount)/sum(tl.Amount))-1 AmountGrowth
	, tc.CountryId
	, c.CountryCode
	, max(tc.LastUpdate) LastUpdate
	, max(tc.UpdatedBy) UpdatedBy
	, max(u.EmployeeName) UpdatedByLogonName
from (
		select isnull(min(TargetId),-1) TargetId,Year, CountryId, TargetTypeId, sum(Qty) Qty, sum(Amount) Amount 
			, max(ISNULL(LastModifiedDate, CreationDate)) LastUpdate
			, max(ISNULL(LastModifiedById, CreatedById)) UpdatedBy
		from sales.Target 
		where TargetTypeId  = 2 
		group by Year, CountryId, TargetTypeId) tc
	left join (select Year, CountryId, TargetTypeId, sum(Qty) Qty, sum(Amount) Amount from sales.Target where TargetTypeId  = 2 group by Year, CountryId, TargetTypeId)tl on tc.TargetTypeId = tl.TargetTypeId and tc.Year = tl.Year+1 and tc.CountryId = tl.CountryId
	left join Shared.Country c on tc.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = tc.UpdatedBy
where tc.TargetTypeId  = 2
group by tc.Year, tc.CountryId
	, c.CountryCode