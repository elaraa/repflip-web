﻿create view [Sales].[vw_TargetProduct_Details]
as
select t.TargetId,t.Year,t.ProductId, p.ProductCode+' | '+p.ProductName ProductName, t.Qty, t.Amount
	, case when tl.Qty != 0 then (t.Qty/tl.Qty)end-1 QtyGrowth, case when tl.Amount != 0 then (t.Amount/tl.Amount) end -1 AmountGrowth
	,t.CountryId
	, c.CountryCode
	,ISNULL(t.LastModifiedDate, t.CreationDate) LastUpdate
	,ISNULL(t.LastModifiedById, t.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from sales.Target t
	left join sales.Target tl on t.TargetTypeId = tl.TargetTypeId and t.CountryId = tl.CountryId
		and t.Year = tl.Year+1 and t.ProductId = tl.ProductId
	join Shared.Product p on t.ProductId = p.ProductId and t.CountryId = p.CountryId
	join Shared.Country c on t.CountryId = c.CountryId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(t.LastModifiedById, t.CreatedById)
where t.TargetTypeId = 2