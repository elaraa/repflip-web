﻿CREATE VIEW Sales.vw_BonusTracking
AS
SELECT        SalesQty, BonusQty, Qty, BQty, InvoiceNumber, AccountName, 1 AS IsFollow
FROM            (SELECT        tb.SalesQty, tb.BonusQty, SUM(ms.Qty) AS Qty, SUM(msb.Qty) AS BQty, ms.InvoiceNumber, a.AccountName
                           FROM            Sales.TierBonus AS tb INNER JOIN
                                                    Shared.ProductDosageForm AS d ON d.ProductId = tb.ProductId INNER JOIN
                                                    Sales.InMarketSales AS ms ON ms.DosageFormId = d.DosageFormId INNER JOIN
                                                    Shared.Account AS a ON a.AccountId = ms.AccountId INNER JOIN
                                                    Shared.TimeDefinition AS td ON td.TimeDefinitionId = ms.TimeDefinitionId LEFT OUTER JOIN
                                                    Sales.InMarketSalesBonus AS msb ON msb.DosageFormId = d.DosageFormId INNER JOIN
                                                    Shared.TimeDefinition AS tdb ON tdb.TimeDefinitionId = msb.TimeDefinitionId AND ms.InvoiceNumber = msb.InvoiceNumber
                           WHERE        (Shared.fnCompareDates(tb.FromPeriod, tb.ToPeriod, td.FromDate, td.ToDate) = 1) AND (msb.InMarketSalesBonusId IS NULL) OR
                                                    (Shared.fnCompareDates(tb.FromPeriod, tb.ToPeriod, td.FromDate, td.ToDate) = 1) AND (Shared.fnCompareDates(tb.FromPeriod, tb.ToPeriod, tdb.FromDate, tdb.ToDate) = 1)
                           GROUP BY tb.SalesQty, tb.BonusQty, a.AccountName, ms.InvoiceNumber, msb.InvoiceNumber) AS T
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_BonusTracking';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_BonusTracking';

