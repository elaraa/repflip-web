﻿CREATE VIEW Sales.vw_CeilingAccount
AS
SELECT        Sales.CeilingAccountException.CeilingAccountExceptionId, Shared.Account.AccountName, Shared.Product.ProductName, tdfrom.PeriodName AS FromPeriod, tdto.PeriodName AS ToPeriod
FROM            Sales.CeilingAccountException INNER JOIN
                         Shared.Account ON Sales.CeilingAccountException.AccountId = Shared.Account.AccountId LEFT OUTER JOIN
                         Shared.Product ON Sales.CeilingAccountException.ProductId = Shared.Product.ProductId INNER JOIN
                         Shared.TimeDefinition AS tdfrom ON tdfrom.TimeDefinitionId = Sales.CeilingAccountException.FromTimeId LEFT OUTER JOIN
                         Shared.TimeDefinition AS tdto ON tdto.TimeDefinitionId = Sales.CeilingAccountException.ToTimeId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_CeilingAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CeilingAccountException (Sales)"
            Begin Extent = 
               Top = 6
               Left = 37
               Bottom = 136
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Account (Shared)"
            Begin Extent = 
               Top = 6
               Left = 309
               Bottom = 136
               Right = 491
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "Product (Shared)"
            Begin Extent = 
               Top = 6
               Left = 529
               Bottom = 136
               Right = 712
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tdfrom"
            Begin Extent = 
               Top = 161
               Left = 339
               Bottom = 291
               Right = 521
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tdto"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
 ', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_CeilingAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'Sales', @level1type = N'VIEW', @level1name = N'vw_CeilingAccount';

