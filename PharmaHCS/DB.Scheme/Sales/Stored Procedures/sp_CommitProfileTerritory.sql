﻿

-- =============================================
-- Author:		Hesham
-- Create date: 21-6-2020
-- =============================================
CREATE PROCEDURE [Sales].[sp_CommitProfileTerritory]
	@TableName as nvarchar(max),
	@userId as int,
    @TimeId as char(5)
AS
BEGIN
set @TableName='[Temp].'+@tableName
declare @EmptyString as char(1) = ''
declare @StartDate as date = (select top 1 FromDate from Shared.TimeDefinition where TimeDefinitionId = CAST(@TimeId AS INT))	

declare @SQL as nvarchar(max), @Query as nvarchar(max) ,@SQLV as nvarchar(max),@SQLX as nvarchar(max)
set @Query = '

IF COL_LENGTH('''+ @TableName +''', ''Status'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD Status bit NOT NULL DEFAULT(1)
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidProfileCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidProfileCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidTerritoryCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidTerritoryCode nvarchar(max) NOT NULL DEFAULT(''False'')
END



IF COL_LENGTH('''+ @TableName +''', ''IsDuplicated'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsDuplicated nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''IsOverlapped'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsOverlapped nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''IsExceedPercentage'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsExceedPercentage nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''ExceededPercentage'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD ExceededPercentage nvarchar(max) default NULL
END
'

set @SQL = '
UPDATE t set t.InvalidProfileCode = (case when p.ProfileId is not null then ''False'' else ''True'' end)
, Status = (case when p.ProfileId is not null and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
Shared.TeamProfile p on t.[Profile Code]  = p.ProfileCode

UPDATE t set t.InvalidTerritoryCode = (case when a.TerritoryId is not null then ''False'' else ''True'' end)
, Status = (case when a.TerritoryId is not null and Status = 1 then 1 else 0 end) from '+ @TableName +' t left join 
Shared.Territory a on t.[Territory Code]  = a.TerritoryCode

select * into #DuplicatedRows from (
select t.AutoID from '+ @TableName +' t join 
'+ @TableName +' tn on t.[Profile Code] = tn.[Profile Code] and t.[Territory Code] = tn.[Territory Code] 
where t.AutoID <> tn.AutoID
) as DuplicatedRows
update '+ @TableName +' set IsDuplicated = ''True'' where AutoID in (select * from #DuplicatedRows)'

set @SQLV = '

select * into #Overlapped from (
select t.AutoID, a.TerritoryId, p.ProfileId, t.[Percentage] as newPercentage, pt.[Percentage], t.[Profile Code]
from '+ @TableName +'  t join
 Shared.Territory a on a.TerritoryCode = t.[Territory Code] join
 Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] join
 Sales.ProfileTerritory pt on pt.TerritoryId = a.TerritoryId and pt.ProfileId = p.ProfileId
 where Status = 1 
 and ((pt.ToDate is null and ''' + cast(@StartDate as nvarchar(100)) + ''' <= pt.FromDate) or (pt.ToDate is not null and ''' + cast(@StartDate as nvarchar(100)) + ''' <= pt.ToDate))) as Overlapped
update '+ @TableName +' set IsOverlapped = ''True'' where AutoID in (select AutoID from #Overlapped)


select * into #ExceedPercentage from (
select AutoID, newPercentage, SUM(Percentage) as [TotalPercentage],TerritoryCode, ProfileCode
from(select distinct t.AutoID, a.TerritoryId, t.[Percentage] as newPercentage,  v.[Percentage]
, t.[Territory Code] as TerritoryCode, t.[Profile Code] as ProfileCode from '+ @TableName + ' t join
 Shared.Territory a on a.TerritoryCode = t.[Territory Code] join
 Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] left join
 Sales.vw_ProfileTerritory v on v.TeamId = p.TeamId and v.TerritoryId = a.TerritoryId join
 Sales.ProfileTerritory pt on pt.TerritoryId = a.TerritoryId and pt.ProfileId = p.ProfileId
 where  Status = 1 
 and (v.ToDate is null or (v.ToDate is not null and ''' + cast(@StartDate as nvarchar(100)) + '''  <= v.ToDate)))T
group by AutoID, TerritoryId, newPercentage, TerritoryCode, ProfileCode) as ExceedPercentage
where (newPercentage + TotalPercentage) > 100
update t set t.IsExceedPercentage = ''True'', t.ExceededPercentage = e.TotalPercentage + t.Percentage from 
'+ @TableName +' t join #ExceedPercentage e on e.AutoID = t.AutoID

Select * into #ReturnData from (select AutoID as RowIndex, [Profile Code] as ProfileCode, [Territory Code] as TerritoryCode, Percentage,
 InvalidProfileCode, 
InvalidTerritoryCode,  IsDuplicated, IsOverlapped, IsExceedPercentage, ExceededPercentage from '+ @TableName +' 
Where Status = 0 or IsDuplicated = ''True''  or IsOverlapped = ''True''  or IsExceedPercentage = ''True'') as ReturnData
'
set @SQLX= '
If (Select Count(*) from #ReturnData) = 0
 begin

 Update pt set ToDate = DATEADD(day, -1, ' + cast(@StartDate as nvarchar(50)) + ')  from '+ @TableName +'  t  join
   Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] join
   Shared.Territory a on a.TerritoryCode = t.[Territory Code] join
   Sales.ProfileTerritory pt on pt.TerritoryId = a.TerritoryId and pt.ProfileId = p.ProfileId
   Where pt.ToDate is null and pt.FromDate <= DATEADD(day, -1, ' + cast(@StartDate as nvarchar(50)) + ')

   Insert into Sales.ProfileTerritory ([ProfileId],[TerritoryId],[Percentage],[FromDate],[ToDate],[CreationDate],[LastModifiedDate],[CreatedById],[LastModifiedById])
   select p.ProfileId,a.TerritoryId,t.[Percentage],''' + cast(@StartDate as nvarchar(50)) + ''', NULL, GETDATE(), NULL, '+cast(@UserId as varchar(50))+', NULL from '+ @TableName +'  t join
   Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] join
   Shared.Territory a on a.TerritoryCode = t.[Territory Code] 

 end

drop table '+ @TableName +'

Select * from #ReturnData
'
exec sp_executesql @Query
exec sp_executesql @SQL
EXEC (@SQLV+@SQLX)

END