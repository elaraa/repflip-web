﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 1 Oct 19
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_SalesCrunch_Ceiling] 
	-- Add the parameters for the stored procedure here
	@fromTimeId int = 0, 
	@toTimeId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   --1. dist sales percentage
	truncate table Staging.DistributorAccountDosagePercentage
	insert into Staging.DistributorAccountDosagePercentage
	select DistributorId,s.AccountId, s.DosageFormId, s.CountryId, s.TimeDefinitionId, case when sum(AccDosSales.Qty) <> 0 then sum(s.Qty)/sum(AccDosSales.Qty) else 0 end DistributorSalesPercentage
	from sales.InMarketSales s with (nolock)
		join (
			select AccountId, DosageFormId, CountryId, TimeDefinitionId, sum(Qty) Qty
			from sales.InMarketSales s with (nolock)
			group by AccountId, DosageFormId, CountryId, TimeDefinitionId
		)AccDosSales on s.AccountId = AccDosSales.AccountId and s.DosageFormId = AccDosSales.DosageFormId and s.CountryId = AccDosSales.CountryId and s.TimeDefinitionId = AccDosSales.TimeDefinitionId
	where not exists (select top 1 1 from sales.CeilingAccountException cae where cae.AccountId = s.AccountId and s.TimeDefinitionId between cae.FromTimeId and isnull(cae.ToTimeId,100000))
		and s.TimeDefinitionId between @fromTimeId and @toTimeId
	group by DistributorId,s.AccountId, s.DosageFormId, s.CountryId, s.TimeDefinitionId

	--2 sales after removing ceiling
	truncate table Staging.InMarketSalesIncludingCeilingQty
	insert into Staging.InMarketSalesIncludingCeilingQty
	select s.AccountId, s.DosageFormId, s.CountryId, s.TimeDefinitionId
		,case when sum(Qty)> isnull(sum(cdaa.CeilingQty), sum(cda.CeilingQty)) then isnull(sum(cdaa.CeilingQty), sum(cda.CeilingQty)) else sum(Qty) end Qty
		,case when sum(Qty)> isnull(sum(cdaa.CeilingQty), sum(cda.CeilingQty)) then sum(Qty)-isnull(sum(cdaa.CeilingQty), sum(cda.CeilingQty)) else 0 end RemainingQty
	--into Staging.InMarketSalesIncludingCeilingQty
	from sales.InMarketSales s with (nolock)
		left join sales.CeilingDosageAccount cda on s.DosageFormId = cda.DosageFormId and s.TimeDefinitionId between cda.FromTimeId and isnull(cda.ToTimeId,10000)
			and cda.AccountId is null
		left join sales.CeilingDosageAccount cdaa on s.DosageFormId = cdaa.DosageFormId and s.TimeDefinitionId between cdaa.FromTimeId and isnull(cdaa.ToTimeId,10000)
			and cdaa.AccountId  = s.AccountId
	where not exists (select top 1 1 from sales.CeilingAccountException cae where cae.AccountId = s.AccountId and s.TimeDefinitionId between cae.FromTimeId and isnull(cae.ToTimeId,10000))
		and s.TimeDefinitionId between @fromTimeId and @toTimeId
	group by s.AccountId, s.DosageFormId, CountryId, TimeDefinitionId
	having sum(Qty) <> 0

	--3. prepare sales by distributor
	truncate table Staging.InMarketSalesByDistributorIncludingCeiledQty
	insert into Staging.InMarketSalesByDistributorIncludingCeiledQty
	select d.DistributorId,s.AccountId, s.DosageFormId, s.CountryId, s.TimeDefinitionId, s.Qty * d.DistributorSalesPercentage Qty
	--into Staging.InMarketSalesByDistributorIncludingCeiledQty
	from Staging.InMarketSalesIncludingCeilingQty s
		join Staging.DistributorAccountDosagePercentage d on s.AccountId = d.AccountId and s.CountryId = d.CountryId and s.DosageFormId = d.DosageFormId and s.TimeDefinitionId = d.TimeDefinitionId
	where s.TimeDefinitionId between @fromTimeId and @toTimeId
	--4. prepare profileAccountPercentage per dosage
	--5. insert into sales after profileaccount percentage per dosage
	------>truncate table Staging.InMarketSalesFact
	delete from Staging.InMarketSalesFact where TimeDefinitionId between @fromTimeId and @toTimeId
	insert into Staging.InMarketSalesFact(CountryId,DistributorId, AccountId, ProductId, DosageFormId, TimeDefinitionId, ProfileId, TeamId, SharePercentage, Qty, ShareType)
	select s.CountryId, s.DistributorId, s.AccountId, pa.ProductId,s.DosageFormId, s.TimeDefinitionId, pa.ProfileId, pa.TeamId, pa.Perccentage SharePercentage
		, s.Qty * pa.Perccentage Qty, 'Profile' ShareType
	--into Staging.InMarketSalesFact
	from Staging.InMarketSalesByDistributorIncludingCeiledQty s
		join (
			select tp.TeamId,pa.ProfileId, AccountId, tp.ProductId, tp.DosageFormId, Perccentage, td.TimeDefinitionId
			from sales.ProfileAccount pa with (nolock)
				join Shared.TimeDefinition td on Shared.fnCompareDates(pa.FromDate, pa.ToDate, td.FromDate, td.ToDate) = 1
				join (
					select tsp.TeamId,tp.ProfileId,tsp.ProductId, df.DosageFormId, td.TimeDefinitionId
					from Sales.TeamSalesProduct tsp
						join Shared.ProductDosageForm df on tsp.ProductId = df.ProductId
						join Shared.TimeDefinition td on Shared.fnCompareDates(tsp.FromDate, tsp.ToDate, td.FromDate, td.ToDate) = 1
						join Shared.TeamProfile tp on tsp.TeamId = tp.TeamId and tp.Active = 1
				)tp on td.TimeDefinitionId = tp.TimeDefinitionId and  pa.ProfileId = tp.ProfileId
		)pa on s.AccountId = pa.AccountId and s.DosageFormId = pa.DosageFormId and s.TimeDefinitionId = pa.TimeDefinitionId
	where s.TimeDefinitionId between @fromTimeId and @toTimeId

	update s set territoryId = a.TerritoryId from Staging.InMarketSalesFact s join Shared.Account a on s.AccountId = a.AccountId

	--6. insert into sales after calculating remaining ceiling
	--6.a ach(sales, target) percentage per dosage/territory/profile
	truncate table Staging.ProfileProductTerritoryPercentages
	insert into Staging.ProfileProductTerritoryPercentages
	select isnull(sales.CountryId,targett.CountryId) CountryId
		, isnull(sales.TeamId,targett.TeamId) TeamId, isnull(sales.ProfileId,targett.ProfileId) ProfileId
		, isnull(sales.ProductId, targett.ProductId) ProductId, isnull(sales.DosageFormId,targett.DosageFormId) DosageFormId
		, isnull(sales.TerritoryId,targett.TerritoryId) TerritoryId, isnull(sales.TimeDefinitionId,targett.TimeDefinitionId) TimeDefinitionId
		, TargetPercentage, SalesPercentage
		, case when TargetPercentage is null then SalesPercentage when SalesPercentage is null then TargetPercentage
			else (TargetPercentage + SalesPercentage)/2.0 end SalesTargetAvgPercentage
	--into Staging.ProfileProductTerritoryPercentages
	from (
		select ppt.CountryId, ptotal.TeamId, ppt.ProfileId, ppt.ProductId, df.DosageFormId, TerritoryId,ppt.TimeDefinitionId, Qty/TotalQty TargetPercentage
		from sales.Target ppt with (nolock)
			join shared.productDosageForm df on ppt.productId = df.productId
			join (
				select CountryId, tp.TeamId, t.ProfileId,TimeDefinitionId, sum(Qty) TotalQty
				from sales.Target t with (nolock)
					join Shared.TeamProfile tp on t.ProfileId = tp.ProfileId
				where TargetTypeId = 6
					and t.TimeDefinitionId between @fromTimeId and @toTimeId
				group by CountryId, tp.TeamId, t.ProfileId,TimeDefinitionId
			) ptotal on ppt.CountryId = ptotal.CountryId and ppt.ProfileId = ptotal.ProfileId and ppt.TimeDefinitionId = ptotal.TimeDefinitionId
		where TargetTypeId = 6 and ppt.TimeDefinitionId between @fromTimeId and @toTimeId
	)targett
	Full outer join (
		select s.CountryId,s.TeamId, s.ProfileId,s.ProductId, s.DosageFormId,s.TerritoryId, s.TimeDefinitionId,case when Sum(TotalQty) = 0 then null else sum(Qty)/Sum(TotalQty) end SalesPercentage
		from Staging.InMarketSalesFact s with (nolock)
			join (
				select TeamId,ProfileId,ProductId, DosageFormId, s.TerritoryId, TimeDefinitionId,sum(Qty) TotalQty
				from Staging.InMarketSalesFact s with (nolock)
				where s.TimeDefinitionId between @fromTimeId and @toTimeId
				group by TeamId,ProfileId,ProductId, DosageFormId, s.TerritoryId, TimeDefinitionId
				having sum(Qty) <> 0
			)stotal on s.ProfileId = stotal.ProfileId and s.ProductId = stotal.ProductId and s.DosageFormId = stotal.DosageFormId
				and s.TerritoryId = stotal.TerritoryId and s.TimeDefinitionId = stotal.TimeDefinitionId
				and s.TeamId = stotal.TeamId
		where s.TimeDefinitionId between @fromTimeId and @toTimeId
		group by  s.CountryId, s.TeamId, s.ProfileId,s.ProductId, s.DosageFormId,s.TerritoryId, s.TimeDefinitionId
		having Sum(TotalQty) <> 0 and case when Sum(TotalQty) = 0 then null else sum(Qty)/Sum(TotalQty) end is not null
	)sales on sales.ProfileId = targett.ProfileId and sales.ProductId = targett.ProductId and sales.DosageFormId = targett.DosageFormId
		and sales.TerritoryId = targett.TerritoryId and sales.TimeDefinitionId = targett.TimeDefinitionId
		and sales.TeamId = targett.TeamId
	--6.b ach * remaining
	--for rep
	insert into Staging.InMarketSalesFact(DistributorId, AccountId, ProductId, DosageFormId, TimeDefinitionId, CountryId, ProfileId,TeamId, TerritoryId, Qty, SharePercentage, ShareType)
	select d.DistributorId,s.AccountId, stPercent.ProductId, s.DosageFormId, s.TimeDefinitionId,  s.CountryId
		, stPercent.ProfileId--, stPercent.SalesTargetAvgPercentage
		, stPercent.TeamId, a.TerritoryId
		, s.RemainingQty*d.DistributorSalesPercentage * stPercent.SalesTargetAvgPercentage Qty
		--, s.RemainingQty
		,stPercent.SalesTargetAvgPercentage
		,'Ceiling Remaining'	
	from Staging.InMarketSalesIncludingCeilingQty s
		join Staging.DistributorAccountDosagePercentage d on s.AccountId = d.AccountId and s.DosageFormId = d.DosageFormId 
			and s.TimeDefinitionId = d.TimeDefinitionId
		join Shared.Account a on s.AccountId = a.AccountId
		join Staging.ProfileProductTerritoryPercentages stPercent on s.CountryId = stPercent.CountryId 
			and stPercent.TerritoryId = a.TerritoryId and stPercent.DosageFormId = s.DosageFormId
			and s.TimeDefinitionId = stPercent.TimeDefinitionId 
	where RemainingQty <> 0 and s.TimeDefinitionId between @fromTimeId and @toTimeId
	--for reps in team
	insert into Staging.InMarketSalesFact(DistributorId, AccountId, ProductId, DosageFormId, TimeDefinitionId, CountryId, ProfileId,TeamId, TerritoryId, Qty, SharePercentage, ShareType)
	select d.DistributorId, s.AccountId,tsp.ProductId, df.DosageFormId, td.TimeDefinitionId, s.CountryId, ppt.ProfileId
		, tsp.TeamId, a.TerritoryId
		, s.RemainingQty * d.DistributorSalesPercentage * ppt.SalesTargetAvgPercentage
		, ppt.SalesTargetAvgPercentage
		, 'Ceiling Remaining'
	from sales.TeamSalesProduct tsp 
		join Shared.ProductDosageForm df on tsp.ProductId = df.ProductId
		join Shared.TimeDefinition td on Shared.fnCompareDates(tsp.FromDate, tsp.ToDate ,td.FromDate ,td.ToDate ) = 1
		join Staging.InMarketSalesIncludingCeilingQty s on s.TimeDefinitionId = td.TimeDefinitionId
			and s.DosageFormId = df.DosageFormId 
	
		join Staging.DistributorAccountDosagePercentage d on s.AccountId = d.AccountId and s.DosageFormId = d.DosageFormId 
			and s.TimeDefinitionId = d.TimeDefinitionId

		join Staging.ProfileProductTerritoryPercentages ppt on tsp.TeamId = ppt.TeamId and td.TimeDefinitionId = ppt.TimeDefinitionId
			and ppt.CountryId = s.CountryId and ppt.ProductId = tsp.ProductId and ppt.DosageFormId = df.DosageFormId
		join Shared.Account a with (nolock)	on s.AccountId = a.AccountId
	where RemainingQty <> 0 and td.TimeDefinitionId between @fromTimeId and @toTimeId
	--7. insert into sales after calculating stores
	insert into Staging.InMarketSalesFact(DistributorId, AccountId, ProductId, DosageFormId, TimeDefinitionId, CountryId, ProfileId,TeamId, TerritoryId, Qty, SharePercentage, ShareType)
	select s.DistributorId, s.AccountId, ppt.ProductId, s.DosageFormId, s.TimeDefinitionId, s.CountryId, ppt.ProfileId, ppt.TeamId, a.TerritoryId , s.Qty*ppt.SalesTargetAvgPercentage, ppt.SalesTargetAvgPercentage, 'Stores'
	from sales.StoreAccount sa
		join sales.InMarketSales s with (nolock) on sa.AccountId = s.AccountId and s.TimeDefinitionId between sa.FromTimeId and isnull(sa.ToTimeId,10000)
		join Shared.Account a with (nolock) on s.AccountId = a.AccountId
		join Staging.ProfileProductTerritoryPercentages ppt on ppt.CountryId = s.CountryId
			and ppt.DosageFormId = s.DosageFormId
			and ppt.TerritoryId = a.TerritoryId
			and ppt.TimeDefinitionId = s.TimeDefinitionId
	where s.TimeDefinitionId between @fromTimeId and @toTimeId
	/*update prices*/
	update s set
	Amount = s.Qty * isnull(pa.price, pp.Price)
	from Staging.InMarketSalesFact s
		join Shared.TimeDefinition td on s.TimeDefinitionId = td.TimeDefinitionId
		left join sales.ProductPrice pa on s.ProductId = pa.ProductId
			and pa.AccountId = s.AccountId
			and Shared.fnCompareDates(pa.FromDate, pa.ToDate, td.FromDate, td.ToDate) = 1
		left join sales.ProductPrice pp on s.ProductId = pp.ProductId
			and pp.AccountId is null and pp.AccountCategoryId is null and pp.DistributorId is null and pp.DosageFormId is null
			and pp.TerritoryId is null
			and Shared.fnCompareDates(pp.FromDate, pp.ToDate, td.FromDate, td.ToDate) = 1
END