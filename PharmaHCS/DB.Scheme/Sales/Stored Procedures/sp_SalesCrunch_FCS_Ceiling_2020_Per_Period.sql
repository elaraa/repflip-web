﻿
-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 18 Feb 20
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_SalesCrunch_FCS_Ceiling_2020_Per_Period] 
@periodId int 
AS
BEGIN
	SET NOCOUNT ON;

--Ceiling	
/*
1- Rep/Profile/Account/%
2- Rep/Profile/Brick [get remaining]
3- get sales for rep/profile [per year [from/to] ]
4- apply ceiling product account
5- apply ceiling product
6- restore ceiling exception account
7- restore ceiling exception account/product
*/


declare @year int, @fromDate date, @todate date
select @year = year, @fromDate = FromDate, @todate = ToDate from Shared.TimeDefinition where TimeDefinitionId = @periodId

IF OBJECT_ID('Temp.profileaccount') IS NOT NULL drop table Temp.profileaccount
select tp.TeamId,pa.ProfileId, AccountId, sum(Perccentage) Perccentage, td.TimeDefinitionId, cast('ProfileAccount' as varchar(50)) ProfileAccountType, td.FromDate, td.ToDate
into Temp.profileaccount
from sales.ProfileAccount pa
	join Shared.TeamProfile tp on pa.ProfileId = tp.ProfileId and tp.Active = 1
	join Shared.TimeDefinition td on Shared.fnCompareDates(td.FromDate, td.ToDate, pa.FromDate, pa.ToDate) = 1
where td.TimeDefinitionId = @periodId and ProductId is null
group by tp.TeamId,pa.ProfileId, AccountId, td.TimeDefinitionId, td.FromDate, td.ToDate

insert into Temp.profileaccount
select tp.TeamId,pa.ProfileId, AccountId, pa.Percentage, td.TimeDefinitionId, 'ProfileTerritory' ProfileAccountType, td.FromDate, td.ToDate
from sales.ProfileTerritory pa
	join Shared.TeamProfile tp on pa.ProfileId = tp.ProfileId and tp.Active = 1
	join Shared.Account a on pa.TerritoryId = a.TerritoryId and a.Active = 1 and a.Approved = 1
	join Shared.TimeDefinition td on Shared.fnCompareDates(td.FromDate, td.ToDate, pa.FromDate, pa.ToDate) = 1
where td.TimeDefinitionId = @periodId
	and not exists(select top 1 1 from sales.StoreAccount sa where @periodId between sa.FromTimeId and isnull(sa.ToTimeId,@periodId) and sa.AccountId = a.AccountId )

--- remove from territory account where exists in profileaccount
delete p from Temp.profileaccount p
join (
select TeamId, ProfileId, AccountId
from Temp.profileaccount
group by TeamId, ProfileId, AccountId
having count(*) > 1
) d on p.AccountId = d.AccountId and p.TeamId = d.TeamId and p.ProfileId = d.ProfileId --and p.Perccentage = d.Perccentage
and p.ProfileAccountType = 'ProfileTerritory'

---delete from territory where account has 100% from profile account
delete p from Temp.profileaccount p
join (
select TeamId, AccountId
from Temp.profileaccount
where ProfileAccountType = 'ProfileAccount'
group by TeamId, AccountId
having cast(sum(Perccentage) as decimal(18,6)) >= 1.0
) d on p.AccountId = d.AccountId and p.TeamId = d.TeamId
and p.ProfileAccountType = 'ProfileTerritory'

/*
missing:
1. check total percentage is 100% per account per team
2. add remaining account % for accountterr
*/

IF OBJECT_ID('Temp.ProfileAccountProductShare') IS NOT NULL drop table Temp.ProfileAccountProductShare
--insert into Temp.ProfileAccountProductShare
select distinct p.TeamId, p.ProfileId, p.AccountId, tp.ProductId, d.DosageFormId , p.Perccentage, p.TimeDefinitionId, d.PackFactor, p.FromDate, p.ToDate
into Temp.ProfileAccountProductShare
from temp.profileaccount p
	join sales.TeamSalesProduct tp on p.TeamId = tp.TeamId
		and Shared.fnCompareDates(p.FromDate, p.ToDate, tp.FromDate, tp.ToDate)=1
	join Shared.ProductDosageForm d on tp.ProductId = d.ProductId

/*
prepare for special accounts with special products

1. get product account percentage
2. update existing shares with (1-percentage) * profile.percentage
3. delete shares of 0
4. add the profiles with special shares
*/
declare @AccountProduct table (accountid int, productid int, Perccentage decimal(18,6) )
insert into @AccountProduct
select AccountId, ProductId, sum(Perccentage) Perccentage
from sales.ProfileAccount
where ProductId is not null
and Shared.fnCompareDates(@fromDate, @todate, FromDate, ToDate) = 1
group by AccountId, ProductId

update aps set
Perccentage = case when ap.Perccentage >=1.0 then 0 else 1.0-ap.Perccentage end * aps.Perccentage
from temp.ProfileAccountProductShare aps
	join @AccountProduct ap on aps.AccountId = ap.accountid 
		and aps.ProductId = ap.productid

delete from temp.ProfileAccountProductShare where Perccentage = 0

insert into temp.ProfileAccountProductShare
select p.TeamId, tp.ProfileId, tp.AccountId, tp.ProductId, d.DosageFormId , tp.Perccentage, td.TimeDefinitionId, d.PackFactor, td.FromDate, td.ToDate
from sales.ProfileAccount tp
	join Shared.TeamProfile p on tp.ProfileId = p.ProfileId and p.Active = 1
	join Shared.TimeDefinition td on Shared.fnCompareDates(td.FromDate, td.ToDate, tp.FromDate, tp.ToDate) = 1
		and td.TimeDefinitionId = @periodId
	join Shared.ProductDosageForm d on tp.ProductId = d.ProductId
where tp.ProductId is not null
/* done */
IF OBJECT_ID('Temp.Target_InMarketSales') IS NOT NULL drop table temp.Target_InMarketSales
select s.CountryId,TeamId, ProfileId, P.ProductId, p.DosageFormId, p.AccountId, sum(s.Qty * p.PackFactor * p.Perccentage) Qty
	, sum(s.Qty * p.PackFactor * p.Perccentage * pp.Price) Amount, s.TimeDefinitionId, p.Perccentage, cast('Profile' as varchar(50)) SalesType
into temp.Target_InMarketSales
from temp.ProfileAccountProductShare p
	join sales.InMarketSales s with (nolock) on p.AccountId = s.AccountId and p.DosageFormId = s.DosageFormId
	join sales.ProductPrice pp on p.ProductId = pp.ProductId and pp.AccountId is null
		and pp.DosageFormId is null and Shared.fnCompareDates(p.FromDate, p.ToDate, pp.FromDate, pp.ToDate) = 1
where s.TimeDefinitionId = @periodId
GROUP BY s.CountryId,TeamId, ProfileId, P.ProductId, p.DosageFormId, p.AccountId, s.TimeDefinitionId, p.Perccentage

--ceiling dosage/account
insert into temp.Target_InMarketSales
select s.CountryId, s.TeamId,s.ProfileId, ProductId, s.DosageFormId,s.AccountId
	, (s.Qty-(c.CeilingQty*sign(s.Qty)))*-1*sign(s.Qty)
	, (s.Qty-(c.CeilingQty*sign(s.Qty)))*-1 * s.Price*sign(s.Qty), s.TimeDefinitionId, 0, 'Cut Over Ceiling Account'
from(
	select CountryId, TeamId, ProfileId,AccountId, ProductId, DosageFormId, TimeDefinitionId, sum(Qty) Qty, sum(Amount) Amount, case when sum(Qty) = 0 then 0 else sum(Amount)/sum(Qty) end Price 
	from temp.Target_InMarketSales
	group by CountryId, TeamId, ProfileId,AccountId, ProductId, DosageFormId, TimeDefinitionId
) s 
join  sales.CeilingDosageAccount c on 
	@periodId between FromTimeId and isnull(ToTimeId, @periodId)
	and c.AccountId = s.AccountId and c.DosageFormId = s.DosageFormId
	and abs(s.Qty) > c.CeilingQty
where not exists (select top 1 1 from sales.CeilingAccountException ce where ce.AccountId = s.AccountId and ce.ProductId = s.ProductId and @periodId between ce.FromTimeId and isnull(ce.ToTimeId,@periodId) )

---ceiling dosage
insert into temp.Target_InMarketSales
select s.CountryId, s.TeamId,s.ProfileId, s.ProductId, s.DosageFormId,s.AccountId
	, (s.Qty-(c.CeilingQty*sign(s.Qty)))*-1*sign(s.Qty)
	, (s.Qty-(c.CeilingQty*sign(s.Qty)))*-1 * s.Price*sign(s.Qty), s.TimeDefinitionId, 0, 'Cut Over Ceiling Product'
from(
	select CountryId, TeamId,AccountId,ProfileId,ProductId, DosageFormId, TimeDefinitionId, sum(Qty) Qty, sum(Amount) Amount, case when sum(Qty) = 0 then 0 else sum(Amount)/sum(Qty) end Price 
	from temp.Target_InMarketSales
	group by CountryId, TeamId,AccountId,ProfileId, ProductId, DosageFormId, TimeDefinitionId
) s 
join  sales.CeilingDosageAccount c on 
	@periodId between FromTimeId and isnull(ToTimeId, @periodId)
	and c.AccountId is null and c.DosageFormId = s.DosageFormId
	and s.Qty > c.CeilingQty
left join sales.CeilingAccountException ce on ce.AccountId = s.AccountId and (ce.ProductId = s.ProductId or ce.ProductId is null) and @periodId between ce.FromTimeId and isnull(ce.ToTimeId,@periodId)
left join sales.CeilingDosageAccount da on da.AccountId = s.AccountId and da.DosageFormId = s.DosageFormId and @periodId between da.FromTimeId and isnull(da.ToTimeId,@periodId)
where ce.CeilingAccountExceptionId is null and da.CeilingDosageAccountId is null
/*not exists (select top 1 1 from sales.CeilingAccountException ce where ce.AccountId = s.AccountId and ce.ProductId = s.ProductId and @periodId between ce.FromTimeId and isnull(ce.ToTimeId,@periodId) )
and not exists(select top 1 1 from sales.CeilingDosageAccount da where da.AccountId = s.AccountId and da.DosageFormId = s.DosageFormId and @periodId between da.FromTimeId and isnull(da.ToTimeId,@periodId) )
*/
---- calculate profile/brick/product/%
IF OBJECT_ID('Temp.target_profileTerritoryProductSales') IS NOT NULL drop table temp.target_profileTerritoryProductSales
select TeamId, ProfileId, ProductId, TimeDefinitionId, sum(Qty) Qty
into temp.target_profileTerritoryProductSales
from temp.Target_InMarketSales s
	join Shared.Account a on s.AccountId = a.AccountId
	join shared.fn_AccountCategory_GetParentLevelForLeaf(1,1) ac
		on a.CategoryId = ac.CategoryId
		and ac.RootCategoryId = 29 /*PM sales*/
group by TeamId, ProfileId, ProductId, TimeDefinitionId

IF OBJECT_ID('Temp.target_productsales') IS NOT NULL drop table temp.target_productsales
select TeamId, ProductId, sum(Qty) Qty
into temp.target_productsales
	from temp.Target_InMarketSales s
		join Shared.Account a on s.AccountId = a.AccountId 
		join shared.fn_AccountCategory_GetParentLevelForLeaf(1,1) ac
			on a.CategoryId = ac.CategoryId
			and ac.RootCategoryId = 29 /*PM sales*/
	group by TeamId, ProductId
IF OBJECT_ID('Temp.Target_SalesPercentage') IS NOT NULL drop table temp.Target_SalesPercentage
select pq.TeamId, ProfileId, pq.ProductId
	, case when sum(s.Qty) <> 0 then sum(pq.Qty)/s.Qty else 0 end SalesPecentage
into Temp.Target_SalesPercentage
from temp.target_profileTerritoryProductSales pq
	join Temp.target_productsales s on pq.TeamId = s.TeamId and pq.ProductId = s.ProductId
where case when s.Qty <> 0 then pq.Qty/s.Qty else 0 end <> 0 
group by pq.TeamId, ProfileId,pq.ProductId,s.Qty

drop table temp.target_profileTerritoryProductSales
drop table temp.target_productsales
---calculate target percentage
IF OBJECT_ID('Temp.Target_TargetPercentage') IS NOT NULL drop table temp.Target_TargetPercentage
select  TeamId, ProfileId, t.ProductId
	, case when ta.Qty <> 0 then t.Qty/ta.Qty  else 0 end TargetPecentage
into temp.Target_TargetPercentage
from (
	select tp.TeamId, t.ProductId, t.ProfileId, sum(Qty) Qty
	from sales.Target t
	join Shared.TeamProfile tp on tp.ProfileId = t.ProfileId
	where TargetTypeId = 6 and t.TimeDefinitionId = @periodId
	group by tp.TeamId, t.ProductId, t.ProfileId
	) t
	join (
		select ProductId, sum(Qty) Qty
		from sales.Target t
		where t.TargetTypeId = 6 and t.TimeDefinitionId = @periodId
		group by ProductId
	) ta on t.ProductId = ta.ProductId
		
---- calculate avg. percentage
IF OBJECT_ID('Temp.Target_AvgPercentage') IS NOT NULL drop table temp.Target_AvgPercentage
select isnull(s.TeamId, t.TeamId) TeamId
, isnull(s.ProfileId, t.ProfileId) ProfileId
, isnull(s.ProductId, t.ProductId) ProductId
, s.SalesPecentage, t.TargetPecentage
, case 
	when  s.SalesPecentage is null then t.TargetPecentage
	when  t.TargetPecentage is null then s.SalesPecentage
	else (s.SalesPecentage+ t.TargetPecentage)/2.0 end/* isnull(t.TargetPecentage,0)*/ AvgPercentage
into temp.Target_AvgPercentage
from temp.Target_SalesPercentage s
	full outer join (
	select t.* 
	from temp.Target_TargetPercentage t
	) t on s.TeamId = t.TeamId
		and s.ProfileId = t.ProfileId and s.ProductId = t.ProductId

--clean tables
drop table temp.Target_SalesPercentage
drop table temp.Target_TargetPercentage
--- ceiling distribution - Account
insert into temp.Target_InMarketSales
select s.CountryId, s.TeamId, p.ProfileId, s.ProductId, s.DosageFormId, s.AccountId
	, -1 * s.Qty * p.TargetPecentage --p.AvgPercentage
	, -1 * s.Amount * p.TargetPecentage --p.AvgPercentage
	, s.TimeDefinitionId
	, p.AvgPercentage
	, 'Ceiling Account'
from  temp.Target_InMarketSales s
	join temp.Target_AvgPercentage p on s.ProductId = p.ProductId
		and s.TeamId = p.TeamId
where s.SalesType = 'Cut Over Ceiling Account'		
--- ceiling distribution - Product
insert into temp.Target_InMarketSales
select s.CountryId, s.TeamId, p.ProfileId, s.ProductId, s.DosageFormId, s.AccountId
	, -1 * s.Qty * p.TargetPecentage --p.AvgPercentage
	, -1 * s.Amount * p.TargetPecentage --p.AvgPercentage
	, s.TimeDefinitionId
	, p.AvgPercentage
	, 'Ceiling Product'
from  temp.Target_InMarketSales s
	join Shared.Account a on s.AccountId = a.AccountId
	join temp.Target_AvgPercentage p on s.ProductId = p.ProductId
		and s.TeamId = p.TeamId
where s.SalesType = 'Cut Over Ceiling Product'

--- calculate store
insert into temp.Target_InMarketSales
select s.CountryId,per.TeamId,per.ProfileId, df.ProductId, s.DosageFormId,s.AccountId
	, sum(s.Qty*df.PackFactor*per.TargetPecentage /*per.AvgPercentage*/)
	, sum(s.Qty*pp.Price*df.PackFactor*per.TargetPecentage /*per.AvgPercentage*/)
	, s.TimeDefinitionId
	, per.AvgPercentage
	, 'Stores'
from sales.StoreAccount sa
	join sales.InMarketSales s with (nolock) on sa.AccountId = s.AccountId
		and @periodId between sa.FromTimeId and isnull(sa.ToTimeId ,@periodId)
	join shared.ProductDosageForm df on s.DosageFormId = df.DosageFormId
	join Shared.Account a on s.AccountId = a.AccountId
	join temp.Target_AvgPercentage per on per.ProductId = df.ProductId
	join sales.ProductPrice pp on df.ProductId = pp.ProductId and pp.AccountId is null
		and pp.DosageFormId is null and Shared.fnCompareDates(@fromDate, @todate, pp.FromDate, pp.ToDate) = 1
where s.TimeDefinitionId = @periodId
group by s.CountryId,per.TeamId,per.ProfileId, df.ProductId, s.DosageFormId,s.AccountId, s.TimeDefinitionId, per.AvgPercentage

delete from Staging.InMarketSalesByProfileAccount where TimeDefinitionId = @periodId

insert into Staging.InMarketSalesByProfileAccount(CountryId, TeamId, ProfileId, ProductId, DosageFormId, AccountId, Qty, Amount, TimeDefinitionId, SharePercentage, ShareType)
SELECT CountryId, TeamId, ProfileId, ProductId, DosageFormId, AccountId, Qty, Amount, TimeDefinitionId, Perccentage, SalesType
FROM     Temp.Target_InMarketSales

truncate table Temp.Target_InMarketSales

end