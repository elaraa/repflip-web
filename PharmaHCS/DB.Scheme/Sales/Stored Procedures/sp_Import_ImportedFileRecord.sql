﻿-- =============================================
-- Author:		<Hesham>
-- Create date: <10-20-18>
-- Stored Procedure: <sp_Import_ImportedFileRecord>
-- =============================================

CREATE PROCEDURE [Sales].[sp_Import_ImportedFileRecord]  
@SourceTable nvarchar(max),
@UserId nvarchar(50)
AS
 
declare @Query nvarchar(max),  @BonusQuery nvarchar(max), @DeleteSql  nvarchar(max)

Select @Query = 'insert into [Sales].[ImportedFileRecord]([FileId],[DistributorBranchId],[RawRecord],[CustomerCode],[CustomerName],[CustomerAddress]
      ,[CustomerTerritory],[DosageFormCode],[DosageFormName],[FileQuantity],[FileAmount],[InvoiceNumber],[SalesDate],[AccountId]
      ,[DosageFormId],[Quantity] ,[Amount],[DistributorBranchCode],[DistributorBranchName],[CustomerMapped] ,[ProductMapped]
      ,[DuplicateRow] ,[InvalidQty],[InvalidRow] ,[AccountToBeCreated],[CreationDate],[LastModifiedDate],[CreatedById]
      ,[LastModifiedById],[IsBonus])
select [FileId],[DistributorBranchId],[RawRecord],[CustomerCode],[CustomerName],[CustomerAddress]
      ,[CustomerTerritory],[DosageFormCode],[DosageFormName],[FileQuantity],[FileAmount],[InvoiceNumber],[SalesDate],[AccountId]
      ,[DosageFormId],[Quantity] ,[Amount],[DistributorBranchCode],[DistributorBranchName],[CustomerMapped] ,[ProductMapped]
      ,[DuplicateRow] ,[InvalidQty],[InvalidRow] ,[AccountToBeCreated],[CreationDate],[LastModifiedDate],[CreatedById]
      ,[LastModifiedById], 0 from Temp.' + @SourceTable + ' Where cast([Quantity] as decimal(18,4)) is not null and cast([Quantity] as decimal(18,4)) != 0.0' 



 Select @BonusQuery = 'insert into [Sales].[ImportedFileRecord]([FileId],[DistributorBranchId],[RawRecord],[CustomerCode],[CustomerName],[CustomerAddress]
      ,[CustomerTerritory],[DosageFormCode],[DosageFormName],[FileQuantity],[FileAmount],[InvoiceNumber],[SalesDate],[AccountId]
      ,[DosageFormId],[Quantity] ,[Amount],[DistributorBranchCode],[DistributorBranchName],[CustomerMapped] ,[ProductMapped]
      ,[DuplicateRow] ,[InvalidQty],[InvalidRow] ,[AccountToBeCreated],[CreationDate],[LastModifiedDate],[CreatedById]
      ,[LastModifiedById],[IsBonus])
select [FileId],[DistributorBranchId],[RawRecord],[CustomerCode],[CustomerName],[CustomerAddress]
      ,[CustomerTerritory],[DosageFormCode],[DosageFormName],[FileQuantity],[FileAmount],[InvoiceNumber],[SalesDate],[AccountId]
      ,[DosageFormId],[BQuantity] ,[Amount],[DistributorBranchCode],[DistributorBranchName],[CustomerMapped] ,[ProductMapped]
      ,[DuplicateRow] ,[InvalidQty],[InvalidRow] ,[AccountToBeCreated],[CreationDate],[LastModifiedDate],[CreatedById]
      ,[LastModifiedById], 1 from Temp.' + @SourceTable + ' Where [IsBonus] = ''True'' ' 

 EXEC sp_executesql @Query;
 EXEC sp_executesql @BonusQuery;


SET @DeleteSql = 'DROP TABLE Temp.'+ @SourceTable;
EXEC sp_executesql @DeleteSql;