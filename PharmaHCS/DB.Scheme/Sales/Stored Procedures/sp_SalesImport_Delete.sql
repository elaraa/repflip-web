﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: Aug-2019
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_SalesImport_Delete]
	-- Add the parameters for the stored procedure here
	@fileId int, 
	@userId int,
	@deleteMappings bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	delete from sales.InMarketSales where FileId = @fileId
	if @deleteMappings = 1
	begin
		BEGIN TRY  
			delete from Shared.Account where exists (select top 1 1 from sales.ImportedFileRecord r where FileId = @fileId and AccountToBeCreated = 1 and r.AccountId = Shared.Account.AccountId)
		END TRY  
		BEGIN CATCH  
		END CATCH;
		delete from sales.DistributorAccountMapping where FileId = @fileId
		delete from sales.DistributorProductMapping where FileId = @fileId
	end
	update sales.ImportedFile set 
	StatusId = case when @deleteMappings = 1 then 4 else 5 end,
	LastModifiedById = @userId,
	LastModifiedDate = getdate()
	where FileId = @fileId
END