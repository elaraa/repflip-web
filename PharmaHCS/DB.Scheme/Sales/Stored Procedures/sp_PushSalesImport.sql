﻿CREATE PROCEDURE [Sales].[sp_PushSalesImport] 
	@fileId int,
	@userId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	/*if file status is saved without territory => then delete the sales and push it */
	declare @statusid int = 1, @desiredStatus int = 3 /*saved*/ /*2 if at least one record of customer is not mapped*/
	select @statusid = statusid from sales.ImportedFile where FileId = @fileId

	if @statusid not in (1,2) /*if cancelled or deleted or failed => do nothing*/
	/*if product is not mapped => do nothing*/
	or exists (select top 1 1 from sales.ImportedFileRecord where FileId = @fileId and ProductMapped = 0)
	begin
		return
	end
	/*if file contains at least one record invalid as account => push into sales and update status*/
	if exists (select top 1 1 from sales.ImportedFileRecord where FileId = @fileId and CustomerMapped = 0)
	begin
		set @desiredStatus = 2 /*saved without customer*/
	end

    declare @importmode char(1)
	select @importmode = ImportMode
	from Sales.ImportedFile
	where FileId = @fileId

	if lower(@importmode) = 'o' --overwrite
	begin 
		-- delete from sales existing records
		DELETE FROM Sales.InMarketSales
		FROM  Sales.InMarketSales 
				JOIN Sales.ImportedFile f 
					ON Sales.InMarketSales.DistributorId = f.DistributorId 
						AND Sales.InMarketSales.TimeDefinitionId = f.TimeDefinitionId
		WHERE f.FileId = @fileId

		DELETE FROM Sales.InMarketSalesBonus
		FROM  Sales.InMarketSalesBonus 
				JOIN Sales.ImportedFile f 
					ON Sales.InMarketSalesBonus.DistributorId = f.DistributorId 
						AND Sales.InMarketSalesBonus.TimeDefinitionId = f.TimeDefinitionId
		WHERE f.FileId = @fileId
	end
	else if lower(@importmode) = 'e'--overwrite existing
	begin 
		DELETE FROM Sales.InMarketSales
		FROM  Sales.InMarketSales 
				JOIN Sales.ImportedFile f 
					ON Sales.InMarketSales.DistributorId = f.DistributorId 
						AND Sales.InMarketSales.TimeDefinitionId = f.TimeDefinitionId
				JOIN Sales.ImportedFileRecord fr 
					ON fr.FileId = f.FileId
						AND (Sales.InMarketSales.AccountId = fr.AccountId or @desiredStatus = 2)
						AND Sales.InMarketSales.DosageFormId = fr.DosageFormId
						AND (Sales.InMarketSales.SalesDate is null or Sales.InMarketSales.SalesDate = fr.SalesDate)
		WHERE f.FileId = @fileId

		DELETE FROM Sales.InMarketSalesBonus
		FROM  Sales.InMarketSalesBonus 
				JOIN Sales.ImportedFile f 
					ON Sales.InMarketSalesBonus.DistributorId = f.DistributorId 
						AND Sales.InMarketSalesBonus.TimeDefinitionId = f.TimeDefinitionId
				JOIN Sales.ImportedFileRecord fr 
					ON fr.FileId = f.FileId
						AND (Sales.InMarketSalesBonus.AccountId = fr.AccountId or @desiredStatus = 2)
						AND Sales.InMarketSalesBonus.DosageFormId = fr.DosageFormId
						AND (Sales.InMarketSalesBonus.SalesDate is null or Sales.InMarketSalesBonus.SalesDate = fr.SalesDate)
		WHERE f.FileId = @fileId
	end

	-- insert records
	INSERT INTO [Sales].[InMarketSales]([FileId],[CountryId],[DistributorId],[DistributorBranchId],[AccountId]
			,[DosageFormId],[Qty],Amount,[SalesDate],[InvoiceNumber],[TimeDefinitionId],[CreationDate],[CreatedById])
	select f.FileId,f.CountryId, f.DistributorId, fr.DistributorBranchId, fr.AccountId, fr.DosageFormId, fr.Quantity
		,fr.Quantity * isnull(pa.price, pp.Price), case when td.TimeDefinitionId is not null then fr.SalesDate else ftd.FromDate end, fr.InvoiceNumber, isnull(td.TimeDefinitionId, f.TimeDefinitionId), getdate(),@userId
	from sales.ImportedFile f
		join sales.ImportedFileRecord fr on f.FileId = fr.FileId
		join Shared.TimeDefinition ftd on f.TimeDefinitionId = ftd.TimeDefinitionId
		left join Shared.TimeDefinition td on fr.SalesDate between td.FromDate and td.ToDate
			and ftd.TimeDefinitionId = td.TimeDefinitionId
		join shared.ProductDosageForm p on fr.DosageFormId = p.DosageFormId
		left join sales.ProductPrice pa on p.ProductId = pa.ProductId
			and pa.AccountId = fr.AccountId
			and Shared.fnCompareDates(pa.FromDate, pa.ToDate, ftd.FromDate, ftd.ToDate) = 1
		left join sales.ProductPrice pp on p.ProductId = pp.ProductId
			and pp.AccountId is null and pp.AccountCategoryId is null and pp.DistributorId is null and pp.DosageFormId is null
			and pp.TerritoryId is null
			and Shared.fnCompareDates(pp.FromDate, pp.ToDate, ftd.FromDate, ftd.ToDate) = 1
	where f.FileId = @fileId and fr.IsBonus = 0
	and (CustomerMapped = 1 or @desiredStatus = 2) and ProductMapped = 1 and InvalidQty = 0 --and InvalidRow = 0


	INSERT INTO [Sales].[InMarketSalesBonus]([FileId],[CountryId],[DistributorId],[DistributorBranchId],[AccountId]
			,[DosageFormId],[Qty],Amount,[SalesDate],[InvoiceNumber],[TimeDefinitionId],[CreationDate],[CreatedById])
	select f.FileId,f.CountryId, f.DistributorId, fr.DistributorBranchId, fr.AccountId, fr.DosageFormId, fr.Quantity
		,fr.Quantity * isnull(pa.price, pp.Price), case when td.TimeDefinitionId is not null then fr.SalesDate else ftd.FromDate end, 
		fr.InvoiceNumber, isnull(td.TimeDefinitionId, f.TimeDefinitionId), getdate(),@userId
	from sales.ImportedFile f
		join sales.ImportedFileRecord fr on f.FileId = fr.FileId
		join Shared.TimeDefinition ftd on f.TimeDefinitionId = ftd.TimeDefinitionId
		left join Shared.TimeDefinition td on fr.SalesDate between td.FromDate and td.ToDate
			and ftd.TimeDefinitionId = td.TimeDefinitionId
		join shared.ProductDosageForm p on fr.DosageFormId = p.DosageFormId
		left join sales.ProductPrice pa on p.ProductId = pa.ProductId
			and pa.AccountId = fr.AccountId
			and Shared.fnCompareDates(pa.FromDate, pa.ToDate, ftd.FromDate, ftd.ToDate) = 1
		left join sales.ProductPrice pp on p.ProductId = pp.ProductId
			and pp.AccountId is null and pp.AccountCategoryId is null and pp.DistributorId is null and pp.DosageFormId is null
			and pp.TerritoryId is null
			and Shared.fnCompareDates(pp.FromDate, pp.ToDate, ftd.FromDate, ftd.ToDate) = 1
	where f.FileId = @fileId and fr.IsBonus = 1
	and (CustomerMapped = 1 or @desiredStatus = 2) and ProductMapped = 1 and InvalidQty = 0 --and InvalidRow = 0


	update sales.ImportedFile set
	StatusId = @desiredStatus,
	LastModifiedById = @userId,
	LastModifiedDate = GETDATE()
	where   FileId = @fileId

END