﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 16 Oct 19
-- Description:	
-- =============================================
CREATE PROCEDURE Sales.sp_ImportedFile_ImportAndMapAccounts 
	-- Add the parameters for the stored procedure here
@tableName varchar(100), 
	@userId int = 1,
	@fileId int = 3
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @countryId varchar(50), @distributerId varchar(50)
	select @countryId = cast(CountryId as varchar(50)), @distributerId = cast(DistributorId as varchar(50)) from sales.ImportedFile where FileId = @fileId
	
	set @tableName = '[Temp].'+@tableName
	

    -- add columns for TerritoryId, AccountTypeId, CategoryId
	declare @sql_db varchar(1000)= 'ALTER TABLE '+@tableName+'
	ADD TerritoryId INT NULL 
	, AccountTypeId int NULL
	, CategoryId decimal(18,6) NULL;'
	-- update the newly created Ids
	declare @sql nvarchar(max) = 'update t set
	TerritoryId = terr.TerritoryId,
	AccountTypeId = act.AccountTypeId,
	CategoryId = ac.CategoryId
	from '+@tableName+' t
		left join Shared.AccountType act on (ltrim(rtrim(t.[Account Type])) = ltrim(rtrim(act.AccountTypeCode))
			or ltrim(rtrim(t.[Account Type])) = ltrim(rtrim(act.AccountTypeName))
			)and act.CountryId = '+@countryId+'
		left join Shared.AccountCategory ac on (ltrim(rtrim(t.Category)) = ltrim(rtrim(ac.CategoryCode))
			or ltrim(rtrim(t.Category)) = ltrim(rtrim(ac.CategoryName)))
			and ac.CountryId = '+@countryId+'
		left join Shared.Territory terr on (ltrim(rtrim(t.Territory)) = ltrim(rtrim(terr.TerritoryCode))
			or ltrim(rtrim(t.Territory)) = ltrim(rtrim(terr.TerritoryName)))
			and terr.CountryId = '+@countryId+';'
	-- if any id is null return
	set @sql=@sql+' if exists (select top 1 1 from '+@tableName+' where TerritoryId is null or AccountTypeId is null or CategoryId is null
	or ltrim(rtrim([Customer Code])) = '''' or [Customer Code] is null or ltrim(rtrim([Customer Name])) = '''' or [Customer Name] is null or ltrim(rtrim([Customer Address])) = '''' or [Customer Address] is null
	)
	begin
		select t.AutoID RecordNumber,
			case when t.TerritoryId is null then ''Territory "''+isnull(t.Territory,'''')+''" does not exist.'' else '''' end
			+case when t.AccountTypeId is null then '' Type "''+isnull(t.[Account Type],'''')+''" does not exist.'' else '''' end
			+case when t.CategoryId is null then '' Category "''+isnull(t.Category,'''')+''" does not exist.'' else '''' end 
			+case when ltrim(rtrim([Customer Code])) = '''' or [Customer Code] is null then ''Customer code cannot be empty.'' else '''' end
			+case when ltrim(rtrim([Customer Name])) = '''' or [Customer Name] is null  then '' Customer name cannot be empty.'' else '''' end
			+case when ltrim(rtrim([Customer Address])) = '''' or [Customer Address] is null  then '' Customer address cannot be empty.'' else '''' end Error
		
		from '+@tableName+' t
		where TerritoryId is null or AccountTypeId is null or CategoryId is null
		or ltrim(rtrim([Customer Code])) = '''' or [Customer Code] is null or ltrim(rtrim([Customer Name])) = '''' or [Customer Name] is null or ltrim(rtrim([Customer Address])) = '''' or [Customer Address] is null
		
		drop table '+@tableName+'
		return
	end
	'-- add the records into account table
	set @sql=@sql+' insert into Shared.Account(AccountCode, AccountName, AccountTypeId, CategoryId, TerritoryId, CountryId, Address,active, approved, CreatedById)
	select ltrim(rtrim([Customer Code])), [Customer Name], AccountTypeId, CategoryId, TerritoryId, '+@countryId+', [Customer Address],1,1, '+cast(@userId as varchar(50))+'
	from '+@tableName+' t
	'-- add the codes into distributoraccount mapping table
	set @sql=@sql+' insert into sales.DistributorAccountMapping(DistributorId, AccountCode, AccountName, AccountAddress, AccountId, FileId, CreatedById)
	select '+@distributerId+', ltrim(rtrim([Customer Code])), [Customer Name], [Customer Address], a.AccountId, '+cast(@fileId as varchar(50))+', '+cast(@userId as varchar(50))+'
	from '+@tableName+' t
		join Shared.Account a on ltrim(rtrim([Customer Code])) = a.AccountCode
			and a.CountryId = '+@countryId+'
	'-- update the records in importedfilerecord with accountid and update customermapped = 1
	set @sql=@sql+' update r set
	AccountId = a.AccountId,
	CustomerMapped = 1,
	LastModifiedById = '+cast(@userId as varchar(50))+',
	LastModifiedDate = GETDATE()
	from sales.ImportedFileRecord r
		join '+@tableName+' t on 
			ltrim(rtrim(t.[Customer Code]))  = r.CustomerCode
			and r.FileId = '+cast(@fileId as varchar(50))+'
		join Shared.Account a on ltrim(rtrim([Customer Code])) = a.AccountCode
			and a.CountryId = '+@countryId+'
	'-- drop the table
	set @sql=@sql+' drop table '+@tableName+''

	exec (@sql_db)
	exec ( @sql)
END