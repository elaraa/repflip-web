﻿-- =============================================
-- Author:		Hesham
-- Create date: 21-6-2020
-- =============================================
CREATE PROCEDURE [Sales].[sp_ValidateProfileAccount]
	@TableName as nvarchar(max),
    @TimeId as int
AS
BEGIN
declare @EmptyString as char(1) = ''
declare @StartDate as date = (select top 1 FromDate from Shared.TimeDefinition where TimeDefinitionId = @TimeId)	

declare @SQL as nvarchar(max), @Query as nvarchar(max) 
set @Query = '

IF COL_LENGTH('''+ @TableName +''', ''Status'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD Status bit NOT NULL DEFAULT(1)
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidProfileCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidProfileCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidAccountCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidAccountCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidProductCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidProductCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidDosageFormCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidDosageFormCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''IsDuplicated'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsDuplicated nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''IsOverlapped'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsOverlapped nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''IsExceedPercentage'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsExceedPercentage nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''ExceededPercentage'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD ExceededPercentage nvarchar(max) default NULL
END
'
exec sp_executesql @Query
set @SQL = '
UPDATE t set t.InvalidProfileCode = (case when p.ProfileId is not null then ''False'' else ''True'' end)
, Status = (case when p.ProfileId is not null and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
Shared.TeamProfile p on t.[Profile Code]  = p.ProfileCode

UPDATE t set t.InvalidAccountCode = (case when a.AccountId is not null then ''False'' else ''True'' end)
, Status = (case when a.AccountId is not null and Status = 1 then 1 else 0 end) from '+ @TableName +' t left join 
Shared.Account a on t.[Account Code]  = a.AccountCode

UPDATE t set t.InvalidProductCode = (case when a.ProductId is not null then ''False'' else ''True'' end)
, Status = (case when a.ProductId is not null and Status = 1 then 1 else 0 end) from '+ @TableName +' t left join 
Shared.Product a on t.[Product Code]  = a.ProductCode where (t.[Product Code] is not null and t.[Product Code] != '''+@EmptyString+''')

UPDATE t set t.InvalidDosageFormCode = (case when a.DosageFormId is not null then ''False'' else ''True'' end)
, Status = (case when a.DosageFormId is not null and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
Shared.ProductDosageForm a on t.[DosageForm Code]  = a.DosageFormCode where (t.[DosageForm Code] is not null and t.[DosageForm Code] !='''+@EmptyString+''')

select * into #DuplicatedRows from (
select t.AutoID from '+ @TableName +' t join 
'+ @TableName +' tn on t.[Product Code] = tn.[Product Code] and t.[Account Code] = tn.[Account Code] and
isnull(t.[DosageForm Code], ''`'') = isnull(tn.[DosageForm Code], ''`'')  and isnull(t.[Product Code], ''`'') = isnull(tn.[Product Code], ''`'')
where t.AutoID <> tn.AutoID
) as DuplicatedRows
update '+ @TableName +' set IsDuplicated = ''True'' where AutoID in (select * from #DuplicatedRows)

select * into #Overlapped from (
select t.AutoID, a.AccountId, p.ProfileId, t.[Percentage] as newPercentage, pa.[Perccentage], pp.ProductId, d.DosageFormId, 
t.[Account Code], t.[Profile Code]
from '+ @TableName +'  t join
 Shared. Account a on a.AccountCode = t.[Account Code] join
 Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] left join
 Shared.ProductDosageForm d on d.DosageFormCode = t.[DosageForm Code] left join
 Shared.Product pp on  pp.ProductCode = t.[Product Code] join
 Sales.ProfileAccount pa on pa.AccountId = a.AccountId and pa.ProfileId = p.ProfileId
 where Status = 1 and (d.DosageFormId is null or d.DosageFormId = pa.DosageFromId) and (pp.ProductId is null or pp.ProductId = pa.ProductId)
 and ((pa.ToDate is null and ''' + cast(@StartDate as nvarchar(100)) + ''' <= pa.FromDate) or (pa.ToDate is not null and ''' + cast(@StartDate as nvarchar(100)) + ''' <= pa.ToDate))) as Overlapped
update '+ @TableName +' set IsOverlapped = ''True'' where AutoID in (select AutoID from #Overlapped)


select * into #ExceedPercentage from (
select AutoID, newPercentage, SUM(Perccentage) as [TotalPercentage], ProductId, DosageFormId, AccountCode, ProfileCode
from(select distinct t.AutoID, a.AccountId, t.[Percentage] as newPercentage,  v.[Perccentage], pp.ProductId, d.DosageFormId
, t.[Account Code] as AccountCode, t.[Profile Code] as ProfileCode from '+ @TableName + ' t join
 Shared. Account a on a.AccountCode = t.[Account Code] join
 Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] left join
 Sales.vw_ProfileAccount v on v.Team = p.TeamId and v.AccountId = a.AccountId left join
 Shared.ProductDosageForm d on d.DosageFormCode = t.[DosageForm Code] left join
 Shared.Product pp on  pp.ProductCode = t.[Product Code] join
 Sales.ProfileAccount pa on pa.AccountId = a.AccountId and pa.ProfileId = p.ProfileId
 where  Status = 1 and (d.DosageFormId is null or d.DosageFormId = pa.DosageFromId) and (pp.ProductId is null or pp.ProductId = pa.ProductId)
 and (v.ToDate is null or (v.ToDate is not null and ''' + cast(@StartDate as nvarchar(100)) + '''  <= v.ToDate)))T
group by AutoID, AccountId, newPercentage, ProductId, DosageFormId, AccountCode, ProfileCode) as ExceedPercentage
where (newPercentage + TotalPercentage) > 100
update t set t.IsExceedPercentage = ''True'', t.ExceededPercentage = e.TotalPercentage + t.Percentage from 
'+ @TableName +' t join #ExceedPercentage e on e.AutoID = t.AutoID

Select * into #ReturnData from (select AutoID as RowIndex, [Profile Code] as ProfileCode, [Account Code] as AccountCode, Percentage, [Product Code] as ProductCode,
[DosageForm Code] as DosageFormCode, InvalidProfileCode, 
InvalidAccountCode, InvalidProductCode, InvalidDosageFormCode, IsDuplicated, IsOverlapped, IsExceedPercentage, ExceededPercentage from '+ @TableName +' 
Where Status = 0 or IsDuplicated = ''True''  or IsOverlapped = ''True''  or IsExceedPercentage = ''True'') as ReturnData

drop table '+ @TableName +'

Select * from #ReturnData
'
exec sp_executesql @SQL

END