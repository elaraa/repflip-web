﻿CREATE PROCEDURE [Sales].[sp_Target_Analysis_ProfileTarget]
	@countryId int, 
	@year int,
	@userId int,
	@teamId int = null,
	@productId int = null,
	@territoryId int = null,
	@employeeId	int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#profiles') IS NOT NULL drop table #profiles
	Create table #profiles (EmployeeId int, ProfileId int)
	insert into #profiles
	select EmployeeId, ProfileId from Shared.fn_Security_GetProfilesForEmployee(@userId,getdate())
	OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 

	IF OBJECT_ID('tempdb..#profileTarget') IS NOT NULL drop table #profileTarget
	Create table #profileTarget (CountryId int, year int,TeamId int, ProfileId int, ProfileName nvarchar(max), ProfileTarget decimal(18,6), ProfileTargetGrowth decimal(18,6)
		, ProfileTarget_Qty decimal(18,6), ProfileTargetGrowth_Qty decimal(18,6))
	insert into #profileTarget
	select t.CountryId,t.Year, p.TeamId,
	p.ProfileId, p.ProfileName, sum(t.Amount) ProfileTarget, case when (t1.Amount) <> 0 then (sum(t.Amount)-t1.Amount)/(t1.Amount) else 1.0 end ProfileTargetGrowth
	, sum(t.Qty) ProfileTarget_Qty,case when (t1.Qty) <> 0 then (sum(t.Qty)-t1.Qty)/(t1.Qty) else 1.0 end ProfileTargetGrowth_Qty
	from (
		select t.CountryId, t.Year,ep.EmployeeId, t.ProfileId, sum(t.Qty) Qty, sum(t.Amount) Amount
		from sales.Target t with (nolock) 
			join Shared.timedefinition td on t.timedefinitionId = td.timedefinitionId
			join shared.EmployeeProfile ep on t.profileId = ep.ProfileId
				and shared.fnCompareDates(td.fromdate, td.todate, ep.fromdate, ep.todate)=1
		where t.CountryId = @countryId and t.Year= @year and t.TargetTypeId = 6
		group by t.CountryId, t.Year,ep.EmployeeId, t.ProfileId
		)t 
		join Shared.TeamProfile p on t.ProfileId = p.ProfileId
		left join (
		select ep.EmployeeId, sum(t1.Qty) Qty, sum(t1.Amount) Amount
		from sales.Target t1 with (nolock) 
			join Shared.timedefinition td on t1.timedefinitionId = td.timedefinitionId
			join shared.EmployeeProfile ep on t1.profileId = ep.ProfileId
				and shared.fnCompareDates(td.fromdate, td.todate, ep.fromdate, ep.todate)=1
		where t1.CountryId = @countryId and t1.Year= @year-1 and t1.TargetTypeId = 6
		group by ep.EmployeeId
		)t1 on t.EmployeeId = t1.EmployeeId
		join #profiles sp on sp.ProfileId=t.ProfileId
			and (@employeeId is null or sp.EmployeeId = @employeeId)
	group by t.CountryId,t.Year, p.TeamId, p.ProfileId, p.ProfileName, t1.Qty, t1.Amount
	OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))

	select m.EmployeeId ManagerId,m.EmployeeName ManagerName,e.EmployeeId,e.EmployeeName,pt.* 
	from #profileTarget pt
		left join Shared.EmployeeProfile ep on ep.ProfileId = pt.ProfileId 
			and shared.fnCompareDates(cast(@year as varchar(5))+'-12-31', cast(@year as varchar(5))+'-12-31', ep.FromDate, ep.ToDate) = 1 
		left join hr.Employee e on ep.EmployeeId = e.EmployeeId
		left join hr.EmployeeHistory eh on e.EmployeeId = eh.EmployeeId
			and Shared.fnCompareDates(cast(@year as varchar(5))+'-12-31', cast(@year as varchar(5))+'-12-31', eh.FromDate, eh.ToDate) = 1
		left join hr.Employee m on eh.ManagerId = m.EmployeeId
	OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))
END