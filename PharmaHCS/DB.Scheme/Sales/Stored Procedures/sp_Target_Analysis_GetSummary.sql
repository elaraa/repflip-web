﻿CREATE PROCEDURE [Sales].[sp_Target_Analysis_GetSummary] 
	-- Add the parameters for the stored procedure here
	@countryId int, 
	@year int,
	@userId int,
	@teamId int,
	@productId int,
	@territoryId int,
	@employeeId	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF OBJECT_ID('tempdb..#profiles') IS NOT NULL drop table #profiles
	Create table #profiles (EmployeeId int, ProfileId int)
	insert into #profiles
	select EmployeeId, ProfileId from Shared.fn_Security_GetProfilesForEmployee(@userId,getdate())
	----OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 

	
	declare @TargetAmount decimal(18,6), @TargetGrowth decimal(18,6)
		, @ProductName nvarchar(max), @ProductTarget decimal(18,6), @ProductTargetGrowth decimal(18,6)
		,@TerritoryName nvarchar(max), @TerritoryTarget decimal(18,6), @TerritoryTargetGrowth decimal(18,6), @terrId int
		,@ProfileName nvarchar(max),@ProfileTarget decimal(18,6),@ProfileTargetGrowth decimal(18,6),@ProfileId int

	select @TargetAmount = TargetAmount, @TargetGrowth = TargetGrowth
		, @ProductName = ProductName, @ProductTarget = ProductTarget, @ProductTargetGrowth = ProductTargetGrowth
	from (
		select sum(t.Amount) TargetAmount,case when t1.Amount <> 0 then ((sum(t.Amount)-t1.Amount)/t1.Amount) else null end TargetGrowth
		from sales.Target t with (nolock)
			cross join (
				select Sum(Amount) Amount
				from sales.Target t1 with (nolock) 
				where t1.Year=@year-1 and t1.TargetTypeId = 2
			) t1
			join Shared.fn_Security_GetProductsForEmployee(@userId,getdate()) sp on sp.ProductId=t.ProductId
				and (@teamId is null or @teamId = sp.TeamId)
				and (@productId is null or sp.ProductId = @productId)
		where t.TargetTypeId = 2 /*product*/
			and t.Year = @year
			and t.CountryId = @countryId
		group by t1.Amount
	) total
	cross join (
		select top 1 t.CountryId,t.Year, ProductName, t.Amount ProductTarget, case when t1.Amount <> 0 then ((t.Amount-t1.Amount)/t1.Amount) else null end ProductTargetGrowth
		from sales.Target t with (nolock)
			join Shared.Product p on t.ProductId = p.ProductId
			left join sales.Target t1 with (nolock) on t.TargetTypeId = t1.TargetTypeId and t.Year = t1.Year+1 and t.ProductId = t1.ProductId
			join Shared.fn_Security_GetProductsForEmployee(@userId,getdate()) sp on sp.ProductId=t.ProductId
				and (@teamId is null or @teamId = sp.TeamId)
		where t.TargetTypeId = 2 /*product*/
			and t.Year = @year
			and t.CountryId = @countryId
		order by 4 desc
	)ptotal
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 

	select top 1 /*t.CountryId, t.Year,*/
	@terrId = terr.territoryId,
	@TerritoryName = terr.TerritoryName, @TerritoryTarget = sum(t.Amount)--, TerritoryTargetGrowth = case when sum(t1.Amount) <> 0 then ((sum(t.Amount)-sum(t1.Amount))/sum(t1.Amount))-1.0 else null end
	from sales.Target t with (nolock)
		join Shared.Territory terr on t.TerritoryId = terr.TerritoryId
		join #profiles sp on sp.ProfileId=t.ProfileId
			and (@employeeId is null or sp.EmployeeId = @employeeId)
	where t.TargetTypeId = 6 /*profile/territory/product*/
		and t.Year = @year
		and t.CountryId = @countryId
		--and exists (select top 1 1 from Shared.fn_Security_GetProfilesForEmployee(@userId,getdate()) sp where sp.ProfileId=t.ProfileId and (@employeeId is null or sp.EmployeeId = @employeeId) )
	group by t.CountryId, t.Year,terr.territoryId,terr.TerritoryName
	order by sum(t.Amount) desc

	select @TerritoryTargetGrowth = (@TerritoryTarget-Sum(Amount))/Sum(Amount)
	from sales.Target t1 with (nolock) 
	where t1.TargetTypeId = 6 and t1.TerritoryId = @terrId
		and t1.Year = @year -1 and t1.countryId = @countryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 
	
	select top 1 /*t.CountryId,t.Year, */
	@ProfileId = t.ProfileId,
	@ProfileName = p.ProfileName, @ProfileTarget  = sum(t.Amount)--, @ProfileTargetGrowth = case when sum(t1.Amount) <> 0 then (sum(t.Amount)/sum(t1.Amount))-1.0 else null end 
	from sales.Target t with (nolock)
		join Shared.TeamProfile p on t.ProfileId = p.ProfileId
		join #profiles sp on sp.ProfileId=t.ProfileId
			and (@employeeId is null or sp.EmployeeId = @employeeId)
	where t.TargetTypeId = 6 /*profile/territory/product*/
		and t.Year = @year
		and t.CountryId = @countryId
	group by t.CountryId,t.Year,t.ProfileId, p.ProfileName
	order by sum(t.Amount) desc

	select @ProfileTargetGrowth = (@ProfileTarget-Sum(Amount))/Sum(Amount)
	from sales.Target t1 with (nolock) 
	where t1.TargetTypeId = 6 and t1.ProfileId = @ProfileId
		and t1.Year = @year -1 and t1.countryId = @countryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 

	select @TargetAmount TargetAmount, @TargetGrowth TargetGrowth, @ProductName ProductName, @ProductTarget ProductTarget, @ProductTargetGrowth ProductTargetGrowth
		,@TerritoryName TerritoryName, @TerritoryTarget TerritoryTarget, @TerritoryTargetGrowth TerritoryTargetGrowth
		,@ProfileName ProfileName, @ProfileTarget ProfileTarget, @ProfileTargetGrowth ProfileTargetGrowth
END