﻿


-- =============================================
-- Author:		Alaa
-- Create date: 21-6-2020
-- =============================================
CREATE PROCEDURE [Sales].[sp_CommitTeamProfile]
	@TableName as nvarchar(max),
	@userId as int
AS
BEGIN
set @TableName='[Temp].'+@tableName

declare @SQL as nvarchar(max), @Query as nvarchar(max) ,@SQLV as nvarchar(max),@SQLX as nvarchar(max)
set @Query = '

IF COL_LENGTH('''+ @TableName +''', ''Status'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD Status bit NOT NULL DEFAULT(1)
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidProfileCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidProfileCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidTeam'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidTeam nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''IsDuplicated'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsDuplicated nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidAvgCallRate'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidAvgCallRate nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''', ''InvalidListTargetDoctorsCount'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidListTargetDoctorsCount nvarchar(max) NOT NULL DEFAULT(''False'')
END
'

set @SQL = '
UPDATE t set t.InvalidProfileCode = (case when p.ProfileId is not null then ''False'' else ''True'' end)
, Status = (case when p.ProfileId is not null and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
Shared.TeamProfile p on t.[Profile Code]  = p.ProfileCode

UPDATE t set t.InvalidTeam = (case when a.TeamId is not null then ''False'' else ''True'' end)
, Status = (case when a.TeamId is not null and Status = 1 then 1 else 0 end) from '+ @TableName +' t left join 
Shared.Team a on t.[Team]  = a.TeamName

select * into #DuplicatedRows from (
select t.AutoID from '+ @TableName +' t join 
'+ @TableName +' tn on t.[Profile Code] = tn.[Profile Code] and t.[Team] = tn.[Team] 
where t.AutoID <> tn.AutoID
) as DuplicatedRows
update '+ @TableName +' set IsDuplicated = ''True'' where AutoID in (select * from #DuplicatedRows)'

set @SQLV = '

UPDATE t set t.InvalidAvgCallRate = (case when ISNUMERIC(t.AvgCallRate) = 1 and CONVERT(NUMERIC, t.AvgCallRate) >0 then ''False'' else ''True'' end)
, Status = (case when t.AvgCallRate is not null and Status = 1 then 1 else 0 end) from '+ @TableName +' t  


UPDATE t set t.InvalidListTargetDoctorsCount = (case when ISNUMERIC(t.ListTargetDoctorsCount) = 1 and CONVERT(NUMERIC, t.ListTargetDoctorsCount) >0 then ''False'' else ''True'' end)
, Status = (case when t.ListTargetDoctorsCount is not null and Status = 1 then 1 else 0 end) from '+ @TableName +' t 

Select * into #ReturnData from (select AutoID as RowIndex, [Profile Code] as ProfileCode, [Team] as Team,
 InvalidProfileCode, InvalidTeam,  IsDuplicated, InvalidAvgCallRate, InvalidListTargetDoctorsCount from '+ @TableName +' 
Where Status = 0 or IsDuplicated = ''True''or InvalidAvgCallRate = ''True''or InvalidListTargetDoctorsCount = ''True'') as ReturnData
'
set @SQLX= '

If (Select Count(*) from #ReturnData) = 0
 begin

 UPDATE Shared.TeamProfile  SET  AvgCallRate = t.AvgCallRate , ListTargetDoctorsCount=t.ListTargetDoctorsCount from '+ @TableName +' t left join
 Shared.TeamProfile tp on t.[Profile Code]  = tp.ProfileCode


 end

drop table '+ @TableName +'

Select * from #ReturnData
'
exec sp_executesql @Query
exec sp_executesql @SQL
EXEC (@SQLV+@SQLX)

END