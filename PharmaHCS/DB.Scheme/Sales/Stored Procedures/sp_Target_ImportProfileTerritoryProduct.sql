﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 12 Oct 19
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_Target_ImportProfileTerritoryProduct] 
	-- Add the parameters for the stored procedure here
	@tableName varchar(max), 
	@userId varchar (10),
	@TerritoryLevel varchar(10),
	@countryId varchar(10),
	@FromTimeId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	set @tableName = '[Temp].'+@tableName
		
	declare @sql nvarchar(max) = '', @sql_db varchar(max)

set @sql_db = '
ALTER TABLE '+@tableName+' 
	ADD qty decimal(18,6) NULL
	, ProductInvalid bit NULL
	, ProfileInvalid bit NULL
	, ProductNotInProfile bit null
	, QtyInvalid decimal(18,6) NULL;
'
set @sql = '
update '+@tableName+' set
qty = TRY_CONVERT(decimal(18,6),tgt_qty)

declare @year int, @enddate date 
select @year = Year from shared.TimeDefinition where TimeDefinitionId = '+@FromTimeId+'
select @enddate = cast(@year as varchar(50))+''-12-31''

delete from '+@tableName+' where qty is null or qty=0 or (tgt_qty is null or ltrim(rtrim(tgt_qty)) = '''')

update t set
ProductInvalid = case when p.ProductId is null then 1 else 0 end
, ProfileInvalid = case when tp.ProfileId is null then 1 else 0 end
, ProductNotInProfile = case when tsp.TeamSalesProductId is null then 1 else 0 end
from '+@tableName+' t 
	left join Shared.Product p on try_convert(int, t.ProductId) = p.ProductId
	left join Shared.TeamProfile tp on try_convert(int, t.ProfileId) = tp.ProfileId
	join Shared.TimeDefinition td on td.TimeDefinitionId = 61
	left join sales.TeamSalesProduct tsp on p.ProductId = tsp.ProductId 
		and tp.TeamId = tsp.TeamId
		and Shared.fnCompareDates(td.FromDate, td.ToDate, tsp.FromDate, tsp.ToDate) = 1

/*remove products which are not related to user*/
--delete from '+@tableName+'  where ProductId not in (select distinct ProductId from Shared.fn_Security_GetProductsForEmployee('+@userId+', @enddate))

/*validate master data*/
if(exists(select top 1 1 from '+@tableName+' where qty is null or ProductInvalid = 1 or ProfileInvalid = 1 or ProductNotInProfile = 1 or qty < 0))
begin 
	select AutoID Row#, 
		case when qty is null then ''Quantity is not a number: ''+isnull(tgt_qty,'''')+'' '' else '''' end+
		case when ProductInvalid is null then ''Product does not exist.'' else '''' end+
		case when ProfileInvalid is null then ''Profile does not exist.'' else '''' end +
		case when ProductNotInProfile is null then ''Product does not belong to Profile '' else '''' end +
		case when qty < 0 then '' Target quantity cannot be negative value: ''+tgt_qty+'' '' else '''' end errorerror
	from '+@tableName+'
	where qty is null 
		or ProductInvalid = 1 
		or ProfileInvalid = 1 
		or ProductNotInProfile = 1 
		or qty < 0
	
	drop table '+@tableName+'
	return;
end

/*validateproduct Total*/
/*
update t set
ProductTotalInvalid = 1,
TotalTargetQty = tar.Qty
from '+@tableName+' t
join (
	select ProductId,product, sum(qty) qty
	from '+@tableName+'
	group by ProductId, product
)import on t.ProductId = import.ProductId
	full outer join (
	select ProductId,t.ProductName, Qty
	from sales.fn_Target_GetRemainingProductTarget('+@countryId+','+@userId+','+@FromTimeId+') t
	) tar on import.ProductId = tar.ProductId

if(exists(select top 1 1 from '+@tableName+' where ProductTotalInvalid is null or ProductTotalInvalid = 1))
begin 
	select distinct product+'' does not match its total target of: ''+ cast(TotalTargetQty as varchar(50))
	from '+@tableName+'
	where ProductTotalInvalid is null or ProductTotalInvalid = 1
	
	drop table '+@tableName+'
	return;
end
*/

IF OBJECT_ID(''tempdb..#ProfileTerritoryProduct'') IS NOT NULL drop table #ProfileTerritoryProduct
create table #ProfileTerritoryProduct (ProductId int, TerritoryId int, rootTerritoryId int, ProfileId int, perccentage decimal(18,6) NULL)
insert into #ProfileTerritoryProduct
select ProductId, TerritoryId, RootTerritoryId, ProfileId, null
from [Sales].[fn_Target_GetProfileTerritoryProduct]('+@userId+',@enddate,'+@TerritoryLevel+')
--OPTION(USE HINT(''ENABLE_PARALLEL_PLAN_PREFERENCE''))	


update p set
perccentage = 1.0/cast(cnt as decimal(18,6))
from #ProfileTerritoryProduct p
join (
select ProfileId, ProductId, count(distinct TerritoryId) cnt
from #ProfileTerritoryProduct
group by ProfileId, ProductId
) terr on p.ProfileId = terr.ProfileId and p.ProductId = terr.ProductId

/*prepare the product phasing*/
IF OBJECT_ID(''tempdb..#ProductPhasing'') IS NOT NULL drop table #ProductPhasing
create table #ProductPhasing (ProductId int, CountryId int, TimeDefinitionId int, perccentage decimal(18,6) NULL)
insert into #ProductPhasing
select ISNULL(phased.ProductId, def.ProductId) ProductId, ISNULL(phased.CountryId,def.CountryId) CountryId,ISNULL(phased.TimeDefinitionId,def.TimeDefinitionId) TimeDefinitionId, ISNULL(phased.Percentage, def.defaultPercentage) Percentage
from(
	select ProductId, TimeDefinitionId, CountryId, Percentage 
	from sales.Target
	where TargetTypeId = 3 /*product phasing*/
	and TimeDefinitionId >= '+@FromTimeId+'
	and year = @year
	and countryId = '+@countryId+'
	) phased
	full outer join (
	--select distinct t.CountryId,ProductId, td.TimeDefinitionId, 1.0/12.0 defaultPercentage
	--from sales.Target t
	--	join Shared.TimeDefinition td 
	--		on t.CountryId = td.CountryId and t.Year = td.Year and td.TimeDefinitionId >= 61
	--where TargetTypeId = 2 /*product*/
	select p.CountryId,ProductId,td.TimeDefinitionId,1.0/12.0 defaultPercentage
	from Shared.Product p
		join Shared.TimeDefinition td on td.TimeDefinitionId >= '+@FromTimeId+' and td.Year = @year
	) def on phased.ProductId = def.ProductId
		and phased.TimeDefinitionId = def.TimeDefinitionId
		and phased.CountryId = def.CountryId

delete from sales.Target where TargetTypeId = 6 and TimeDefinitionId >= '+@FromTimeId+' and Year = @year and CountryId = '+@countryId+'
insert into sales.Target (TargetTypeId, TimeDefinitionId, ProfileId, ProductId, TerritoryId, Qty, Amount, CountryId, Year)
select 6,tp.TimeDefinitionId, t.ProfileId, t.ProductId,ptp.territoryId, sum(t.qty * ptp.perccentage * tp.perccentage) Qty
,sum(t.qty * ptp.perccentage* tp.perccentage* pp.Price),'+@countryId+',@year
from '+@tableName+' t
	join #ProfileTerritoryProduct ptp on ptp.ProductId = t.ProductId
		and ptp.profileId = t.ProfileId
	join #ProductPhasing tp on tp.productId = ptp.productId
		and tp.TimeDefinitionId >= '+@FromTimeId+'
	join Shared.TimeDefinition td on tp.TimeDefinitionId = td.TimeDefinitionId
	left join sales.ProductPrice pp on t.ProductId = pp.ProductId
		and pp.AccountId is null and pp.AccountCategoryId is null and pp.DistributorId is null and pp.DosageFormId is null
		and pp.TerritoryId is null
		and Shared.fnCompareDates(td.FromDate,td.ToDate,pp.FromDate, pp.ToDate) = 1
where qty <> 0
group by tp.TimeDefinitionId, t.ProfileId, t.ProductId,ptp.territoryId

drop table '+@tableName

exec (@sql_db)
exec (@sql)


END