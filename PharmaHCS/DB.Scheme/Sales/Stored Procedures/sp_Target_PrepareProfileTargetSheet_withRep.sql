﻿

-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 7 Oct 19
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_Target_PrepareProfileTargetSheet_withRep] 
	-- Add the parameters for the stored procedure here
	@fromDate date,
	@StructureDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   --select * from Shared.TimeDefinition where Year = 2019
declare @timeId int 
declare @year int 
declare @cyfactorDivider int , @date date 
select @cyfactorDivider = month(FromDate),@date = FromDate , @timeId = TimeDefinitionId, @year= [Year]
from Shared.TimeDefinition 
where @fromDate between FromDate and ToDate



truncate table Temp.Target_Sheet
--IF OBJECT_ID('tempdb..#baseTable') IS NOT NULL drop table #baseTable
--create table Temp.Target_Sheet (TeamId int, ProductId int, RootTerritoryId int, RootCategoryId int
--	, price decimal(18,6), area nvarchar(max), productName nvarchar(max)
--	, TeamName nvarchar(max), categoryName nvarchar(max), ManagerName nvarchar(max)
--	, Qty_Lastyear2	decimal(18,6), Amount_Lastyear2	decimal(18,6)
--	, Qty_Lastyear1	decimal(18,6), Amount_Lastyear1	decimal(18,6)
--	, Qty_Currentyear	decimal(18,6), Amount_Currentyear	decimal(18,6)
--	, Qty_Currentyear_estimate	decimal(18,6), Amount_Currentyear_estimate decimal(18,6)	
--	, Sales_Office	decimal(18,6), MarketShare	decimal(18,6)
--	, Line_Sales	decimal(18,6), Product_Sales	decimal(18,6)
--	)
insert into Temp.Target_Sheet(CountryId, TeamId, ProductId, RootTerritoryId, RootCategoryId)
select t.CountryId,tsp.TeamId, tsp.ProductId, terr.TerritoryId, act.RootCategoryId
from sales.TeamSalesProduct tsp
	join sales.Target t on tsp.ProductId = t.ProductId
		and t.TargetTypeId = 2
	join Shared.TimeDefinition td on Shared.fnCompareDates(tsp.FromDate, tsp.ToDate, td.FromDate, td.ToDate) = 1
		and td.TimeDefinitionId = @timeId
	cross join (
		select distinct TerritoryId
		from Shared.fn_GetLeafTerritories(1)
	) terr
	cross join (
		select distinct RootCategoryId 
		from Shared.fn_AccountCategory_GetParentLevelForLeaf(1,1)
	) act
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	/*base sales for all calculations*/
	truncate table Temp.Target_Sheet_Sales
	--IF OBJECT_ID('tempdb..#baseSales') IS NOT NULL drop table #baseSales
	--create table Temp.Target_Sheet_Sales (ProductId int, year int, Qty decimal(18,6), Amount decimal(18,6), countryId int, TerritoryId int, CategoryId int)
	insert into Temp.Target_Sheet_Sales
	select df.ProductId,year(SalesDate),sum(df.PackFactor * qty), sum(df.PackFactor * Amount), s.CountryId, t.TerritoryId
		,ac.RootCategoryId CategoryId
	from sales.InMarketSales s with (nolock)
		join shared.ProductDosageForm df on s.DosageFormId = df.DosageFormId
		join Shared.Account a on s.AccountId = a.AccountId
		join shared.Territory t on t.TerritoryId = a.TerritoryId
		join (select distinct CountryId, ProductId from sales.Target t where TargetTypeId = 2) targetp on targetp.CountryId = s.CountryId
			and targetp.ProductId = df.ProductId
		join [Shared].[fn_AccountCategory_GetParentLevelForLeaf](1, 1) ac on a.CategoryId = ac.CategoryId
		join Temp.Target_Sheet b on b.ProductId =df.ProductId
			and b.RootTerritoryId = t.TerritoryId
			and b.RootCategoryId = ac.RootCategoryId
	--where exists (select top 1 1 from Shared.fn_Security_GetProductsForEmployee(@employeeId,@date) sp where df.ProductId = sp.ProductId)
	group by df.ProductId,year(SalesDate), s.CountryId, t.TerritoryId,ac.RootCategoryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	
	/* territory sales percentage */
	update b set
	Sales_Office = terrSalesPercent
	from Temp.Target_Sheet b 
	join(
	select TerritoryId, sum(Qty)/tsales.total terrSalesPercent
	from Temp.Target_Sheet_Sales tes
		join (
			select year, countryId,sum(qty) total
			from Temp.Target_Sheet_Sales
			where year = @year
			group by year, countryId
		)tsales on tes.year = tsales.year and tes.countryId = tsales.countryId
	group by TerritoryId,tsales.total) ts 
		on b.RootTerritoryId = ts.TerritoryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	/*market share*/
	--update b set
	--MarketShare = ms.MarketShare
	--from Temp.Target_Sheet b 
	--	join (
	--	select terr.TerritoryId, CategoryId, sum(MarketShare) MarketShare
	--	from Shared.Territory terr 
	--	join sales.MarketShareTerritory mst on terr.TerritoryId = mst.TerritoryId
	--	join sales.MarketShare ms on mst.MarketShareId = ms.MarketShareId
	--		and ms.CountryId = 1 and ms.Year = @year
	--	group by terr.TerritoryId, CategoryId
	--	) ms on b.RootTerritoryId = ms.TerritoryId 
	--	and b.RootCategoryId = ms.CategoryId
	update b set
	MarketShare = ms.MarketShare
	from Temp.Target_Sheet b 
		join (
		select rt.TerritoryId, CategoryId, (MarketShare)/rtCount.countTerr MarketShare
		from
		sales.MarketShareTerritory mst
		join sales.MarketShare ms on mst.MarketShareId = ms.MarketShareId
			and ms.CountryId = 1 and ms.Year = @year
		join Shared.fn_Territory_GetParentLevelForLeaf(3,1) rt on mst.TerritoryId = rt.RootTerritoryId
			and rt.RootTerritoryId <> rt.TerritoryId
	join (select RootTerritoryId, count(distinct TerritoryId) countTerr 
		from shared.fn_Territory_GetParentLevelForLeaf(3,1) 
		where TerritoryId <> RootTerritoryId
		group by RootTerritoryId) rtCount on mst.TerritoryId = rtCount.RootTerritoryId
		) ms on b.RootTerritoryId = ms.TerritoryId 
		and b.RootCategoryId = ms.CategoryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	/*sales */
	update b set
	Qty_Currentyear = s.Qty
	, Qty_Currentyear_estimate = (s.Qty/@cyfactorDivider)*12
	,Amount_Currentyear = s.Amount
	,Amount_Currentyear_estimate = (s.Amount/@cyfactorDivider)*12
	,Qty_Lastyear1 = sy1.Qty
	,Amount_Lastyear1 = sy1.Amount
	,Qty_Lastyear2 = sy2.Qty
	,Amount_Lastyear2 = sy2.Amount
	from Temp.Target_Sheet b
		join Temp.Target_Sheet_Sales s on b.ProductId = s.ProductId and b.RootCategoryId = s.CategoryId
			and b.RootTerritoryId = s.TerritoryId and s.year = @year
		left join Temp.Target_Sheet_Sales sy1 on b.ProductId = sy1.ProductId and b.RootCategoryId = sy1.CategoryId
			and b.RootTerritoryId = sy1.TerritoryId and sy1.year = @year-1
		left join Temp.Target_Sheet_Sales sy2 on b.ProductId = sy2.ProductId and b.RootCategoryId = sy2.CategoryId
			and b.RootTerritoryId = sy2.TerritoryId and sy2.year = @year-2
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	/*line sales*/
	update b set
	Line_Sales = lsQty
	from Temp.Target_Sheet b 
	join (
		select b.TeamId, RootTerritoryId, sum(Qty_Currentyear)/ls.lsQty lsQty 
		from Temp.Target_Sheet b
			join (
			select TeamId, sum(Qty_Currentyear) lsQty 
			from Temp.Target_Sheet
			group by TeamId
			) ls on b.TeamId = ls.TeamId
		group by b.TeamId, RootTerritoryId, ls.lsQty
	)ls on b.TeamId = ls.TeamId and b.RootTerritoryId = ls.RootTerritoryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	/*product sales*/
	update b set
	Product_Sales = lsQty
	from Temp.Target_Sheet b 
	join (
		select b.TeamId,b.ProductId, b.RootTerritoryId, case when ls.lsQty <> 0 then sum(bs.Qty)/ls.lsQty else null end lsQty 
		from Temp.Target_Sheet b
			join Temp.Target_Sheet_Sales bs on b.ProductId = bs.ProductId and b.RootTerritoryId = bs.TerritoryId and bs.year = @year
			join (
				select TeamId,ts.ProductId, sum(tss.Qty) lsQty 
				from Temp.Target_Sheet ts
					join Temp.Target_Sheet_Sales tss on ts.ProductId = tss.ProductId and ts.RootTerritoryId = tss.TerritoryId
				where tss.year = @year
				group by TeamId ,ts.ProductId
			) ls on b.TeamId = ls.TeamId and ls.ProductId = b.ProductId  
		group by b.TeamId,b.ProductId, b.RootTerritoryId, ls.lsQty
	)ls on b.TeamId = ls.TeamId and b.ProductId = ls.ProductId 
	and b.RootTerritoryId = ls.RootTerritoryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	
	/* update master date [prod, area, price, categoryname]
	*/
	update b set
	productName = p.ProductName
	,area = TerritoryName
	,TeamName = t.TeamName
	,categoryName = ac.CategoryName
	,price = pp.Price
	from Temp.Target_Sheet b
		join Shared.Product p on b.ProductId = p.ProductId
		join Shared.Team t on b.TeamId = t.TeamId
		join Shared.AccountCategory ac on b.RootCategoryId = ac.CategoryId
		join Shared.Territory terr on b.RootTerritoryId = terr.TerritoryId
		join sales.ProductPrice pp on pp.ProductId = b.ProductId
			and pp.AccountId is null and pp.DosageFormId is null and pp.AccountCategoryId is null and pp.DistributorId is null and pp.TerritoryId is null
		join Shared.TimeDefinition td on td.TimeDefinitionId = @timeId
			and Shared.fnCompareDates(pp.FromDate, pp.ToDate, td.FromDate, td.ToDate) = 1
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	

	update b set
	ManagerName = tm.sup,
	[ProfileId] = tm.ProfileId,
	ProfileCode = tm.ProfileCode,
	RepName = tm.Rep
	from Temp.Target_Sheet b
	join 
	(
		select dat.TeamId, dat.TerritoryId, dat.ProfileId, dat.ProfileCode, ep.EmployeeId, ee.EmployeeName Rep, eh.ManagerId, e.EmployeeName sup
		from (
			select TeamId, tp.ProfileId, tp.ProfileCode, pt.TerritoryId
			from Shared.TeamProfile tp
				join sales.ProfileTerritory pt on pt.ProfileId = tp.ProfileId
			union all 
			select TeamId, tp.ProfileId, tp.ProfileCode, a.TerritoryId
			from Shared.TeamProfile tp
				join sales.ProfileAccount pa on pa.ProfileId = tp.ProfileId
				join Shared.Account a on pa.AccountId = a.AccountId
					and Shared.fnCompareDates(@StructureDate,@StructureDate,pa.FromDate, pa.ToDate) = 1
			)dat 
			left join Shared.EmployeeProfile ep on ep.ProfileId = dat.ProfileId
				and Shared.fnCompareDates(@StructureDate,@StructureDate,ep.FromDate, ep.ToDate) = 1
			left join hr.EmployeeHistory eh on eh.EmployeeId = ep.EmployeeId
				and Shared.fnCompareDates(@StructureDate,@StructureDate,eh.FromDate, eh.ToDate)=1
			left join hr.Employee e on e.EmployeeId = eh.ManagerId
			left join hr.Employee ee on ee.EmployeeId = eh.EmployeeId
	)tm on b.TeamId = tm.TeamId and b.RootTerritoryId = tm.TerritoryId
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))	

END