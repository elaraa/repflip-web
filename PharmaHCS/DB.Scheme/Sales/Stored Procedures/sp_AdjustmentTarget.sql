﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Sales].[sp_AdjustmentTarget]
@TimeId int,
@ProductId int,
@TerritoryId int = NULL,
@ProfileId int = NULL,
@TargetValue decimal(18,5),
@IsSales bit = 0
AS
BEGIN

declare @ValueQty as decimal(18,5) = @TargetValue

SET NOCOUNT ON
select * into #Target from(select * from [Sales].[Target] t 
where t.ProductId = @ProductId and t.TimeDefinitionId = @TimeId 
and (@TerritoryId is null or t.TerritoryId = @TerritoryId) and (@ProfileId is null or t.ProfileId = @ProfileId)) as T


SET NOCOUNT ON
insert into Sales.TargetHistory ([TargetTypeId],[Year],[TimeDefinitionId],[ProductId],[DosageFormId],[AccountId],[TerritoryId],[TeamId]
 ,[ProfileId],[EmployeeId],[Qty],[Amount],[Percentage],[CountryId],[CreationDate],[LastModifiedDate],[CreatedById],[LastModifiedById]
 ,[Version])

 select [TargetTypeId],[Year],[TimeDefinitionId],[ProductId],[DosageFormId],[AccountId],[TerritoryId],[TeamId],[ProfileId],[EmployeeId],
 [Qty],[Amount],[Percentage],[CountryId],[CreationDate],[LastModifiedDate],[CreatedById],[LastModifiedById] ,
 case when [Version] is null then 1 else [Version] end
 from  #Target

If @IsSales = 1
begin
select * into #Sales from (select * from [Staging].[InMarketSalesFact] 
where ProductId = @ProductId and TimeDefinitionId = @TimeId 
and (@TerritoryId is null or TerritoryId = @TerritoryId) and (@ProfileId is null or ProfileId = @ProfileId)) as S

declare @SalesQtys decimal(18,5) = (select Sum (Qty) from #Sales), --30
        @TargetQtys decimal(18,5) = (select Sum (Qty) from #Target), --40
        @SalesCount int = (select Count(*) from #Sales), --1000
		@TargetCount int = (select Count(*) from #Target) --200

if @TargetQtys > @SalesQtys and @SalesQtys <> 0
begin 
   set @ValueQty = @SalesQtys/@TargetCount
end

--SET NOCOUNT OFF
--update t set t.Qty = (case when s.Qty is not null and t.Qty > s.Qty then s.Qty else t.Qty end),
--t.Amount = (case when s.Qty is not null and s.Qty <> 0 and s.Amount is not null and t.Qty > s.Qty then (t.Amount/t.Qty)*s.Qty else t.Amount end), 
--t.[Version] = case when [Version] is null then 2 else [Version] + 1 end,LastModifiedDate = GETDATE()
--from [Sales].[Target] t left join
--[Staging].[InMarketSalesFact] s on t.ProductId = s.ProductId and t.TimeDefinitionId = s.TimeDefinitionId 
--  and (t.TerritoryId = s.TerritoryId) and (t.ProfileId = s.ProfileId)
--where t.ProductId = @ProductId and t.TimeDefinitionId = @TimeId 
--and (@TerritoryId is null or t.TerritoryId = @TerritoryId) and (@ProfileId is null or t.ProfileId = @ProfileId)
end

SET NOCOUNT OFF
update  [Sales].[Target] set Amount = case when Qty is not null and Qty <> 0 and  Amount is not null then (Amount/Qty) * @ValueQty end
, Qty = @ValueQty, [Version] = case when [Version] is null then 2 else [Version] + 1 end
,LastModifiedDate = GETDATE()
where ProductId = @ProductId and TimeDefinitionId = @TimeId 
and (@TerritoryId is null or TerritoryId = @TerritoryId) and (@ProfileId is null or ProfileId = @ProfileId)

END