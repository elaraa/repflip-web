﻿-- =============================================
-- Author:		Nader Ghattas
-- Create date: 13 Jan 2020
-- Description:	
-- =============================================
create PROCEDURE [Sales].[sp_UploadCeilingProduct] 
	-- Add the parameters for the stored procedure here
	@PeriodId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    delete from temp.uploadceilingproduct where DosageFormId is null and CustId is null and Line is null

	if exists (
	select top 1 1 from(
	select *
	from temp.uploadceilingproduct z
	except
	select z.*
	from temp.uploadceilingproduct z
		join Shared.TimeDefinition td on td.TimeDefinitionId = 61
		join Shared.ProductDosageForm d on z.DosageFormId = d.DosageFormId
		join sales.TeamSalesProduct tsp on d.ProductId = tsp.ProductId
			and z.Line = tsp.TeamId
			and Shared.fnCompareDates(td.FromDate,td.ToDate, tsp.FromDate, tsp.ToDate) = 1 ) dat

	) 
	begin 

		select 'Dosage '''+DosageFormName+''' does not belong to Team '''+Line+''' for period '+cast(@PeriodId as varchar(50)) 'Error'
		from(
			select *
			from temp.uploadceilingproduct z
			except
			select z.*
			from temp.uploadceilingproduct z
				join Shared.TimeDefinition td on td.TimeDefinitionId = 61
				join Shared.ProductDosageForm d on z.DosageFormId = d.DosageFormId
				join sales.TeamSalesProduct tsp on d.ProductId = tsp.ProductId
					and z.Line = tsp.TeamId
					and Shared.fnCompareDates(td.FromDate,td.ToDate, tsp.FromDate, tsp.ToDate) = 1 
			) dat

	end
	else
	begin
		---delete from upload where account with profile with percentage th		
		delete z
		from temp.uploadceilingproduct z
			join sales.CeilingDosageAccount a on z.DosageFormId = a.DosageFormId
				and (z.CustId = a.AccountId or (a.AccountId is null and z.CustId is null))
				and a.CeilingQty = cast(z.New_CeilingQty as decimal(18,6))
				and @PeriodId between a.FromTimeId and isnull(a.ToTimeId, 1000)
		---close records for the same account with different profile or different percentage
		update a set
		ToTimeId = @PeriodId-1,
		LastModifiedDate = getdate()
		from temp.uploadceilingproduct z
			join sales.CeilingDosageAccount a on z.DosageFormId = a.DosageFormId
				and (z.CustId = a.AccountId or (a.AccountId is null and z.CustId is null))
				and a.CeilingQty <> cast(z.New_CeilingQty as decimal(18,6))
				and @PeriodId between a.FromTimeId and isnull(a.ToTimeId, 1000)

		delete from sales.CeilingDosageAccount where ToTimeId < FromTimeId

		--insert data to profileaccount table
		insert into sales.CeilingDosageAccount(DosageFormId, AccountId, CeilingQty, FromTimeId, CreationDate)
		select DosageFormId, CustId, cast(z.New_CeilingQty as int),@PeriodId,getdate()
		from temp.uploadceilingproduct z
	end
END