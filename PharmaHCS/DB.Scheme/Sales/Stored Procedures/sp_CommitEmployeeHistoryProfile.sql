﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Sales].[sp_CommitEmployeeHistoryProfile]
@TableName  nvarchar(100),
@TimeId int,
@UserId  int
AS
BEGIN
set @TableName='[Temp].'+@tableName
declare @StartDate as date = (select top 1 FromDate from Shared.TimeDefinition where TimeDefinitionId = @TimeId)	
declare @Query as nvarchar(max), @SQL as nvarchar(max) , @SQLV as nvarchar(max), @SQLX as nvarchar(max)

set @Query = '
IF COL_LENGTH('''+ @TableName +''', ''Status'') IS NULL
BEGIN 
  ALTER TABLE '+ @TableName +' ADD Status bit NOT NULL DEFAULT(1)
END

IF COL_LENGTH('''+ @TableName +''',''InvalidDepartment'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidDepartment nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''InvalidJobTitle'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidJobTitle nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''InvalidTeam'') IS NULL
BEGIN
  ALTER TABLE  '+ @TableName +' ADD InvalidTeam nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''RequiredTeamForProfile'') IS NULL
BEGIN
  ALTER TABLE  '+ @TableName +' ADD RequiredTeamForProfile nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''InvalidManagerCode'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD InvalidManagerCode nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''IsDuplicated'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsDuplicated nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''IsOverlappedProfile'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsOverlappedProfile nvarchar(max) NOT NULL DEFAULT(''False'')
END

IF COL_LENGTH('''+ @TableName +''',''IsOverlappedHistory'') IS NULL
BEGIN
  ALTER TABLE '+ @TableName +' ADD IsOverlappedHistory nvarchar(max) NOT NULL DEFAULT(''False'')
END
'
set @SQL = '
UPDATE t set t.InvalidDepartment = (case when d.DepartmentId is not null then ''False'' else ''True'' end)
, Status = (case when d.DepartmentId  is not null and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
HR.Department d on t.Department  = d.DepartmentCode

UPDATE t set t.InvalidJobTitle = (case when j.JobTitleId is not null then ''False'' else ''True'' end)
, Status = (case when j.JobTitleId  is not null and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
HR.JobTitle j on t.[Job Title]  = j.JobTitledCode


UPDATE t set t.InvalidTeam = (case when te.TeamId is not null or t.Team is null then ''False'' else ''True'' end)
, Status = (case when (te.TeamId is not null  or t.Team is null) and Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
Shared.Team te on t.[Team]  = te.TeamCode

UPDATE t set t.RequiredTeamForProfile = (case when  p.ProfileId is null and t.Team is null then ''True'' else ''False'' end)
, Status = (case when (p.ProfileId is null and t.Team is null) then 0 else 1 end) from '+ @TableName +' t left join 
Shared.TeamProfile p on p.ProfileCode  = t.[Profile Code]


UPDATE t set t.InvalidManagerCode = (case when (e.EmployeeId is not null or  tn.[Employee Code] is not null or t.[Manager Code] is null) then ''False'' else ''True'' end)
, t.Status = (case when (e.EmployeeId is not null  or  tn.[Employee Code] is not null or t.[Manager Code] is null) and t.Status = 1  then 1 else 0 end) from '+ @TableName +' t left join 
HR.Employee e on t.[Manager Code] = e.EmployeeCode left join
'+ @TableName +' tn on tn.[Employee Code] = t.[Manager Code]

select * into #DuplicatedRows from (
select t.AutoID from '+ @TableName +' t join 
'+ @TableName +' tn on t.[Employee Name] = tn.[Employee Name] and t.[Employee Code] = tn.[Employee Code] and
t.[Job Title] = tn.[Job Title] and isnull(t.[Team], ''`'') = isnull(tn.[Team], ''`'') and isnull(t.[Profile Code], ''`'') = isnull(tn.[Profile Code], ''`'') and
isnull(t.[Manager Code], ''`'') = isnull(tn.[Manager Code], ''`'')  and isnull(t.[Department], ''`'') = isnull(tn.[Department], ''`'')
where t.AutoID <> tn.AutoID
) as DuplicatedRows
update '+ @TableName +' set IsDuplicated = ''True'' where AutoID in (select * from #DuplicatedRows)'

set @SQLV=
'
select * into #OverlappedEmp from (
select t.AutoID
from '+ @TableName +'  t join
 HR.Employee e on e.EmployeeCode = t.[Employee Code] left join
 HR.Employee m on m.EmployeeCode = t.[Manager Code] left join 
 Shared.Team team on team.TeamCode = t.Team  join
 HR.JobTitle j on j.JobTitledCode = t.[Job Title]  join
 HR.Department d on d.DepartmentCode = t.Department join
 HR.EmployeeHistory h on h.EmployeeId = e.EmployeeId and h.JobTitleId = j.JobTitleId and h.DepartmentId = d.DepartmentId
 where t.Status = 1 and (m.EmployeeId is null or m.EmployeeId = h.ManagerId) and (team.TeamId is null or team.TeamId = h.TeamId)
 and  [Shared].[fnCompareDates](''' + cast(@StartDate as nvarchar(100)) + ''', ''' + cast(@StartDate as nvarchar(100)) + ''', h.FromDate, h.ToDate) = 1) as OverlappedEmp
update '+ @TableName +' set IsOverlappedHistory = ''True'' where AutoID in (select AutoID from #OverlappedEmp)

select * into #OverlappedProfile from (
select t.AutoID
from '+ @TableName +'  t join
 HR.Employee e on e.EmployeeCode = t.[Employee Code] join
 Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] join
 Shared.EmployeeProfile ep on ep.EmployeeId = e.EmployeeId and ep.ProfileId = p.ProfileId 
 where  t.Status = 1 and [Shared].[fnCompareDates](''' + cast(@StartDate as nvarchar(100)) + ''', ''' + cast(@StartDate as nvarchar(100)) + ''', ep.FromDate, ep.ToDate) = 1) as Overlappedprofile
 update '+ @TableName +' set IsOverlappedProfile = ''True'' where AutoID in (select AutoID from #OverlappedProfile)

select * into #InvalidData from (select AutoID as RowIdex, [Employee Name] as EmployeeName, [Employee Code] as EmployeeCode, 
[Manager Code] as ManagerCode, Department, Team, [Job Title] as JobTitle, [Can Visit] as CanVisit, [Profile Code] as ProfileCode,
InvalidDepartment, InvalidJobTitle, InvalidTeam, InvalidManagerCode, IsDuplicated, IsOverlappedHistory, IsOverlappedProfile,
RequiredTeamForProfile
from '+ @TableName +' where
Status = 0 or IsDuplicated = ''True'' or IsOverlappedHistory = ''True'' or RequiredTeamForProfile = ''True'' or IsOverlappedProfile = ''True'') as InvalidData  
   '
set @SQLX=   
   '
   If (Select Count(*) from #InvalidData) = 0
 begin
   Insert into Shared.TeamProfile ([TeamId],[ProfileCode],[ProfileName],[Active],[CreationDate],[LastModifiedDate],[CreatedById]
      ,[LastModifiedById],[AvgCallRate],[ListTargetDoctorsCount])
   select TeamId, t.[Profile Code], t.[Profile Code], 1, GETDATE(), NULL, '+cast(@UserId as varchar(50))+', NULL , NULL , NULL 
   from  '+ @TableName +' t join
      Shared.Team on TeamCode = t.Team
	  where  t.[Profile Code]  not in (select p.ProfileCode from 
	  Shared.TeamProfile  p)
   
   Insert into [HR].Employee([EmployeeName],[EmployeeCode],[NationalIDNumber],[BirthDate],[MaritalStatus]
      ,[Gender],[HireDate],[LoginId],[Password],[Picture],[RoleId],[JobTitleId],[DepartmentId],[Email],[Mobile],[Active],[CountryId]
      ,[CreationDate],[LastModifiedDate],[CreatedById],[LastModifiedById]) 
   select t.[Employee Name], t.[Employee Code], t.[NationalID], NULL, ''M'',NULL,''' + cast(@StartDate as nvarchar(50)) + ''',NULL,NULL,NULL,NULL, j.JobTitleId, d.DepartmentId,
   NUll, NULL,1,1, GETDATE(),NULL, '+cast(@UserId as varchar(50))+', NULL  from '+ @TableName +' t left join
   [HR].Employee e on e.EmployeeCode =  t.[Employee Code] join
   Hr.JobTitle j on j.JobTitledCode = t.[Job Title] left join
   Hr.Department d on d.DepartmentCode = t.Department
   where e.EmployeeId is null

   update h set ToDate = DATEADD(day, -1, ' + cast(@StartDate as nvarchar(50)) + ') from '+ @TableName +' t join
   Hr.Employee e on e.EmployeeCode = t.[Employee Code] join
   Hr.EmployeeHistory h on h.EmployeeId = e.EmployeeId
   where h.ToDate is null and h.FromDate <= DATEADD(day, -1, ' + cast(@StartDate as nvarchar(50)) + ')

   Insert into [Hr].[EmployeeHistory]([EmployeeId],[JobTitleId],[DepartmentId],[ManagerId],[FromDate],[ToDate],[CountryId],[TeamId]
      ,[CanVisit],[CreationDate],[LastModifiedDate],[CreatedById],[LastModifiedById])
   select e.EmployeeId, j.JobTitleId,  d.DepartmentId, m.EmployeeId, ''' + cast(@StartDate as nvarchar(50)) + ''', NULL, 1, team.TeamId,
   case when t.[Can Visit] = ''True'' then 1 else 0 end, GETDATE(),
   NULL, '+cast(@UserId as varchar(50))+', NULL from '+ @TableName +'  t join
   HR.Employee e on e.EmployeeCode = t.[Employee Code] left join
   HR.Employee m on m.EmployeeCode = t.[Manager Code] left join 
   Shared.Team team on team.TeamCode = t.Team  join
   HR.JobTitle j on j.JobTitledCode = t.[Job Title]  join
   HR.Department d on d.DepartmentCode = t.Department
   Where m.EmployeeId is null or m.EmployeeName not like ''%_OLD%''

   Update ep set ToDate = DATEADD(day, -1, ' + cast(@StartDate as nvarchar(50)) + ')  from '+ @TableName +'  t join
   HR.Employee e on e.EmployeeCode = t.[Employee Code] join
   Shared.TeamProfile p on p.ProfileCode = t.[Profile Code] join
   Shared.EmployeeProfile ep on ep.EmployeeId = e.EmployeeId and ep.ProfileId = p.ProfileId 
   Where ep.ToDate is null and ep.FromDate <= DATEADD(day, -1, ' + cast(@StartDate as nvarchar(50)) + ')

   Insert into Shared.EmployeeProfile ([EmployeeId],[ProfileId],[FromDate],[ToDate],[CreationDate],[LastModifiedDate],[CreatedById],[LastModifiedById])
   select e.EmployeeId, p.ProfileId, ''' + cast(@StartDate as nvarchar(50)) + ''', NULL, GETDATE(), NULL, '+cast(@UserId as varchar(50))+', NULL from '+ @TableName +'  t join
   HR.Employee e on e.EmployeeCode = t.[Employee Code] join
   Shared.TeamProfile p on p.ProfileCode = t.[Profile Code]
 end
 
drop table '+ @TableName +'
select * from #InvalidData
'
exec sp_executesql @Query
exec sp_executesql @Sql
EXEC (@SQLV+@SQLX)

 
END