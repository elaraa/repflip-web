﻿CREATE PROCEDURE [Sales].[sp_Target_TerritoryAnalysis]
	-- Add the parameters for the stored procedure here
	@countryId int, 
	@year int,
	@userId int,
	@teamId int = null,
	@productId int = null,
	@territoryId int = null,
	@employeeId	int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#profiles') IS NOT NULL drop table #profiles
	Create table #profiles (EmployeeId int, ProfileId int)
	insert into #profiles
	select EmployeeId, ProfileId from Shared.fn_Security_GetProfilesForEmployee(@userId,getdate())
	--OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE')) 


    select t.CountryId, t.Year,terr.TerritoryName, t.Qty TerritoryTarget_Qty, t.Amount TerritoryTarget
		, case when t1.Qty <> 0 then ((t.Qty-t1.Qty)/t1.Qty) else 1 end TerritoryTargetGrowth_Qty
		, case when (t1.Amount) <> 0 then ((t.Amount-t1.Amount)/(t1.Amount)) else 1 end TerritoryTargetGrowth
	from (
		select t.TerritoryId,Year,CountryId, sum(Qty) Qty, sum(Amount) Amount
		from sales.Target t with (nolock)
			join #profiles sp on sp.ProfileId=t.ProfileId
				and (@employeeId is null or sp.EmployeeId = @employeeId)
		where t.TargetTypeId = 6
			and t.Year =@year
			and t.CountryId = @countryId
		group by t.TerritoryId,Year,CountryId
		) t
		join Shared.Territory terr on t.TerritoryId = terr.TerritoryId
		left join (
		select t1.TerritoryId, sum(Qty) Qty, sum(Amount) Amount
		from sales.Target t1 with (nolock) 
		where t1.TargetTypeId = 6
			and t1.Year =@year-1 
			and t1.CountryId = @countryId
		group by t1.TerritoryId
		)t1 on t.TerritoryId = t1.TerritoryId		
	OPTION(USE HINT('ENABLE_PARALLEL_PLAN_PREFERENCE'))
END