﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 20Sep19
-- Description:	
-- =============================================
CREATE PROCEDURE [Sales].[sp_SalesCrunchByEmpByProfile] 
	-- Add the parameters for the stored procedure here
	@fromDate date = null, 
	@toDate date = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    /*inmarket sales by employee/profile*/
/*
employee sales share

1. employee/profile/account share * sales => inmarketsalesbyEmployee
2. employee/profile/territory share * account/territory * (sales - inmarketsalesbyEmployee) => inmarketsalesbyEmployee
*/
-- .1.
truncate table temp.EmployeeProfileAccountProductShare
insert into temp.EmployeeProfileAccountProductShare
select Shared.fnGetMaxDate(Shared.fnGetMaxDate(pa.FromDate , ep.FromDate),tp.FromDate) FromDate  
,Shared.fnGetMinDate(shared.fnGetMinDate(pa.ToDate, ep.ToDate),tp.ToDate) ToDate  
,d.DosageFormId
,pa.AccountId
,pa.Perccentage
,ep.ProfileId
,ep.EmployeeId
,p.TeamId
from sales.ProfileAccount pa
	join Shared.EmployeeProfile ep on pa.ProfileId = ep.ProfileId 
		and shared.fnCompareDates(pa.FromDate, pa.ToDate, ep.FromDate, ep.ToDate) = 1 
	join Shared.TeamProfile p on ep.ProfileId = p.ProfileId
	join Sales.TeamSalesProduct tp on tp.TeamId = p.TeamId
		and shared.fnCompareDates(Shared.fnGetMaxDate(pa.FromDate , ep.FromDate), shared.fnGetMinDate(pa.ToDate, ep.ToDate), tp.FromDate, tp.ToDate) = 1 
	join Shared.ProductDosageForm d on
		/*(pa.ProductId is not null and d.ProductId = pa.ProductId and (d.DosageFormId = pa.DosageFromId or pa.DosageFromId is null) )
		or*/ (pa.ProductId is null and tp.ProductId = d.ProductId)

truncate table Sales.InMarketSalesByEmployeeByProfile
insert into Sales.InMarketSalesByEmployeeByProfile
select es.TeamId
,es.EmployeeId
,es.ProfileId
,es.Perccentage
,es.AccountId
,es.DosageFormId
,s.CountryId
,s.DistributorId
,s.DistributorBranchId
,s.FileId
,s.InvoiceNumber
,df.PackFactor * s.Qty * es.Perccentage Qty
,df.PackFactor * s.Amount * es.Perccentage FileAmount
,s.SalesDate
,s.TimeDefinitionId
--into Sales.InMarketSalesByEmployeeByProfile
from sales.InMarketSales s with (nolock)
	join Shared.ProductDosageForm df on s.DosageFormId = s.DosageFormId
	join Shared.TimeDefinition td on s.TimeDefinitionId = td.TimeDefinitionId
	join Temp.EmployeeProfileAccountProductShare es on s.AccountId = es.AccountId
		and s.DosageFormId = es.DosageFormId
		and (s.SalesDate between es.FromDate and es.ToDate or s.SalesDate is null)
		and (shared.fnCompareDates(td.FromDate, td.ToDate, es.FromDate, es.ToDate) = 1)
-- .2.
truncate table temp.EmployeeProfileTerritoryProductShare
insert into temp.EmployeeProfileTerritoryProductShare
select Shared.fnGetMaxDate(ep.FromDate,tp.FromDate) FromDate  
,Shared.fnGetMinDate(ep.ToDate,tp.ToDate) ToDate  
,d.DosageFormId
,pt.TerritoryId
,a.AccountId
,pt.[Percentage]
,ep.ProfileId
,ep.EmployeeId
,p.TeamId
--into temp.EmployeeProfileTerritoryProductShare
from sales.ProfileTerritory pt
	join Shared.EmployeeProfile ep on pt.ProfileId = ep.ProfileId
	join Shared.TeamProfile p on ep.ProfileId = p.ProfileId
	join Sales.TeamSalesProduct tp on tp.TeamId = p.TeamId
		and shared.fnCompareDates(ep.FromDate, ep.ToDate, tp.FromDate, tp.ToDate) = 1 
	join Shared.ProductDosageForm d on tp.ProductId = d.ProductId	
	join shared.Account a with (nolock) on pt.TerritoryId = a.TerritoryId
	join Shared.AccountType act on a.AccountTypeId = act.AccountTypeId and act.IncludeInSales = 1
	
insert into Sales.InMarketSalesByEmployeeByProfile
select es.TeamId
,es.EmployeeId
,es.ProfileId
,es.[Percentage]
,es.AccountId
,es.DosageFormId
,s.CountryId
,s.DistributorId
,s.DistributorBranchId
,s.FileId
,s.InvoiceNumber
,s.Qty * es.[Percentage] Qty
,s.Amount * es.[Percentage] FileAmount
,s.SalesDate
,s.TimeDefinitionId
from (
	select sales.CountryId, sales.DistributorId, sales.DistributorBranchId, sales.InvoiceNumber, sales.FileId, sales.DosageFormId, sales.AccountId, sales.SalesDate, sales.TimeDefinitionId, sales.Qty - isnull(empaccountshare.Qty,0) Qty, sales.Amount - isnull(empaccountshare.Amount,0) Amount
	from (
			select s.CountryId, DistributorId, DistributorBranchId, InvoiceNumber, FileId, s.DosageFormId, AccountId, SalesDate, TimeDefinitionId, sum(Qty*df.PackFactor) Qty, sum(Amount*df.PackFactor) Amount
			from sales.InMarketSales s with (nolock)
				join Shared.ProductDosageForm df on s.DosageFormId = s.DosageFormId
			group by s.CountryId, DistributorId, DistributorBranchId, InvoiceNumber, FileId, s.DosageFormId, AccountId, SalesDate, TimeDefinitionId
		)sales
		left join (
			select CountryId, DistributorId, DistributorBranchId, InvoiceNumber, FileId, DosageFormId, AccountId, SalesDate, TimeDefinitionId, sum(Qty) Qty, sum(FileAmount) Amount
			from sales.InMarketSalesByEmployeeByProfile with (nolock)
			group by CountryId, DistributorId, DistributorBranchId, InvoiceNumber, FileId, DosageFormId, AccountId, SalesDate, TimeDefinitionId
		)empaccountshare on sales.CountryId = empaccountshare.CountryId
			and sales.DistributorId = empaccountshare.DistributorId
			and sales.DistributorBranchId = empaccountshare.DistributorBranchId
			and sales.InvoiceNumber = empaccountshare.InvoiceNumber
			and sales.FileId = empaccountshare.FileId
			and sales.DosageFormId = empaccountshare.DosageFormId
			and sales.AccountId = empaccountshare.AccountId
			and sales.SalesDate = empaccountshare.SalesDate
			and sales.TimeDefinitionId = empaccountshare.TimeDefinitionId 
	)s
	join Shared.TimeDefinition td on s.TimeDefinitionId = td.TimeDefinitionId
	join temp.EmployeeProfileTerritoryProductShare es on s.AccountId = es.AccountId
		and s.DosageFormId = es.DosageFormId
		and (s.SalesDate between es.FromDate and es.ToDate or s.SalesDate is null)
		and (shared.fnCompareDates(td.FromDate, td.ToDate, es.FromDate, es.ToDate) = 1)
END