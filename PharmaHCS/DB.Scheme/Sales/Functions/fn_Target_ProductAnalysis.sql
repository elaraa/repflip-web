﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 3 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_Target_ProductAnalysis] 
(	
	-- Add the parameters for the function here
	@countryId int, 
	@year int,
	@userId int,
	@teamId int = null,
	@productId int = null,
	@territoryId int = null,
	@employeeId	int = null
)
RETURNS TABLE 
AS
RETURN 
(
	select t.CountryId,t.Year, ProductName,t.Qty ProductTarget_Qty, t.Amount ProductTarget
		, case when t1.Qty <> 0 then ((t.Qty-t1.Qty)/t1.Qty) else 1 end ProductTargetGrowth_Qty
		, case when t1.Amount <> 0 then ((t.Amount-t1.Amount)/t1.Amount) else 1 end ProductTargetGrowth
	from sales.Target t with (nolock)
		join Shared.Product p on t.ProductId = p.ProductId
		left join sales.Target t1 with (nolock) on t.TargetTypeId = t1.TargetTypeId and t.Year = t1.Year+1 and t.ProductId = t1.ProductId
		join Shared.fn_Security_GetProductsForEmployee(@userId,getdate()) sp on sp.ProductId=t.ProductId
			and (@teamId is null or @teamId = sp.TeamId)
			and (@productId is null or @productId = sp.ProductId)
	where t.TargetTypeId = 2 /*product*/
		and t.Year = @year
		and t.CountryId = @countryId
		
)