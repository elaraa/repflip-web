﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 29 Sep 19
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_Target_GetProfileTerritoryProduct] 
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date,
	@terrlevel int = 3
)
RETURNS TABLE 
AS
RETURN 
(
	select tp.ProfileId, tsp.ProductId, p.ProductName, pt.TerritoryId, t.RootTerritoryId,t.RootTerritoryName
	from Shared.TeamProfile tp
		left join sales.TeamSalesProduct tsp on tp.TeamId = tsp.TeamId
			and Shared.fnCompareDates(@date, @date, tsp.FromDate, tsp.ToDate) = 1
		left join Shared.Product p on p.ProductId = tsp.ProductId
		--left join Shared.ProductDosageForm df on df.ProductId = df.DosageFormId
		left join (
			select distinct ProfileId, TerritoryId
			from sales.ProfileTerritory pa
			where  Shared.fnCompareDates(@date, @date, pa.FromDate, pa.ToDate) = 1
		)pt on tp.ProfileId = pt.ProfileId
		join shared.fn_Territory_GetParentLevelForLeaf(@terrlevel,1) t on pt.TerritoryId = t.TerritoryId
		join Shared.fn_Security_GetProfilesForEmployee(@employeeId, @date) se  on se.ProfileId = tp.ProfileId
	--where exists(select 1 from Shared.fn_Security_GetProfilesForEmployee(@employeeId, @date) se where se.ProfileId = tp.ProfileId)
)