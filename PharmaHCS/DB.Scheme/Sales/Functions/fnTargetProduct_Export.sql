﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 21Sep19
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fnTargetProduct_Export] 
(	
	-- Add the parameters for the function here
	@exportDate date,
	@countryId int
)
RETURNS TABLE 
AS
RETURN 
(
	select tpy.CountryId, tpy.TeamName Line, tpy.GroupName [Group], tpy.ProductId,tpy.ProductName Product
	, t.Amount/t.Qty 'Price'
	, t.Qty LastYearQty, t.Amount LastYearAmount
	,tc.Qty CurrentYearQty, tc.Amount CurrentYearAmount
	from (
	select distinct tm.CountryId, tm.TeamId, tm.TeamName,g.GroupName,tp.ProductId, p.ProductName, tp.FromDate, tp.ToDate, td.Year
	from Shared.Team tm
		join sales.TeamSalesProduct tp on tm.TeamId = tp.TeamId
		join Shared.Product p on tp.ProductId = p.ProductId and tm.CountryId = p.CountryId
		left join Shared.ProductGroup g on p.GroupId = g.GroupId
		join Shared.TimeDefinition td on 
			Shared.fnCompareDates(tp.FromDate, tp.ToDate, td.FromDate, td.ToDate) = 1
			and Shared.fnCompareDates(@exportDate, @exportDate, tp.FromDate, tp.ToDate) = 1
	where tm.CountryId = @countryId
		and [Year] = year(@exportDate)
		) tpy
		left join sales.Target t on tpy.Year = t.Year+1 and tpy.CountryId = t.CountryId
			and tpy.ProductId = t.ProductId and t.TargetTypeId = 2
		left join sales.Target tc on tpy.Year = tc.Year and tpy.CountryId = tc.CountryId
			and tpy.ProductId = tc.ProductId and tc.TargetTypeId = 2
	where tpy.CountryId = @countryId
)