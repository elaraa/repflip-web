﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: Aug-2019
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_GetImportedFileMappedProducts] 
(	
	-- Add the parameters for the function here
	@fileId int
)
RETURNS TABLE 
AS
RETURN 
(
	select max(r.FileRecordId) FileRecordId,r.FileId,r.DosageFormCode DistributorDFCode, r.DosageFormName DistributorDFName, r.DosageFormId, df.DosageFormCode+'|'+ df.DosageFormName DosageFormName, p.ProductCode+'|'+p.ProductName ProductName
		, max(ISNULL(r.LastModifiedDate, r.CreationDate)) LastUpdate
		, max(ISNULL(r.LastModifiedById, r.CreatedById)) UpdatedBy
		, max(u.EmployeeName) UpdatedByLogonName
	from sales.ImportedFileRecord r
		join Shared.ProductDosageForm df on df.DosageFormId = r.DosageFormId
		join Shared.Product p on df.ProductId = p.ProductId
		LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(r.LastModifiedById, r.CreatedById)
	where (ProductMapped = 1 or r.DosageFormId is not null)
		and r.FileId = @fileId
	group by r.FileId,r.DosageFormCode, r.DosageFormName, r.DosageFormId, df.DosageFormCode+'|'+ df.DosageFormName, p.ProductCode+'|'+p.ProductName
)