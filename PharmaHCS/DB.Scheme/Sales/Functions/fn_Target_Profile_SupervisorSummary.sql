﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 13 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION Sales.fn_Target_Profile_SupervisorSummary
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@year int,
	@countryId	int
)
RETURNS TABLE 
AS
RETURN 
(
	select pt.TeamId,pt.TeamName,ss.SupId, ss.SupName, sum(pt.Qty) Qty, sum(pt.Amount) Amount, pt_past.LY_Qty, pt_past.LY_Amount
	from (
		select tp.TeamId, tm.TeamName,t.ProfileId, t.TimeDefinitionId, sum(Qty) Qty, Sum(Amount) Amount
		from sales.Target t
			join Shared.TeamProfile tp on t.ProfileId = tp.ProfileId
			join Shared.Team tm on tm.TeamId = tp.TeamId
		where TargetTypeId = 6 and Year = @year and t.CountryId = @countryId
		group by tp.TeamId, tm.TeamName,t.ProfileId, t.TimeDefinitionId
	) pt
	left join (
		select tp.TeamId,t.ProfileId, t.TimeDefinitionId, sum(Qty) LY_Qty, Sum(Amount) LY_Amount
		from sales.Target t
			join Shared.TeamProfile tp on t.ProfileId = tp.ProfileId
		where TargetTypeId = 6 and Year = @year -1 and CountryId = @countryId
		group by tp.TeamId,t.ProfileId, t.TimeDefinitionId
	) pt_past on pt.ProfileId = pt_past.ProfileId and pt.TimeDefinitionId = pt_past.TimeDefinitionId - 12
	join  (
		select distinct e.EmployeeId SupId,e.EmployeeName SupName, ep.ProfileId, td.TimeDefinitionId
		from Shared.EmployeeProfile ep 
			join Shared.TimeDefinition td on Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1
			join hr.EmployeeHistory eh on ep.EmployeeId = eh.EmployeeId
				and Shared.fnCompareDates(td.FromDate, td.ToDate, eh.FromDate, eh.ToDate) = 1
			left join hr.Employee e on eh.ManagerId = e.EmployeeId
		where td.Year = @year and td.CountryId = @countryId
	) ss on pt.ProfileId = ss.ProfileId
		and pt.TimeDefinitionId = ss.TimeDefinitionId
	join hr.fn_Security_GetEmployeesForEmployee(@employeeId,cast(@year as varchar(10))+'-12-31') se on ss.SupId = se.EmployeeId
	group by pt.TeamId,pt.TeamName,ss.SupId, ss.SupName, pt_past.LY_Qty, pt_past.LY_Amount
)