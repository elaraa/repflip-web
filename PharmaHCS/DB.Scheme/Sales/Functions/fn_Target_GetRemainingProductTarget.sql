﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 29 Sep 19
-- Description:	
-- =============================================
CREATE FUNCTION sales.fn_Target_GetRemainingProductTarget 
(	
	-- Add the parameters for the function here
	@countryId int, 
	@employeeId int,
	@timedefinitionId int
)
RETURNS TABLE 
AS
RETURN 
(
	select t.ProductId,p.ProductName, t.Qty - isnull(pt.pQty,0) Qty
	from sales.Target t
		left join (
		select CountryId,Year,ProductId, sum(Qty) pQty
		from sales.Target tt
		where TargetTypeId = 6 and TimeDefinitionId < @timedefinitionId
		group by CountryId,Year,ProductId
		)pt on t.Year = pt.Year and t.ProductId = pt.ProductId and t.CountryId = pt.CountryId
		join Shared.Product p on t.ProductId = p.ProductId
		join Shared.fn_Security_GetProductsForEmployee(@employeeId, (select FromDate from shared.timedefinition where TimedefinitionId = @timedefinitionId)) sp
			on  sp.ProductId = p.ProductId
	where TargetTypeId = 2
		and t.CountryId = @countryId
)