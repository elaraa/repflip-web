﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: Aug-2019
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_GetImportedFileSummary] 
(	
	-- Add the parameters for the function here
	@fileId int
)
RETURNS TABLE 
AS
RETURN 
(
	select FileId, 'TotalRecords' Factor, TotalRecords 'Original' , null 'Current'
	from sales.ImportedFile	e
	where FileId = @fileId
	union all
	select e.FileId, 'Unmapped Accounts' Factor, TotalUnmappedAccounts 'Original' , count(distinct CustomerCode) 'Current'
	from sales.ImportedFile	e
		left join Sales.ImportedFileRecord r on e.FileId = r.FileId and CustomerMapped = 0
	where e.FileId = @fileId
	group by e.FileId, e.TotalUnmappedAccounts
	union all
	select e.FileId, 'Unmapped Products' Factor, TotalUnmappedProducts 'Original' , count(distinct DosageFormCode) 'Current'
	from sales.ImportedFile	e
		left join Sales.ImportedFileRecord r on e.FileId = r.FileId and ProductMapped = 0
	where e.FileId = @fileId
	group by e.FileId, e.TotalUnmappedProducts
	union all
	select e.FileId, 'Total Invalid Records' Factor, TotalInvalidEntries 'Original' , sum(cast(r.InvalidRow as int)) 'Current'
	from sales.ImportedFile	e
		left join Sales.ImportedFileRecord r on e.FileId = r.FileId
	where e.FileId = @fileId
	group by e.FileId, e.TotalInvalidEntries

)