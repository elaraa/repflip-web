﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: Aug-2019
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_GetImportedFileDetails] 
(	
	-- Add the parameters for the function here
	@fileId int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT  e.FileId
		, Sales.Distributor.DistributorName
		, e.StatusId
		, Sales.ImportedFileStatus.StatusName
		, Shared.TimeDefinition.PeriodName
		, case when lower(e.ImportMode) = 'o' then 'Overwrite' when lower(e.ImportMode) = 'a' then 'Append' else 'Overwrite Existing' end ImportMode
		, e.CountryId
		, c.CountryName
		, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
		--, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
		, u.EmployeeName UpdatedByLogonName
		, case when e.StatusId in (3,4,5,7) then DATEDIFF(hour, e.CreationDate, isnull(e.LastModifiedDate, getdate())) when e.StatusId = 6 then 0  else DATEDIFF(HOUR, e.CreationDate, getdate()) end UploadProcessingTime_Hr
	FROM  Sales.ImportedFile AS e 
			JOIN Sales.Distributor ON e.DistributorId = Sales.Distributor.DistributorId 
			JOIN Sales.ImportedFileStatus ON e.StatusId = Sales.ImportedFileStatus.StatusId 
			left join Shared.Country c on e.CountryId = c.CountryId
			LEFT OUTER JOIN  Shared.TimeDefinition ON e.TimeDefinitionId = Shared.TimeDefinition.TimeDefinitionId					 
			LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	where e.FileId = @fileId
)