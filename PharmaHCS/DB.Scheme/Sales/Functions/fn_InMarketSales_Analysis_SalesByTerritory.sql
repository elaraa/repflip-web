﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 6 Oct 19
-- Description:	
-- =============================================
CReate FUNCTION [Sales].[fn_InMarketSales_Analysis_SalesByTerritory] 
(	
	@countryId int, 
	@year int,
	@userId int,
	@teamId int,
	@productId int,
	@territoryId int,
	@employeeId	int
)
RETURNS TABLE 
AS
RETURN 
(	
	select t.TerritoryName, sum(Qty) Qty, sum(Amount) Amount
	from sales.InMarketSales s with (nolock)
		left join Shared.Account a on s.AccountId = a.AccountId
		join Shared.fn_Territory_GetParentLevelForLeaf(3,@countryId) pt on pt.TerritoryId = a.TerritoryId
		join Shared.Territory t on pt.RootTerritoryId = t.TerritoryId
	where year(SalesDate) = @year
		and s.CountryId = @countryId
	group by t.TerritoryName
)