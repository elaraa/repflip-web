﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: Aug-2019
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_GetImportedFileMappedAccounts] 
(	
	-- Add the parameters for the function here
	@fileId int
)
RETURNS TABLE 
AS
RETURN 
(
	select max(r.FileRecordId) FileRecordId,r.FileId,r.CustomerCode, r.CustomerName, r.CustomerAddress, r.CustomerTerritory,
		a.AccountId, isnull(a.AccountCode,'')+'|'+a.AccountName AccountName, act.AccountTypeName, ac.CategoryName, terr.TerritoryCode+'|'+terr.TerritoryName TerritoryName 
		, max(ISNULL(r.LastModifiedDate, r.CreationDate)) LastUpdate
		, max(ISNULL(r.LastModifiedById, r.CreatedById)) UpdatedBy
		, max(u.EmployeeName) UpdatedByLogonName
	from sales.ImportedFileRecord r
		join Shared.Account a on a.AccountId = r.AccountId
		join Shared.AccountType act on act.AccountTypeId = a.AccountTypeId
		join Shared.AccountCategory ac on a.CategoryId = ac.CategoryId
		join shared.Territory terr on a.TerritoryId = terr.TerritoryId
		LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(r.LastModifiedById, r.CreatedById)
	where (CustomerMapped = 1 or r.AccountId is not null)
		and r.FileId = @fileId
	group by r.FileId,r.CustomerCode, r.CustomerName, r.CustomerAddress, r.CustomerTerritory,
		a.AccountId, isnull(a.AccountCode,'')+'|'+a.AccountName, act.AccountTypeName, ac.CategoryName, terr.TerritoryCode+'|'+terr.TerritoryName
)