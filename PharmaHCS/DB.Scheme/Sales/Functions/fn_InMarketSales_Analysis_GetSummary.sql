﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 6 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_InMarketSales_Analysis_GetSummary] 
(	
	@countryId int, 
	@year int,
	@userId int,
	@teamId int,
	@productId int,
	@territoryId int,
	@employeeId	int
)
RETURNS TABLE 
AS
RETURN 
(
	select sum(s.Qty) Qty, sum(s.Amount) Amount, cast(100.0*((sum(s.Qty)-sl.Qty)/sl.Qty) as decimal(18,2)) Qty_Growth, cast(100.0*(sum(s.Amount)-sl.Amount)/sl.Amount as decimal(18,2)) Amount_Growth
		, sc.Qty Qty_Distributed, sc.Amount Amount_Distributed
		,  t.Qty_Ach, t.Amount_Ach
	from sales.InMarketSales s with (nolock)
		cross join (
				select sum(s.Qty) Qty, sum(s.Amount) Amount
				from sales.InMarketSales s with (nolock)	
					join Shared.TimeDefinition td on s.TimeDefinitionId = td.TimeDefinitionId
						and td.Year = @year-1
						and cast(@year-1 as varchar(6))+'-'+cast(Month(Getdate()) as varchar(5))+'-1' >= td.ToDate
				where year(SalesDate) = @year-1
				and s.CountryId = @countryId
		) sl
		cross join (
				select sum(Qty) Qty, sum(Amount) Amount
				from Staging.InMarketSalesByProfileAccount i with (nolock)
					join Shared.TimeDefinition td on i.TimeDefinitionId = td.TimeDefinitionId
						and td.Year = @year
				where i.CountryId = @countryId
				
		)sc
		cross join (
				select sum(s.qty)/sum(t.Qty) Qty_Ach, sum(s.amount)/sum(t.Amount) Amount_Ach
				from (
					select ProductId, sum(t.Qty) Qty,sum(t.Amount) Amount
					from sales.Target t with (nolock)
						join Shared.TimeDefinition td on t.TimeDefinitionId = td.TimeDefinitionId
							and td.Year = @year
							and cast(@year as varchar(6))+'-'+cast(Month(Getdate()) as varchar(5))+'-1' >= td.ToDate
					where TargetTypeId = 6
						and t.year = @year
						and t.CountryId = @countryId
					group by ProductId
					) t
					join (	
						select ProductId, sum(qty) Qty, Sum(Amount) Amount
						from sales.InMarketSales s with (nolock)
							join Shared.ProductDosageForm df on s.DosageFormId = df.DosageFormId
						where year(SalesDate) = @year
							and s.CountryId = @countryId
						group by ProductId
					) s on t.ProductId = s.ProductId
		) t
	where year(SalesDate) = @year
		--and exists (select top 1 1 from Shared.fn_Security_GetProductsForEmployee(@userId, getdate()) sp where  )
	group by sl.Qty, sl.Amount, sc.Qty, sc.Amount, t.Qty_Ach, t.Amount_Ach
)