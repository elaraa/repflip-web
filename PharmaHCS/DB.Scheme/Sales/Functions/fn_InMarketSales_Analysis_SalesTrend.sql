﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 6 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION [Sales].[fn_InMarketSales_Analysis_SalesTrend] 
(	
	@countryId int, 
	@year int,
	@userId int,
	@teamId int,
	@productId int,
	@territoryId int,
	@employeeId	int
)
RETURNS TABLE 
AS
RETURN 
(
	select left(DATENAME(Month,SalesDate),3) SalesMonthName ,MONTH(SalesDate) SalesMonth,  sum(s.Qty) SalesQty, sum(s.Amount) SalesAmount, Qty_t TargetQty, Amount_t TargetAmount
		,ts.tQty SalesQty_InTarget, ts.tAmount SalesAmount_InTarget
	from sales.InMarketSales s with (nolock)
		full outer join (
			select TimeDefinitionId, sum(Qty) Qty_t, SUM(Amount) Amount_t 
			from sales.Target t
			where TargetTypeId = 6
				and CountryId = @countryId and Year = @year
			group by TimeDefinitionId
		) t on t.TimeDefinitionId = s.TimeDefinitionId
		left join (
			select TimeDefinitionId, sum(Qty) tQty, Sum(Amount) tAmount
			from sales.InMarketSales s with (nolock)
				join Shared.ProductDosageForm df on s.DosageFormId = df.DosageFormId
				join (select distinct ProductId from sales.Target where TargetTypeId = 2 and year = @year and CountryId = @countryId) pt
					on df.ProductId = pt.ProductId
			group by TimeDefinitionId
		)ts on ts.TimeDefinitionId = s.TimeDefinitionId
	where year(SalesDate) = @year and s.CountryId = @countryId
	group by left(DATENAME(Month,SalesDate),3),MONTH(SalesDate) , Qty_t, Amount_t	,ts.tQty, ts.tAmount

)