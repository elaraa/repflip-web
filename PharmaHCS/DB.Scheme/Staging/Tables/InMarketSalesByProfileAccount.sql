﻿CREATE TABLE [Staging].[InMarketSalesByProfileAccount] (
    [CountryId]        INT              NOT NULL,
    [AccountId]        INT              NULL,
    [ProductId]        INT              NOT NULL,
    [DosageFormId]     INT              NOT NULL,
    [TimeDefinitionId] INT              NULL,
    [ProfileId]        INT              NOT NULL,
    [TeamId]           INT              NOT NULL,
    [SharePercentage]  DECIMAL (18, 6)  NOT NULL,
    [Qty]              DECIMAL (38, 10) NULL,
    [Amount]           DECIMAL (38, 10) NULL,
    [ShareType]        VARCHAR (50)     NOT NULL
);

