﻿CREATE TABLE [Staging].[InMarketSalesByDistributorIncludingCeiledQty] (
    [DistributorId]    INT             NOT NULL,
    [AccountId]        INT             NULL,
    [DosageFormId]     INT             NOT NULL,
    [CountryId]        INT             NOT NULL,
    [TimeDefinitionId] INT             NULL,
    [Qty]              DECIMAL (38, 6) NULL
);

