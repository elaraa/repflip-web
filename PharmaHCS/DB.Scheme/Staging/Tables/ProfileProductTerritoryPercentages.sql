﻿CREATE TABLE [Staging].[ProfileProductTerritoryPercentages] (
    [CountryId]                INT              NULL,
    [TeamId]                   INT              NULL,
    [ProfileId]                INT              NULL,
    [ProductId]                INT              NULL,
    [DosageFormId]             INT              NULL,
    [TerritoryId]              INT              NULL,
    [TimeDefinitionId]         INT              NULL,
    [TargetPercentage]         DECIMAL (38, 20) NULL,
    [SalesPercentage]          DECIMAL (38, 6)  NULL,
    [SalesTargetAvgPercentage] DECIMAL (38, 6)  NULL
);

