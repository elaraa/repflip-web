﻿CREATE VIEW CRM.vw_RepCovarageTypeCategoryAnalyzer
AS
SELECT        tp.ProfileId, tp.ProfileCode, ep.EmployeeId, e.EmployeeName, ac.CategoryId, ac.CategoryName, act.AccountTypeId, act.AccountTypeName, td.TimeDefinitionId, td.PeriodName, COUNT(DISTINCT ptvd.AccountId) 
                         AS VisitsAccountsTarget, COUNT(DISTINCT v.AccountId) AS VisitsAccounts
FROM            Shared.TeamProfile AS tp INNER JOIN
                         Shared.EmployeeProfile AS ep ON ep.ProfileId = tp.ProfileId INNER JOIN
                         HR.Employee AS e ON e.EmployeeId = ep.EmployeeId INNER JOIN
                         Shared.TimeDefinition AS td ON Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1 LEFT OUTER JOIN
                         CRM.ProfileTargetVisit AS ptv ON ptv.ProfileId = tp.ProfileId AND td.TimeDefinitionId >= ptv.FromTimeDefinitionId AND (ptv.ToTimeDefinitionId IS NULL OR
                         td.TimeDefinitionId <= ptv.ToTimeDefinitionId) LEFT OUTER JOIN
                         CRM.ProfileTargetVisitDetail AS ptvd ON ptvd.ProfileTargetId = ptv.ProfileTargetId LEFT OUTER JOIN
                         Shared.Account AS a ON a.AccountId = ptvd.AccountId LEFT OUTER JOIN
                         Shared.AccountCategory AS ac ON ac.CategoryId = a.CategoryId LEFT OUTER JOIN
                         Shared.AccountType AS act ON act.AccountTypeId = a.AccountTypeId LEFT OUTER JOIN
                         CRM.Visit AS v ON v.EmployeeId = e.EmployeeId AND v.VisitDate BETWEEN td.FromDate AND td.ToDate AND v.IsFalseVisit = 0 LEFT OUTER JOIN
                         Shared.Account AS va ON va.AccountId = v.AccountId LEFT OUTER JOIN
                         Shared.AccountCategory AS vac ON vac.CategoryId = va.CategoryId AND vac.CategoryId = ac.CategoryId LEFT OUTER JOIN
                         Shared.AccountType AS vact ON vact.AccountTypeId = va.AccountTypeId AND vact.AccountTypeId = act.AccountTypeId
GROUP BY tp.ProfileId, tp.ProfileCode, ep.EmployeeId, e.EmployeeName, ac.CategoryId, ac.CategoryName, act.AccountTypeId, act.AccountTypeName, td.TimeDefinitionId, td.PeriodName
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_RepCovarageTypeCategoryAnalyzer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' End
         Begin Table = "ac"
            Begin Extent = 
               Top = 930
               Left = 38
               Bottom = 1060
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "act"
            Begin Extent = 
               Top = 1062
               Left = 38
               Bottom = 1192
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v"
            Begin Extent = 
               Top = 1194
               Left = 38
               Bottom = 1324
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "va"
            Begin Extent = 
               Top = 1326
               Left = 38
               Bottom = 1456
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vac"
            Begin Extent = 
               Top = 1458
               Left = 38
               Bottom = 1588
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vact"
            Begin Extent = 
               Top = 1590
               Left = 38
               Bottom = 1720
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_RepCovarageTypeCategoryAnalyzer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tp"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ep"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 239
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "td"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ptv"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ptvd"
            Begin Extent = 
               Top = 666
               Left = 38
               Bottom = 796
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "a"
            Begin Extent = 
               Top = 798
               Left = 38
               Bottom = 928
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
        ', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_RepCovarageTypeCategoryAnalyzer';

