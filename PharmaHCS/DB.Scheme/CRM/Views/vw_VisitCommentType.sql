﻿



CREATE VIEW [CRM].[vw_VisitCommentType]
as
select  vct.CommentTypeId
	,	vct.CommentType
	,	vct.Active
	,	ISNULL(vct.LastModifiedDate, vct.CreationDate) LastUpdate
	,	ISNULL(vct.LastModifiedById, vct.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from CRM.VisitCommentType vct
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(vct.LastModifiedById, vct.CreatedById)