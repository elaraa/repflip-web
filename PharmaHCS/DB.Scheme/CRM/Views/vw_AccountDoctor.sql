﻿





CREATE view [CRM].[vw_AccountDoctor]
as
select ad.AccountId, ad.DoctorId, aty.AccountTypeCode, a.AccountName, t.TerritoryName,a.Address from CRM.AccountDoctor ad join
Shared.Account a on a.AccountId = ad.AccountId join
Shared.AccountType aty on aty.AccountTypeId = a.AccountTypeId join
Shared.Territory t on t.territoryID = a.TerritoryID