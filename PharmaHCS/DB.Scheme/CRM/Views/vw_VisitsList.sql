﻿
/*create view crm.vw_Visits
as
select VisitDate, v.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName EmployeeName, v.CountryId
	, count(distinct v.AccountId) AccountsCount, count(distinct DoctorId) DoctorsCount
	, count(distinct a.TerritoryId) TerritoriesCount
from crm.Visit v
	left join crm.VisitDoctor vd on v.VisitId = vd.VisitId
	join Shared.Account a on v.AccountId = a.AccountId
	join hr.Employee e on v.EmployeeId = e.EmployeeId
group by VisitDate, v.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName, v.CountryId
*/
CREATE view crm.vw_VisitsList
as
select v.VisitId, VisitDate, v.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName EmployeeName, v.CountryId
	, v.AccountId, a.AccountCode+' | '+a.AccountName AccountName, a.Address
	, a.TerritoryId, t.TerritoryCode+' | '+t.TerritoryName TerritoryName
	, count(distinct DoctorId) DoctorsCount
	, v.EntryTypeId, v.EntrySource
	, v.IsReviewed
	, v.ReviewedById
	, r.EmployeeCode+' | '+r.EmployeeName ReviewerName
	, v.CreationDate
	, v.Longitude, v.Latitude
from crm.Visit v
	join hr.Employee e on v.EmployeeId = e.EmployeeId
	join Shared.Account a on v.AccountId = a.AccountId
	join Shared.Territory t on a.TerritoryId = t.TerritoryId
	left join crm.VisitDoctor vd on v.VisitId = vd.VisitId
	left join hr.Employee r on v.ReviewedById = r.EmployeeId
group by v.VisitId, VisitDate, v.EmployeeId, e.EmployeeCode+' | '+e.EmployeeName, v.CountryId
	, v.AccountId, a.AccountCode+' | '+a.AccountName, a.Address
	, a.TerritoryId, t.TerritoryCode+' | '+t.TerritoryName 
	, v.EntryTypeId, v.EntrySource
	, v.IsReviewed
	, v.ReviewedById
	, r.EmployeeCode+' | '+r.EmployeeName
	, v.CreationDate
	, v.Longitude, v.Latitude