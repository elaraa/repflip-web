﻿




CREATE view [CRM].[vw_Doctors]
as
select d.DoctorId, d.DoctorName, d.Mobile, d.BirthDate, d.Approved, p.PotentialName, s.SpecialtyName, c.CountryName from crm.Doctor d join
crm.Potential p on d.PotentialId = p.PotentialId join
crm.Specialty s on s.SpecialtyId = d.SpecialtyId join
Shared.Country c on d.CountryId = c.CountryId