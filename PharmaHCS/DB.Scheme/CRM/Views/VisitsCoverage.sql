﻿CREATE VIEW CRM.VisitsCoverage
AS
SELECT        t.ProfileTargetId, e.EmployeeId, e.EmployeeCode + ' | ' + e.EmployeeName AS EmployeeName, t.ProfileId, tp.ProfileCode + ' | ' + tp.ProfileName AS ProfileName, t.CountryId, t.FromTimeDefinitionId, t.ToTimeDefinitionId, 
                         td.PeriodName, td.Year, tlist.TargetAccounts, v_1.VisitedAccounts, CAST(v_1.VisitedAccounts AS decimal(18, 5)) / CAST(tlist.TargetAccounts AS decimal(18, 5)) AS AccountsCoverage
FROM            CRM.ProfileTargetVisit AS t INNER JOIN
                         Shared.TimeDefinition AS td ON td.TimeDefinitionId >= t.FromTimeDefinitionId AND (t.ToTimeDefinitionId IS NULL OR
                         td.TimeDefinitionId <= t.ToTimeDefinitionId) INNER JOIN
                         Shared.EmployeeProfile AS ep ON t.ProfileId = ep.ProfileId AND Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1 INNER JOIN
                         HR.Employee AS e ON ep.EmployeeId = e.EmployeeId INNER JOIN
                         Shared.TeamProfile AS tp ON t.ProfileId = tp.ProfileId LEFT OUTER JOIN
                             (SELECT        ProfileTargetId, COUNT(DISTINCT AccountId) AS TargetAccounts
                                FROM            CRM.ProfileTargetVisitDetail
                                GROUP BY ProfileTargetId) AS tlist ON t.ProfileTargetId = tlist.ProfileTargetId LEFT OUTER JOIN
                             (SELECT        v.EmployeeId, td.TimeDefinitionId, COUNT(DISTINCT v.AccountId) AS VisitedAccounts
                                FROM            CRM.Visit AS v INNER JOIN
                                                         Shared.TimeDefinition AS td ON Shared.fnCompareDates(v.VisitDate, v.VisitDate, td.FromDate, td.ToDate) = 1
                                GROUP BY v.EmployeeId, td.TimeDefinitionId) AS v_1 ON ep.EmployeeId = v_1.EmployeeId AND v_1.TimeDefinitionId >= t.FromTimeDefinitionId AND (t.ToTimeDefinitionId IS NULL OR
                         v_1.TimeDefinitionId <= t.ToTimeDefinitionId)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'VisitsCoverage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'VisitsCoverage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "td"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 136
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ep"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 138
               Left = 261
               Bottom = 268
               Right = 450
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tp"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tlist"
            Begin Extent = 
               Top = 6
               Left = 486
               Bottom = 102
               Right = 658
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v_1"
            Begin Extent = 
               Top = 6
               Left = 696
               Bottom = 119
               Right = 873
            End
            DisplayFlags = 280
            TopColumn = 0
         ', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'VisitsCoverage';

