﻿CREATE VIEW CRM.vw_Target_Universe
AS
SELECT        AccountId, AccountName, DoctorId, AccountTypeId, AccountTypeName, TerritoryId, TerritoryName, Address, PotentialId, PotentialCode, SpecialtyId, SpecialtyName, ProfileId, FromTimeDefinitionId, ToTimeDefinitionId
FROM            (SELECT        a.AccountId, a.AccountCode + ' | ' + a.AccountName AS AccountName, NULL AS DoctorId, act.AccountTypeId, act.AccountTypeName, a.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName AS TerritoryName, a.Address, 
                                                    p.PotentialId, p.PotentialCode, NULL AS SpecialtyId, NULL AS SpecialtyName, pa.ProfileId, td.TimeDefinitionId AS FromTimeDefinitionId, td2.TimeDefinitionId AS ToTimeDefinitionId
                           FROM            CRM.ProfileAccount AS pa INNER JOIN
                                                    Shared.Account AS a ON pa.AccountId = a.AccountId INNER JOIN
                                                    Shared.AccountType AS act ON a.AccountTypeId = act.AccountTypeId INNER JOIN
                                                    Shared.Territory AS t ON a.TerritoryId = t.TerritoryId LEFT OUTER JOIN
                                                    CRM.Potential AS p ON a.PotentialId = p.PotentialId INNER JOIN
                                                    Shared.TimeDefinition AS td ON CAST(DATEADD(mm, DATEDIFF(mm, 0, pa.FromDate), 0) AS date) = td.FromDate LEFT OUTER JOIN
                                                    Shared.TimeDefinition AS td2 ON CAST(DATEADD(mm, DATEDIFF(mm, 0, pa.ToDate), 0) AS date) = td2.FromDate
                           UNION ALL
                           SELECT        a.AccountId, a.AccountCode + ' | ' + a.AccountName AS AccountName, NULL AS DoctorId, act.AccountTypeId, act.AccountTypeName, a.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName AS TerritoryName, a.Address, 
                                                    p.PotentialId, p.PotentialCode, NULL AS SpecialtyId, NULL AS SpecialtyN, pa.ProfileId, td.TimeDefinitionId AS FromTimeDefinitionId, td2.TimeDefinitionId AS ToTimeDefinitionId
                           FROM            Sales.ProfileAccount AS pa INNER JOIN
                                                    Shared.Account AS a ON pa.AccountId = a.AccountId INNER JOIN
                                                    Shared.AccountType AS act ON a.AccountTypeId = act.AccountTypeId INNER JOIN
                                                    Shared.Territory AS t ON a.TerritoryId = t.TerritoryId LEFT OUTER JOIN
                                                    CRM.Potential AS p ON a.PotentialId = p.PotentialId INNER JOIN
                                                    Shared.TimeDefinition AS td ON CAST(DATEADD(mm, DATEDIFF(mm, 0, pa.FromDate), 0) AS date) = td.FromDate LEFT OUTER JOIN
                                                    Shared.TimeDefinition AS td2 ON td2.FromDate = CAST(DATEADD(mm, DATEDIFF(mm, 0, pa.ToDate), 0) AS date)
                           UNION ALL
                           SELECT        a.AccountId, ISNULL(d.DoctorName, a.AccountCode + ' | ' + a.AccountName) AS AccountName, d.DoctorId, act.AccountTypeId, act.AccountTypeName, pt.TerritoryId, 
                                                    t.TerritoryCode + ' | ' + t.TerritoryName AS TerritoryName, a.Address, p.PotentialId, p.PotentialCode, s.SpecialtyId, s.SpecialtyCode + ' | ' + s.SpecialtyName AS Expr1, pt.ProfileId, 
                                                    td.TimeDefinitionId AS FromTimeDefinitionId, td2.TimeDefinitionId AS ToTimeDefinitionId
                           FROM            Sales.ProfileTerritory AS pt INNER JOIN
                                                    Shared.Territory AS t ON pt.TerritoryId = t.TerritoryId INNER JOIN
                                                    Shared.Account AS a ON t.TerritoryId = a.TerritoryId INNER JOIN
                                                    Shared.AccountType AS act ON a.AccountTypeId = act.AccountTypeId LEFT OUTER JOIN
                                                    CRM.Potential AS p ON a.PotentialId = p.PotentialId LEFT OUTER JOIN
                                                    CRM.AccountDoctor AS ad ON a.AccountId = ad.AccountId AND act.MustHaveDoctor = 1 LEFT OUTER JOIN
                                                    CRM.Doctor AS d ON ad.DoctorId = d.DoctorId LEFT OUTER JOIN
                                                    CRM.Specialty AS s ON d.SpecialtyId = s.SpecialtyId INNER JOIN
                                                    Shared.TimeDefinition AS td ON CAST(DATEADD(mm, DATEDIFF(mm, 0, pt.FromDate), 0) AS date) = td.FromDate LEFT OUTER JOIN
                                                    Shared.TimeDefinition AS td2 ON CAST(DATEADD(mm, DATEDIFF(mm, 0, pt.ToDate), 0) AS date) = td2.FromDate LEFT OUTER JOIN
                                                        (SELECT        tsp.TeamId, tp.ProfileId, tsp.ProductId, ps.SpecialtyId
                                                           FROM            Sales.TeamSalesProduct AS tsp INNER JOIN
                                                                                    Shared.TeamProfile AS tp ON tsp.TeamId = tp.TeamId AND tp.Active = 1 INNER JOIN
                                                                                    CRM.ProductSpecialty AS ps ON tsp.ProductId = ps.ProductId) AS ps_2 ON d.SpecialtyId = ps_2.SpecialtyId
                           UNION ALL
                           SELECT        a.AccountId, ISNULL(d.DoctorName, a.AccountCode + ' | ' + a.AccountName) AS AccountName, d.DoctorId, act.AccountTypeId, act.AccountTypeName, pt.TerritoryId, 
                                                    t.TerritoryCode + ' | ' + t.TerritoryName AS TerritoryName, a.Address, p.PotentialId, p.PotentialCode, s.SpecialtyId, s.SpecialtyCode + ' | ' + s.SpecialtyName AS Expr1, pt.ProfileId, NULL AS Expr2, NULL 
                                                    AS Expr3
                           FROM            CRM.ProfileTerritory AS pt INNER JOIN
                                                    Shared.Territory AS t ON pt.TerritoryId = t.TerritoryId INNER JOIN
                                                    Shared.Account AS a ON t.TerritoryId = a.TerritoryId INNER JOIN
                                                    Shared.AccountType AS act ON a.AccountTypeId = act.AccountTypeId LEFT OUTER JOIN
                                                    CRM.Potential AS p ON a.PotentialId = p.PotentialId LEFT OUTER JOIN
                                                    CRM.AccountDoctor AS ad ON a.AccountId = ad.AccountId AND act.MustHaveDoctor = 1 LEFT OUTER JOIN
                                                    CRM.Doctor AS d ON ad.DoctorId = d.DoctorId LEFT OUTER JOIN
                                                    CRM.Specialty AS s ON d.SpecialtyId = s.SpecialtyId LEFT OUTER JOIN
                                                        (SELECT        tsp.TeamId, tp.ProfileId, tsp.ProductId, ps.SpecialtyId
                                                           FROM            Sales.TeamSalesProduct AS tsp INNER JOIN
                                                                                    Shared.TeamProfile AS tp ON tsp.TeamId = tp.TeamId AND tp.Active = 1 INNER JOIN
                                                                                    CRM.ProductSpecialty AS ps ON tsp.ProductId = ps.ProductId) AS ps_1 ON d.SpecialtyId = ps_1.SpecialtyId) AS dat
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_Target_Universe';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dat"
            Begin Extent = 
               Top = 30
               Left = 90
               Bottom = 160
               Right = 295
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_Target_Universe';

