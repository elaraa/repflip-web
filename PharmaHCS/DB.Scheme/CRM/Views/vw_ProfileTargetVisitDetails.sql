﻿CREATE VIEW CRM.vw_ProfileTargetVisitDetails
AS
SELECT        tvd.ProfileTargetDetailId, tvd.ProfileTargetId, tvd.AccountId, ISNULL(d.DoctorName, a.AccountCode + ' | ' + a.AccountName) AS AccountName, act.AccountTypeId, 
                         act.AccountTypeCode + ' | ' + act.AccountTypeName AS AccountTypeName, tvd.DoctorId, s.SpecialtyId, s.SpecialtyCode, a.Address, t.TerritoryId, t.TerritoryCode + ' | ' + t.TerritoryName AS TerritoryName, tvd.PotentialId, 
                         p.PotentialCode, tvd.Frequency, a.Latitude, a.Longitude
FROM            CRM.ProfileTargetVisitDetail AS tvd INNER JOIN
                         Shared.Account AS a ON tvd.AccountId = a.AccountId INNER JOIN
                         Shared.AccountType AS act ON a.AccountTypeId = act.AccountTypeId INNER JOIN
                         Shared.Territory AS t ON a.TerritoryId = t.TerritoryId INNER JOIN
                         CRM.Potential AS p ON tvd.PotentialId = p.PotentialId LEFT OUTER JOIN
                         CRM.Doctor AS d ON tvd.DoctorId = d.DoctorId LEFT OUTER JOIN
                         CRM.Specialty AS s ON d.SpecialtyId = s.SpecialtyId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfileTargetVisitDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfileTargetVisitDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tvd"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 272
               Bottom = 136
               Right = 454
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "act"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 270
               Left = 258
               Bottom = 400
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 402
               Left = 258
               Bottom = 532
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         En', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfileTargetVisitDetails';

