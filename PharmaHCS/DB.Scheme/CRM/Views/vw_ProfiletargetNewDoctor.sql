﻿CREATE VIEW CRM.vw_ProfiletargetNewDoctor
AS
SELECT        pt.TargetDoctorApprovalId, pt.DoctorId, pt.DoctorName, pt.PotentialId, p.PotentialName, pt.SpecialtyId, s.SpecialtyName, pt.Gender, pt.AccountId, pt.AccountName, pt.AccountCode, pt.TerritoryId, t.TerritoryName, pt.Frequency, 
                         pt.Longitude, pt.Latitude, pt.AccountTypeId, act.AccountTypeName, pt.ProfileId, tp.ProfileName, em.EmployeeId, em.EmployeeName, pt.SupervisorId, emp.EmployeeName AS SupervisorName, pt.AdminApprove, 
                         pt.ApprovalStatus, pt.CountryId, c.CountryName, pt.Mobile, pt.Address, pt.AccountPotentialId, pt.CategoryId, pt.CreatedById, pt.LastModifiedById
FROM            CRM.ProfileTargetNewDoctorApproval AS pt INNER JOIN
                         CRM.Potential AS p ON pt.PotentialId = p.PotentialId LEFT OUTER JOIN
                         Shared.TeamProfile AS tp ON pt.ProfileId = tp.ProfileId INNER JOIN
                         CRM.Specialty AS s ON pt.SpecialtyId = s.SpecialtyId INNER JOIN
                         Shared.Territory AS t ON pt.TerritoryId = t.TerritoryId INNER JOIN
                         Shared.AccountType AS act ON pt.AccountTypeId = act.AccountTypeId LEFT OUTER JOIN
                         HR.Employee AS em ON em.EmployeeId = pt.CreatedById LEFT OUTER JOIN
                         HR.Employee AS emp ON emp.EmployeeId = pt.SupervisorId INNER JOIN
                         Shared.Country AS c ON c.CountryId = pt.CountryId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfiletargetNewDoctor';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
         Begin Table = "emp"
            Begin Extent = 
               Top = 282
               Left = 732
               Bottom = 412
               Right = 921
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 348
               Left = 38
               Bottom = 478
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfiletargetNewDoctor';








GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[61] 4[1] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pt"
            Begin Extent = 
               Top = 6
               Left = 5
               Bottom = 215
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 19
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 149
               Left = 741
               Bottom = 279
               Right = 923
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tp"
            Begin Extent = 
               Top = 216
               Left = 38
               Bottom = 346
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 0
               Left = 370
               Bottom = 130
               Right = 552
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 0
               Left = 712
               Bottom = 130
               Right = 894
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "act"
            Begin Extent = 
               Top = 220
               Left = 466
               Bottom = 350
               Right = 694
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "em"
            Begin Extent = 
               Top = 216
               Left = 266
               Bottom = 346
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         En', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfiletargetNewDoctor';







