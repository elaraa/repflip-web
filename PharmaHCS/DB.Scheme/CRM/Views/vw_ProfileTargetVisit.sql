﻿CREATE VIEW CRM.vw_ProfileTargetVisit
AS
SELECT        t.ProfileTargetId, t.FromTimeDefinitionId, t.ToTimeDefinitionId, td.PeriodName, td.Year, t.ProfileId, tp.ProfileCode, e.EmployeeId, e.EmployeeCode + ' | ' + e.EmployeeName AS EmployeeName, 
                         CASE WHEN lower(ManagerResponse) = 'd' THEN 'Draft' WHEN lower(ManagerResponse) = 'p' THEN 'Pending Approval' WHEN lower(ManagerResponse) = 'a' THEN 'Approved' WHEN lower(ManagerResponse) 
                         = 'r' THEN 'Rejected' END AS TargetStatus, t.ManagerId, m.EmployeeCode + ' | ' + m.EmployeeName AS ManagerName, targetDetails.TargetAccountsCount, targetDetails.TargetDoctorsCount, targetDetails.TargetVisits, 
                         0.1 AS EstimatedCallRate, tp.AvgCallRate, tp.ListTargetDoctorsCount, t.CountryId, c.CountryCode, ISNULL(t.LastModifiedDate, t.CreationDate) AS LastUpdate, ISNULL(t.LastModifiedById, t.CreatedById) AS UpdatedBy, 
                         u.EmployeeName AS UpdatedByLogonName
FROM            CRM.ProfileTargetVisit AS t INNER JOIN
                         Shared.TimeDefinition AS td ON td.TimeDefinitionId >= t.FromTimeDefinitionId AND (t.ToTimeDefinitionId IS NULL OR
                         td.TimeDefinitionId <= t.ToTimeDefinitionId) INNER JOIN
                         Shared.TeamProfile AS tp ON t.ProfileId = tp.ProfileId INNER JOIN
                         Shared.EmployeeProfile AS ep ON tp.ProfileId = ep.ProfileId AND Shared.fnCompareDates(td.FromDate, td.ToDate, ep.FromDate, ep.ToDate) = 1 INNER JOIN
                         HR.Employee AS e ON ep.EmployeeId = e.EmployeeId LEFT OUTER JOIN
                         HR.Employee AS m ON t.ManagerId = m.EmployeeId LEFT OUTER JOIN
                             (SELECT        ProfileTargetId, COUNT(AccountId) AS TargetAccountsCount, COUNT(DoctorId) AS TargetDoctorsCount, SUM(Frequency) AS TargetVisits
                                FROM            CRM.ProfileTargetVisitDetail
                                GROUP BY ProfileTargetId) AS targetDetails ON t.ProfileTargetId = targetDetails.ProfileTargetId LEFT OUTER JOIN
                         HR.Employee AS u ON u.EmployeeId = ISNULL(t.LastModifiedById, t.CreatedById) LEFT OUTER JOIN
                         Shared.Country AS c ON t.CountryId = c.CountryId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfileTargetVisit';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       End
         Begin Table = "u"
            Begin Extent = 
               Top = 402
               Left = 265
               Bottom = 532
               Right = 454
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 534
               Left = 281
               Bottom = 664
               Right = 463
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfileTargetVisit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "td"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 136
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tp"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ep"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 270
               Left = 261
               Bottom = 400
               Right = 450
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "targetDetails"
            Begin Extent = 
               Top = 6
               Left = 486
               Bottom = 136
               Right = 691
            End
            DisplayFlags = 280
            TopColumn = 0
  ', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'VIEW', @level1name = N'vw_ProfileTargetVisit';



