﻿




CREATE VIEW [CRM].[vw_Potential]
as
select  p.PotentialId
	,	p.PotentialCode
	,	p.PotentialName
	,	isnull(pcc.AccountCount,0) AccountCount
	,	p.CountryId
	,	c.CountryCode
	,	ISNULL(p.LastModifiedDate, p.CreationDate) LastUpdate
	,	ISNULL(p.LastModifiedById, p.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from CRM.Potential p
	left join (select distinct count(AccountId) AccountCount, PotentialId from Shared.Account group by PotentialId) pcc
		on p.PotentialId = pcc.PotentialId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(p.LastModifiedById, p.CreatedById)
	left join Shared.Country c on p.CountryId = c.CountryId