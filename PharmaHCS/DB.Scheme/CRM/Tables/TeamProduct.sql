﻿CREATE TABLE [CRM].[TeamProduct] (
    [TeamProductId]    INT      NOT NULL,
    [TeamId]           INT      NOT NULL,
    [ProductId]        INT      NOT NULL,
    [FromDate]         DATE     NOT NULL,
    [ToDate]           DATE     NULL,
    [CreationDate]     DATETIME CONSTRAINT [TeamProduct_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_TeamProduct] PRIMARY KEY CLUSTERED ([TeamProductId] ASC),
    CONSTRAINT [FK_TeamProduct_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TeamProduct_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_TeamProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_TeamProduct_Team] FOREIGN KEY ([TeamId]) REFERENCES [Shared].[Team] ([TeamId])
);

