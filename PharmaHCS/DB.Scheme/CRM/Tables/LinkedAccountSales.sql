﻿CREATE TABLE [CRM].[LinkedAccountSales] (
    [AccountId]        INT      NOT NULL,
    [LinkedAccountId]  INT      NOT NULL,
    [IsPrimary]        BIT      NOT NULL,
    [CreationDate]     DATETIME CONSTRAINT [LinkedAccountSales_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_LinkedAccountSales] PRIMARY KEY CLUSTERED ([AccountId] ASC, [LinkedAccountId] ASC),
    CONSTRAINT [FK_LinkedAccountSales_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_LinkedAccountSales_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_LinkedAccountSales_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);




GO



GO



GO





GO



GO



GO


