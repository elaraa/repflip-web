﻿CREATE TABLE [CRM].[VisitRequest] (
    [VisitRequestId]     INT              NOT NULL,
    [VisitId]            UNIQUEIDENTIFIER NOT NULL,
    [VisitRequestTypeId] INT              NOT NULL,
    [RequestDescription] NVARCHAR (MAX)   COLLATE Latin1_General_CI_AS NULL,
    [RequestStatus]      INT              NOT NULL,
    [Delivered]          BIT              CONSTRAINT [DF_VisitRequest_Delivered_1] DEFAULT ((0)) NOT NULL,
    [FullfilledById]     INT              NULL,
    [FullfillmentDate]   DATETIME         NULL,
    [CreationDate]       DATETIME         CONSTRAINT [VisitRequest_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]   DATETIME         NULL,
    [CreatedById]        INT              NULL,
    [LastModifiedById]   INT              NULL,
    CONSTRAINT [PK_VisitRequest] PRIMARY KEY CLUSTERED ([VisitRequestId] ASC),
    CONSTRAINT [FK_VisitRequest_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitRequest_Employee] FOREIGN KEY ([FullfilledById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitRequest_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitRequest_Visit] FOREIGN KEY ([VisitId]) REFERENCES [CRM].[Visit] ([VisitId]),
    CONSTRAINT [FK_VisitRequest_VisitRequestType] FOREIGN KEY ([VisitRequestTypeId]) REFERENCES [CRM].[VisitRequestType] ([VisitRequestTypeId])
);

