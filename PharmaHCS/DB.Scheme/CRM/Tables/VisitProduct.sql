﻿CREATE TABLE [CRM].[VisitProduct] (
    [VisitId]          UNIQUEIDENTIFIER NOT NULL,
    [ProductId]        INT              NOT NULL,
    [CreationDate]     DATETIME         CONSTRAINT [VisitProduct_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME         NULL,
    [CreatedById]      INT              NULL,
    [LastModifiedById] INT              NULL,
    CONSTRAINT [PK_VisitProduct] PRIMARY KEY CLUSTERED ([VisitId] ASC, [ProductId] ASC),
    CONSTRAINT [FK_VisitProduct_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitProduct_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [Shared].[Product] ([ProductId]),
    CONSTRAINT [FK_VisitProduct_Visit] FOREIGN KEY ([VisitId]) REFERENCES [CRM].[Visit] ([VisitId])
);

