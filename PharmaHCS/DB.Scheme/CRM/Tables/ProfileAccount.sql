﻿CREATE TABLE [CRM].[ProfileAccount] (
    [ProfileAccountId] INT      NOT NULL,
    [ProfileId]        INT      NOT NULL,
    [AccountId]        INT      NOT NULL,
    [FromDate]         DATE     NOT NULL,
    [ToDate]           DATE     NULL,
    [Frequency]        INT      NULL,
    [CreationDate]     DATETIME CONSTRAINT [ProfileAccount_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_ProfileAccount_1] PRIMARY KEY CLUSTERED ([ProfileAccountId] ASC),
    CONSTRAINT [FK_ProfileAccount_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_ProfileAccount_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileAccount_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileAccount_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId])
);

