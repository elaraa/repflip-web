﻿CREATE TABLE [CRM].[Doctor] (
    [DoctorId]         INT            IDENTITY (1, 1) NOT NULL,
    [DoctorName]       NVARCHAR (MAX) NOT NULL,
    [PotentialId]      INT            NOT NULL,
    [SpecialtyId]      INT            NOT NULL,
    [Gender]           CHAR (1)       NOT NULL,
    [BirthDate]        DATE           NULL,
    [Mobile]           NVARCHAR (50)  NULL,
    [Approved]         BIT            NOT NULL,
    [CountryId]        INT            NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [Doctor_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_Doctor] PRIMARY KEY CLUSTERED ([DoctorId] ASC),
    CONSTRAINT [FK_Doctor_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Doctor_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Doctor_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Doctor_Potential] FOREIGN KEY ([PotentialId]) REFERENCES [CRM].[Potential] ([PotentialId]),
    CONSTRAINT [FK_Doctor_Specialty] FOREIGN KEY ([SpecialtyId]) REFERENCES [CRM].[Specialty] ([SpecialtyId])
);



