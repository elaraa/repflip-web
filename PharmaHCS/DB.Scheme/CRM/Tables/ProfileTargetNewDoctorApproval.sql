﻿CREATE TABLE [CRM].[ProfileTargetNewDoctorApproval] (
    [TargetDoctorApprovalId] INT              IDENTITY (1, 1) NOT NULL,
    [ProfileId]              INT              NULL,
    [DoctorId]               INT              NULL,
    [DoctorName]             NVARCHAR (MAX)   NOT NULL,
    [PotentialId]            INT              NOT NULL,
    [SpecialtyId]            INT              NOT NULL,
    [Gender]                 CHAR (1)         NOT NULL,
    [Mobile]                 NVARCHAR (50)    NULL,
    [AccountId]              INT              NULL,
    [AccountName]            NVARCHAR (MAX)   NOT NULL,
    [AccountCode]            NVARCHAR (50)    NULL,
    [Address]                NVARCHAR (MAX)   NULL,
    [AccountTypeId]          INT              NOT NULL,
    [TerritoryId]            INT              NOT NULL,
    [AccountPotentialId]     INT              NULL,
    [CategoryId]             INT              NOT NULL,
    [Latitude]               DECIMAL (18, 10) NULL,
    [Longitude]              DECIMAL (18, 10) NULL,
    [Frequency]              INT              NOT NULL,
    [ApprovalStatus]         CHAR (1)         CONSTRAINT [DF_ProfileTargetNewDoctorApproval_ApprovalStatus] DEFAULT ('p') NULL,
    [CreationDate]           DATETIME         CONSTRAINT [DF_ProfileTargetNewDoctorApproval_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]       DATETIME         NULL,
    [CreatedById]            INT              NULL,
    [LastModifiedById]       INT              NULL,
    [CountryId]              INT              NOT NULL,
    [AdminApprove]           CHAR (1)         NULL,
    [SupervisorId]           INT              NULL,
    CONSTRAINT [PK_ProfileTargetNewDoctorApproval] PRIMARY KEY CLUSTERED ([TargetDoctorApprovalId] ASC),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Account] FOREIGN KEY ([AccountId]) REFERENCES [Shared].[Account] ([AccountId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountCategory] FOREIGN KEY ([CategoryId]) REFERENCES [Shared].[AccountCategory] ([CategoryId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountPotential] FOREIGN KEY ([AccountPotentialId]) REFERENCES [CRM].[Potential] ([PotentialId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountType] FOREIGN KEY ([AccountTypeId]) REFERENCES [Shared].[AccountType] ([AccountTypeId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Doctor] FOREIGN KEY ([DoctorId]) REFERENCES [CRM].[Doctor] ([DoctorId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_DoctorPotential] FOREIGN KEY ([PotentialId]) REFERENCES [CRM].[Potential] ([PotentialId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_LastModifiedById] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_SupervisorId] FOREIGN KEY ([SupervisorId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_TeamProfile] FOREIGN KEY ([ProfileId]) REFERENCES [Shared].[TeamProfile] ([ProfileId]),
    CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Territory] FOREIGN KEY ([TerritoryId]) REFERENCES [Shared].[Territory] ([TerritoryId])
);


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Account];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountCategory];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountPotential];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountType];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_CreatedById];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Doctor];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_DoctorPotential];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_LastModifiedById];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_TeamProfile];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Territory];




GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Account];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountCategory];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountPotential];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountType];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_CreatedById];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Doctor];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_DoctorPotential];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_LastModifiedById];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Territory];




GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Account];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountCategory];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountPotential];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_AccountType];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_CreatedById];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Doctor];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_DoctorPotential];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_LastModifiedById];


GO
ALTER TABLE [CRM].[ProfileTargetNewDoctorApproval] NOCHECK CONSTRAINT [FK_ProfileTargetNewDoctorApproval_Territory];




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'p -> Pending Approval
a -> Approved
m -> Merged
r -> Rejected', @level0type = N'SCHEMA', @level0name = N'CRM', @level1type = N'TABLE', @level1name = N'ProfileTargetNewDoctorApproval', @level2type = N'COLUMN', @level2name = N'ApprovalStatus';



