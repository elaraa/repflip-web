﻿CREATE TABLE [CRM].[VisitDoctor] (
    [VisitId]          UNIQUEIDENTIFIER NOT NULL,
    [DoctorId]         INT              NOT NULL,
    [CreationDate]     DATETIME         CONSTRAINT [VisitDoctor_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME         NULL,
    [CreatedById]      INT              NULL,
    [LastModifiedById] INT              NULL,
    CONSTRAINT [PK_VisitDoctor] PRIMARY KEY CLUSTERED ([VisitId] ASC, [DoctorId] ASC),
    CONSTRAINT [FK_VisitDoctor_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitDoctor_Doctor] FOREIGN KEY ([DoctorId]) REFERENCES [CRM].[Doctor] ([DoctorId]),
    CONSTRAINT [FK_VisitDoctor_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_VisitDoctor_Visit] FOREIGN KEY ([VisitId]) REFERENCES [CRM].[Visit] ([VisitId])
);

