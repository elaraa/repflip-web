﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 29 Sep 19
-- Description:	
-- =============================================
Create FUNCTION [CRM].[fn_GetRepCoaches]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	WITH Employees_CTE (ManagerId) AS (
		SELECT ManagerId 
		FROM Hr.EmployeeHistory  
		WHERE EmployeeId = @employeeId  
			and shared.fnCompareDates(@date, @date, FromDate, ToDate) = 1
		UNION ALL
		SELECT eh.ManagerId 
		FROM Hr.EmployeeHistory  eh
			join Employees_CTE m on eh.EmployeeId = m.ManagerId
				and shared.fnCompareDates(@date, @date, FromDate, ToDate) = 1
		and eh.ManagerId is not null
	)
 
	SELECT     e.*
	FROM     Employees_CTE
		join hr.Employee e on e.EmployeeId = ManagerId
)