﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [CRM].[fn_GetWorkingDays] 
(	
	-- Add the parameters for the function here
	@profileId int,
	@fromPeriodId int,
	@toPeriodId int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = (select top 1 SettingValue from App.Setting where SettingId = 6 and SettingLabel='WorkingDays')
		

	-- Return the result of the function
	RETURN cast(@Result AS INT) 

END