﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [CRM].[fn_GetRepPresentations] 
(	
	-- Add the parameters for the function here
	@employeeId int,
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	select pp.*
	from Shared.EmployeeProfile h 
		join Shared.TeamProfile tp on h.ProfileId = tp.ProfileId
		left join sales.TeamSalesProduct p on tp.TeamId = p.TeamId
			and Shared.fnCompareDates(@date, @date, p.FromDate, p.ToDate) = 1
		left join crm.TeamProduct ctp on ctp.TeamId = tp.TeamId
			and Shared.fnCompareDates(@date, @date, ctp.FromDate, ctp.ToDate) = 1
		join Shared.Product pr on (p.ProductId = pr.ProductId or ctp.ProductId = pr.ProductId ) and pr.Active = 1
		join crm.ProductPresentation pp on pp.ProductId = pr.ProductId and pp.Active = 1
	where h.EmployeeId = @employeeId
		and Shared.fnCompareDates(@date, @date, h.FromDate, h.ToDate)=1
)