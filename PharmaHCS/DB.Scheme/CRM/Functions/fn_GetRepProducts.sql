﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 
-- Description:	
-- =============================================
create FUNCTION [CRM].[fn_GetRepProducts] 
(	
	-- Add the parameters for the function here
	@employeeId int,
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	select pr.*
	from Shared.EmployeeProfile h 
		join Shared.TeamProfile tp on h.ProfileId = tp.ProfileId
		join sales.TeamSalesProduct p on tp.TeamId = p.TeamId
			and Shared.fnCompareDates(@date, @date, p.FromDate, p.ToDate) = 1
		join Shared.Product pr on p.ProductId = pr.ProductId and pr.Active = 1
	where h.EmployeeId = @employeeId
		and Shared.fnCompareDates(@date, @date, h.FromDate, h.ToDate)=1
)