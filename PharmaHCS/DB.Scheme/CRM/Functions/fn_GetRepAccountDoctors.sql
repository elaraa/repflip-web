﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [CRM].[fn_GetRepAccountDoctors] 
(	
	-- Add the parameters for the function here
	@employeeId int,
	@date date,
	@accountId int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	select distinct d.*
	from Shared.EmployeeProfile h 
			join Shared.TeamProfile tp on h.ProfileId = tp.ProfileId
			join crm.ProfileTargetVisit t on tp.ProfileId = t.ProfileId
			join Shared.TimeDefinition td on (td.TimeDefinitionId >= t.FromTimeDefinitionId and (t.ToTimeDefinitionId is null or td.TimeDefinitionId  <= t.ToTimeDefinitionId))
				and Shared.fnCompareDates(@date, @date, td.FromDate, td.ToDate) = 1
			join crm.ProfileTargetVisitDetail vtd on t.ProfileTargetId = vtd.ProfileTargetId
			left join crm.AccountDoctor da on vtd.AccountId = da.AccountId
			left join crm.Doctor d on ( vtd.DoctorId  = d.DoctorId or d.DoctorId = da.DoctorId ) 
				and d.Approved = 1
	where h.EmployeeId = @employeeId
		and Shared.fnCompareDates(@date, @date, h.FromDate, h.ToDate) = 1
		and d.DoctorId is not null
		and vtd.AccountId = @accountId
)