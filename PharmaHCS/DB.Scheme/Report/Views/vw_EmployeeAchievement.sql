﻿


CREATE view [Report].[vw_EmployeeAchievement]
as
SELECT        isnull(e.TeamId, s.TeamId) TeamId, e.EmployeeId, EmployeeCode, EmployeeName, e.ProfileId, ProfileCode, e.TimeDefinitionId, SupId, SupCode, SupName, PMId, PMCode, PMName, BUMId, BUMCode, BUMName, PMDId, PMDCode, PMDName, SODId, 
                         SODCode, SODName, sum(s.Qty) SalesQty, sum(s.Amount) SalesAmount
						  ,sum(t.Qty) TargetQty , sum(t.Amount) as TargetAmount, PeriodName, s.ProductId, p.ProductCode, p.ProductName
FROM            Report.tb_EmployeeStructurewProfile e
	join Staging.InMarketSalesByProfileAccount s on e.ProfileId = s.ProfileId
		and e.TimeDefinitionId = s.TimeDefinitionId
	join Shared.Product p on s.ProductId = p.ProductId
	left join sales.target t with (nolock) on e.ProfileId = t.ProfileId and e.TimeDefinitionId = t.TimeDefinitionId
		and t.TargetTypeId = 6
		and s.ProductId = t.ProductId
group by  isnull(e.TeamId, s.TeamId), e.EmployeeId, EmployeeCode, EmployeeName, e.ProfileId, ProfileCode, e.TimeDefinitionId, SupId, SupCode, SupName, PMId, PMCode, PMName, BUMId, BUMCode, BUMName, PMDId, PMDCode, PMDName, SODId, 
                         SODCode, SODName, PeriodName, s.ProductId, p.ProductCode, p.ProductName