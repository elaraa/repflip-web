﻿

CREATE view [Report].[vw_EmployeeStructurewProfile]
as
SELECT        isnull(p.TeamId,em1.TeamId) TeamId,e.EmployeeId, e.EmployeeCode, e.EmployeeName,rp.ProfileId, p.ProfileCode, td.TimeDefinitionId, td.PeriodName, m1.EmployeeId AS SupId, m1.EmployeeCode AS SupCode, m1.EmployeeName AS SupName, m2.EmployeeId AS PMId, m2.EmployeeCode AS PMCode, m2.EmployeeName AS PMName, m3.EmployeeId AS BUMId,
                         m3.EmployeeCode AS BUMCode, m3.EmployeeName AS BUMName, m4.EmployeeId AS PMDId, m4.EmployeeCode AS PMDCode, m4.EmployeeName AS PMDName, m5.EmployeeId AS SODId, m5.EmployeeCode AS SODCode, m5.EmployeeName AS SODName
FROM            HR.Employee AS e INNER JOIN
                         hr.EmployeeHistory AS em1 ON e.EmployeeId = em1.EmployeeId
join Shared.TimeDefinition td on
shared.fnCompareDates(td.FromDate, td.ToDate, em1.fromDate, em1.ToDate) = 1
left join shared.EmployeeProfile rp on rp.EmployeeId = e.EmployeeId
and Shared.fnCompareDates(td.FromDate, td.ToDate,rp.FromDate, rp.ToDate) = 1
left join Shared.TeamProfile p on rp.ProfileId = p.ProfileId
INNER JOIN
                         hr.Employee AS m1 ON m1.EmployeeId = em1.ManagerId INNER JOIN
                         hr.EmployeeHistory AS em2 ON m1.EmployeeId = em2.EmployeeId
--AND em1.PeriodId = em2.PeriodId
and Shared.fnCompareDates(td.FromDate, td.ToDate,em2.FromDate, em2.ToDate) = 1
INNER JOIN
                         hr.Employee AS m2 ON m2.EmployeeId = em2.ManagerId INNER JOIN
                         hr.EmployeeHistory AS em3 ON m2.EmployeeId = em3.EmployeeId
--AND em1.PeriodId = em3.PeriodId
and Shared.fnCompareDates(td.FromDate, td.ToDate,em3.FromDate, em3.ToDate) = 1
INNER JOIN
                         hr.Employee AS m3 ON m3.EmployeeId = em3.ManagerId INNER JOIN
                         hr.EmployeeHistory AS em4 ON m3.EmployeeId = em4.EmployeeId
--AND em1.PeriodId = em4.PeriodId
and Shared.fnCompareDates(td.FromDate, td.ToDate,em4.FromDate, em4.ToDate) = 1
INNER JOIN
                         hr.Employee AS m4 ON m4.EmployeeId = em4.ManagerId INNER JOIN
                         hr.EmployeeHistory AS em5 ON m4.EmployeeId = em5.EmployeeId
--AND em1.PeriodId = em5.PeriodId
 and Shared.fnCompareDates(td.FromDate, td.ToDate,em5.FromDate, em5.ToDate) = 1
INNER JOIN
                         hr.Employee AS m5 ON m5.EmployeeId = em5.ManagerId