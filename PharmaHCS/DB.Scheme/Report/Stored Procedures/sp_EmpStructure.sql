﻿-- =============================================
-- Author:		Nader
-- Create date: 24/Feb/2020
-- Description:	
-- =============================================
CREATE PROCEDURE [Report].[sp_EmpStructure] 
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF OBJECT_ID('Report.tb_EmployeeStructurewProfile') IS NOT NULL drop table Report.tb_EmployeeStructurewProfile
	select distinct * 
	into Report.tb_EmployeeStructurewProfile
	from Report.vw_EmployeeStructurewProfile
END