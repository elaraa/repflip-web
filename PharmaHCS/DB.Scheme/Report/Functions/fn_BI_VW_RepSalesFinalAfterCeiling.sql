﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 10 Nov 19
-- Description:	replicate VW_RepSalesFinalAfterCeiling of the old cube
-- =============================================
CREATE FUNCTION [Report].[fn_BI_VW_RepSalesFinalAfterCeiling]()
RETURNS TABLE 
AS
RETURN 
(

    SELECT EmployeeId, PeriodId, ProdId, ProfileId, AccountId,	-1 TotalProductQty,
		0 SalesPercentage, -1 RepTargetQty, -1 LineTargetQty,0 RepTargetPercentage, 0 AveragePercentage,
		0 TotalProductAmount, -1 RemainingAmount,
		[0] 'qty', [1] 'RemainingShare'--, [2] 'Stores' 
		,[10] Amount,[11] RemainingShareAmount
		,BUMId, SupId, LineId, PMDId, PMId, SODId
		FROM  
		(
			select e.EmployeeId, BUMId,SupId, PMId,s.TeamId LineId, PMDId, SODId, s.TimeDefinitionId PeriodId, DosageFormId ProdId,s.ProfileId, AccountId,Qty, Amount
				,case when ShareType = 'Profile' then 0 when ShareType = 'Ceiling' then 1 else 2 end ShareType
				,case when ShareType = 'Profile' then 10 when ShareType = 'Ceiling' then 11 else 12 end ShareType_Amt
			from [Staging].[InMarketSalesFact] s with(nolock)
				join Temp.vw_EmployeeStructurewProfile e on s.ProfileId = e.ProfileId and s.TimeDefinitionId = e.TimeDefinitionId
			where ShareType <> 'Stores'
		) AS SourceTable  
		PIVOT  
		(  
		sum(Qty)
		FOR ShareType IN ([0], [1]/*, [2]*/)  
		) AS PivotTable
		PIVOT  
		(  
		sum(Amount)
		FOR ShareType_Amt IN ([10], [11]/*, [12]*/)  
		) AS PivotTable2
)