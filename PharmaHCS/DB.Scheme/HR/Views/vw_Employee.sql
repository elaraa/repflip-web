﻿



CREATE view [HR].[vw_Employee]
as
select  e.EmployeeId
	, e.EmployeeCode
	, e.EmployeeName
	, e.OrganizationNode
	, e.OrganizationLevel
	, e.Picture
	, e.JobTitleId
	, j.JobTitledCode +' | '+ j.JobTitleName JobTitleName 
	, e.DepartmentId
	, d.DepartmentCode +' | '+ d.DepartmentName DepartmentName 
	, e.Active
	, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from HR.Employee e
	left join HR.JobTitle j on j.JobTitleId = e.JobTitleId
	left join HR.Department d on d.DepartmentId = e.DepartmentId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId