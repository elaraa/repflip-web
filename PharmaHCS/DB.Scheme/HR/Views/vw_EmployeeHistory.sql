﻿CREATE VIEW HR.vw_EmployeeHistory
AS
SELECT        e.EmployeeHistoryId, e.EmployeeId, uu.EmployeeCode, uu.EmployeeName AS Employee, uu.EmployeeCode + ' | ' + uu.EmployeeName AS EmployeeName, e.JobTitleId, j.JobTitleName AS JobTitle, 
                         j.JobTitledCode + ' | ' + j.JobTitleName AS JobTitleName, e.DepartmentId, d.DepartmentName AS Department, d.DepartmentCode + ' | ' + d.DepartmentName AS DepartmentName, e.ManagerId, 
                         u1.EmployeeCode AS ManagerCode, u1.EmployeeCode + ' | ' + u1.EmployeeName AS ManagerName, e.OrganizationNode, e.OrganizationLevel, e.FromDate, e.ToDate, e.TeamId, t.TeamName AS Team, 
                         t.TeamCode + ' | ' + t.TeamName AS TeamName, e.CountryId, c.CountryCode, ISNULL(e.LastModifiedDate, e.CreationDate) AS LastUpdate, ISNULL(e.LastModifiedById, e.CreatedById) AS UpdatedBy, 
                         u.EmployeeName AS UpdatedByLogonName, e.CanVisit, u.NationalIDNumber
FROM            HR.EmployeeHistory AS e LEFT OUTER JOIN
                         Shared.Team AS t ON t.TeamId = e.TeamId LEFT OUTER JOIN
                         HR.JobTitle AS j ON j.JobTitleId = e.JobTitleId LEFT OUTER JOIN
                         HR.Department AS d ON d.DepartmentId = e.DepartmentId LEFT OUTER JOIN
                         HR.Employee AS u1 ON u1.EmployeeId = e.ManagerId LEFT OUTER JOIN
                         HR.Employee AS uu ON uu.EmployeeId = e.EmployeeId LEFT OUTER JOIN
                         HR.Employee AS u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById) LEFT OUTER JOIN
                         Shared.Country AS c ON e.CountryId = c.CountryId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'VIEW', @level1name = N'vw_EmployeeHistory';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
         Begin Table = "c"
            Begin Extent = 
               Top = 402
               Left = 265
               Bottom = 532
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'VIEW', @level1name = N'vw_EmployeeHistory';








GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[4] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -278
         Left = 0
      End
      Begin Tables = 
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 136
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 146
               Left = 280
               Bottom = 276
               Right = 469
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u1"
            Begin Extent = 
               Top = 61
               Left = 517
               Bottom = 191
               Right = 706
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "uu"
            Begin Extent = 
               Top = 312
               Left = 485
               Bottom = 442
               Right = 674
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 2
         En', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'VIEW', @level1name = N'vw_EmployeeHistory';







