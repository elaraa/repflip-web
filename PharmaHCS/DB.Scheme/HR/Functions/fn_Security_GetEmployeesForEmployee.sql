﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 29 Sep 19
-- Description:	
-- =============================================
CREATE FUNCTION [HR].[fn_Security_GetEmployeesForEmployee]
(	
	-- Add the parameters for the function here
	@employeeId int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	WITH Employees_CTE (EmployeeId)  
	AS  
	-- Define the CTE query.  
	(  
		SELECT EmployeeId 
		FROM Hr.EmployeeHistory  
		WHERE ManagerId = @employeeId  
			and shared.fnCompareDates(@date, @date, FromDate, ToDate) = 1
		UNION ALL
		SELECT eh.EmployeeId 
		FROM Hr.EmployeeHistory  eh
			join Employees_CTE m on eh.ManagerId = m.EmployeeId
				and shared.fnCompareDates(@date, @date, FromDate, ToDate) = 1
	)  
	-- get employees for logged in employee.  
	select EmployeeId
	from Employees_CTE 
	where exists(select 1 from hr.Employee e join hr.Department d on e.DepartmentId = d.DepartmentId where /*e.CountryId is not null and*/ d.IsCRMDepartment = 1 and e.EmployeeId = @employeeId)
	/*get all employees for admin employee*/
	union all
	select distinct e.EmployeeId
	from hr.Employee e
			join hr.Employee se on ((se.CountryId = e.CountryId and se.CountryId is not null)
				or (se.CountryId is null)
				)
			left join hr.Department d on se.DepartmentId = d.DepartmentId and d.IsCRMDepartment = 0
	where se.EmployeeId = @employeeId
	and exists(select 1 from hr.Employee e join hr.Department d on e.DepartmentId = d.DepartmentId where d.IsCRMDepartment = 0 and e.EmployeeId = @employeeId)

)