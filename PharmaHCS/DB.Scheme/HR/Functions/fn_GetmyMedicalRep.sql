﻿

-- =============================================
-- Author:			
-- Create date: 03/06/2020
-- Description:	
-- =============================================
CREATE FUNCTION [HR].[fn_GetmyMedicalRep]
(	
	-- Add the parameters for the function here
	@EmployeeID int, 
	@date date
)
RETURNS TABLE 
AS
RETURN 
(
	

	select f.employeeid, e.EmployeeName from hr.fn_Security_GetEmployeesForEmployee(@EmployeeID, @date)  f join
	hr.Employee e on e.employeeid = f.EmployeeId join
	hr.EmployeeHistory t on t.EmployeeId = e.EmployeeId 
	where t.CanVisit = 1
)