﻿CREATE TABLE [HR].[EmployeeVacation] (
    [EmployeeVacationId]  INT            NOT NULL,
    [EmployeeId]          INT            NOT NULL,
    [VacationTypeId]      INT            NOT NULL,
    [FromDate]            DATE           NOT NULL,
    [ToDate]              DATE           NOT NULL,
    [NumberOfDays]        DECIMAL (6, 2) NOT NULL,
    [Comment]             NVARCHAR (MAX) NOT NULL,
    [ManagerResponse]     CHAR (1)       NULL,
    [ManagerComment]      NVARCHAR (MAX) NULL,
    [ManagerResponseDate] DATETIME       NULL,
    [ManagerId]           INT            NULL,
    [CreationDate]        DATETIME       CONSTRAINT [EmployeeVacation_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]    DATETIME       NULL,
    [CreatedById]         INT            NULL,
    [LastModifiedById]    INT            NULL,
    CONSTRAINT [PK_EmployeeVacation] PRIMARY KEY CLUSTERED ([EmployeeVacationId] ASC),
    CONSTRAINT [FK_EmployeeVacation_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeVacation_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeVacation_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeVacation_ManagerApproval] FOREIGN KEY ([ManagerId]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_EmployeeVacation_VacationType] FOREIGN KEY ([VacationTypeId]) REFERENCES [HR].[VacationType] ([VacationTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'R => Rejected
A => Accepted', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'EmployeeVacation', @level2type = N'COLUMN', @level2name = N'ManagerResponse';

