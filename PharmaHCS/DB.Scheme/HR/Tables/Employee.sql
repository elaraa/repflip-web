﻿CREATE TABLE [HR].[Employee] (
    [EmployeeId]        INT                 IDENTITY (1, 1) NOT NULL,
    [EmployeeName]      NVARCHAR (50)       NOT NULL,
    [EmployeeCode]      NVARCHAR (50)       NULL,
    [NationalIDNumber]  NVARCHAR (50)       NOT NULL,
    [OrganizationNode]  [sys].[hierarchyid] NULL,
    [OrganizationLevel] AS                  ([OrganizationNode].[GetLevel]()),
    [BirthDate]         DATE                NULL,
    [MaritalStatus]     NCHAR (1)           NOT NULL,
    [Gender]            NCHAR (1)           NULL,
    [HireDate]          DATE                NOT NULL,
    [LoginId]           NVARCHAR (MAX)      NULL,
    [Password]          NVARCHAR (MAX)      NULL,
    [Picture]           NVARCHAR (MAX)      NULL,
    [RoleId]            INT                 NULL,
    [JobTitleId]        INT                 NOT NULL,
    [DepartmentId]      INT                 NULL,
    [Email]             NVARCHAR (MAX)      NULL,
    [Mobile]            NVARCHAR (50)       NULL,
    [Active]            BIT                 CONSTRAINT [DF_Employee_Active_1] DEFAULT ((1)) NOT NULL,
    [CountryId]         INT                 NULL,
    [CreationDate]      DATETIME            CONSTRAINT [Employee_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]  DATETIME            NULL,
    [CreatedById]       INT                 NULL,
    [LastModifiedById]  INT                 NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeId] ASC),
    CONSTRAINT [FK_Employee_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_Employee_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Employee_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [HR].[Department] ([DepartmentId]),
    CONSTRAINT [FK_Employee_JobTitle] FOREIGN KEY ([JobTitleId]) REFERENCES [HR].[JobTitle] ([JobTitleId]),
    CONSTRAINT [FK_Employee_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Employee_SystemRole] FOREIGN KEY ([RoleId]) REFERENCES [App].[SystemRole] ([SystemRoleId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Employee hired on this date.', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'HireDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'M = Male, F = Female', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'Gender';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'M = Married, S = Single', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'MaritalStatus';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date of birth.', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'BirthDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The depth of the employee in the corporate hierarchy.', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'OrganizationLevel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Where the employee is located in corporate hierarchy.', @level0type = N'SCHEMA', @level0name = N'HR', @level1type = N'TABLE', @level1name = N'Employee', @level2type = N'COLUMN', @level2name = N'OrganizationNode';

