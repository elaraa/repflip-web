﻿CREATE TABLE [Temp].[Target_Sheet_Sales] (
    [ProductId]   INT             NULL,
    [year]        INT             NULL,
    [Qty]         DECIMAL (18, 6) NULL,
    [Amount]      DECIMAL (18, 6) NULL,
    [countryId]   INT             NULL,
    [TerritoryId] INT             NULL,
    [CategoryId]  INT             NULL
);

