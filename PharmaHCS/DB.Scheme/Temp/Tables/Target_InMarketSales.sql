﻿CREATE TABLE [Temp].[Target_InMarketSales] (
    [CountryId]        INT             NOT NULL,
    [TeamId]           INT             NOT NULL,
    [ProfileId]        INT             NOT NULL,
    [ProductId]        INT             NOT NULL,
    [DosageFormId]     INT             NOT NULL,
    [AccountId]        INT             NOT NULL,
    [Qty]              DECIMAL (38, 6) NULL,
    [Amount]           DECIMAL (38, 6) NULL,
    [TimeDefinitionId] INT             NULL,
    [Perccentage]      DECIMAL (38, 6) NULL,
    [SalesType]        VARCHAR (50)    NULL
);

