﻿CREATE TABLE [Temp].[uploadceilingproduct] (
    [CustId]         INT            NULL,
    [CustName]       NVARCHAR (255) NULL,
    [Line]           NVARCHAR (255) NULL,
    [New_Line]       NVARCHAR (255) NULL,
    [DosageFormId]   INT            NULL,
    [DosageFormName] NVARCHAR (255) NULL,
    [Old_CeilingQty] FLOAT (53)     NULL,
    [New_CeilingQty] FLOAT (53)     NULL,
    [FromTimeId]     INT            NULL,
    [ToTimeId]       INT            NULL
);

