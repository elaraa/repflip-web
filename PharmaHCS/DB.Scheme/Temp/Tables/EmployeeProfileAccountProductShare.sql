﻿CREATE TABLE [Temp].[EmployeeProfileAccountProductShare] (
    [FromDate]     DATE            NULL,
    [ToDate]       DATE            NULL,
    [DosageFormId] INT             NOT NULL,
    [AccountId]    INT             NOT NULL,
    [Perccentage]  DECIMAL (18, 6) NOT NULL,
    [ProfileId]    INT             NOT NULL,
    [EmployeeId]   INT             NOT NULL,
    [TeamId]       INT             NOT NULL
);

