﻿CREATE TABLE [Temp].[ProfileSales_For_Store] (
    [CountryId]        INT              NOT NULL,
    [TeamId]           INT              NOT NULL,
    [ProfileId]        INT              NOT NULL,
    [RootTerritoryId]  INT              NULL,
    [ProductId]        INT              NOT NULL,
    [DosageFormId]     INT              NOT NULL,
    [TimeDefinitionId] INT              NULL,
    [Qty]              DECIMAL (38, 10) NULL
);

