﻿CREATE TABLE [Temp].[profileaccount] (
    [TeamId]             INT             NOT NULL,
    [ProfileId]          INT             NOT NULL,
    [AccountId]          INT             NOT NULL,
    [Perccentage]        DECIMAL (38, 6) NULL,
    [TimeDefinitionId]   INT             NOT NULL,
    [ProfileAccountType] VARCHAR (50)    NULL,
    [FromDate]           DATE            NOT NULL,
    [ToDate]             DATE            NOT NULL
);

