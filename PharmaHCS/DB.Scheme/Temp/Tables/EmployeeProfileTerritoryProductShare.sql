﻿CREATE TABLE [Temp].[EmployeeProfileTerritoryProductShare] (
    [FromDate]     DATE            NULL,
    [ToDate]       DATE            NULL,
    [DosageFormId] INT             NOT NULL,
    [TerritoryId]  INT             NOT NULL,
    [AccountId]    INT             NOT NULL,
    [Percentage]   DECIMAL (18, 6) NOT NULL,
    [ProfileId]    INT             NOT NULL,
    [EmployeeId]   INT             NOT NULL,
    [TeamId]       INT             NOT NULL
);

