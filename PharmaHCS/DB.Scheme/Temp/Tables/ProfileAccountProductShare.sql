﻿CREATE TABLE [Temp].[ProfileAccountProductShare] (
    [TeamId]           INT             NOT NULL,
    [ProfileId]        INT             NOT NULL,
    [AccountId]        INT             NOT NULL,
    [ProductId]        INT             NOT NULL,
    [DosageFormId]     INT             NOT NULL,
    [Perccentage]      DECIMAL (38, 6) NULL,
    [TimeDefinitionId] INT             NOT NULL,
    [PackFactor]       DECIMAL (18, 6) NULL,
    [FromDate]         DATE            NOT NULL,
    [ToDate]           DATE            NOT NULL
);

