﻿CREATE TABLE [Temp].[Account_ImportedFile_654_40381991c7704ab6a493ba9065c1996b] (
    [AutoID]             INT            NULL,
    [Customer Code]      NVARCHAR (MAX) NULL,
    [Customer Name]      NVARCHAR (MAX) NULL,
    [Customer Address]   NVARCHAR (MAX) NULL,
    [Customer Territory] NVARCHAR (MAX) NULL,
    [Territory]          NVARCHAR (MAX) NULL,
    [Account Type]       NVARCHAR (MAX) NULL,
    [Category]           NVARCHAR (MAX) NULL,
    [RecordId]           NVARCHAR (MAX) NULL
);

