﻿CREATE TABLE [Temp].[Target_AvgPercentage] (
    [TeamId]          INT             NULL,
    [ProfileId]       INT             NULL,
    [ProductId]       INT             NULL,
    [SalesPecentage]  DECIMAL (38, 6) NULL,
    [TargetPecentage] DECIMAL (38, 6) NULL,
    [AvgPercentage]   DECIMAL (38, 6) NULL
);

