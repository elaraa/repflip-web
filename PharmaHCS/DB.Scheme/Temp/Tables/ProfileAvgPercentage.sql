﻿CREATE TABLE [Temp].[ProfileAvgPercentage] (
    [ProfileId]        INT             NULL,
    [ProductId]        INT             NULL,
    [TimeDefinitionId] INT             NULL,
    [salesPercentage]  DECIMAL (38, 6) NULL,
    [targetPercentage] DECIMAL (38, 6) NULL,
    [AvgPercenatege]   NUMERIC (38, 6) NULL
);

