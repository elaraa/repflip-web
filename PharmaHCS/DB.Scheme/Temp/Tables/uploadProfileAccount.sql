﻿CREATE TABLE [Temp].[uploadProfileAccount] (
    [ProfileCode] NVARCHAR (255)  NULL,
    [AccountId]   FLOAT (53)      NULL,
    [Percentage]  DECIMAL (18, 2) NULL,
    [Line]        FLOAT (53)      NULL
);

