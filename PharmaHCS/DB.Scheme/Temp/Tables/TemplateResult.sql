﻿CREATE TABLE [Temp].[TemplateResult] (
    [RowIndex]              NVARCHAR (50) NOT NULL,
    [ProfileCode]           NVARCHAR (50) NULL,
    [AccountCode]           NVARCHAR (50) NULL,
    [Percentage]            NVARCHAR (50) NULL,
    [ProductCode]           NVARCHAR (50) NULL,
    [DosageFromCode]        NVARCHAR (50) NULL,
    [InvalidProfileCode]    NVARCHAR (50) NULL,
    [InvalidAccountCode]    NVARCHAR (50) NULL,
    [InvalidProductCode]    NVARCHAR (50) NULL,
    [InvalidDosageFormCode] NVARCHAR (50) NULL,
    [IsDuplicated]          NVARCHAR (50) NULL,
    [IsOverlapped]          NVARCHAR (50) NULL,
    [IsExceedPercentage]    NVARCHAR (50) NULL,
    [ExceededPercentage]    NVARCHAR (50) NULL
);



