﻿CREATE TABLE [Temp].[TeamSalesProduct] (
    [TeamSalesProductId] INT      IDENTITY (1, 1) NOT NULL,
    [TeamId]             INT      NOT NULL,
    [ProductId]          INT      NOT NULL,
    [FromDate]           DATE     NOT NULL,
    [ToDate]             DATE     NULL,
    [CreationDate]       DATETIME NULL,
    [LastModifiedDate]   DATETIME NULL,
    [CreatedById]        INT      NULL,
    [LastModifiedById]   INT      NULL
);

