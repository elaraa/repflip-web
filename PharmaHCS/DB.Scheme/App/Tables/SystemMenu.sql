﻿CREATE TABLE [App].[SystemMenu] (
    [MenuId]           INT            NOT NULL,
    [MenuName]         NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NOT NULL,
    [MenuIcon]         NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NULL,
    [ParentMenuId]     INT            NULL,
    [Color]            NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NULL,
    [Controller]       NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NULL,
    [Action]           NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NULL,
    [SortOrder]        INT            CONSTRAINT [DF_SystemMenu_SortOrder] DEFAULT ((1)) NOT NULL,
    [Module]           NVARCHAR (50)  NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [SystemMenu_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_SystemMenu] PRIMARY KEY CLUSTERED ([MenuId] ASC),
    CONSTRAINT [FK_SystemMenu_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_SystemMenu_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_SystemMenu_SystemMenu] FOREIGN KEY ([ParentMenuId]) REFERENCES [App].[SystemMenu] ([MenuId])
);


GO
ALTER TABLE [App].[SystemMenu] NOCHECK CONSTRAINT [FK_SystemMenu_CreatedBy];


GO
ALTER TABLE [App].[SystemMenu] NOCHECK CONSTRAINT [FK_SystemMenu_LastModifiedBy];


GO
ALTER TABLE [App].[SystemMenu] NOCHECK CONSTRAINT [FK_SystemMenu_SystemMenu];









