﻿CREATE TABLE [App].[ScheduledJob] (
    [JobId]           INT             NOT NULL,
    [JobName]         NVARCHAR (MAX)  NOT NULL,
    [Module]          NVARCHAR (50)   CONSTRAINT [DF_ScheduledJob_Module] DEFAULT (N'Sales') NOT NULL,
    [StoredProcedure] NVARCHAR (MAX)  NOT NULL,
    [FromDate]        DATE            NOT NULL,
    [ToDate]          DATE            NULL,
    [ScheduleType]    CHAR (1)        CONSTRAINT [DF_ScheduledJob_ScheduleType] DEFAULT ('d') NOT NULL,
    [Active]          BIT             CONSTRAINT [DF_ScheduledJob_Active] DEFAULT ((1)) NOT NULL,
    [LastRunTime]     DATETIME        NULL,
    [LastRunSpan]     DECIMAL (18, 6) CONSTRAINT [DF_ScheduledJob_LastRunSpan] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_ScheduledJob] PRIMARY KEY CLUSTERED ([JobId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'h -> hour
d -> day
w -> week
m ->month
', @level0type = N'SCHEMA', @level0name = N'App', @level1type = N'TABLE', @level1name = N'ScheduledJob', @level2type = N'COLUMN', @level2name = N'ScheduleType';

