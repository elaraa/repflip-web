﻿CREATE TABLE [App].[SystemRoleMenu] (
    [MenuId]           INT      NOT NULL,
    [RoleId]           INT      NOT NULL,
    [CreationDate]     DATETIME CONSTRAINT [SystemRoleMenu_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME NULL,
    [CreatedById]      INT      NULL,
    [LastModifiedById] INT      NULL,
    CONSTRAINT [PK_SystemRoleMenu] PRIMARY KEY CLUSTERED ([MenuId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_SystemRoleMenu_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_SystemRoleMenu_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_SystemRoleMenu_SystemMenu] FOREIGN KEY ([MenuId]) REFERENCES [App].[SystemMenu] ([MenuId]),
    CONSTRAINT [FK_SystemRoleMenu_SystemRole] FOREIGN KEY ([RoleId]) REFERENCES [App].[SystemRole] ([SystemRoleId])
);

