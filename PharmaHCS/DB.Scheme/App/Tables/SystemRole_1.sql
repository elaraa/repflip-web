﻿CREATE TABLE [App].[SystemRole] (
    [SystemRoleId]     INT            IDENTITY (1, 1) NOT NULL,
    [RoleName]         NVARCHAR (MAX) COLLATE Latin1_General_CI_AS NOT NULL,
    [IsAdmin]          BIT            CONSTRAINT [DF_SystemRole_IsAdmin_1] DEFAULT ((0)) NOT NULL,
    [Active]           BIT            CONSTRAINT [DF_SystemRole_Active_1] DEFAULT ((1)) NOT NULL,
    [CountryId]        INT            NULL,
    [CreationDate]     DATETIME       CONSTRAINT [SystemRole_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_SystemRole] PRIMARY KEY CLUSTERED ([SystemRoleId] ASC),
    CONSTRAINT [FK_SystemRole_Country] FOREIGN KEY ([CountryId]) REFERENCES [Shared].[Country] ([CountryId]),
    CONSTRAINT [FK_SystemRole_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_SystemRole_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);

