﻿CREATE TABLE [App].[Setting] (
    [SettingId]              INT            NOT NULL,
    [SettingLabel]           NVARCHAR (MAX) NOT NULL,
    [SettingValue]           NVARCHAR (MAX) NULL,
    [SettingType]            VARCHAR (10)   CONSTRAINT [DF_Setting_SettingType] DEFAULT ('t') NOT NULL,
    [SettingAvailableValues] NVARCHAR (MAX) NULL,
    [Module]                 NVARCHAR (50)  CONSTRAINT [DF_Setting_Module] DEFAULT (N'Shared') NOT NULL,
    [CountryId]              INT            NULL,
    [CreationDate]           DATETIME       CONSTRAINT [DF_Setting_CreationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate]       DATETIME       NULL,
    [CreatedById]            INT            NULL,
    [LastModifiedById]       INT            NULL,
    CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED ([SettingId] ASC),
    CONSTRAINT [FK_Setting_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [HR].[Employee] ([EmployeeId]),
    CONSTRAINT [FK_Setting_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [HR].[Employee] ([EmployeeId])
);


GO
ALTER TABLE [App].[Setting] NOCHECK CONSTRAINT [FK_Setting_CreatedBy];


GO
ALTER TABLE [App].[Setting] NOCHECK CONSTRAINT [FK_Setting_LastModifiedBy];



