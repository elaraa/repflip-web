﻿create View app.[vw_User]
as
select e.EmployeeId, e.EmployeeName, e.LoginId, e.Picture, e.RoleId, r.RoleName, r.IsAdmin , e.Active, e.CountryId
	, c.CountryCode
	, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
	, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from hr.Employee e
	left join App.SystemRole r on e.RoleId = r.SystemRoleId
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(e.LastModifiedById, e.CreatedById)
	left join Shared.Country c on e.CountryId = c.CountryId