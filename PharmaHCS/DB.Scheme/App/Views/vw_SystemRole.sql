﻿create view app.vw_SystemRole
as
select r.SystemRoleId, r.RoleName, r.IsAdmin, r.Active, isnull(e.EmployeesCount,0) EmployeesCount
	,r.CountryId
	, c.CountryCode
	, ISNULL(r.LastModifiedDate, r.CreationDate) LastUpdate
	, ISNULL(r.LastModifiedById, r.CreatedById) UpdatedBy
	, u.EmployeeName UpdatedByLogonName
from app.SystemRole r
	left join (select count(*) EmployeesCount, RoleId from hr.Employee where RoleId is not null group by RoleId ) e on e.RoleId = r.SystemRoleId  
	LEFT JOIN HR.Employee u ON u.EmployeeId = ISNULL(r.LastModifiedById, r.CreatedById)
	left join Shared.Country c on r.CountryId = c.CountryId