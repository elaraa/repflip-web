﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 10 Oct 19
-- =============================================
CREATE FUNCTION [App].[fn_CanAccessController]
(	
	@EmployeeId int,
	@Controller nvarchar(max)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT DISTINCT m.MenuId
FROM App.SystemRole r
	LEFT JOIN HR.Employee s ON s.RoleId = r.SystemRoleId
	LEFT JOIN App.SystemRoleMenu rm ON r.SystemRoleId = rm.RoleId AND r.Active = 1
	LEFT JOIN App.SystemMenu m ON (r.IsAdmin = 1 AND r.Active = 1) OR (r.IsAdmin = 0 AND r.Active = 1 AND rm.MenuId = m.MenuId)
where s.EmployeeId = @EmployeeId and m.Controller = @Controller
)