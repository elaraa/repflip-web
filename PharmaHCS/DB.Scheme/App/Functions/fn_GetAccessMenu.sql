﻿-- =============================================
-- Author:		Ahmed ELAraby
-- Create date: 10 Oct 19
-- =============================================
CREATE FUNCTION [App].[fn_GetAccessMenu]
(	
  @EmployeeId int
)
RETURNS TABLE 
AS
RETURN 
(
	with NodePaths as
	(
		select MenuId, MenuName Name, ParentMenuId, MenuId BaseID
		from app.SystemMenu
		where MenuId in( 
			select isnull(rm.MenuId, m.MenuId) MenuId
			from app.SystemRole r 
				join hr.Employee e on e.RoleId = r.SystemRoleId
				left join app.SystemRoleMenu rm on r.SystemRoleId = rm.RoleId and r.IsAdmin = 0
				left join app.SystemMenu m on r.IsAdmin = 1
			where e.EmployeeId = @EmployeeId
		)

		union all

		select n.MenuId, n.MenuName, n.ParentMenuId, np.BaseID
		from app.SystemMenu n
		join NodePaths np
			on np.ParentMenuId = n.MenuId    
	)

	select distinct m.*
	from app.SystemMenu m 
	join (
		select distinct BaseID MenuId
		from NodePaths
		union all 
		select distinct ParentMenuId 
		from NodePaths 
		where ParentMenuId is not null
	) ma on m.MenuId = ma.MenuId
)