﻿-- =============================================
-- Author:		Ahmed ElAraby
-- Create date: 28 Oct 19
-- Description:	
-- =============================================
CREATE FUNCTION App.fn_ScheduleJobs_Get()
RETURNS TABLE 
AS
RETURN 
(
	select *
	from app.ScheduledJob j
	where j.Active = 1
		and Shared.fnCompareDates(GETDATE(), getdate(), j.FromDate, j.ToDate) = 1
		and (j.LastRunTime is null 
			OR (
				(lower(j.ScheduleType) = 'd' AND DATEADD(DAY,1, j.LastRunTime) < getdate() )
				OR
				(lower(j.ScheduleType) = 'm' AND DATEADD(MONTH,1, j.LastRunTime) < getdate() )
				OR
				(lower(j.ScheduleType) = 'w' AND DATEADD(WEEK,1, j.LastRunTime) < getdate() )
				OR
				(lower(j.ScheduleType) = 'q' AND DATEADD(QUARTER,1, j.LastRunTime) < getdate() )
				) 
			)
)