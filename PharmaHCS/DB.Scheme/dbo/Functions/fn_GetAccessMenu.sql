﻿
-- =============================================
-- Author: Ahmed ELAraby
-- Create date: 04 Mar 20
-- =============================================
CREATE FUNCTION [dbo].[fn_GetAccessMenu]
(
  @UserId int
)
RETURNS TABLE
AS
RETURN
(
with NodePaths as
(
select MenuId, MenuName Name, ParentMenuId, MenuId BaseID
from SystemMenu
where MenuId in(
select isnull(rm.MenuId, m.MenuId) MenuId
from SystemRole r
join SystemUser e on e.RoleId = r.SystemRoleId
left join SystemRoleMenu rm on r.SystemRoleId = rm.RoleId and r.IsAdmin = 0
left join SystemMenu m on r.IsAdmin = 1
where e.UserId = @UserId
)

union all

select n.MenuId, n.MenuName, n.ParentMenuId, np.BaseID
from SystemMenu n
join NodePaths np
on np.ParentMenuId = n.MenuId    
)

select distinct m.*
from SystemMenu m
join (
select distinct BaseID MenuId
from NodePaths
union all
select distinct ParentMenuId
from NodePaths
where ParentMenuId is not null
) ma on m.MenuId = ma.MenuId
)