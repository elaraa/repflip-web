﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetLastOfferToRate]
(	
  @ConsumerId int
)
RETURNS TABLE 
AS
RETURN 
(
select r.PromotionRunId,p.PromotionName,p.PromotionNameAr,m.MerchantName,cp.CreationDate from PromotionRun  r join
[dbo].[Promotion] p on p.PromotionId=r.PromotionId  join
[dbo].[Merchant] m on m.MerchantId=p.MerchantId join 
[dbo].[ConsumerRedeemedPromotionRun] cp on cp.PromotionRunId=r.PromotionRunId left join
[dbo].[PromotionRate] pr on pr.ConsumerId = cp.ConsumerId and pr.PromotionRunId = r.PromotionRunId 

where cp.ConsumerId =@ConsumerId  and pr.PromotionRunRateId is null

)