﻿
-- =============================================
-- Author: Ahmed ElAraby
-- Create date: 04 Mar 20
-- =============================================
CREATE FUNCTION [dbo].[fn_CanAccessController]
(
@UserId int,
@Controller nvarchar(max)
)
RETURNS TABLE
AS
RETURN
(
SELECT DISTINCT m.MenuId
FROM SystemRole r
LEFT JOIN SystemUser s ON s.RoleId = r.SystemRoleId
LEFT JOIN SystemRoleMenu rm ON r.SystemRoleId = rm.RoleId AND r.Active = 1
LEFT JOIN SystemMenu m ON (r.IsAdmin = 1 AND r.Active = 1) OR (r.IsAdmin = 0 AND r.Active = 1 AND rm.MenuId = m.MenuId)
where s.UserId = @UserId and m.Controller = @Controller
)