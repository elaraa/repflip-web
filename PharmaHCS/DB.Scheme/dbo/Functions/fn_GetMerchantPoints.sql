﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetMerchantPoints] 
(	
 @merchantId as int ,
 @points as int 
)
RETURNS TABLE 
AS
RETURN 
(
  select ROW_NUMBER() over(order by mp.PointValue asc) as PointOrder, mp.PointValue, mp.PointDescription ,mp.PointDescriptionAr
  ,case when mp.PointValue <= @points then cast(1 as bit) else cast(0 as bit)  end as IsChecked
  from MerchantPoint mp 
 where  MerchantId = @merchantId
 
)