﻿
create view [dbo].[vw_SystemRole]
as
select r.SystemRoleId, r.RoleName, r.IsAdmin, r.Active, isnull(e.UsersCount,0) UsersCount
, ISNULL(r.LastModifiedDate, r.CreationDate) LastUpdate
, ISNULL(r.LastModifiedById, r.CreatedById) UpdatedBy
, u.UserName UpdatedByLogonName
from SystemRole r
left join (select count(*) UsersCount, RoleId from systemuser where RoleId is not null group by RoleId ) e on e.RoleId = r.SystemRoleId  
LEFT JOIN systemuser u ON u.UserId = ISNULL(r.LastModifiedById, r.CreatedById)