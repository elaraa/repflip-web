﻿CREATE VIEW dbo.vw_PromotionOffers
AS
SELECT        dbo.PromotionRun.PromotionRunId, dbo.PromotionRun.PromotionId, dbo.PromotionRun.NumberOfPieces, dbo.Promotion.Description, dbo.Promotion.DescriptionAr, dbo.Promotion.PromotionName, 
                         dbo.Promotion.PromotionNameAr, dbo.Merchant.MerchantBrandName, dbo.PromotionType.PromotionTypeName, DATEDIFF(day, dbo.PromotionRun.StartDate, dbo.PromotionRun.EndDate) AS Duration
FROM            dbo.PromotionRun INNER JOIN
                         dbo.Promotion ON dbo.PromotionRun.PromotionId = dbo.Promotion.PromotionId INNER JOIN
                         dbo.Merchant ON dbo.Promotion.MerchantId = dbo.Merchant.MerchantId INNER JOIN
                         dbo.PromotionType ON dbo.Promotion.PromotionTypeId = dbo.PromotionType.PromotionTypeId
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_PromotionOffers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[5] 2[38] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -37
      End
      Begin Tables = 
         Begin Table = "PromotionRun"
            Begin Extent = 
               Top = 4
               Left = 49
               Bottom = 134
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "Promotion"
            Begin Extent = 
               Top = 26
               Left = 291
               Bottom = 156
               Right = 481
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Merchant"
            Begin Extent = 
               Top = 6
               Left = 532
               Bottom = 136
               Right = 754
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "PromotionType"
            Begin Extent = 
               Top = 99
               Left = 508
               Bottom = 195
               Right = 711
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vw_PromotionOffers';







