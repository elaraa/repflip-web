﻿

CREATE VIEW [dbo].[vw_User]
AS
SELECT ( (SELECT TOP 1 AppSettingValue FROM dbo.AppSettings WHERE AppSettingCode = 'BaseFileUrl')+  e.UserLogo) AS Logo, e.UserId, e.UserName, e.LoginId, /*e.Picture, */e.RoleId, r.RoleName, r.IsAdmin , e.Active
, ISNULL(e.LastModifiedDate, e.CreationDate) LastUpdate
, ISNULL(e.LastModifiedById, e.CreatedById) UpdatedBy
, u.UserName UpdatedByLogonName
FROM SystemUser e
LEFT JOIN SystemRole r ON e.RoleId = r.SystemRoleId
LEFT JOIN SystemUser u ON u.UserId = ISNULL(e.LastModifiedById, e.CreatedById)