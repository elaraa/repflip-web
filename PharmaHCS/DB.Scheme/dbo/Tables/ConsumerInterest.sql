﻿CREATE TABLE [dbo].[ConsumerInterest] (
    [ConsumerInterestId] INT IDENTITY (1, 1) NOT NULL,
    [ConsumerId]         INT NOT NULL,
    [InterestId]         INT NOT NULL,
    CONSTRAINT [PK_ConsumerInterest] PRIMARY KEY CLUSTERED ([ConsumerInterestId] ASC)
);

