﻿CREATE TABLE [dbo].[MerchantBranchCashier] (
    [MerchantBranchCashierId] INT            IDENTITY (1, 1) NOT NULL,
    [BranchId]                INT            NOT NULL,
    [CashierName]             NVARCHAR (MAX) NOT NULL,
    [LoginId]                 NVARCHAR (MAX) NOT NULL,
    [Password]                NVARCHAR (MAX) NOT NULL,
    [Language]                NVARCHAR (5)   CONSTRAINT [DF_MerchantBranchCashier_Language] DEFAULT (N'en') NULL,
    CONSTRAINT [PK_MerchantBranchCashier] PRIMARY KEY CLUSTERED ([MerchantBranchCashierId] ASC),
    CONSTRAINT [FK_MerchantBranchCashier_MerchantBranch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[MerchantBranch] ([BranchId])
);







