﻿CREATE TABLE [dbo].[MerchantCategory] (
    [MerchantCategoryId] INT      IDENTITY (1, 1) NOT NULL,
    [MerchantId]         INT      NOT NULL,
    [CategoryId]         INT      NOT NULL,
    [CreationDate]       DATETIME NOT NULL,
    [EndDate]            DATETIME NULL,
    CONSTRAINT [PK_MerchantCategory_1] PRIMARY KEY CLUSTERED ([MerchantCategoryId] ASC),
    CONSTRAINT [FK_MerchantCategory_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([CategoryId]),
    CONSTRAINT [FK_MerchantCategory_Merchant] FOREIGN KEY ([MerchantId]) REFERENCES [dbo].[Merchant] ([MerchantId])
);

