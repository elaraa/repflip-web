﻿CREATE TABLE [dbo].[ConsumerNotification] (
    [NotificationId]      INT            NOT NULL,
    [ConsumerId]          INT            NOT NULL,
    [IsRead]              BIT            DEFAULT ((0)) NOT NULL,
    [NotificationMessage] NVARCHAR (MAX) NOT NULL,
    [CreationDate]        DATETIME       DEFAULT (getdate()) NOT NULL,
    [MerchantId]          INT            NULL,
    PRIMARY KEY CLUSTERED ([NotificationId] ASC)
);

