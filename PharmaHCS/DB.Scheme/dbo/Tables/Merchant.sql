﻿CREATE TABLE [dbo].[Merchant] (
    [MerchantId]               INT              IDENTITY (1, 1) NOT NULL,
    [MerchantName]             NVARCHAR (MAX)   NOT NULL,
    [MerchantBrandName]        NVARCHAR (MAX)   NOT NULL,
    [MerchantLogo]             NVARCHAR (MAX)   NULL,
    [InterestId]               INT              NOT NULL,
    [LoginId]                  NVARCHAR (MAX)   NULL,
    [Password]                 NVARCHAR (MAX)   NULL,
    [Website]                  NVARCHAR (MAX)   NULL,
    [Facebook]                 NVARCHAR (MAX)   NULL,
    [HasLoyalty]               BIT              CONSTRAINT [DF_Merchant_HasLoyalty] DEFAULT ((0)) NOT NULL,
    [PointsAmount]             DECIMAL (18, 10) NULL,
    [Pounds]                   DECIMAL (18, 10) NULL,
    [PointsActivationDuration] INT              NULL,
    [PointsExpirationDuration] INT              NULL,
    CONSTRAINT [PK_Merchant] PRIMARY KEY CLUSTERED ([MerchantId] ASC),
    CONSTRAINT [FK_Merchant_Interest] FOREIGN KEY ([InterestId]) REFERENCES [dbo].[Interest] ([InterestId])
);











