CREATE TABLE [dbo].[Promotion] (
    [PromotionId]     INT            IDENTITY (1, 1) NOT NULL,
    [PromotionName]   NVARCHAR (MAX) NOT NULL,
    [PromotionNameAr] NVARCHAR (MAX) NULL,
    [PromotionTypeId] INT            NOT NULL,
    [Description]     NVARCHAR (MAX) NULL,
    [DescriptionAr]   NVARCHAR (MAX) NULL,
    [MerchantId]      INT            NOT NULL,
    [CreationDate]    DATETIME       NULL,
    [IsDraft]         BIT            CONSTRAINT [DF_Promotion_IsDraft] DEFAULT ((1)) NOT NULL,
    [ApprovedDate]    DATETIME       NULL,
    [RejectedDate]    DATETIME       NULL,
    CONSTRAINT [PK_Promotion] PRIMARY KEY CLUSTERED ([PromotionId] ASC),
    CONSTRAINT [FK_Promotion_Merchant] FOREIGN KEY ([MerchantId]) REFERENCES [dbo].[Merchant] ([MerchantId]),
    CONSTRAINT [FK_Promotion_OfferType] FOREIGN KEY ([PromotionTypeId]) REFERENCES [dbo].[PromotionType] ([PromotionTypeId])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0: Draft, 1:Pending, 2: Approved, 3 Rejected', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Promotion', @level2type = N'COLUMN', @level2name = N'IsDraft';

