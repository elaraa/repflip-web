﻿CREATE TABLE [dbo].[Area] (
    [AreaId]     INT              IDENTITY (1, 1) NOT NULL,
    [AreaName]   NVARCHAR (MAX)   NOT NULL,
    [AreaNameAr] NVARCHAR (MAX)   NULL,
    [Lat]        DECIMAL (18, 10) NULL,
    [Lng]        DECIMAL (18, 10) NULL,
    CONSTRAINT [PK__Area__70B820488A1BFF08] PRIMARY KEY CLUSTERED ([AreaId] ASC)
);



