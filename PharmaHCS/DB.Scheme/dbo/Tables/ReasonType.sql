﻿CREATE TABLE [dbo].[ReasonType] (
    [ReasonTypeId]     INT            IDENTITY (1, 1) NOT NULL,
    [ReasonTypeText]   NVARCHAR (MAX) NULL,
    [ReasonTypeTextAr] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ReasonType] PRIMARY KEY CLUSTERED ([ReasonTypeId] ASC)
);

