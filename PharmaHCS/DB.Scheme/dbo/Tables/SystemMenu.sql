﻿CREATE TABLE [dbo].[SystemMenu] (
    [MenuId]           INT            NOT NULL,
    [MenuName]         NVARCHAR (MAX) NOT NULL,
    [MenuIcon]         NVARCHAR (MAX) NULL,
    [ParentMenuId]     INT            NULL,
    [Color]            NVARCHAR (MAX) NULL,
    [Controller]       NVARCHAR (MAX) NULL,
    [Action]           NVARCHAR (MAX) NULL,
    [SortOrder]        INT            CONSTRAINT [DF_SystemMenu_SortOrder] DEFAULT ((1)) NOT NULL,
    [Module]           NVARCHAR (50)  NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [SystemMenu_creationDate] DEFAULT (getdate()) NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_SystemMenu] PRIMARY KEY CLUSTERED ([MenuId] ASC),
    CONSTRAINT [FK_SystemMenu_CreatedBy] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[SystemUser] ([UserId]),
    CONSTRAINT [FK_SystemMenu_LastModifiedBy] FOREIGN KEY ([LastModifiedById]) REFERENCES [dbo].[SystemUser] ([UserId]),
    CONSTRAINT [FK_SystemMenu_SystemMenu] FOREIGN KEY ([ParentMenuId]) REFERENCES [dbo].[SystemMenu] ([MenuId])
);

