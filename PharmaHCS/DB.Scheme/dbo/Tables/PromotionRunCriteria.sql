﻿CREATE TABLE [dbo].[PromotionRunCriteria] (
    [PromotionRunCriteriaId] INT      IDENTITY (1, 1) NOT NULL,
    [PromotionRunId]         INT      NOT NULL,
    [AgeFrom]                INT      NULL,
    [AgeTo]                  INT      NULL,
    [Gender]                 CHAR (1) NULL,
    [PhoneType]              INT      NULL,
    CONSTRAINT [PK_PromotionRunCriteria] PRIMARY KEY CLUSTERED ([PromotionRunCriteriaId] ASC),
    CONSTRAINT [FK_PromotionRunCriteria_PromotionRun] FOREIGN KEY ([PromotionRunId]) REFERENCES [dbo].[PromotionRun] ([PromotionRunId])
);



