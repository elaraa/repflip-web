﻿CREATE TABLE [dbo].[PromotionRun] (
    [PromotionRunId]       INT              IDENTITY (1, 1) NOT NULL,
    [PromotionId]          INT              NOT NULL,
    [CreationDate]         DATETIME         NULL,
    [StartDate]            DATETIME         NOT NULL,
    [EndDate]              DATETIME         NULL,
    [CloseDate]            DATETIME         NULL,
    [RedeemCountPerPerson] INT              NULL,
    [NumberOfPoints]       INT              NULL,
    [NumberOfPieces]       INT              NULL,
    [Savings]              DECIMAL (18, 10) NULL,
    CONSTRAINT [PK_PromotionRun] PRIMARY KEY CLUSTERED ([PromotionRunId] ASC),
    CONSTRAINT [FK_PromotionRun_Promotion] FOREIGN KEY ([PromotionId]) REFERENCES [dbo].[Promotion] ([PromotionId])
);









