﻿CREATE TABLE [dbo].[AppSettings] (
    [AppSettingId]           INT            NOT NULL,
    [AppSettingCode]         NVARCHAR (MAX) NOT NULL,
    [AppSettingValue]        NVARCHAR (MAX) NULL,
    [AppSettingDefaultValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppSettings] PRIMARY KEY CLUSTERED ([AppSettingId] ASC)
);

