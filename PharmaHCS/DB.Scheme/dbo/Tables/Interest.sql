﻿CREATE TABLE [dbo].[Interest] (
    [InterestId]     INT            IDENTITY (1, 1) NOT NULL,
    [InterestName]   NVARCHAR (MAX) NOT NULL,
    [InterestNameAr] NVARCHAR (MAX) NULL,
    [Icon]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Interest] PRIMARY KEY CLUSTERED ([InterestId] ASC)
);





