﻿CREATE TABLE [dbo].[PromotionType] (
    [PromotionTypeId]   INT            IDENTITY (1, 1) NOT NULL,
    [PromotionTypeName] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_OfferType] PRIMARY KEY CLUSTERED ([PromotionTypeId] ASC)
);

