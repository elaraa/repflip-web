﻿CREATE TABLE [dbo].[ConsumerLoyaltyPoint] (
    [ConsumerPointId]    INT              IDENTITY (1, 1) NOT NULL,
    [ConsumerId]         INT              NOT NULL,
    [MerchantId]         INT              NOT NULL,
    [Points]             INT              NOT NULL,
    [PromotionRunId]     INT              NULL,
    [InvoiceNumber]      NVARCHAR (MAX)   NULL,
    [InvoiceTotalAmount] DECIMAL (18, 10) NULL,
    [CreationDate]       DATETIME         NOT NULL,
    [ActivationDate]     DATE             NOT NULL,
    [ExpiryDate]         DATE             NOT NULL,
    [Remain]             INT              NOT NULL,
    CONSTRAINT [PK__Consumer__6AFE34211AC76078] PRIMARY KEY CLUSTERED ([ConsumerPointId] ASC)
);





