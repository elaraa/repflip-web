﻿CREATE TABLE [dbo].[MerchantBranch] (
    [BranchId]   INT              IDENTITY (1, 1) NOT NULL,
    [MerchantId] INT              NOT NULL,
    [Address]    NVARCHAR (MAX)   NOT NULL,
    [AddressAr]  NVARCHAR (MAX)   NULL,
    [AreaId]     INT              NOT NULL,
    [Lat]        DECIMAL (18, 10) NOT NULL,
    [Lng]        DECIMAL (18, 10) NOT NULL,
    CONSTRAINT [PK_MerchantBranch] PRIMARY KEY CLUSTERED ([BranchId] ASC),
    CONSTRAINT [FK_MerchantBranch_Area] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    CONSTRAINT [FK_MerchantBranch_Merchant] FOREIGN KEY ([MerchantId]) REFERENCES [dbo].[Merchant] ([MerchantId])
);



