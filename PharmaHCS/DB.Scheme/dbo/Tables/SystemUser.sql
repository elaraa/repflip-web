﻿CREATE TABLE [dbo].[SystemUser] (
    [UserId]           INT            IDENTITY (1, 1) NOT NULL,
    [UserName]         NVARCHAR (MAX) NOT NULL,
    [LoginId]          NVARCHAR (MAX) NOT NULL,
    [Password]         NVARCHAR (MAX) NOT NULL,
    [UserLogo]         NVARCHAR (MAX) NULL,
    [Website]          NVARCHAR (MAX) NULL,
    [Facebook]         NVARCHAR (MAX) NULL,
    [RoleId]           INT            NOT NULL,
    [Active]           BIT            CONSTRAINT [DF_SystemUser_Active] DEFAULT ((1)) NOT NULL,
    [CreationDate]     DATETIME       CONSTRAINT [DF_SystemUser_CreationDate] DEFAULT (getdate()) NOT NULL,
    [LastModifiedDate] DATETIME       NULL,
    [CreatedById]      INT            NULL,
    [LastModifiedById] INT            NULL,
    CONSTRAINT [PK_SystemUser] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_SystemUser_SystemRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[SystemRole] ([SystemRoleId])
);



