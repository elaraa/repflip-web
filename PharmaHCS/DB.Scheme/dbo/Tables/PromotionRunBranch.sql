﻿CREATE TABLE [dbo].[PromotionRunBranch] (
    [PromotionRunBranchId] INT IDENTITY (1, 1) NOT NULL,
    [PromotionRunId]       INT NOT NULL,
    [BranchId]             INT NOT NULL,
    CONSTRAINT [PK_PromotionRunBranch] PRIMARY KEY CLUSTERED ([PromotionRunBranchId] ASC),
    CONSTRAINT [FK_PromotionRunBranch_MerchantBranch] FOREIGN KEY ([BranchId]) REFERENCES [dbo].[MerchantBranch] ([BranchId]),
    CONSTRAINT [FK_PromotionRunBranch_PromotionRun] FOREIGN KEY ([PromotionRunId]) REFERENCES [dbo].[PromotionRun] ([PromotionRunId])
);





