﻿CREATE TABLE [dbo].[MerchantPoint] (
    [PointId]            INT            IDENTITY (1, 1) NOT NULL,
    [MerchantId]         INT            NOT NULL,
    [PointValue]         INT            NOT NULL,
    [PointDescription]   NVARCHAR (MAX) NULL,
    [PointDescriptionAr] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_MerchantPoint] PRIMARY KEY CLUSTERED ([PointId] ASC),
    CONSTRAINT [FK_MerchantPoint_Merchant] FOREIGN KEY ([MerchantId]) REFERENCES [dbo].[Merchant] ([MerchantId])
);

