﻿CREATE TABLE [dbo].[PromotionRateType] (
    [PromotionRateTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [PromotionRateType]   NVARCHAR (MAX) NOT NULL,
    [PromotionRateTypeAr] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK__OfferRat__E1BD6175ECAB69B3] PRIMARY KEY CLUSTERED ([PromotionRateTypeId] ASC)
);

