﻿CREATE TABLE [dbo].[FAQCategory] (
    [FaqCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [CategoryName]  NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_FAQCategory] PRIMARY KEY CLUSTERED ([FaqCategoryId] ASC)
);



