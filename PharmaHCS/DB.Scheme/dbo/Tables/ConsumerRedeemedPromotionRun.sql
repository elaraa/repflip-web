﻿CREATE TABLE [dbo].[ConsumerRedeemedPromotionRun] (
    [ConsumerRedeemedPromotionId] INT      IDENTITY (1, 1) NOT NULL,
    [ConsumerId]                  INT      NOT NULL,
    [PromotionRunId]              INT      NOT NULL,
    [CreationDate]                DATETIME NULL,
    CONSTRAINT [PK_ConsumerRedeemedPromotionRun] PRIMARY KEY CLUSTERED ([ConsumerRedeemedPromotionId] ASC)
);



