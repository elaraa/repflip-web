﻿CREATE TABLE [dbo].[ConsumerReportPromotionRun] (
    [ReportPromotionRunId] INT            IDENTITY (1, 1) NOT NULL,
    [PromotionRunId]       INT            NOT NULL,
    [ConsumerId]           INT            NOT NULL,
    [ReasonTypeId]         INT            NOT NULL,
    [ReasonText]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ConsumerReportPromotionRun] PRIMARY KEY CLUSTERED ([ReportPromotionRunId] ASC),
    CONSTRAINT [FK_ConsumerReportPromotionRun_Consumer] FOREIGN KEY ([ConsumerId]) REFERENCES [dbo].[Consumer] ([ConsumerId]),
    CONSTRAINT [FK_ConsumerReportPromotionRun_PromotionRun] FOREIGN KEY ([PromotionRunId]) REFERENCES [dbo].[PromotionRun] ([PromotionRunId]),
    CONSTRAINT [FK_ConsumerReportPromotionRun_ReasonType] FOREIGN KEY ([ReasonTypeId]) REFERENCES [dbo].[ReasonType] ([ReasonTypeId])
);

