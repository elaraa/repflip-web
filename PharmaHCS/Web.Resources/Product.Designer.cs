﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PH {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Product {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Product() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Web.Resources.Product", typeof(Product).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account.
        /// </summary>
        public static string Account {
            get {
                return ResourceManager.GetString("Account", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account Category.
        /// </summary>
        public static string AccountCategory {
            get {
                return ResourceManager.GetString("AccountCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to صنف الحساب.
        /// </summary>
        public static string AccountCategoryId {
            get {
                return ResourceManager.GetString("AccountCategoryId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to الحساب.
        /// </summary>
        public static string AccountId {
            get {
                return ResourceManager.GetString("AccountId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        public static string Active {
            get {
                return ResourceManager.GetString("Active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contraindications.
        /// </summary>
        public static string Contraindications {
            get {
                return ResourceManager.GetString("Contraindications", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country.
        /// </summary>
        public static string Country {
            get {
                return ResourceManager.GetString("Country", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country Code.
        /// </summary>
        public static string CountryCode {
            get {
                return ResourceManager.GetString("CountryCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to البلد.
        /// </summary>
        public static string CountryId {
            get {
                return ResourceManager.GetString("CountryId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string Create {
            get {
                return ResourceManager.GetString("Create", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Details.
        /// </summary>
        public static string Details {
            get {
                return ResourceManager.GetString("Details", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Distributor.
        /// </summary>
        public static string Distributor {
            get {
                return ResourceManager.GetString("Distributor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to الموزع.
        /// </summary>
        public static string DistributorId {
            get {
                return ResourceManager.GetString("DistributorId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dosage Form.
        /// </summary>
        public static string DosageForm {
            get {
                return ResourceManager.GetString("DosageForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DosageForm Code.
        /// </summary>
        public static string DosageFormCode {
            get {
                return ResourceManager.GetString("DosageFormCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DosageForm Count.
        /// </summary>
        public static string DosageFormCount {
            get {
                return ResourceManager.GetString("DosageFormCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to الجرعة.
        /// </summary>
        public static string DosageFormId {
            get {
                return ResourceManager.GetString("DosageFormId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DosageForm Name.
        /// </summary>
        public static string DosageFormName {
            get {
                return ResourceManager.GetString("DosageFormName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dosage Forms.
        /// </summary>
        public static string DosageForms {
            get {
                return ResourceManager.GetString("DosageForms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to From Date.
        /// </summary>
        public static string FromDate {
            get {
                return ResourceManager.GetString("FromDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Group.
        /// </summary>
        public static string Group {
            get {
                return ResourceManager.GetString("Group", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Group Code.
        /// </summary>
        public static string GroupCode {
            get {
                return ResourceManager.GetString("GroupCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Indications.
        /// </summary>
        public static string Indications {
            get {
                return ResourceManager.GetString("Indications", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PackFactor.
        /// </summary>
        public static string PackFactor {
            get {
                return ResourceManager.GetString("PackFactor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parent Group.
        /// </summary>
        public static string ParentGroup {
            get {
                return ResourceManager.GetString("ParentGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parent Product.
        /// </summary>
        public static string ParentProduct {
            get {
                return ResourceManager.GetString("ParentProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to المنتج الرئيسي.
        /// </summary>
        public static string ParentProductId {
            get {
                return ResourceManager.GetString("ParentProductId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price.
        /// </summary>
        public static string Price {
            get {
                return ResourceManager.GetString("Price", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price Type.
        /// </summary>
        public static string PriceType {
            get {
                return ResourceManager.GetString("PriceType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price Type Name.
        /// </summary>
        public static string PriceTypeName {
            get {
                return ResourceManager.GetString("PriceTypeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string product {
            get {
                return ResourceManager.GetString("product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Code.
        /// </summary>
        public static string ProductCode {
            get {
                return ResourceManager.GetString("ProductCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to المنتج.
        /// </summary>
        public static string ProductId {
            get {
                return ResourceManager.GetString("ProductId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string ProductName {
            get {
                return ResourceManager.GetString("ProductName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Price.
        /// </summary>
        public static string ProductPrice {
            get {
                return ResourceManager.GetString("ProductPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Prices.
        /// </summary>
        public static string ProductPrices {
            get {
                return ResourceManager.GetString("ProductPrices", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DosageForm Code is Required.
        /// </summary>
        public static string RequiredDosageFormCode {
            get {
                return ResourceManager.GetString("RequiredDosageFormCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DosageForm Name is Required.
        /// </summary>
        public static string RequiredDosageFormName {
            get {
                return ResourceManager.GetString("RequiredDosageFormName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ext is Required.
        /// </summary>
        public static string RequiredExt {
            get {
                return ResourceManager.GetString("RequiredExt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Path is Required.
        /// </summary>
        public static string RequiredFilePath {
            get {
                return ResourceManager.GetString("RequiredFilePath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Group Name is Required.
        /// </summary>
        public static string RequiredGroupName {
            get {
                return ResourceManager.GetString("RequiredGroupName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Presentation Name Required.
        /// </summary>
        public static string RequiredPresentationName {
            get {
                return ResourceManager.GetString("RequiredPresentationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Name Required.
        /// </summary>
        public static string RequiredProductName {
            get {
                return ResourceManager.GetString("RequiredProductName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SideEffects.
        /// </summary>
        public static string SideEffects {
            get {
                return ResourceManager.GetString("SideEffects", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Size.
        /// </summary>
        public static string Size {
            get {
                return ResourceManager.GetString("Size", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Storage.
        /// </summary>
        public static string Storage {
            get {
                return ResourceManager.GetString("Storage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Territory.
        /// </summary>
        public static string Territory {
            get {
                return ResourceManager.GetString("Territory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to المنطقة.
        /// </summary>
        public static string TerritoryId {
            get {
                return ResourceManager.GetString("TerritoryId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Date.
        /// </summary>
        public static string ToDate {
            get {
                return ResourceManager.GetString("ToDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unit.
        /// </summary>
        public static string Unit {
            get {
                return ResourceManager.GetString("Unit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to وحدة.
        /// </summary>
        public static string UnitId {
            get {
                return ResourceManager.GetString("UnitId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Products.
        /// </summary>
        public static string ViewNamePlural {
            get {
                return ResourceManager.GetString("ViewNamePlural", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Groups.
        /// </summary>
        public static string ViewNamePluralProductGroup {
            get {
                return ResourceManager.GetString("ViewNamePluralProductGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Prices.
        /// </summary>
        public static string ViewNamePluralProductPrice {
            get {
                return ResourceManager.GetString("ViewNamePluralProductPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string ViewNameSingular {
            get {
                return ResourceManager.GetString("ViewNameSingular", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Group.
        /// </summary>
        public static string ViewNameSingularProductGroup {
            get {
                return ResourceManager.GetString("ViewNameSingularProductGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Price.
        /// </summary>
        public static string ViewNameSingularProductPrice {
            get {
                return ResourceManager.GetString("ViewNameSingularProductPrice", resourceCulture);
            }
        }
    }
}
