﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;

namespace DB.ORM.DB
{
    public partial class DBModel
    {
        public int Sp_SalesCrunch_FCS_Ceiling_2020_Per_Period(int timeId)
        {
            //Thread.Sleep(90000);
            Database.CommandTimeout = 0;
            return Database.ExecuteSqlCommand(string.Format("exec Sales.sp_SalesCrunch_FCS_Ceiling_2020_Per_Period {0}", timeId));
        }
        public int Sp_EmpStructure()
        {
            Database.CommandTimeout = 0;
            return Database.ExecuteSqlCommand(string.Format("exec Report.sp_EmpStructure"));
        }
        public int Sp_DeleteTarget(int targetId)
        {
            Database.CommandTimeout = 0;
            return Database.ExecuteSqlCommand(string.Format("exec sales.sp_DeleteTarget {0}", targetId));
        }
        public int Sp_ProcessSalesImportedFile(int fileId, int userId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<int>("[Sales].[sp_ProcessSalesImportedFile] @fileId, @userId", new SqlParameter("@fileId", fileId), new SqlParameter("@userId", userId)).FirstOrDefault();
        }
        public int Sp_PushSalesImport(int fileId, int userId)
        {
            Database.CommandTimeout = 0;
            return Database.ExecuteSqlCommand("[Sales].[sp_PushSalesImport] @fileId, @userId"
                , new SqlParameter("@fileId", fileId)
                , new SqlParameter("@userId", userId));
        }
        public int Sp_SalesImport_Delete(int fileId, int userId, bool deleteMappings = true)
        {
            Database.CommandTimeout = 0;
            return Database.ExecuteSqlCommand("[Sales].[sp_SalesImport_Delete] @fileId, @userId, @deleteMappings"
                , new SqlParameter("@fileId", fileId)
                , new SqlParameter("@userId", userId)
                , new SqlParameter("@deleteMappings", deleteMappings));
        }
        public IQueryable<fn_GetImportedFileMappedAccounts> Fn_GetImportedFileMappedAccounts(int fileId)
        {
            Database.CommandTimeout = 0;
            var sql = $"select * from [Sales].[fn_GetImportedFileMappedAccounts]({fileId})";
            return Database.SqlQuery<fn_GetImportedFileMappedAccounts>(sql).AsQueryable();
        }
        public IQueryable<fn_GetImportedFileMappedProducts> Fn_GetImportedFileMappedProducts(int fileId)
        {
            Database.CommandTimeout = 0;
            SqlParameter pFileId_Fn_GetImportedFileMappedProducts = new SqlParameter("@fileId", fileId);
            return Database.SqlQuery<fn_GetImportedFileMappedProducts>("select * from [Sales].[fn_GetImportedFileMappedProducts](@fileId)",
                pFileId_Fn_GetImportedFileMappedProducts).AsQueryable();
        }

        public int Sp_SalesAdjustmentTarget(int timeId, int productId, int? territoryId, int? profileId, decimal targetValue, bool isSales , bool ForceSales)
        {
            Database.CommandTimeout = 0;
            return Database.ExecuteSqlCommand("[Sales].[sp_AdjustmentTarget] @TimeId, @ProductId, @TerritoryId, @ProfileId, @TargetValue, @IsSales,@ForceSales"
                , new SqlParameter("@TimeId", timeId)
                , new SqlParameter("@ProductId", productId)
                , new SqlParameter("@TerritoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@ProfileId", (object)profileId ?? DBNull.Value)
                , new SqlParameter("@TargetValue", targetValue)
                , new SqlParameter("@IsSales", isSales)
                , new SqlParameter("@ForceSales", ForceSales));
        }
        public fn_GetImportedFileDetails Fn_GetImportedFileDetails(int fileId)
        {
            Database.CommandTimeout = 0;
            SqlParameter pFileId_Fn_GetImportedFileDetails = new SqlParameter("@fileId", fileId);
            return Database.SqlQuery<fn_GetImportedFileDetails>("select * from [Sales].[fn_GetImportedFileDetails](@fileId)"
                , pFileId_Fn_GetImportedFileDetails).FirstOrDefault();
        }
        public IQueryable<fn_GetImportedFileSummary> Fn_GetImportedFileSummary(int fileId)
        {
            Database.CommandTimeout = 0;
            //SqlParameter pFileId_Fn_GetImportedFileSummary = new SqlParameter("@fileId", fileId);
            return Database.SqlQuery<fn_GetImportedFileSummary>(string.Format("select * from [Sales].[fn_GetImportedFileSummary]({0})", fileId)).AsQueryable();
        }
        public IQueryable<fnTargetProduct_Export> Fn_TargetProduct_Export(DateTime exportDate, int countryId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fnTargetProduct_Export>("select * from [Sales].[fn_TargetProduct_Export](@exportDate, @countryId)"
                , new SqlParameter("@exportDate", exportDate)
                , new SqlParameter("@countryId", countryId)).AsQueryable();
        }
        public IQueryable<fn_TargetProductPhasing_Export> Fn_TargetProductPhasing_Export(int year, int countryId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_TargetProductPhasing_Export>("select * from [Sales].[fn_TargetProductPhasing_Export](@year, @countryId)"
                , new SqlParameter("@year", year)
                , new SqlParameter("@countryId", countryId)).AsQueryable();
        }
        public IQueryable<int> fn_Security_GetEmployeesForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<int>("select * from [HR].[fn_Security_GetEmployeesForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }

        public IQueryable<fn_Security_GetProductsForEmployee> fn_Security_GetProductsForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Security_GetProductsForEmployee>("select * from [Shared].[fn_Security_GetProductsForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
        public IQueryable<fn_GetmyMedicalRep> fn_GetmyMedicalRep(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_GetmyMedicalRep>("select * from [HR].[fn_GetmyMedicalRep](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
        
        public IQueryable<fn_Security_GetProfilesForEmployee> fn_Security_GetProfilesForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Security_GetProfilesForEmployee>("select * from [Shared].[fn_Security_GetProfilesForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
        public IQueryable<fn_GetProfilesForEmployeeTemplate> fn_GetProfilesForEmployeeTemplate(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_GetProfilesForEmployeeTemplate>("select * from [Shared].[fn_GetProfilesForEmployeeTemplate](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
      

        public IQueryable<fn_Target_GetProfileTerritoryProduct> fn_Target_GetProfileTerritoryProduct(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Target_GetProfileTerritoryProduct>("select * from [Sales].[fn_Target_GetProfileTerritoryProduct](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
        public IQueryable<fn_Target_GetRemainingProductTarget> fn_Target_GetRemainingProductTarget(int countryId, int employeeId, int timedefinitionId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Target_GetRemainingProductTarget>("select * from [Sales].[fn_Target_GetRemainingProductTarget](@countryId,@employeeId, @timedefinitionId)"
                , new SqlParameter("@countryId", countryId)
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@timedefinitionId", timedefinitionId)).AsQueryable();
        }
        public IQueryable<Territory> fn_GetLeafTerritories(int countryId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<Territory>("select * from [Shared].[fn_GetLeafTerritories](@countryId)"
                , new SqlParameter("@countryId", countryId)).AsQueryable();
        }
        public IQueryable<sp_Target_GetTargetProfileSheet> sp_Target_GetTargetProfileSheet(int employeeId, DateTime selectedDate,int? teamId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<sp_Target_GetTargetProfileSheet>("exec [Sales].[sp_Target_GetTargetProfileSheet] @employeeId,@date,@teamId"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value)).AsQueryable();
        }
        public IQueryable<TemplateResult> sp_Read_Comments(string tableName) 
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<TemplateResult>("exec [Sales].[sp_ReadComments] @tableName"
                , new SqlParameter("@tableName", tableName)).AsQueryable();
            
        }
        public IQueryable<sp_Target_Analysis_GetSummary> Sp_Target_Analysis_GetSummary(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {

            Database.CommandTimeout = 0;
            return Database.SqlQuery<sp_Target_Analysis_GetSummary>("exec [Sales].[sp_Target_Analysis_GetSummary] @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)
                ).AsQueryable();
        }

        public IQueryable<fn_Target_ProductAnalysis> fn_Target_ProductAnalysis(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {

            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Target_ProductAnalysis>("select * from sales.fn_Target_ProductAnalysis (@countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId)",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)
                ).AsQueryable();
        }
        public IQueryable<sp_Target_TerritoryAnalysis> sp_Target_TerritoryAnalysis(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<sp_Target_TerritoryAnalysis>("exec [sales].[sp_Target_TerritoryAnalysis] @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)
                ).AsQueryable();
        }
        public IQueryable<sp_Target_Analysis_ProfileTarget> sp_Target_Analysis_ProfileTarget(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<sp_Target_Analysis_ProfileTarget>("exec [sales].[sp_Target_Analysis_ProfileTarget] @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)).AsQueryable();
        }

        public IQueryable<fn_InMarketSales_Analysis_GetSummary> fn_InMarketSales_Analysis_GetSummary(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_InMarketSales_Analysis_GetSummary>("select * from [sales].[fn_InMarketSales_Analysis_GetSummary]( @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId)",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)).AsQueryable();
        }
        public IQueryable<fn_InMarketSales_Analysis_DistributorSales> fn_InMarketSales_Analysis_DistributorSales(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_InMarketSales_Analysis_DistributorSales>("select * from [sales].[fn_InMarketSales_Analysis_DistributorSales]( @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId)",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)).AsQueryable();
        }
        public IQueryable<fn_InMarketSales_Analysis_SalesTrend> fn_InMarketSales_Analysis_SalesTrend(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_InMarketSales_Analysis_SalesTrend>("select * from [sales].[fn_InMarketSales_Analysis_SalesTrend]( @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId)",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)).AsQueryable();
        }
        public IQueryable<fn_InMarketSales_Analysis_SalesByAccountCategory> fn_InMarketSales_Analysis_SalesByAccountCategory(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_InMarketSales_Analysis_SalesByAccountCategory>("select * from [sales].[fn_InMarketSales_Analysis_SalesByAccountCategory]( @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId)",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)).AsQueryable();
        }
        public IQueryable<fn_InMarketSales_Analysis_SalesByTerritory> fn_InMarketSales_Analysis_SalesByTerritory(int countryId, int year, int userId, int? teamId, int? productId, int? territoryId, int? employeeId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_InMarketSales_Analysis_SalesByTerritory>("select * from [sales].[fn_InMarketSales_Analysis_SalesByTerritory]( @countryId, @year, @userId,@teamId,@productId,@territoryId,@employeeId)",
                new SqlParameter("@countryId", countryId), new SqlParameter("@year", year), new SqlParameter("@userId", userId)
                , new SqlParameter("@teamId", (object)teamId ?? DBNull.Value), new SqlParameter("@productId", (object)productId ?? DBNull.Value), new SqlParameter("@territoryId", (object)territoryId ?? DBNull.Value)
                , new SqlParameter("@employeeId", (object)employeeId ?? DBNull.Value)).AsQueryable();
        }
        public IQueryable<fn_Territory_GetParentLevelForLeaf> fn_Territory_GetParentLevelForLeaf(int countryId, int level)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Territory_GetParentLevelForLeaf>("select * from [Shared].[fn_Territory_GetParentLevelForLeaf]( @Level, @countryId)",
                new SqlParameter("@Level", level), new SqlParameter("@countryId", countryId)).AsQueryable();
        }
       
        public IQueryable<SystemMenu> fn_GetAccessMenu(int userId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<SystemMenu>("select * from [App].[fn_GetAccessMenu]( @EmployeeId)",
                new SqlParameter("@EmployeeId", userId)).AsQueryable();
        }
        public bool fn_CanAccessController(int userId, string controllerName)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<int>("select * from [App].[fn_CanAccessController]( @EmployeeId, @Controller)",
                new SqlParameter("@EmployeeId", userId), new SqlParameter("@Controller", controllerName)).FirstOrDefault() > 0;
        }
        public IQueryable<fn_Target_Profile_SupervisorSummary> fn_Target_Profile_SupervisorSummary(int year, int userId, int countryId)
        {            
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Target_Profile_SupervisorSummary>("select * from [Sales].[fn_Target_Profile_SupervisorSummary]( "+UserId+", "+year+", "+countryId+")").AsQueryable();       
        }
        public IQueryable<ScheduledJob> fn_ScheduleJobs_Get()
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<ScheduledJob>("select * from [App].[fn_ScheduleJobs_Get]()").AsQueryable();
        }
        public IQueryable<fn_MarketShare_GetTemplate> fn_MarketShare_GetTemplate(int year, int level, int countryId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_MarketShare_GetTemplate>("select * from [Sales].[fn_MarketShare_GetTemplate]( @year, @level, @countryId)",
                new SqlParameter("@year", year), new SqlParameter("@level", level), new SqlParameter("@countryId", countryId)).AsQueryable();
        }
        public int sp_RepTargetCreate(int repId, int timeDefinitionId, int createOption)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<int>(string.Format("exec crm.sp_RepTargetCreate {0},{1},{2}", repId, timeDefinitionId, createOption)).FirstOrDefault();
        }
        public IQueryable<crm_fn_Target_Universe> crm_fn_Target_Universe(int targetId) {
            Database.CommandTimeout = 0;
            var sql = string.Format("select * from crm.fn_Target_Universe({0})", targetId);
            return Database.SqlQuery<crm_fn_Target_Universe>(sql).AsQueryable();
        }
        public IQueryable<Product> crm_fnGetRepProducts(int repId, DateTime date) {
            Database.CommandTimeout = 0;
            var sql = string.Format("select * from CRM.fn_GetRepProducts({0},'{1}')", repId, GetDateFormat(date));
            return Database.SqlQuery<Product>(sql).AsQueryable();
        }
        public IQueryable<ProductPresentation> crm_fnGetRepPresentations(int repId, DateTime date)
        {
            Database.CommandTimeout = 0;
            var sql = string.Format("select * from CRM.fn_GetRepPresentations({0},'{1}')", repId, GetDateFormat(date));
            return Database.SqlQuery<ProductPresentation>(sql).AsQueryable();
        }
        public IQueryable<Account> crm_fnGetRepAccounts(int repId, DateTime date)
        {
            Database.CommandTimeout = 0;
            var sql = string.Format("select * from CRM.fn_GetRepAccounts({0},'{1}')", repId, GetDateFormat(date));
            return Database.SqlQuery<Account>(sql).AsQueryable();
        }
        public IQueryable<Doctor> crm_fnGetRepAccountDoctors(int repId, DateTime date, int accountId)
        {
            Database.CommandTimeout = 0;
            var sql = string.Format("select * from CRM.fn_GetRepAccountDoctors({0},'{1}',{2})", repId, GetDateFormat(date),accountId);
            return Database.SqlQuery<Doctor>(sql).AsQueryable();
        }
        public IQueryable<Employee> crm_fnGetRepCoaches(int repId, DateTime date)
        {
            Database.CommandTimeout = 0;
            var sql = string.Format("select * from CRM.fn_GetRepCoaches({0},'{1}')", repId, GetDateFormat(date));
            return Database.SqlQuery<Employee>(sql).AsQueryable();
        }

        string GetDateFormat(DateTime date) => date.ToString("yyyy-MMM-dd", new System.Globalization.CultureInfo("en-us"));

        public IQueryable<fn_GetEmployeesForEmployee> fn_GetEmployeesForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_GetEmployeesForEmployee>("select * from [Shared].[fn_GetEmployeesForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
        public IQueryable<fn_Security_GetTeamsForEmployee> fn_Security_GetTeamsForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Security_GetTeamsForEmployee>("select * from [Shared].[fn_Security_GetTeamsForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }
        public IQueryable<fn_Security_GetTerritoriesForEmployee> fn_Security_GetTerritoriesForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Security_GetTerritoriesForEmployee>("select * from [Shared].[fn_Security_GetTerritoriesForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }

        public IQueryable<fn_GetProductsForEmployee> fn_GetProductsForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_GetProductsForEmployee>("select * from [Shared].[fn_GetProductsForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }

        public IQueryable<fn_Security_GetDosageFormsForEmployee> fn_Security_GetDosageFormsForEmployee(int employeeId, DateTime selectedDate)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<fn_Security_GetDosageFormsForEmployee>("select * from [Shared].[fn_Security_GetDosageFormsForEmployee](@employeeId, @date)"
                , new SqlParameter("@employeeId", employeeId)
                , new SqlParameter("@date", selectedDate)).AsQueryable();
        }

        public int fn_GetWorkingDays(int profileId, int fromPeriodId,int? toPeriodId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<int>("select  [CRM].[fn_GetWorkingDays](@profileId, @fromPeriodId,@toPeriodId)"
                , new SqlParameter("@profileId", profileId)
                , new SqlParameter("@fromPeriodId", fromPeriodId)
                , new SqlParameter("@toPeriodId", (object)toPeriodId ?? DBNull.Value)).FirstOrDefault();
        }
    }

    public class crm_fn_Target_Universe
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int? DoctorId { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountTypeName { get; set; }
        public int TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public string Address { get; set; }
        public int? PotentialId { get; set; }
        public string PotentialCode { get; set; }
        public int? SpecialtyId { get; set; }
        public string SpecialtyName { get; set; }
    }

    public class fn_Target_Profile_SupervisorSummary
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public int? SupId { get; set; }
        public string SupName { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Amount { get; set; }
        public decimal? LY_Qty { get; set; }
        public decimal? LY_Amount { get; set; }
    }

    public class fn_MarketShare_GetTemplate
    {
        public string TerritoryName { get; set; }
        public decimal? MarketShare { get; set; }
    }

    public class fn_Territory_GetParentLevelForLeaf
    {
        public int TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public int? ParentTerritoryId { get; set; }
        public int Lvl { get; set; }
        public int? RootTerritoryId { get; set; }
        public string RootTerritoryName { get; set; }
    }

    public class fn_InMarketSales_Analysis_SalesByTerritory
    {
        public string TerritoryName { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Amount { get; set; }
    }

    public class fn_InMarketSales_Analysis_SalesByAccountCategory
    {
        public string CategoryName { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Amount { get; set; }
    }

    public class fn_InMarketSales_Analysis_SalesTrend
    {
        public string SalesMonthName { get; set; }
        public int SalesMonth { get; set; }
        public decimal? SalesQty { get; set; }
        public decimal? SalesAmount { get; set; }
        public decimal? TargetQty { get; set; }
        public decimal? TargetAmount { get; set; }
        public decimal? SalesQty_InTarget { get; set; }
        public decimal? SalesAmount_InTarget { get; set; }
    }

    public class fn_InMarketSales_Analysis_DistributorSales
    {
        public string DistributorName { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Amount { get; set; }
    }

    public class fn_InMarketSales_Analysis_GetSummary
    {
        public decimal? Qty { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Qty_Growth { get; set; }
        public decimal? Amount_Growth { get; set; }
        public decimal? Qty_Distributed { get; set; }
        public decimal? Amount_Distributed { get; set; }
        public decimal? Qty_Ach { get; set; }
        public decimal? Amount_Ach { get; set; }

    }

    public class sp_Target_Analysis_ProfileTarget
    {
        public int? ManagerId { get; set; }
        public string ManagerName { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int CountryId { get; set; }
        public int year { get; set; }
        public int TeamId { get; set; }
        public int ProfileId { get; set; }
        public string ProfileName { get; set; }
        public decimal? ProfileTarget { get; set; }
        public decimal? ProfileTargetGrowth { get; set; }
        public decimal? ProfileTarget_Qty { get; set; }
        public decimal? ProfileTargetGrowth_Qty { get; set; }

    }

    public class sp_Target_TerritoryAnalysis
    {
        public int CountryId { get; set; }
        public int Year { get; set; }
        public string TerritoryName { get; set; }
        public decimal? TerritoryTarget_Qty { get; set; }
        public decimal? TerritoryTarget { get; set; }
        public decimal? TerritoryTargetGrowth_Qty { get; set; }
        public decimal? TerritoryTargetGrowth { get; set; }
    }

    public class fn_Target_ProductAnalysis
    {
        public int CountryId { get; set; }
        public int Year { get; set; }
        public string ProductName { get; set; }
        public decimal? ProductTarget_Qty { get; set; }
        public decimal? ProductTarget { get; set; }
        public decimal? ProductTargetGrowth_Qty { get; set; }
        public decimal? ProductTargetGrowth { get; set; }
    }
    
  

    public class sp_Target_Analysis_GetSummary
    {
        public decimal? TargetAmount { get; set; }
        public decimal? TargetGrowth { get; set; }
        public string ProductName { get; set; }
        public decimal? ProductTarget { get; set; }
        public decimal? ProductTargetGrowth { get; set; }
        public string TerritoryName { get; set; }
        public decimal? TerritoryTarget { get; set; }
        public decimal? TerritoryTargetGrowth { get; set; }
        public string ProfileName { get; set; }
        public decimal? ProfileTarget { get; set; }
        public decimal? ProfileTargetGrowth { get; set; }
    }

    public class sp_Target_GetTargetProfileSheet
    {
        public string Team { get; set; }
        public string Sup { get; set; }
        public string area { get; set; }
        public string Product { get; set; }
        public decimal? XFactory { get; set; }
        public string category { get; set; }
        public decimal? Qty_Lastyear2 { get; set; }
        public decimal? Amount_Lastyear2 { get; set; }
        public decimal? Qty_Lastyear1 { get; set; }
        public decimal? Amount_Lastyear1 { get; set; }
        public decimal? Qty_Currentyear { get; set; }
        public decimal? Amount_Currentyear { get; set; }
        public decimal? Qty_Currentyear_estimate { get; set; }
        public decimal? Amount_Currentyear_estimate { get; set; }
        public decimal? MarketShare { get; set; }
        public decimal? Sales_Office { get; set; }
        public decimal? Line_Sales { get; set; }
        public decimal? Product_Sales { get; set; }
        public decimal? tgt_qty { get; set; }
    }

    public class fn_Security_GetProfilesForEmployee
    {
        public int EmployeeId { get; set; }
        public int ProfileId { get; set; }
        public int TeamId { get; set; }
    }

    public class fn_GetProfilesForEmployeeTemplate
    {
        public string EmployeeCode { get; set; }
        public string Employee { get; set; }
        public string NationalIDNumber { get; set; }
        public string Department { get; set; }
        public string ManagerCode { get; set; } 
        public bool? CanVisit { get; set; } 
        public string JobTitle { get; set; }
        public string Team { get; set; }
        public string ProfileCode { get; set; }
        public string ProfileName { get; set; }
        public int EmployeeId { get; set; }
        public int ProfileId { get; set; }
        public int TeamId { get; set; }
        public int EmployeeHistoryId { get; set; } 
    }

    public class fn_GetImportedFileMappedAccounts
    {
        public int FileRecordId { get; set; }
        public int FileId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerTerritory { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountTypeName { get; set; }
        public string CategoryName { get; set; }
        public string TerritoryName { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int UpdatedBy { get; set; }
        public string UpdatedByLogonName { get; set; }
    }
    public class fn_GetImportedFileMappedProducts
    {
        public int FileRecordId { get; set; }
        public int FileId { get; set; }
        public string DistributorDFCode { get; set; }
        public string DistributorDFName { get; set; }
        public int DosageFormId { get; set; }
        public string DosageFormName { get; set; }
        public string ProductName { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int UpdatedBy { get; set; }
        public string UpdatedByLogonName { get; set; }
    }
    public class fn_GetImportedFileSummary
    {
        public int FileId { get; set; }
        public string Factor { get; set; }
        public int Original { get; set; }
        public int? Current { get; set; }
    }
    public class fn_GetImportedFileDetails
    {
        public int FileId { get; set; }
        public string DistributorName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string PeriodName { get; set; }
        public string ImportMode { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public string UpdatedByLogonName { get; set; }
        public int UploadProcessingTime_Hr { get; set; }

    }
    public class fnTargetProduct_Export
    {
        public int CountryId { get; set; }
        public string Line { get; set; }
        public string Group { get; set; }
        public int ProductId { get; set; }
        public string Product { get; set; }
        public decimal? Price { get; set; }
        public decimal? LastYearQty { get; set; }
        public decimal? LastYearAmount { get; set; }
        public decimal? CurrentYearQty { get; set; }
        public decimal? CurrentYearAmount { get; set; }
    }
    public class fn_TargetProductPhasing_Export
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string PeriodName { get; set; }
        public decimal? Percentage { get; set; }
    }
    public class fn_Security_GetProductsForEmployee
    {
        public int TeamId { get; set; }
        public int ProductId { get; set; }
    }
    public class fn_Target_GetProfileTerritoryProduct
    {
        public int ProfileId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int TerritoryId { get; set; }
        public int RootTerritoryId { get; set; }
        public string RootTerritoryName { get; set; }

    }
    public class fn_Target_GetRemainingProductTarget
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
    }

    public class fn_GetmyMedicalRep 
    {
        public int employeeid { get; set; } 
        public string EmployeeName { get; set; } 
    }
    
    public class fn_GetEmployeesForEmployee
    {
        public int EmployeeId { get; set; } 
        public string EmployeeName { get; set; }
    }

    public class fn_Security_GetTeamsForEmployee
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; } 
    }

    public class fn_Security_GetTerritoriesForEmployee
    {
        public int TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public string TerritoryCode { get; set; } 
    }
    public class fn_GetProductsForEmployee
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; } 
    }

    public class fn_Security_GetDosageFormsForEmployee
    {
        public int DosageFormId { get; set; } 
        public string DosageFormName { get; set; }
    }
}
