﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace DB.ORM.DB
{
    public class EntityAudit
    {

        public const string CreatedByProperty = "CreatedById";
        public const string CreatedAtProperty = "CreationDate";

        public const string LastModifiedByProperty = "LastModifiedById";
        public const string LastModifiedAtProperty = "LastModifiedDate";
    }
    public partial class DBModel : DbContext
    {
        public int UserId { get; set; }
        public override int SaveChanges()
        {
            var autoDetectChanges = Configuration.AutoDetectChangesEnabled;
            try
            {
                Configuration.AutoDetectChangesEnabled = false;
                ChangeTracker.DetectChanges();
                foreach (
                    var entry in
                    ChangeTracker.Entries()
                                 .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
                    ApplyAuditLogging(entry, UserId);
                ChangeTracker.DetectChanges();
                Configuration.ValidateOnSaveEnabled = false;
                Configuration.AutoDetectChangesEnabled = autoDetectChanges;
                //var audit = new Audit();
                //audit.PreSaveChanges(this);
                int rowAffecteds = base.SaveChanges();
                //audit.PostSaveChanges();
                //if (audit.Configuration.AutoSavePreAction != null)
                //{
                //    audit.Configuration.AutoSavePreAction(this, audit);
                //    base.SaveChanges();
                //}
                return rowAffecteds;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            }
        }
        private static void ApplyAuditLogging(DbEntityEntry entityEntry, int userId)
        {
            var type = entityEntry.Entity.GetType();
            if (entityEntry.State == EntityState.Added)
            {
                SetPropValue(entityEntry, type, EntityAudit.CreatedByProperty, userId);
                SetPropValue(entityEntry, type, EntityAudit.CreatedAtProperty, DateTime.Now);
            }
            if (entityEntry.State == EntityState.Modified /*&& userId.HasValue*/)
            {
                SetPropValue(entityEntry, type, EntityAudit.LastModifiedByProperty, userId);
                SetPropValue(entityEntry, type, EntityAudit.LastModifiedAtProperty, DateTime.Now);
            }
        }
        public static void SetPropValue(DbEntityEntry entityEntry, Type type, string prop, object newVal)
        {
            if (type.GetProperty(prop) != null)
                entityEntry.Property(prop).CurrentValue = newVal;
        }
    }
}
