namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Temp.Target_Sheet")]
    public partial class Target_Sheet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        public int? TeamId { get; set; }

        public int? ProductId { get; set; }

        public int? RootTerritoryId { get; set; }

        public int? RootCategoryId { get; set; }

        public int? ProfileId { get; set; }

        public string ProfileCode { get; set; }

        public decimal? price { get; set; }

        public string area { get; set; }

        public string productName { get; set; }

        public string TeamName { get; set; }

        public string categoryName { get; set; }

        public string RepName { get; set; }

        public string ManagerName { get; set; }

        public decimal? Qty_Lastyear2 { get; set; }

        public decimal? Amount_Lastyear2 { get; set; }

        public decimal? Qty_Lastyear1 { get; set; }

        public decimal? Amount_Lastyear1 { get; set; }

        public decimal? Qty_Currentyear { get; set; }

        public decimal? Amount_Currentyear { get; set; }

        public decimal? Qty_Currentyear_estimate { get; set; }

        public decimal? Amount_Currentyear_estimate { get; set; }

        public decimal? Sales_Office { get; set; }

        public decimal? MarketShare { get; set; }

        public decimal? Line_Sales { get; set; }

        public decimal? Product_Sales { get; set; }
    }
}
