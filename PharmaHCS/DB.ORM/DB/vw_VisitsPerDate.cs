namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_VisitsPerDate")]
    public partial class vw_VisitsPerDate
    {
        [Key]
        [Column(Order = 0)]
        public Guid VisitId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? VisitDate { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [StringLength(103)]
        public string EmployeeName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        public int? AccountsCount { get; set; }

        public int? DoctorsCount { get; set; }

        public int? TerritoriesCount { get; set; }
    }
}
