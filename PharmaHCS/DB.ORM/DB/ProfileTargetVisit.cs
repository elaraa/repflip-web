namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.ProfileTargetVisit")]
    public partial class ProfileTargetVisit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProfileTargetVisit()
        {
            ProfileTargetVisitDetails = new HashSet<ProfileTargetVisitDetail>();
        }

        [Key]
        public int ProfileTargetId { get; set; }

        public int FromTimeDefinitionId { get; set; }

        public int? ToTimeDefinitionId { get; set; }

        public int ProfileId { get; set; }

        public int CountryId { get; set; }

        [StringLength(1)]
        public string ManagerResponse { get; set; }

        public int? ManagerId { get; set; }

        public string ManagerComment { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Country Country { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual TeamProfile TeamProfile { get; set; }

        public virtual TimeDefinition TimeDefinition { get; set; }

        public virtual TimeDefinition TimeDefinition1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTargetVisitDetail> ProfileTargetVisitDetails { get; set; }
    }
}
