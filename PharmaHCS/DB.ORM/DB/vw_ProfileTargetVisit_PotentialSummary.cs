namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_ProfileTargetVisit_PotentialSummary")]
    public partial class vw_ProfileTargetVisit_PotentialSummary
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProfileTargetId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PotentialId { get; set; }

        [StringLength(50)]
        public string PotentialCode { get; set; }

        [Key]
        [Column(Order = 2)]
        public string PotentialName { get; set; }

        public int? TargetVisit { get; set; }

        public int? TargetDoctors { get; set; }
    }
}
