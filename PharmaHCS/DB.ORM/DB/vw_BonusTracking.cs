namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_BonusTracking")]
    public partial class vw_BonusTracking
    {
        public decimal? SalesQty { get; set; }

        public decimal? BonusQty { get; set; }

        public decimal? Qty { get; set; }

        public decimal? BQty { get; set; }

        public string InvoiceNumber { get; set; }

        [Key]
        [Column(Order = 0)]
        public string AccountName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IsFollow { get; set; }
    }
}
