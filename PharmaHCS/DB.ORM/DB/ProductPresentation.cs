namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.ProductPresentation")]
    public partial class ProductPresentation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductPresentation()
        {
            VisitPresentations = new HashSet<VisitPresentation>();
        }

        [Key]
        public int PresentationId { get; set; }

        [Required]
        public string PresentationName { get; set; }

        public string FilePath { get; set; }

        [StringLength(50)]
        public string Ext { get; set; }

        public int ProductId { get; set; }

        public bool Active { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Product Product { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitPresentation> VisitPresentations { get; set; }
    }
}
