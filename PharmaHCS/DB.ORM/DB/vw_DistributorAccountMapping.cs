namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_DistributorAccountMapping")]
    public partial class vw_DistributorAccountMapping
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DistributorAccountMappingId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DistributorId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        public string AccountName { get; set; }

        public string AccountNameFile { get; set; }

        public string AccountAddress { get; set; }

        public string AccountPhone { get; set; }

        public int? FileId { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
