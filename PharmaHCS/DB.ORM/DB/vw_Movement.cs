namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_Movement")]
    public partial class vw_Movement
    {
        [Key]
        [Column(Order = 0)]
        public Guid VisitId { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime VisitDate { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string AccountName { get; set; }

        [Key]
        [Column(Order = 4)]
        public string AccountTypeName { get; set; }

        [StringLength(50)]
        public string AccountTypeCode { get; set; }

        public int? DoctorId { get; set; }

        public string DoctorName { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }
    }
}
