namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_Target_Universe")]
    public partial class vw_Target_Universe
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        public string AccountName { get; set; }

        public int? DoctorId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string AccountTypeName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TerritoryId { get; set; }

        [Key]
        [Column(Order = 4)]
        public string TerritoryName { get; set; }

        public string Address { get; set; }

        public int? PotentialId { get; set; }

        [StringLength(50)]
        public string PotentialCode { get; set; }

        public int? SpecialtyId { get; set; }

        public string SpecialtyName { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProfileId { get; set; }

        public int? FromTimeDefinitionId { get; set; }

        public int? ToTimeDefinitionId { get; set; }
    }
}
