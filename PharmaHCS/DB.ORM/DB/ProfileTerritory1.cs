namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.ProfileTerritory")]
    public partial class ProfileTerritory1
    {
        [Key]
        public int ProfileTerritoryId { get; set; }

        public int ProfileId { get; set; }

        public int TerritoryId { get; set; }

        public decimal Percentage { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual TeamProfile TeamProfile { get; set; }

        public virtual Territory Territory { get; set; }
    }
}
