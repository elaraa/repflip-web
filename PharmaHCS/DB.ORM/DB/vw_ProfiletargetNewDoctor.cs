namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_ProfiletargetNewDoctor")]
    public partial class vw_ProfiletargetNewDoctor
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TargetDoctorApprovalId { get; set; }

        public int? DoctorId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string DoctorName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PotentialId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PotentialName { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SpecialtyId { get; set; }

        [Key]
        [Column(Order = 5)]
        public string SpecialtyName { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(1)]
        public string Gender { get; set; }

        public int? AccountId { get; set; }

        [Key]
        [Column(Order = 7)]
        public string AccountName { get; set; }

        [StringLength(50)]
        public string AccountCode { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TerritoryId { get; set; }

        [Key]
        [Column(Order = 9)]
        public string TerritoryName { get; set; }

        [Key]
        [Column(Order = 10)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Frequency { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        [Key]
        [Column(Order = 11)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountTypeId { get; set; }

        [Key]
        [Column(Order = 12)]
        public string AccountTypeName { get; set; }

        public int? ProfileId { get; set; }

        public string ProfileName { get; set; }

        public int? EmployeeId { get; set; }

        [StringLength(50)]
        public string EmployeeName { get; set; }

        public int? SupervisorId { get; set; }

        [StringLength(50)]
        public string SupervisorName { get; set; }

        [StringLength(1)]
        public string AdminApprove { get; set; }

        [StringLength(1)]
        public string ApprovalStatus { get; set; }

        [Key]
        [Column(Order = 13)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        [Key]
        [Column(Order = 14)]
        public string CountryName { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        public string Address { get; set; }

        public int? AccountPotentialId { get; set; }

        [Key]
        [Column(Order = 15)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CategoryId { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }
    }
}
