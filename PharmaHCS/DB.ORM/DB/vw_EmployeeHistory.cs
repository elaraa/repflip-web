namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HR.vw_EmployeeHistory")]
    public partial class vw_EmployeeHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeHistoryId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [StringLength(50)]
        public string EmployeeCode { get; set; }

        [StringLength(50)]
        public string Employee { get; set; }

        [StringLength(103)]
        public string EmployeeName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobTitleId { get; set; }

        public string JobTitle { get; set; }

        public string JobTitleName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartmentId { get; set; }

        public string Department { get; set; }

        public string DepartmentName { get; set; }

        public int? ManagerId { get; set; }

        [StringLength(50)]
        public string ManagerCode { get; set; }

        [StringLength(103)]
        public string ManagerName { get; set; }

        public short? OrganizationLevel { get; set; }

        [Key]
        [Column(Order = 4, TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        public int? TeamId { get; set; }

        public string Team { get; set; }

        public string TeamName { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }

        public bool? CanVisit { get; set; }

        [StringLength(50)]
        public string NationalIDNumber { get; set; }
    }
}
