namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HR.EmployeeVacation")]
    public partial class EmployeeVacation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeVacationId { get; set; }

        public int EmployeeId { get; set; }

        public int VacationTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime ToDate { get; set; }

        public decimal NumberOfDays { get; set; }

        [Required]
        public string Comment { get; set; }

        [StringLength(1)]
        public string ManagerResponse { get; set; }

        public string ManagerComment { get; set; }

        public DateTime? ManagerResponseDate { get; set; }

        public int? ManagerId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual Employee Employee3 { get; set; }

        public virtual VacationType VacationType { get; set; }
    }
}
