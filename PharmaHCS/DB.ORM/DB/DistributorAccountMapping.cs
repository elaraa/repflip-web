namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.DistributorAccountMapping")]
    public partial class DistributorAccountMapping
    {
        public int DistributorAccountMappingId { get; set; }

        public int DistributorId { get; set; }

        public int AccountId { get; set; }

        [Required]
        public string AccountCode { get; set; }

        public string AccountName { get; set; }

        public string AccountAddress { get; set; }

        public string AccountPhone { get; set; }

        public int? FileId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Distributor Distributor { get; set; }

        public virtual Account Account { get; set; }

        public virtual ImportedFile ImportedFile { get; set; }
    }
}
