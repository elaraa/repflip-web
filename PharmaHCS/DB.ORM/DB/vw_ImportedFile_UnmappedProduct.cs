namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_ImportedFile_UnmappedProduct")]
    public partial class vw_ImportedFile_UnmappedProduct
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RecordId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FileId { get; set; }

        public string DosageFormCode { get; set; }

        public string DosageFormName { get; set; }

        public int? AffectedRecords { get; set; }

        public int? ProductId { get; set; }
    }
}
