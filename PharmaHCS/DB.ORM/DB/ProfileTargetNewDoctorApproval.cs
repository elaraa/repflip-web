namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.ProfileTargetNewDoctorApproval")]
    public partial class ProfileTargetNewDoctorApproval
    {
        [Key]
        public int TargetDoctorApprovalId { get; set; }

        public int? ProfileId { get; set; }

        public int? DoctorId { get; set; }

        [Required]
        public string DoctorName { get; set; }

        public int PotentialId { get; set; }

        public int SpecialtyId { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        public int? AccountId { get; set; }

        [Required]
        public string AccountName { get; set; }

        [StringLength(50)]
        public string AccountCode { get; set; }

        public string Address { get; set; }

        public int AccountTypeId { get; set; }

        public int TerritoryId { get; set; }

        public int? AccountPotentialId { get; set; }

        public int CategoryId { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public int Frequency { get; set; }

        [StringLength(1)]
        public string ApprovalStatus { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public int CountryId { get; set; }

        [StringLength(1)]
        public string AdminApprove { get; set; }

        public int? SupervisorId { get; set; }

        public virtual Doctor Doctor { get; set; }

        public virtual Potential Potential { get; set; }

        public virtual Potential Potential1 { get; set; }

        public virtual Account Account { get; set; }

        public virtual AccountCategory AccountCategory { get; set; }

        public virtual AccountType AccountType { get; set; }

        public virtual Country Country { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual TeamProfile TeamProfile { get; set; }

        public virtual Territory Territory { get; set; }
    }
}
