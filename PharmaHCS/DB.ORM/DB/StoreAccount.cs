namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.StoreAccount")]
    public partial class StoreAccount
    {
        public int StoreAccountId { get; set; }

        public int AccountId { get; set; }

        public int FromTimeId { get; set; }

        public int? ToTimeId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Account Account { get; set; }

        public virtual TimeDefinition TimeDefinition { get; set; }

        public virtual TimeDefinition TimeDefinition1 { get; set; }
    }
}
