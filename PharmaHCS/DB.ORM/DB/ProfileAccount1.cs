namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.ProfileAccount")]
    public partial class ProfileAccount1
    {
        [Key]
        public int ProfileAccountId { get; set; }

        public int ProfileId { get; set; }

        public int AccountId { get; set; }

        public decimal Perccentage { get; set; }

        public int? ProductId { get; set; }

        public int? DosageFromId { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Account Account { get; set; }

        public virtual Product Product { get; set; }

        public virtual ProductDosageForm ProductDosageForm { get; set; }

        public virtual TeamProfile TeamProfile { get; set; }
    }
}
