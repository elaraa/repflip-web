namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("App.vw_User")]
    public partial class vw_User
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string EmployeeName { get; set; }

        public string LoginId { get; set; }

        public string Picture { get; set; }

        public int? RoleId { get; set; }

        public string RoleName { get; set; }

        public bool? IsAdmin { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool Active { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
