namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.MarketShareTerritory")]
    public partial class MarketShareTerritory
    {
        public int MarketShareTerritoryId { get; set; }

        public int MarketShareId { get; set; }

        public int TerritoryId { get; set; }

        public int? CategoryId { get; set; }

        public decimal MarketShare { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual MarketShare MarketShare1 { get; set; }

        public virtual AccountCategory AccountCategory { get; set; }

        public virtual Territory Territory { get; set; }
    }
}
