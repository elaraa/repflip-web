namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shared.AccountType")]
    public partial class AccountType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccountType()
        {
            ProfileTargetNewDoctorApprovals = new HashSet<ProfileTargetNewDoctorApproval>();
        }

        public int AccountTypeId { get; set; }

        [Required]
        public string AccountTypeName { get; set; }

        [StringLength(50)]
        public string AccountTypeCode { get; set; }

        public bool MustHaveDoctor { get; set; }

        public bool TargetAndPlanWithDoctor { get; set; }

        public bool IncludeInSales { get; set; }

        public int? CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTargetNewDoctorApproval> ProfileTargetNewDoctorApprovals { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Country Country { get; set; }
    }
}
