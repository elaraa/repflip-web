namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shared.vw_DosageForm")]
    public partial class vw_DosageForm
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DosageFormId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string DosageFormCode { get; set; }

        [Key]
        [Column(Order = 2)]
        public string DosageFormName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductId { get; set; }

        [StringLength(103)]
        public string ProductName { get; set; }

        public int? Size { get; set; }

        public int? UnitId { get; set; }

        public string UnitName { get; set; }

        public decimal? PackFactor { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool Active { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }
    }
}
