namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_RepCovarageTypeCategoryAnalyzer")]
    public partial class vw_RepCovarageTypeCategoryAnalyzer
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProfileId { get; set; }

        [StringLength(50)]
        public string ProfileCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string EmployeeName { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int? AccountTypeId { get; set; }

        public string AccountTypeName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TimeDefinitionId { get; set; }

        [Key]
        [Column(Order = 4)]
        public string PeriodName { get; set; }

        public int? VisitsAccountsTarget { get; set; }

        public int? VisitsAccounts { get; set; }
    }
}
