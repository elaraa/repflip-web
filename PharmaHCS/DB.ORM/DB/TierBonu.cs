namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.TierBonus")]
    public partial class TierBonu
    {
        [Key]
        public int TierBonusId { get; set; }

        public int ProductId { get; set; }

        public int TierId { get; set; }

        public decimal? SalesQty { get; set; }

        public decimal? BonusQty { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromPeriod { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToPeriod { get; set; }

        public virtual Tier Tier { get; set; }

        public virtual Product Product { get; set; }
    }
}
