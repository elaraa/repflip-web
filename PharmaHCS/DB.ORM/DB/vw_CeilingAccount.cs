namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_CeilingAccount")]
    public partial class vw_CeilingAccount
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CeilingAccountExceptionId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string AccountName { get; set; }

        [StringLength(50)]
        public string ProductName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string FromPeriod { get; set; }

        public string ToPeriod { get; set; }
    }
}
