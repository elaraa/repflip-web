namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_ProductPrice")]
    public partial class vw_ProductPrice
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductPriceId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductId { get; set; }

        [StringLength(103)]
        public string ProductName { get; set; }

        [StringLength(16)]
        public string PriceType { get; set; }

        public string PriceTypeName { get; set; }

        public int? DosageFormId { get; set; }

        public string DosageFormName { get; set; }

        public int? DistributorId { get; set; }

        public string DistributorName { get; set; }

        public int? TerritoryId { get; set; }

        public string TerritoryName { get; set; }

        public int? AccountCategoryId { get; set; }

        public string CategoryName { get; set; }

        public int? AccountId { get; set; }

        public string AccountName { get; set; }

        [Key]
        [Column(Order = 2, TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        [Key]
        [Column(Order = 3)]
        public decimal Price { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
