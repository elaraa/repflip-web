namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.DistributorProductMapping")]
    public partial class DistributorProductMapping
    {
        public int DistributorProductMappingId { get; set; }

        public int DistributorId { get; set; }

        public int DosageFormId { get; set; }

        [Required]
        public string DosageFormCode { get; set; }

        public string DosageFormName { get; set; }

        public int? FileId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Distributor Distributor { get; set; }

        public virtual ImportedFile ImportedFile { get; set; }

        public virtual ProductDosageForm ProductDosageForm { get; set; }
    }
}
