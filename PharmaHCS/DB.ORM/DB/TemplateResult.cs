namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Temp.TemplateResult")]
    public partial class TemplateResult
    {
        [Key]
        [StringLength(50)]
        public string RowIndex { get; set; }

        [StringLength(50)]
        public string ProfileCode { get; set; }

        [StringLength(50)]
        public string AccountCode { get; set; }

        [StringLength(50)]
        public string Percentage { get; set; }

        [StringLength(50)]
        public string ProductCode { get; set; }

        [StringLength(50)]
        public string DosageFromCode { get; set; }

        [StringLength(50)]
        public string InvalidProfileCode { get; set; }

        [StringLength(50)]
        public string InvalidAccountCode { get; set; }

        [StringLength(50)]
        public string InvalidProductCode { get; set; }

        [StringLength(50)]
        public string InvalidDosageFormCode { get; set; }

        [StringLength(50)]
        public string IsDuplicated { get; set; }

        [StringLength(50)]
        public string IsOverlapped { get; set; }

        [StringLength(50)]
        public string IsExceedPercentage { get; set; }

        [StringLength(50)]
        public string ExceededPercentage { get; set; }
    }
}
