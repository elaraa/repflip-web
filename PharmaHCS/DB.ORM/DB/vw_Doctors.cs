namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_Doctors")]
    public partial class vw_Doctors
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DoctorId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string DoctorName { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool Approved { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PotentialName { get; set; }

        [Key]
        [Column(Order = 4)]
        public string SpecialtyName { get; set; }

        [Key]
        [Column(Order = 5)]
        public string CountryName { get; set; }
    }
}
