namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_CeilingAccountException")]
    public partial class vw_CeilingAccountException
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CeilingAccountExceptionId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        public int? ProductId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FromTimeId { get; set; }

        public int? ToTimeId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string AccountName { get; set; }

        [StringLength(50)]
        public string ProductName { get; set; }
    }
}
