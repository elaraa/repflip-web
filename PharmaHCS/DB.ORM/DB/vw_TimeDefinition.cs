namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shared.vw_TimeDefinition")]
    public partial class vw_TimeDefinition
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TimeDefinitionId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string PeriodName { get; set; }

        [Key]
        [Column(Order = 2, TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Key]
        [Column(Order = 3, TypeName = "date")]
        public DateTime ToDate { get; set; }

        public string QuarterName { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Year { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
