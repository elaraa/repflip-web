namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.ProfileTargetVisitDetail")]
    public partial class ProfileTargetVisitDetail
    {
        [Key]
        public int ProfileTargetDetailId { get; set; }

        public int ProfileTargetId { get; set; }

        public int AccountId { get; set; }

        public int? DoctorId { get; set; }

        public int PotentialId { get; set; }

        public int Frequency { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Doctor Doctor { get; set; }

        public virtual Potential Potential { get; set; }

        public virtual ProfileTargetVisit ProfileTargetVisit { get; set; }

        public virtual Account Account { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }
    }
}
