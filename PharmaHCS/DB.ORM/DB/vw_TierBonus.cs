namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_TierBonus")]
    public partial class vw_TierBonus
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TierBonusId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TierId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string TierName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductId { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string ProductName { get; set; }

        [StringLength(50)]
        public string ProductCode { get; set; }

        public decimal? SalesQty { get; set; }

        public decimal? BonusQty { get; set; }
    }
}
