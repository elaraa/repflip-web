namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Report.tb_EmployeeStructurewProfile")]
    public partial class tb_EmployeeStructurewProfile
    {
        public int? TeamId { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [StringLength(50)]
        public string EmployeeCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string EmployeeName { get; set; }

        public int? ProfileId { get; set; }

        [StringLength(50)]
        public string ProfileCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TimeDefinitionId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PeriodName { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SupId { get; set; }

        [StringLength(50)]
        public string SupCode { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(50)]
        public string SupName { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PMId { get; set; }

        [StringLength(50)]
        public string PMCode { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(50)]
        public string PMName { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BUMId { get; set; }

        [StringLength(50)]
        public string BUMCode { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(50)]
        public string BUMName { get; set; }

        [Key]
        [Column(Order = 10)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PMDId { get; set; }

        [StringLength(50)]
        public string PMDCode { get; set; }

        [Key]
        [Column(Order = 11)]
        [StringLength(50)]
        public string PMDName { get; set; }

        [Key]
        [Column(Order = 12)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SODId { get; set; }

        [StringLength(50)]
        public string SODCode { get; set; }

        [Key]
        [Column(Order = 13)]
        [StringLength(50)]
        public string SODName { get; set; }
    }
}
