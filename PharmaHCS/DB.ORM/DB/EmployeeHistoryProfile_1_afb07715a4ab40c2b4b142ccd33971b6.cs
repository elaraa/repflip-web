namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Temp.EmployeeHistoryProfile_1_afb07715a4ab40c2b4b142ccd33971b6")]
    public partial class EmployeeHistoryProfile_1_afb07715a4ab40c2b4b142ccd33971b6
    {
        public int? AutoID { get; set; }

        [Column("Employee Code")]
        public string Employee_Code { get; set; }

        [Column("Employee Name")]
        public string Employee_Name { get; set; }

        public string NationalID { get; set; }

        public string Department { get; set; }

        [Column("Manager Code")]
        public string Manager_Code { get; set; }

        [Column("Can Visit")]
        public string Can_Visit { get; set; }

        [Column("Job Title")]
        public string Job_Title { get; set; }

        public string Team { get; set; }

        [Column("Profile Code")]
        public string Profile_Code { get; set; }

        [Column("Profile Name")]
        public string Profile_Name { get; set; }

        [Key]
        [Column(Order = 0)]
        public bool Status { get; set; }

        [Key]
        [Column(Order = 1)]
        public string InvalidDepartment { get; set; }

        [Key]
        [Column(Order = 2)]
        public string InvalidJobTitle { get; set; }

        [Key]
        [Column(Order = 3)]
        public string InvalidTeam { get; set; }

        [Key]
        [Column(Order = 4)]
        public string RequiredTeamForProfile { get; set; }

        [Key]
        [Column(Order = 5)]
        public string InvalidManagerCode { get; set; }

        [Key]
        [Column(Order = 6)]
        public string IsDuplicated { get; set; }

        [Key]
        [Column(Order = 7)]
        public string IsOverlappedProfile { get; set; }

        [Key]
        [Column(Order = 8)]
        public string IsOverlappedHistory { get; set; }
    }
}
