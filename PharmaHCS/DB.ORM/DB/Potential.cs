namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.Potential")]
    public partial class Potential
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Potential()
        {
            Doctors = new HashSet<Doctor>();
            ProfileTargetNewDoctorApprovals = new HashSet<ProfileTargetNewDoctorApproval>();
            ProfileTargetNewDoctorApprovals1 = new HashSet<ProfileTargetNewDoctorApproval>();
            ProfileTargetVisitDetails = new HashSet<ProfileTargetVisitDetail>();
        }

        public int PotentialId { get; set; }

        [Required]
        public string PotentialName { get; set; }

        [StringLength(50)]
        public string PotentialCode { get; set; }

        public int? CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doctor> Doctors { get; set; }

        public virtual Country Country { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTargetNewDoctorApproval> ProfileTargetNewDoctorApprovals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTargetNewDoctorApproval> ProfileTargetNewDoctorApprovals1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileTargetVisitDetail> ProfileTargetVisitDetails { get; set; }
    }
}
