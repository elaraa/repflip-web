namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shared.ProductDosageForm")]
    public partial class ProductDosageForm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductDosageForm()
        {
            CeilingDosageAccounts = new HashSet<CeilingDosageAccount>();
            DistributorProductMappings = new HashSet<DistributorProductMapping>();
            ImportedFileRecords = new HashSet<ImportedFileRecord>();
            InMarketSales = new HashSet<InMarketSale>();
            InMarketSalesBonus = new HashSet<InMarketSalesBonu>();
            ProductPrices = new HashSet<ProductPrice>();
            ProfileAccount1 = new HashSet<ProfileAccount1>();
            Targets = new HashSet<Target>();
            TargetHistories = new HashSet<TargetHistory>();
        }

        [Key]
        public int DosageFormId { get; set; }

        [Required]
        [StringLength(50)]
        public string DosageFormCode { get; set; }

        [Required]
        public string DosageFormName { get; set; }

        public int ProductId { get; set; }

        public int? Size { get; set; }

        public int? UnitId { get; set; }

        public decimal? PackFactor { get; set; }

        public bool Active { get; set; }

        public int? CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CeilingDosageAccount> CeilingDosageAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DistributorProductMapping> DistributorProductMappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportedFileRecord> ImportedFileRecords { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InMarketSale> InMarketSales { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InMarketSalesBonu> InMarketSalesBonus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductPrice> ProductPrices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileAccount1> ProfileAccount1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Target> Targets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TargetHistory> TargetHistories { get; set; }

        public virtual Country Country { get; set; }

        public virtual DosageFormUnit DosageFormUnit { get; set; }

        public virtual Product Product { get; set; }
    }
}
