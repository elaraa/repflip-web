namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.ProfileTerritory")]
    public partial class ProfileTerritory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProfileTerritoryId { get; set; }

        public int ProfileId { get; set; }

        public int TerritoryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual TeamProfile TeamProfile { get; set; }

        public virtual Territory Territory { get; set; }
    }
}
