namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.vw_LinkedAccountSales")]
    public partial class vw_LinkedAccountSales
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string AccountName { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LinkedAccountId { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DoctorId { get; set; }

        [StringLength(50)]
        public string AccountTypeCode { get; set; }

        [Key]
        [Column(Order = 4)]
        public string LinkedAccount { get; set; }

        [Key]
        [Column(Order = 5)]
        public string TerritoryName { get; set; }

        public string Address { get; set; }
    }
}
