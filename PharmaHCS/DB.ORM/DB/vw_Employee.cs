namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HR.vw_Employee")]
    public partial class vw_Employee
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeId { get; set; }

        [StringLength(50)]
        public string EmployeeCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string EmployeeName { get; set; }

        public short? OrganizationLevel { get; set; }

        public string Picture { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobTitleId { get; set; }

        public string JobTitleName { get; set; }

        public int? DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool Active { get; set; }

        public int? CountryId { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
