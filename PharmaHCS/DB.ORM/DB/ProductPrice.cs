namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.ProductPrice")]
    public partial class ProductPrice
    {
        public int ProductPriceId { get; set; }

        public int ProductId { get; set; }

        public int? DosageFormId { get; set; }

        public int? DistributorId { get; set; }

        public int? TerritoryId { get; set; }

        public int? AccountId { get; set; }

        public int? AccountCategoryId { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        public decimal Price { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Distributor Distributor { get; set; }

        public virtual Account Account { get; set; }

        public virtual AccountCategory AccountCategory { get; set; }

        public virtual Product Product { get; set; }

        public virtual ProductDosageForm ProductDosageForm { get; set; }

        public virtual Territory Territory { get; set; }
    }
}
