namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("App.ScheduledJob")]
    public partial class ScheduledJob
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobId { get; set; }

        [Required]
        public string JobName { get; set; }

        [Required]
        [StringLength(50)]
        public string Module { get; set; }

        [Required]
        public string StoredProcedure { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        [Required]
        [StringLength(1)]
        public string ScheduleType { get; set; }

        public bool Active { get; set; }

        public DateTime? LastRunTime { get; set; }

        public decimal? LastRunSpan { get; set; }
    }
}
