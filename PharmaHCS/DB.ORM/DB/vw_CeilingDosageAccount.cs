namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_CeilingDosageAccount")]
    public partial class vw_CeilingDosageAccount
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CeilingDosageAccountId { get; set; }

        public int? AccountId { get; set; }

        public string AccountName { get; set; }

        public int? TerritoryId { get; set; }

        public string TerritoryName { get; set; }

        public bool? Active { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DosageFormId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string DosageFormName { get; set; }

        [Key]
        [Column(Order = 3)]
        public decimal CeilingQty { get; set; }

        [Key]
        [Column(Order = 4)]
        public string FromPeriod { get; set; }

        public string ToPeriod { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
