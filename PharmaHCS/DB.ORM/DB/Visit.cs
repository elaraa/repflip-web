namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CRM.Visit")]
    public partial class Visit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Visit()
        {
            VisitCoaches = new HashSet<VisitCoach>();
            VisitComments = new HashSet<VisitComment>();
            VisitDoctors = new HashSet<VisitDoctor>();
            VisitPresentations = new HashSet<VisitPresentation>();
            VisitProducts = new HashSet<VisitProduct>();
            VisitRequests = new HashSet<VisitRequest>();
        }

        public Guid VisitId { get; set; }

        public DateTime VisitDate { get; set; }

        public int EmployeeId { get; set; }

        public int AccountId { get; set; }

        public bool IsReviewed { get; set; }

        public int? ReviewedById { get; set; }

        public bool IsFalseVisit { get; set; }

        public string FalseVisitReason { get; set; }

        public int? MarkedFalseById { get; set; }

        public int EntryTypeId { get; set; }

        public string EntrySource { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public int CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Account Account { get; set; }

        public virtual Country Country { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Employee Employee2 { get; set; }

        public virtual Employee Employee3 { get; set; }

        public virtual Employee Employee4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitCoach> VisitCoaches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitComment> VisitComments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitDoctor> VisitDoctors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitPresentation> VisitPresentations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitProduct> VisitProducts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VisitRequest> VisitRequests { get; set; }
    }
}
