namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.vw_TargetProfile_Details")]
    public partial class vw_TargetProfile_Details
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TargetId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CountryId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Year { get; set; }

        public int? ProfileId { get; set; }

        [StringLength(50)]
        public string ProfileCode { get; set; }

        [StringLength(103)]
        public string EmployeeName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TeamId { get; set; }

        [Key]
        [Column(Order = 4)]
        public string TeamName { get; set; }

        public int? ProductId { get; set; }

        [StringLength(103)]
        public string ProductName { get; set; }

        public int? TerritoryId { get; set; }

        [Key]
        [Column(Order = 5)]
        public string TerritoryName { get; set; }

        public int? TimeDefinitionId { get; set; }

        [Key]
        [Column(Order = 6)]
        public string PeriodName { get; set; }

        public decimal? Qty { get; set; }

        public decimal? Amount { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        [StringLength(50)]
        public string UpdatedByLogonName { get; set; }
    }
}
