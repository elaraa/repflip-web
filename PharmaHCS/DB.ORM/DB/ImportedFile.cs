namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sales.ImportedFile")]
    public partial class ImportedFile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ImportedFile()
        {
            DistributorAccountMappings = new HashSet<DistributorAccountMapping>();
            DistributorProductMappings = new HashSet<DistributorProductMapping>();
            ImportedFileRecords = new HashSet<ImportedFileRecord>();
            InMarketSales = new HashSet<InMarketSale>();
            InMarketSalesBonus = new HashSet<InMarketSalesBonu>();
        }

        [Key]
        public int FileId { get; set; }

        [Required]
        public string FileName { get; set; }

        public int DistributorId { get; set; }

        public int PluginId { get; set; }

        public int StatusId { get; set; }

        public string ErrorDescription { get; set; }

        public int? TimeDefinitionId { get; set; }

        [Required]
        [StringLength(1)]
        public string ImportMode { get; set; }

        public int TotalRecords { get; set; }

        public int TotalUnmappedAccounts { get; set; }

        public int TotalUnmappedProducts { get; set; }

        public int TotalDuplicateEntries { get; set; }

        public int TotalInvalidEntries { get; set; }

        public int CountryId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Employee Employee1 { get; set; }

        public virtual Distributor Distributor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DistributorAccountMapping> DistributorAccountMappings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DistributorProductMapping> DistributorProductMappings { get; set; }

        public virtual TimeDefinition TimeDefinition { get; set; }

        public virtual Country Country { get; set; }

        public virtual ImportedFileStatu ImportedFileStatu { get; set; }

        public virtual ImportPlugin ImportPlugin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImportedFileRecord> ImportedFileRecords { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InMarketSale> InMarketSales { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InMarketSalesBonu> InMarketSalesBonus { get; set; }
    }
}
