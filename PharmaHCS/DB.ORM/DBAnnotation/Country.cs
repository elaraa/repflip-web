﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DBAnnotation
{
    public class CountryMetaData
    {
        [Required]
        [Display(Name ="Country Name")]
        [StringLength(500)]
        public string CountryName { get; set; }

        [Required]
        [Display(Name = "Country Code")]
        [StringLength(50)]
        public string CountryCode { get; set; }
    }
}
namespace DB.ORM.DB
{
    [MetadataType(typeof(DBAnnotation.CountryMetaData))]
    public partial class Country { }
}
