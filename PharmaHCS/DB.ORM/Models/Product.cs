﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProductMetadata))]
    public partial class Product
    {
        public class ProductMetadata
        {
            [StringLength(50)]
            public string ProductCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredProductName", ErrorMessageResourceType = typeof(PH.Product))]
            [StringLength(50)]
            public string ProductName { get; set; }
        }
    }
}
