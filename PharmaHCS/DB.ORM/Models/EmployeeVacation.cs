﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(EmployeeVacationMetadata))]
    public partial class EmployeeVacation
    {
        public class EmployeeVacationMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredComment", ErrorMessageResourceType = typeof(PH.Employee))]
            public string Comment { get; set; }

            [StringLength(1)]
            public string ManagerResponse { get; set; }
        }
    }
}
