﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(JobTitleMetadata))]
    public partial class JobTitle
    {
        public class JobTitleMetadata
        {
            [StringLength(50)]
            public string JobTitledCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredJobTitleName", ErrorMessageResourceType = typeof(PH.Employee))]
            public string JobTitleName { get; set; }
        }
    }
}
