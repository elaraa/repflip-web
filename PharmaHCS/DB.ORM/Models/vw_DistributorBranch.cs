﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_DistributorBranchMetadata))]
    public partial class vw_DistributorBranch
    {
        public class vw_DistributorBranchMetadata
        {
            [Key]
            public int BranchId { get; set; }

            [Key]
            public int DistributorId { get; set; }

            [StringLength(50)]
            public string BranchCode { get; set; }

            [Key]
            public string BranchName { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
