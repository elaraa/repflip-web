﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_AccountCategoryMetadata))]
    public partial class vw_AccountCategory
    {
        public class vw_AccountCategoryMetadata
        {
            [Key]
            public int CategoryId { get; set; }

            [StringLength(50)]
            public string CategoryCode { get; set; }

            [Key]
            public string CategoryName { get; set; }

            [Key]
            public int AccountsCount { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
