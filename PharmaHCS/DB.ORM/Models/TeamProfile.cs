﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TeamProfileMetadata))]
    public partial class TeamProfile
    {
        public class TeamProfileMetadata
        {

            [Key]
            public int ProfileId { get; set; }

            public int TeamId { get; set; }

            [StringLength(50)]
            public string ProfileCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredProfileName", ErrorMessageResourceType = typeof(PH.Team))]
            public string ProfileName { get; set; }
        }
    }
}
