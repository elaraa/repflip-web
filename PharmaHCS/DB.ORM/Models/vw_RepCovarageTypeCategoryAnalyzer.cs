﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_RepCovarageTypeCategoryAnalyzerMetadata))]
    public partial class vw_RepCovarageTypeCategoryAnalyzer
    {
        public class vw_RepCovarageTypeCategoryAnalyzerMetadata
        {
            [StringLength(50)]
            public string ProfileCode { get; set; }
            
            [Key]
            [StringLength(50)]
            public string EmployeeName { get; set; }
            
        }
    }
}
