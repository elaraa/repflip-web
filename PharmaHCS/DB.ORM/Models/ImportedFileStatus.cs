﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ImportedFileStatustMetadata))]
    public partial class ImportedFileStatus
    {
        public class ImportedFileStatustMetadata
        {
            [Key]
            public int StatusId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredStatusName", ErrorMessageResourceType = typeof(PH.SalesImport))]
            public string StatusName { get; set; }
        }
    }
}
