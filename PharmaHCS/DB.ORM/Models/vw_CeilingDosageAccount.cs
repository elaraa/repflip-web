﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_CeilingDosageAccountMetadataMetadata))]
    public partial class vw_CeilingDosageAccount
    {
        public class vw_CeilingDosageAccountMetadataMetadata
        {
            [Key]
            public int CeilingDosageAccountId { get; set; }

            [Key]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 2)]
            public string DosageFormName { get; set; }

            [Key]
            public decimal CeilingQty { get; set; }

            [Key]
            [Column(Order = 4)]
            public string FromPeriod { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
