﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TierMetadata))]
    public partial class Tier
    {
        public class TierMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredTierName", ErrorMessageResourceType = typeof(PH.Tier))]
            [StringLength(50)]
            public string TierName { get; set; }

        }
    }
}
