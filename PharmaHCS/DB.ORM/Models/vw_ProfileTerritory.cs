﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileTerritoryMetadata))]
    public partial class vw_ProfileTerritory
    {
        public class vw_ProfileTerritoryMetadata
        {
            [Key]
            public int ProfileTerritoryId { get; set; }

            [Key]
            public int ProfileId { get; set; }

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            public string TerritoryName { get; set; }

            [Key]
            public decimal Percentage { get; set; }

            [Key]
            public DateTime FromDate { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
