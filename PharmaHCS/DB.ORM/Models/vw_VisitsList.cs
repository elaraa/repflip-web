﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_VisitsListMetadata))]
    public partial class vw_VisitsList
    {
        public class vw_VisitsListMetadata
        {
            [Key]
            public Guid VisitId { get; set; }

            [Key]
            public DateTime VisitDate { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [StringLength(103)]
            public string EmployeeName { get; set; }

            [Key]
            public int CountryId { get; set; }

            [Key]
            public int AccountId { get; set; }
            

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            [Column(Order = 6)]
            public string TerritoryName { get; set; }
            

            [Key]
            public int EntryTypeId { get; set; }
            

            [Key]
            [Column(Order = 8)]
            public bool IsReviewed { get; set; }
            

            [StringLength(103)]
            public string ReviewerName { get; set; }
        }
    }
}
