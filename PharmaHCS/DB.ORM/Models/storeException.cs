﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(storeExceptionMetadata))]
    public partial class storeException
    {
        public class storeExceptionMetadata
        {
            [Key]
            public int ProfileId { get; set; }

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            public int TimeDefinitionId { get; set; }
        }
    }
}
