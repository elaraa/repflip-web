﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(AInMarketSalesFactMetadata))]
    public partial class InMarketSalesFact
    {
        public class AInMarketSalesFactMetadata
        {
            [Key]
            [Column(Order = 0)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int CountryId { get; set; }

            [Key]
            [Column(Order = 1)]
            public int DistributorId { get; set; }

            [Key]
            [Column(Order = 2)]
            public int AccountId { get; set; }
            [Key]
            [Column(Order = 3)]
            public int ProductId { get; set; }

            [Key]
            [Column(Order = 4)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 5)]
            public int TimeDefinitionId { get; set; }

            [Key]
            [Column(Order = 6)]
            public int ProfileId { get; set; }

            [Key]
            [Column(Order = 7)]
            public int TeamId { get; set; }

            [Key]
            [Column(Order = 8)]
            public decimal SharePercentage { get; set; }

            [Key]
            [Column(Order = 9)]
            public decimal Qty { get; set; }

            [Key]
            [Column(Order = 10)]
            [StringLength(50)]
            public string ShareType { get; set; }

        }
    }
}
