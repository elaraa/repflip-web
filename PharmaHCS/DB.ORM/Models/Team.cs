﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TeamMetadata))]
    public partial class Team
    {
        public class TeamMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredTeamName", ErrorMessageResourceType = typeof(PH.Team))]
            public string TeamName { get; set; }

            [StringLength(50)]
            public string TeamCode { get; set; }
        }
    }
}
