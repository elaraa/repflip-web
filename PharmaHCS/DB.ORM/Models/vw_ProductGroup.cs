﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProductGroupMetadata))]
    public partial class vw_ProductGroup
    {
        public class vw_ProductGroupMetadata
        {
            [Key]
            public int GroupId { get; set; }

            [StringLength(50)]
            public string GroupCode { get; set; }

            [Key]
            public string GroupName { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [Key]
            public DateTime LastUpdate { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
