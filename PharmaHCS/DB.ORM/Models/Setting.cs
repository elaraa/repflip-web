﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(SettingMetadata))]
    public partial class Setting
    {
        public class SettingMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredSettingLabel", ErrorMessageResourceType = typeof(PH.Role))]
            public string SettingLabel { get; set; }


            [Required(ErrorMessageResourceName = "RequiredSettingType", ErrorMessageResourceType = typeof(PH.Role))]
            [StringLength(10)]
            public string SettingType { get; set; }


            [Required(ErrorMessageResourceName = "RequiredModule", ErrorMessageResourceType = typeof(PH.Role))]
            [StringLength(50)]
            public string Module { get; set; }
        }
    }
}
