﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_AccountMetadata))]
    public partial class vw_Account
    {
        public class vw_AccountMetadata
        {
            [Key]
            public int AccountId { get; set; }

            [StringLength(50)]
            public string AccountCode { get; set; }

            [Key]
            public string AccountName { get; set; }

            [Key]
            public int AccountTypeId { get; set; }
            

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            [Column(Order = 4)]
            public string TerritoryName { get; set; }

            [Key]
            public bool Active { get; set; }

            [Key]
            public bool Approved { get; set; }

            [Key]
            public int CategoryId { get; set; }


            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
