﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_EmployeeMetadata))]
    public partial class vw_Employee
    {
        public class vw_EmployeeMetadata
        {
            [Key]
            public int EmployeeId { get; set; }

            [StringLength(50)]
            public string EmployeeCode { get; set; }

            [Key]
            [StringLength(50)]
            public string EmployeeName { get; set; }

            [Key]
            public int JobTitleId { get; set; }

            [Key]
            public bool Active { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
