﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_DosageFormUnitMetadata))]
    public partial class vw_DosageFormUnit
    {
        public class vw_DosageFormUnitMetadata
        {
            [Key]
            public int UnitId { get; set; }

            [StringLength(50)]
            public string UnitCode { get; set; }

            [Key]
            public string UnitName { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
