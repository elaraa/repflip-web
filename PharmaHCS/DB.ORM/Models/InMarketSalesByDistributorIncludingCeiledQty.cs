﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InMarketSalesByDistributorIncludingCeiledQtyMetadata))]
    public partial class InMarketSalesByDistributorIncludingCeiledQty
    {
        public class InMarketSalesByDistributorIncludingCeiledQtyMetadata
        {
            [Key]
            [Column(Order = 0)]
            public int DistributorId { get; set; }

            [Key]
            [Column(Order = 1)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 2)]
            public int CountryId { get; set; }
        }
    }
}
