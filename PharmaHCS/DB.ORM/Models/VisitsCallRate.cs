﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitsCallRateMetadata))]
    public partial class VisitsCallRate
    {
        public class VisitsCallRateMetadata
        {
            

            [StringLength(103)]
            public string EmployeeName { get; set; }

        }
    }
}
