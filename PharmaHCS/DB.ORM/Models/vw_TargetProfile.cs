﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TargetProfileMetadata))]
    public partial class vw_TargetProfile
    {
        public class vw_TargetProfileMetadata
        {
            [Key]
            public int TargetId { get; set; }

            [Key]
            public int Year { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }
        }
    }
}
