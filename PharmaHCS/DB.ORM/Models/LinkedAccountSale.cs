﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(LinkedAccountSaleMetadata))]
    public partial class LinkedAccountSale
    {
        public class LinkedAccountSaleMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredAccount", ErrorMessageResourceType = typeof(PH.Doctor))]
            public int AccountId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredLinkedAccount", ErrorMessageResourceType = typeof(PH.Doctor))]
            public int LinkedAccountId { get; set; }
        }
        [NotMapped]
        public class LinkedAccountSalesDto : LinkedAccountSale
        {
            public int DoctorId { get; set; }
            public List<int> LinkedAccountIds { get; set; } 
        } 
    }
}
