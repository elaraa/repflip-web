﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(AccountCategoryrMeteData))]
    public partial class AccountCategory
    {
        public class AccountCategoryrMeteData
        {
           [Key]
            public int CategoryId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredCategoryName", ErrorMessageResourceType = typeof(PH.AccountCategory))]
            public string CategoryName { get; set; }

            [StringLength(50)]
            public string CategoryCode { get; set; }
        }
    }
}

 