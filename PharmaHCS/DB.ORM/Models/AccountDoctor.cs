﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(AccountDoctorMeteData))]
    public partial class AccountDoctor
    {
        public class AccountDoctorMeteData
        {
            [Key]
            [Column(Order = 0)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int AccountId { get; set; }

            [Key]
            [Column(Order = 1)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int DoctorId { get; set; }

            
        }
        [NotMapped]
        public class AccountDoctorDto:AccountDoctor
        {
            public List<int> AccountIds { get; set; } 
        }
    }
}
