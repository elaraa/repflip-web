﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_UserMetadata))]
    public partial class vw_User
    {
        public class vw_UserMetadata
        {
            [Key]
            public int EmployeeId { get; set; }

            [Key]
            [StringLength(50)]
            public string EmployeeName { get; set; }
            [Key]
            public bool Active { get; set; }
            

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
