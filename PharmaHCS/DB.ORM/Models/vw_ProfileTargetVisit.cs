﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileTargetVisitMetadata))]
    public partial class vw_ProfileTargetVisit
    {
        public class vw_ProfileTargetVisitMetadata
        {

            [StringLength(50)]
            public string ProfileCode { get; set; }
            
            [StringLength(103)]
            public string EmployeeName { get; set; }

            [StringLength(16)]
            public string TargetStatus { get; set; }
            
            [StringLength(103)]
            public string ManagerName { get; set; }
            
            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }

        [NotMapped]
        public class vw_ProfileTargetVisitDto: vw_ProfileTargetVisit
        {
            public decimal? TotalTargetVisit { get; set; }
            public int UniverseTargetDoctor { get; set; }
            public decimal? ActualCallRate { get; set; }
            public int? ActualTargetDoctor { get; set; } 
             
        }
    }
}
