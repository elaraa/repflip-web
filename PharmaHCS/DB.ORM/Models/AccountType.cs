﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(AccountTypeMetadata))]
    public partial class AccountType
    {
        public class AccountTypeMetadata
        {
            [StringLength(50)]
            public string AccountTypeCode { get; set; }
            [Required(ErrorMessageResourceName = "RequiredAccountTypeName", ErrorMessageResourceType = typeof(PH.AccountType))]
            public string AccountTypeName { get; set; }
        }
    }
}
