﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ImportedFileMetadata))]
    public partial class ImportedFile
    {
        public class ImportedFileMetadata
        {
            [Key]
            public int FileId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredFileName", ErrorMessageResourceType = typeof(PH.SalesImport))]
            public string FileName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredImportMode", ErrorMessageResourceType = typeof(PH.SalesImport))]
            [StringLength(1)]
            public string ImportMode { get; set; }
        }
    }
}
