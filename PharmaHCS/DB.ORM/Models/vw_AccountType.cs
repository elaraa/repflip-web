﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_AccountTypeMetadata))]
    public partial class vw_AccountType
    {
        public class vw_AccountTypeMetadata
        {
            [Key]
            public int AccountTypeId { get; set; }

            [StringLength(50)]
            public string AccountTypeCode { get; set; }

            [Key]
            public string AccountTypeName { get; set; }

            [Key]
            public bool MustHaveDoctor { get; set; }

            [Key]
            public bool TargetAndPlanWithDoctor { get; set; }

            [Key]
            public bool IncludeInSales { get; set; }

            [Key]
            public int AccountsCount { get; set; }
            
            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
