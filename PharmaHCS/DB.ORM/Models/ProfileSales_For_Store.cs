﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProfileSales_For_StoreMetadata))]
    public partial class ProfileSales_For_Store
    {
        public class ProfileSales_For_StoreMetadata
        {
            [Key]
            public int CountryId { get; set; }

            [Key]
            public int TeamId { get; set; }

            [Key]
            public int ProfileId { get; set; }


            [Key]
            public int ProductId { get; set; }

            [Key]
            public int DosageFormId { get; set; }
        }
    }
}
