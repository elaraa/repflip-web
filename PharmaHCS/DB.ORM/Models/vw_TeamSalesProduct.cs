﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TeamSalesProductMetadata))]
    public partial class vw_TeamSalesProduct
    {
        public class vw_TeamSalesProductMetadata
        {
            [Key]
            public int TeamSalesProductId { get; set; }

            [Key]
            public int TeamId { get; set; }

            [Key]
            public int ProductId { get; set; }

            [StringLength(103)]
            public string ProductName { get; set; }

            [Key]
            public DateTime FromDate { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }
            

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
