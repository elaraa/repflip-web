﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TargetTypeMetadata))]
    public partial class TargetType
    {
        public class TargetTypeMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredTargetTypeName", ErrorMessageResourceType = typeof(PH.TargetAnalyzer))]
            public string TargetTypeName { get; set; }
        }
    }
}
