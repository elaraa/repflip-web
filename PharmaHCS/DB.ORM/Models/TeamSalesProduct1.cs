﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(TeamSalesProduct1Metadata))]
    public partial class TeamSalesProduct1
    {
        public class TeamSalesProduct1Metadata
        {
            [Key]
            public int TeamSalesProductId { get; set; }

            [Key]
            public int TeamId { get; set; }

            [Key]
            public int ProductId { get; set; }

            [Key]
            public DateTime FromDate { get; set; }
        }
    }
}
