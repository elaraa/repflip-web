﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitProductMetadata))]
    public partial class VisitProduct
    {
        public class VisitProductMetadata
        {
            [Key]
            public Guid VisitId { get; set; }

            [Key]
            public int ProductId { get; set; }
        }
    }
}
