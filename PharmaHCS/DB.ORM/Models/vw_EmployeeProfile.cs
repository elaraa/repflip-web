﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_EmployeeProfileMetadata))]
    public partial class vw_EmployeeProfile
    {
        public class vw_EmployeeProfileMetadata
        {
            [Key]
            public int EmployeeProfileId { get; set; }

            [Key]
            public int EmployeeId { get; set; }

            [StringLength(103)]
            public string EmployeeName { get; set; }

            [Key]
            public int ProfileId { get; set; }

            public string ProfileName { get; set; }

            [Key]
            public DateTime FromDate { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
