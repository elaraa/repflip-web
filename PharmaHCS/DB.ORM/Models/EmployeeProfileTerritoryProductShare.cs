﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(EmployeeProfileTerritoryProductShareMetadata))]
    public partial class EmployeeProfileTerritoryProductShare
    {
        public class EmployeeProfileTerritoryProductShareMetadata
        {
            [Column(TypeName = "date")]
            public DateTime? FromDate { get; set; }

            [Column(TypeName = "date")]
            public DateTime? ToDate { get; set; }

            [Key]
            [Column(Order = 0)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 1)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int TerritoryId { get; set; }

            [Key]
            [Column(Order = 2)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int AccountId { get; set; }

            [Key]
            [Column(Order = 3)]
            public decimal Percentage { get; set; }

            [Key]
            [Column(Order = 4)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int ProfileId { get; set; }

            [Key]
            [Column(Order = 5)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int EmployeeId { get; set; }

            [Key]
            [Column(Order = 6)]
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int TeamId { get; set; }
        }
    }
}
