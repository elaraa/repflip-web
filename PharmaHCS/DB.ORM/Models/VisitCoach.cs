﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(VisitCoachMetadata))]
    public partial class VisitCoach
    {
        public class VisitCoachMetadata
        {
            [Key]
            public Guid VisitId { get; set; }

            [Key]
            public int CoachId { get; set; }
        }
    }
}
