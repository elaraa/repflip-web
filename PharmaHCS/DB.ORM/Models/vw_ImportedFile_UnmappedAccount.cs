﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ImportedFile_UnmappedAccountMetadata))]
    public partial class vw_ImportedFile_UnmappedAccount
    {
        public class vw_ImportedFile_UnmappedAccountMetadata
        {
            [Key]
            public int RecordId { get; set; }

            [Key]
            public int FileId { get; set; }
        }
    }
}
