﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_target_ProductPriceMetadata))]
    public partial class vw_target_ProductPrice
    {
        public class vw_target_ProductPriceMetadata
        {
            [Key]
            public int ProductId { get; set; }
        }
    }
}
