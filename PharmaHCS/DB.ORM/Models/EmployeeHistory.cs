﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(EmployeeHistoryMetadata))]
    public partial class EmployeeHistory 
    {

        public class EmployeeHistoryMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredJobTitleId", ErrorMessageResourceType = typeof(PH.Employee))]
            public int JobTitleId { get; set; }
            [Required(ErrorMessageResourceName = "RequiredDepartmentId", ErrorMessageResourceType = typeof(PH.Employee))]
            public int? DepartmentId { get; set; }
        }

    }
}
