﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProfileTargetNewDoctorApprovalMetadata))]
    public partial class ProfileTargetNewDoctorApproval
    {
        public class ProfileTargetNewDoctorApprovalMetadata
        {
            [Key]
            public int TargetDoctorApprovalId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredDoctorName", ErrorMessageResourceType = typeof(PH.Team))]
            public string DoctorName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredGender", ErrorMessageResourceType = typeof(PH.Team))]
            [StringLength(1)]
            public string Gender { get; set; }

            [StringLength(50)]
            public string Mobile { get; set; }

            [StringLength(50)]
            public string AccountCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredApprovalStatus", ErrorMessageResourceType = typeof(PH.Team))]
            [StringLength(1)]
            public string ApprovalStatus { get; set; }
        }
        [NotMapped]
        public class ProfileTargetNewDoctorApprovalDto: ProfileTargetNewDoctorApproval
        {
            public int TimeId { get; set; }  
        }
    }
}
