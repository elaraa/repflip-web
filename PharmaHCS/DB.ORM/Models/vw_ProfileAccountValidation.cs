﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProfileAccountValidationMetadata))]
    public partial class vw_ProfileAccountValidation
    {
        public class vw_ProfileAccountValidationMetadata
        {
            [Key]
            public int ProfileAccountId { get; set; }

            [Key]
            public int ProfileId { get; set; }

            [Key]
            public int TerritoryId { get; set; }

            [Key]
            public string TerritoryName { get; set; }

            [Key]
            public decimal Perccentage { get; set; }

            [Key]
            public DateTime FromDate { get; set; }

            [StringLength(50)]
            public string MainProfileCode { get; set; }
        }
    }
}
