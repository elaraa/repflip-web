﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DistributorMetadata))]
    public partial class Distributor
    {
        public class DistributorMetadata
        {

            [Required(ErrorMessageResourceName = "RequiredDistributorName", ErrorMessageResourceType = typeof(PH.Distributor))]
            public string DistributorName { get; set; }

            [StringLength(50)]
            public string DistributorCode { get; set; }
        }
    }
}
