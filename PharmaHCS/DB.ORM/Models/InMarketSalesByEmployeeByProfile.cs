﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InMarketSalesByEmployeeByProfileMetadata))]
    public partial class InMarketSalesByEmployeeByProfile
    {
        public class InMarketSalesByEmployeeByProfileMetadata
        {
            [Key]
            [Column(Order = 0)]
            public int TeamId { get; set; }

            [Key]
            [Column(Order = 1)]
            public int EmployeeId { get; set; }

            [Key]
            [Column(Order = 2)]
            public int ProfileId { get; set; }

            [Key]
            [Column(Order = 3)]
            public decimal Perccentage { get; set; }

            [Key]
            [Column(Order = 4)]
            public int AccountId { get; set; }

            [Key]
            [Column(Order = 5)]
            public int DosageFormId { get; set; }

            [Key]
            [Column(Order = 6)]
            public int CountryId { get; set; }

            [Key]
            [Column(Order = 7)]
            public int DistributorId { get; set; }
        }
    }
}
