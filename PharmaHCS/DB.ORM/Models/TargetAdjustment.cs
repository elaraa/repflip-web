﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    public class vw_TargetAdjustment
    {
        [Required(ErrorMessageResourceName = "RequiredProduct", ErrorMessageResourceType = typeof(PH.TargetAdjustment))]
        public int ProductId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredTime", ErrorMessageResourceType = typeof(PH.TargetAdjustment))]
        public int TimeId { get; set; }
        [Required(ErrorMessageResourceName = "RequiredType", ErrorMessageResourceType = typeof(PH.TargetAdjustment))]
        public int TypeId { get; set; }
        public int? ProfileId { get; set; }
        public int? TerritoryId { get; set; }

        public decimal TargetValue { get; set; }

    }
}
