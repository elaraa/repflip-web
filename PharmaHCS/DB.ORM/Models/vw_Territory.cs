﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TerritoryMetadata))]
    public partial class vw_Territory
    {
        public class vw_TerritoryMetadata
        {
            [Key]
            public int TerritoryId { get; set; }

            [Key]
            [StringLength(50)]
            public string TerritoryCode { get; set; }

            [Key]
            [Column(Order = 2)]
            public string TerritoryName { get; set; }

            [Key]
            public int TerritoryCount { get; set; }

            [Key]
            public int CountryId { get; set; }

            [StringLength(50)]
            public string CountryCode { get; set; }

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
