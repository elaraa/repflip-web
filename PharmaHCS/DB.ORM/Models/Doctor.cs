﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(DoctorMetadata))]
    public partial class Doctor
    {
        public class DoctorMetadata
        {
            [Required(ErrorMessageResourceName = "RequiredDoctorName", ErrorMessageResourceType = typeof(PH.Doctor))]
            public string DoctorName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredGender", ErrorMessageResourceType = typeof(PH.Doctor))]
            [StringLength(1)]
            public string Gender { get; set; }

            [StringLength(11)]
            [DataType(DataType.PhoneNumber)]
            [RegularExpression(@"^([0-9]{11})$", ErrorMessageResourceName = "InvalidMobileNumber", ErrorMessageResourceType = typeof(PH.Doctor))]
            public string Mobile { get; set; }
        }
        [NotMapped]
        public class DoctorDto : Doctor 
        {
            //[Required(ErrorMessageResourceName = "RequiredAccountName", ErrorMessageResourceType = typeof(PH.Account))]
            public string AccountName { get; set; }
         
            public string AccountCode { get; set; }

            public int AccountTypeId { get; set; }

            public string Address { get; set; }

            public int TerritoryId { get; set; }

            public bool Active { get; set; }

            public int CategoryId { get; set; }

            public decimal? Latitude { get; set; }

            public decimal? Longitude { get; set; }

            public int AccountCountryID { get; set; }
            
            public int? AccountPotentialID { get; set; }

            public int? AccountId { get; set; }

            public string  Type { get; set; } 

            public bool AccountApproved { get; set; }


        }
    }
}
