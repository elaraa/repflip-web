﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_ProductPriceMetadata))]
    public partial class vw_ProductPrice
    {
        public class vw_ProductPriceMetadata
        {
            [Key]
            public int ProductPriceId { get; set; }

            [Key]
            public int ProductId { get; set; }

            [StringLength(103)]
            public string ProductName { get; set; }

            [StringLength(16)]
            public string PriceType { get; set; }

            [Key]
            public DateTime FromDate { get; set; }
            

            [Key]
            [Column(Order = 3)]
            public decimal Price { get; set; }
            
            [StringLength(50)]
            public string CountryCode { get; set; }
            

            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
