﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(vw_TargetProfile_DetailsMetadata))]
    public partial class vw_TargetProfile_Details
    {
        public class vw_TargetProfile_DetailsMetadata
        {
            [Key]
            public int TargetId { get; set; }

            [Key]
            public int CountryId { get; set; }

            [Key]
            public int Year { get; set; }

            [StringLength(50)]
            public string ProfileCode { get; set; }

            [StringLength(103)]
            public string EmployeeName { get; set; }

            [Key]
            public int TeamId { get; set; }

            [Key]
            [Column(Order = 4)]
            public string TeamName { get; set; }


            [StringLength(103)]
            public string ProductName { get; set; }
            

            [Key]
            [Column(Order = 5)]
            public string TerritoryName { get; set; }
            

            [Key]
            [Column(Order = 6)]
            public string PeriodName { get; set; }
            

            [StringLength(50)]
            public string CountryCode { get; set; }
            
            [StringLength(50)]
            public string UpdatedByLogonName { get; set; }
        }
    }
}
