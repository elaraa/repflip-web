﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ProductGroupMetadata))]
    public partial class ProductGroup
    {
        public class ProductGroupMetadata
        {
            [Key]
            public int GroupId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredGroupName", ErrorMessageResourceType = typeof(PH.Product))]
            public string GroupName { get; set; }

            [StringLength(50)]
            public string GroupCode { get; set; }
        }
    }
}
