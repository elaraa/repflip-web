﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMUser
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string LoginName { get; set; }

        [DataType( DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }

        public int? RoleId { get; set; }
        public HttpPostedFileBase PictureFile { get; set; }
        public string PictureUrl { get; set; }

    }
}