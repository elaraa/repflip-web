﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMMarketShareImport
    {
        [Required(ErrorMessageResourceName = "RequiredYear", ErrorMessageResourceType = typeof(PH.TargetAnalyzer))]
        public int Year { get; set; }
        public int Level { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessageResourceName = "RequiredCountryId", ErrorMessageResourceType = typeof(PH.TargetAnalyzer))]
        public int CountryId { get; set; }
        [Required(ErrorMessageResourceName = "RequiredFile", ErrorMessageResourceType = typeof(PH.TargetAnalyzer))]
        public HttpPostedFileBase File { get; set; }
    }
}