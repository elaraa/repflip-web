﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.App.Models
{
    public class VMVisit
    {
        public VMVisit()
        {
            Coaches = new List<int>();
            Comments = new List<Tuple<int, string>>();
            Doctors = new List<int>();
            Presentations = new List<int>();
        }
        public DateTime VisitDate { get; set; }
        public int AccountId { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public List<int> Coaches { get; set; }
        public List<Tuple<int, string>> Comments { get; set; }
        public List<int> Doctors { get; set; }
        public List<Tuple<int, string>> Requests { get; set; }
        public List<VisitRequest> PreviousRequests { get; set; }
        public List<int> Presentations { get; set; }

        private ICollection<VisitCoach> GetVisitCoaches(Guid visitId) => Coaches.Select(s => new VisitCoach
        {
            CoachId = s,
            VisitId = visitId
        }).ToList();
        private ICollection<VisitDoctor> GetVisitDoctors(Guid visitId) => Doctors.Select(s => new VisitDoctor
        {
            DoctorId = s,
            VisitId = visitId
        }).ToList();
        private ICollection<VisitPresentation> GetVisitPresentations(Guid visitId) => Presentations.Select(s => new VisitPresentation
        {
            PresentationId = s,
            VisitId = visitId
        }).ToList();
        public Visit Model(System.Web.HttpBrowserCapabilitiesBase broswerInfo)
        {
            var esource = string.Empty;
            if (broswerInfo != null)
                esource = string.Format("Browser: {0}, Version: {1}", broswerInfo.Browser, broswerInfo.Version);

            var vId = Guid.NewGuid();
            return new Visit
            {
                VisitId = vId,
                VisitDate = this.VisitDate,
                AccountId = this.AccountId,
                EntryTypeId = 1, /*Web Browser*/
                EntrySource = esource,
                Latitude = Lat,
                Longitude = Lng,
                VisitCoaches = GetVisitCoaches(vId),
                VisitDoctors = GetVisitDoctors(vId),
                VisitPresentations = GetVisitPresentations(vId)
            };
        }
    }
}