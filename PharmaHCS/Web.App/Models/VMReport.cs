﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Web.App.Models
{
    public class VMReport
    {
        [Display(Name = "Teams")]
        public string TeamId { get; set; }
        public string TerritroyIds { get; set; }
        public string ProfileIds { get; set; }
        [Display(Name ="Time")]
        public List<string> TimeDefinitionId { get; set; }
        public string AccountIds { get; set; }
        public string ProductIds { get; set; }
        public string DosageIds { get; set; }
        public string EmployeeIds { get; set; }
        public string DistributorIds { get; set; }
        public string CountryIds { get; set; }

        public string createdBy { get; set; }//{ get { return HttpContext.Current.Session["UserFullName"].ToString(); } }
        public string FilterText { get; set; }
        public string imagesource { get; set; }//{ get { return "http://server/images/logo.png"; } }
    }
}