﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMTargetProfile
    {
        [Required]
        [Display(Name ="Extract Time")]
        public int ExtractTimeDefId { get; set; }

        [Required]
        [Display(Name = "Upload Time")]
        public int TargetTimeDefId { get; set; }
        [Display(Name = "Country")]
        [Required]
        public int CountryId { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
        [Display(Name = "Team")]
        public int? TeamId { get; set; } 

    }
}