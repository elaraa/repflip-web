﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMSalesImport
    {
        [Display(Name = "Distributor")]
        [Required]
        public int DistributorId { get; set; }
        [Display(Name = "Time Definition")]
        public int TimeDefinitionId { get; set; }
        [Required]
        [Display(Name = "Import Mode")]
        public string ImportMode { get; set; }
        [Required]
        public HttpPostedFileBase[] Files { get; set; }
    }
}