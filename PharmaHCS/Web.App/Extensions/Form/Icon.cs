﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App
{
    public class Icon
    {
        private const string _iconTemplate = "<i class=\"{0}\"></i>";
        public string IconText { get; set; }
        public override string ToString()
        {
            return string.IsNullOrEmpty(IconText) ? "" : string.Format(_iconTemplate, IconText);
        }
    }
}