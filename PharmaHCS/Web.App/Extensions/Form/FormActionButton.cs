﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.App
{
    public class FormActionButton
    {
        private const string _buttonFormat = "<button type = \"{2}\" class=\"btn {1}\" {3}>{0}</button>";
        public string Label { get; set; }
        public string CssClass { get; set; }
        public string Type { get; set; }
        public string OtherProperties { get; set; }
        public Icon Icon { get; set; }
        public static FormActionButton SubmitButton()
        {
            return new FormActionButton
            {
                Type = "submit",
                CssClass = "green",
                Label = "Submit"
            };
        }
        public static FormActionButton ResetButton()
        {
            return new FormActionButton
            {
                Type = "reset",
                CssClass = "default",
                Label = "Reset"
            };
        }
        public override string ToString()
        {
            return string.Format(_buttonFormat, string.Format("{0} {1}", Icon, Label), CssClass, Type, OtherProperties);
        }
        public static string GetButtons(params FormActionButton[] buttons) {

            if (buttons == null || buttons.Length < 1)return "";

            var s = new StringBuilder();

            foreach (var item in buttons)
                s.Append(item.ToString());

            return s.ToString();
        }
    }

    public class FormActionLink : FormActionButton
    {
        private const string _template = "<a class=\"btn {1}\" href=\"{2}{4}\" {3}>{0}</a>";
        public string ViewName { get; set; }
        public object RouteValues { get; set; }
        public string Fragment { get; set; }
        public override string ToString()
        {
            UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);

            return string.Format(_template, string.Format("{0}{1}", Icon, Label), CssClass, url.Action(ViewName, RouteValues), OtherProperties, string.IsNullOrEmpty(Fragment)?"": "#" +Fragment);
        }
        public static FormActionLink BackButton(string returnView = "Index", object returnViewRouteValue = null, string tabFragment = "")
        {
            return new FormActionLink { Label = " Back",Icon = new Icon { IconText= "fa fa-arrow-left" }, CssClass = "gray", ViewName = returnView, RouteValues = returnViewRouteValue, Fragment = tabFragment };
        }
    }
}