﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App
{
    public class FormHeaderCaption
    {
        private const string _template = "    <div class=\"caption {1}\">" +
    "        {2} <span class=\"caption-subject bold uppercase\">{0}</span>" +
    "    </div>";
        public string Text { get; set; }
        public Icon Icon { get; set; }
        public string Color { get; set; }
        public override string ToString()
        {
            return string.Format(_template, Text, Color, Icon);
        }
    }
}