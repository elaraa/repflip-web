﻿using System.Text;
using System.Web.Mvc;

namespace Web.App
{
    public class FormHeaderButton
    {
        private const string _template = "<a class=\"btn btn-circle btn-icon-only {2}\" href=\"{3}\" {4}>" +
                                        "{1} {0}" +
                                    "</a>";
        public override string ToString()
        {
            return string.Format(_template, Text,
                Icon,
                string.IsNullOrEmpty(Color) ? "btn-default" : Color,
                string.IsNullOrEmpty(Link) ? "javascript:;" : Link,
                OtherProperties
                );
        }
        public Icon Icon { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
        public string Link { get; set; }
        public string OtherProperties { get; set; }
        public static string GetButtons(params FormHeaderButton[] buttons)
        {
            var buttonsStr = new StringBuilder();
            
            buttonsStr.Append(" <div class=\"actions\">");

            if (buttons != null && buttons.Length > 0)
            foreach (var item in buttons)
                buttonsStr.Append(item.ToString());

            buttonsStr.Append("        <a class=\"btn btn-circle btn-icon-only btn-default fullscreen\" href=\"javascript:;\" data-original-title=\"\" title=\"\"> </a>" +
                "</div>");
            return buttonsStr.ToString();
        }
        public static FormHeaderButton AddButton() {
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            return new FormHeaderButton{
             Color = "",
             Icon = new Icon { IconText="fa fa-plus" },
             Link=urlHelper.Action("Create"),
            };
        }
    }
}