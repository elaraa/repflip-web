﻿var currentTab = 1, maxTab = 2;
var submittedFormId = "Main";

$('li').click(function (e) {
    if (deactiveBtn(currentTab, submittedFormId != "" ? true : false)) {
        currentTab = this.id.split("-")[1];
        activeBtn(currentTab);
    } else
        return false;
});

$('#continueBtn').on("click", function (e) {
    if (deactiveBtn(currentTab, submittedFormId != "" ? true : false)) {
        currentTab++;
        activeBtn(currentTab);
    }
});

$('#backBtn').on("click", function (e) {
    deactiveBtn(currentTab);
    currentTab--;
    activeBtn(currentTab);
});

activeBtn = (currentTab) => {
    if (currentTab == maxTab) {
        $('#continueBtn').css("display", "none");
        if (noDisplay == 'True')
           $('#submitBtn').css("display", "inline");
    } else {
        $('#submitBtn').css("display", "none");
        $('#continueBtn').css("display", "inline");
    }
    if (currentTab == 1) {
        $('#backBtn').css("display", "none");
        $('#indexBtn').css("display", "inline");
    }
    else {
        $('#backBtn').css("display", "inline");
        $('#indexBtn').css("display", "none");
    }

    $("div#tabs-" + currentTab).addClass('active');
    $("#litabs-" + currentTab).addClass('active');
}

deactiveBtn = (currentTab, submitForm = false) => {
    if (currentTab == 1 && submitForm == true) {
        if ($("#" + submittedFormId).valid()) {
            $("#" + submittedFormId).submit();
        } else
            return false;
    }
    $("div#tabs-" + currentTab).removeClass('active');
    $("#litabs-" + currentTab).removeClass('active');

    return true;
}
