﻿using AutoMapper;
using DB.ORM.DB;
using Services.Business;
using Services.Business.CRM.NewDoctorRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using static DB.ORM.DB.Doctor;
using static DB.ORM.DB.ProductPresentation;
using static DB.ORM.DB.ProfileTargetNewDoctorApproval;
using static DB.ORM.DB.vw_ProfileTargetVisit;

namespace Web.App
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           // SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
            Task.Factory.StartNew(() => { ScheduleJobService.Run(); });
            InitMapper();
        }

        private void InitMapper()
        {


            Mapper.Initialize(cfg => {
                cfg.CreateMap<ProductPresentationDto, ProductPresentation>();
                cfg.CreateMap<ProductPresentation, ProductPresentationDto>();

                cfg.CreateMap<DoctorAccountVM, ProfileTargetNewDoctorApproval>()
                .ForMember(d => d.TargetDoctorApprovalId, s => s.MapFrom(src => src.TargetDoctorApprovalId))
                .ForMember(d => d.DoctorName, s => s.MapFrom(src => string.Format("{0} {1} {2}", src.FirstName, src.MiddleName, src.LastName)));
                cfg.CreateMap<ProfileTargetNewDoctorApproval, DoctorAccountVM>()
                     .ForMember(d => d.TargetDoctorApprovalId, s => s.MapFrom(src => src.TargetDoctorApprovalId))
                     .ForMember(d => d.FirstName, s => s.MapFrom(src => src.DoctorName.Split(' ')[0]))
                     .ForMember(d => d.MiddleName, s => s.MapFrom(src => src.DoctorName.Split(' ')[1]))
                     .ForMember(d => d.LastName, s => s.MapFrom(src => src.DoctorName.Split(' ')[2]))
                     .ForMember(d => d.TerritoryId, s => s.MapFrom(src => src.TerritoryId))
                     .ForMember(d => d.City, s => s.MapFrom(src => src.Address.Split(',')[1]))
                     .ForMember(d => d.CountryId, s => s.MapFrom(src => src.CountryId));

                cfg.CreateMap<DoctorDto, Doctor>().ForMember(d => d.DoctorId, s => s.MapFrom(src => src.DoctorId));
                cfg.CreateMap<Doctor, DoctorDto>().ForMember(d => d.DoctorId, s => s.MapFrom(src => src.DoctorId));

                cfg.CreateMap<DoctorDto, Account>()
                .ForMember(d => d.AccountId, s => s.MapFrom(src => src.AccountId))
                .ForMember(d => d.PotentialId, s => s.MapFrom(src => src.AccountPotentialID))
                .ForMember(d => d.CountryId, s => s.MapFrom(src => src.AccountCountryID))
                .ForMember(d => d.Approved, s => s.MapFrom(src => src.AccountApproved));

                cfg.CreateMap<Account, DoctorDto>()
                .ForMember(d => d.AccountId, s => s.MapFrom(src => src.AccountId))
                .ForMember(d => d.AccountPotentialID, s => s.MapFrom(src => src.PotentialId))
                .ForMember(d => d.AccountCountryID, s => s.MapFrom(src => src.CountryId))
                .ForMember(d => d.AccountApproved, s => s.MapFrom(src => src.Approved));

                cfg.CreateMap<ProfileTargetNewDoctorApprovalDto, ProfileTargetNewDoctorApproval>().ForMember(d => d.TargetDoctorApprovalId, s => s.MapFrom(src => src.TargetDoctorApprovalId));
                cfg.CreateMap<ProfileTargetNewDoctorApproval, ProfileTargetNewDoctorApprovalDto>().ForMember(d => d.TargetDoctorApprovalId, s => s.MapFrom(src => src.TargetDoctorApprovalId));

                cfg.CreateMap<ProfileTargetNewDoctorApprovalDto, Doctor>().ForMember(d => d.DoctorId, s => s.MapFrom(src => src.DoctorId));
                cfg.CreateMap<Doctor, ProfileTargetNewDoctorApprovalDto>().ForMember(d => d.DoctorId, s => s.MapFrom(src => src.DoctorId));

                cfg.CreateMap<ProfileTargetNewDoctorApprovalDto, Account>().ForMember(d => d.AccountId, s => s.MapFrom(src => src.AccountId));
                cfg.CreateMap<Account, ProfileTargetNewDoctorApprovalDto>().ForMember(d => d.AccountId, s => s.MapFrom(src => src.AccountId));

                cfg.CreateMap<vw_ProfileTargetVisitDto, vw_ProfileTargetVisit>().ForMember(d => d.ProfileTargetId, s => s.MapFrom(src => src.ProfileTargetId));
                cfg.CreateMap<vw_ProfileTargetVisit, vw_ProfileTargetVisitDto>().ForMember(d => d.ProfileTargetId, s => s.MapFrom(src => src.ProfileTargetId));


            });
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext httpContext = ((MvcApplication)sender).Context;
            string currentController = " ";
            string currentAction = " ";
            RouteData currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                    currentController = currentRouteData.Values["controller"].ToString();

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                    currentAction = currentRouteData.Values["action"].ToString();
            }

            Exception ex = Server.GetLastError();
            //var controller = new ErrorController();
            var routeData = new RouteData();
            string action = "GenericError";

            if (ex is HttpException)
            {
                var httpEx = ex as HttpException;

                switch (httpEx.GetHttpCode())
                {
                    case 404: action = "NotFound"; break;
                        // others if any
                }
            }

            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
            httpContext.Response.TrySkipIisCustomErrors = true;

            routeData.Values["controller"] = "App";
            routeData.Values["action"] = action;
            routeData.Values["exception"] = new HandleErrorInfo(ex, currentController, currentAction);

            IController errormanagerController = new Controllers.AppController();
            var wrapper = new HttpContextWrapper(httpContext);
            var rc = new RequestContext(wrapper, routeData);
            errormanagerController.Execute(rc);
        }
        //protected void Application_End()
        //{
        //    var runtime = (HttpRuntime)typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime", BindingFlags.NonPublic | BindingFlags.Static |
        //                                                                                          BindingFlags.GetField, null, null, null);

        //    if (runtime == null) return;
        //    var shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage", BindingFlags.NonPublic | BindingFlags.Instance |
        //                                                                                     BindingFlags.GetField, null, runtime, null);
        //    var shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack", BindingFlags.NonPublic | BindingFlags.Instance |
        //                                                                                 BindingFlags.GetField, null, runtime, null);
        //    ApplicationShutdownReason shutdownReason = HostingEnvironment.ShutdownReason;
        //    Elmah.ErrorLog.GetDefault(null).Log(new Error(new Exception(String.Format("{3}_shutDownMessage={0}{3}_shutDownStack={1}{3}_shutDownReason={2}",
        //        shutDownMessage,
        //        shutDownStack, shutdownReason, "\r\n\r\n"))));
        //}
    }
}
