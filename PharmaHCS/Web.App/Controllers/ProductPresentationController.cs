﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.ProductPresentation;

namespace Web.App.Controllers
{
    public class ProductPresentationController : BaseControllerTemplateDto<vw_ProductPresentation, ProductPresentation, ProductPresentationDto, int>
    {

        public ProductPresentationController() : base()
        {
            ViewNamePlural = PH.ProductPresentation.ViewNamePlural;
            ViewNameSingular = PH.ProductPresentation.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<ProductPresentation, object>>[]
                {
                    x=>x.PresentationId,
                    x => x.PresentationName,
                    x => x.ProductId,
                    x => x.FilePath,
                    x => x.Ext,
                    x => x.Active
                };
        }

        protected override void InitTemplateServices()
        {
            ActionService = new ProductPresentationService(UserId);
            ListService = new BaseService<vw_ProductPresentation>(UserId);
        }
        protected override Expression<Func<ProductPresentation, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.PresentationId == id.Value) : (Expression<Func<ProductPresentation, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
            ViewBag.Products = LookupHelper.GetMyProducts(this);
        }

      
        [HttpPost]
        public ActionResult ToogleActive(int? id)
        {
            var result = (ActionService as ProductPresentationService).toogleActive(id.Value);
            return Json(result,JsonRequestBehavior.AllowGet);
        } 
        [HttpPost]
        public ActionResult DeletePresentation(int? id)  
        {
            var result = (ActionService as ProductPresentationService).deletePresentation(id.Value);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


       [HttpPost]
        public ActionResult Download(int? id)
        { 
            var path = (ActionService as ProductPresentationService).GetPath(id.Value);
            var fullPath = Server.MapPath("~/" + path);
           

            var res =System.IO.File.Exists(fullPath);
            return Json(res, JsonRequestBehavior.AllowGet);
            
            
        }
        
    }
}
