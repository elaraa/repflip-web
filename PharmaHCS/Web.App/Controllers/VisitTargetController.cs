﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;

namespace Web.App.Controllers
{
    public class VisitTargetController : BaseControllerList<vw_ProfileTargetVisit, ProfileTargetVisit, int>
    {
        public VisitTargetController() : base()
        {
            ViewNamePlural = PH.RepTargetVisit.ViewNamePlural;
            ViewNameSingular = PH.RepTargetVisit.ViewNameSingular;
            ApplySecurity = false;
        }
        public override ActionResult Create()
        {
            FillViewBag(true);
            return View();
        }
        protected override void InitTemplateServices()
        {
            base.InitTemplateServices();
            ActionService = new VisitTarget(UserId);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMRepTarget model)
        {
            FillViewBag(true);
            if (ModelState.IsValid)
            {
                var res = (ActionService as VisitTarget).CreateTarget(model.RepId,model.TimeDefinitionId, (RepTargetCreateOption)model.CreateOption);
                if (res.Success)
                {
                    ShowSuccess(PH.RepTargetVisit.Targetcreratedsuccessfully, true);
                    return RedirectToAction("Edit", new { id = 1 });
                }
                else
                {
                    ShowError(
                        res.ResultId == -1 ?
                        PH.RepTargetVisit.TargetCreateForthisMonth :
                        PH.RepTargetVisit.Repdoesnothaveprofile);
                    return View(model);
                }
            }
            else
            {
                ShowError(GetModelStateErrors(ModelState));
                return View(model);
            }
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            if (isCreate)
            {
                var createOption = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Copy From Previous Period", Value = "1" },
                    new SelectListItem { Text = "Empty", Value = "2" }
                };
                ViewBag.CreateOption = createOption;
                ViewBag.TimeDef = LookupHelper.GetTimeDefinitions(this);
                ViewBag.Reps = LookupHelper.GetMyEmployees(this);
            }
            else
            {
                ViewBag.potentials = new BaseService<Potential>(UserId).Get(null, true);
            }
        }
        public ActionResult Edit(int? id)
        {
            FillViewBag(false);
            return EditChildEntity(id, id.HasValue ? (x => x.ProfileTargetId == id.Value) : (Expression<Func<vw_ProfileTargetVisit, bool>>)(x => 1 == 0), "Edit", new BaseService<vw_ProfileTargetVisit>(UserId));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveTarget(vw_ProfileTargetVisit model) => ApproveReject(model, true);
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RejectTarget(vw_ProfileTargetVisit model) => ApproveReject(model, false);
        private ActionResult ApproveReject(vw_ProfileTargetVisit model, bool approve)
        {
            if ((ActionService as VisitTarget).ApproveReject(model.ProfileTargetId, approve))
            {
                ShowSuccess(string.Format("Target {0}!",approve?"Approved":"Rejected"), true);
                return RedirectToAction("index");
            }
            ShowError(string.Format("Failed to {0}", approve ? "approve" : "reject"));
            FillViewBag(false);
            return View("Edit", model);
        }
        public ActionResult ReadUniverseList([DataSourceRequest] DataSourceRequest request, int targetId) => Json((ActionService as VisitTarget).GetTargetUniverse(targetId).ToDataSourceResult(request));
        public ActionResult ReadDetailsList([DataSourceRequest] DataSourceRequest request, int targetId) => Json((ActionService as VisitTarget).GetTargetVisitDetails(targetId).ToDataSourceResult(request));
        [HttpPost]
        public JsonResult AddAccount(int accountId, int? doctorId, int targetId) => Json((ActionService as VisitTarget).AddAccount(targetId, accountId, doctorId));
        [HttpPost]
        public JsonResult DeleteAccount(int? id)
        {
            if (id.HasValue)
                return Json((ActionService as VisitTarget).DeleteAccount(id.Value));
            else return Json(false);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDetailsList([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<vw_ProfileTargetVisitDetails> items)
        {
            var srv = (ActionService as VisitTarget);
            if (items != null && ModelState.IsValid)
            {
                foreach (var item in items)
                {
                    srv.UpdateAccount(item.ProfileTargetDetailId, item.PotentialId, item.Frequency);
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

    }
}