﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class TerritoryController : BaseControllerTemplate<vw_Territory, Territory, int>
    {
        public TerritoryController() : base()
        {
            ViewNamePlural = PH.Territory.ViewNamePlural;
            ViewNameSingular = PH.Territory.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Territory, object>>[]
                {
                    x=>x.TerritoryCode,
                    x => x.TerritoryName,
                    x => x.ParentTerritoryId,
                };
        }

        protected override Expression<Func<Territory, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TerritoryId == id.Value) : (Expression<Func<Territory, bool>>)(x => 1 == 0);
        }

    }
}