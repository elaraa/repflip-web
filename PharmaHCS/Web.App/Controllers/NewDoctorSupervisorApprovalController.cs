﻿using AutoMapper;
using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class NewDoctorSupervisorApprovalController : BaseControllerList<vw_ProfiletargetNewDoctor, ProfileTargetNewDoctorApproval, int>
    {
        public NewDoctorSupervisorApprovalController() : base()
        {
            ViewNamePlural = PH.NewDoctorRequest.ViewNamePlural;
            ViewNameSingular = PH.NewDoctorRequest.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<ProfileTargetNewDoctorApproval, object>>[]
            {
                    x => x.ApprovalStatus
            };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new SupervisorService(UserId); 
            ListService = new BaseService<vw_ProfiletargetNewDoctor>(UserId);
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.Potentials = LookupHelper.GetMyPotentials(this);
            ViewBag.Specialties = LookupHelper.GetMySpecialties(this);
            ViewBag.Genders = LookupHelper.GetGenders(this);
            ViewBag.AccountTypes = LookupHelper.GetMyAccountTypes(this);
            ViewBag.AccountCategories = LookupHelper.GetMyAccountCategories(this);

            base.FillViewBag(isCreate);
        }
        public override ActionResult ReadList([DataSourceRequest] DataSourceRequest request)
        {
            return Json((ActionService as SupervisorService).GetList().ToDataSourceResult(request));
        }
        public ActionResult CheckRequest(int? id)
        {
            FillViewBag(false);
            return EditChildEntity(id, id.HasValue ? (x => x.TargetDoctorApprovalId == id.Value) : (Expression<Func<ProfileTargetNewDoctorApproval, bool>>)(x => 1 == 0), "CheckRequest", new BaseService<ProfileTargetNewDoctorApproval>(UserId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(ProfileTargetNewDoctorApproval model) => ApproveReject(model, true);
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reject(ProfileTargetNewDoctorApproval model) => ApproveReject(model, false);
        private ActionResult ApproveReject(ProfileTargetNewDoctorApproval model, bool approve)
        {
            if ((ActionService as SupervisorService).ApproveReject(model.TargetDoctorApprovalId, approve))
            {
                ShowSuccess(string.Format("{0} {1}!", PH.NewDoctorRequest.Doctor, approve ? PH.SWAL.Approved : PH.SWAL.Rejected), true);
                return RedirectToAction("index");
            }
            ShowError(string.Format("{0} {1}", PH.NewDoctorRequest.Failedto, approve ? PH.SWAL.Approve : PH.SWAL.Reject));
            FillViewBag(false);
            return View("CheckRequest", model);
        }

        [HttpPost]
        public ActionResult ApproveRequest(int? id)
        {
            if (!id.HasValue)
                return View(false);
            return Json((ActionService as SupervisorService).ApproveReject(id.Value, true), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RejectRequest(int? id)
        {
            if (!id.HasValue)
                return View(false);
            return Json((ActionService as SupervisorService).ApproveReject(id.Value, false), JsonRequestBehavior.AllowGet);
        }


    }
}
