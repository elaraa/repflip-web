﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class VisitRequestTypeController : BaseControllerTemplate<vw_VisitRequestType, VisitRequestType, int>
    {
        public VisitRequestTypeController() : base()
        {
            ViewNamePlural = PH.VisitCommentType.ViewVisitRequestTypeNamePlural;
            ViewNameSingular = PH.VisitCommentType.ViewVisitRequestTypeNameSingular;

            PropertiesToUpdate = new Expression<Func<VisitRequestType, object>>[]
                {
                    x=>x.VisitRequestTypeName,
                    x => x.Active,
           
                };
        }

        protected override Expression<Func<VisitRequestType, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.VisitRequestTypeId == id.Value) : (Expression<Func<VisitRequestType, bool>>)(x => 1 == 0);
        }
    }
}