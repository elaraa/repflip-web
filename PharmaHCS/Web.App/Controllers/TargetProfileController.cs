﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;

namespace Web.App.Controllers
{
    public class TargetProfileController : BaseControllerList<vw_TargetProfile, Target, int>
    {
        public TargetProfileController()
        {
            ViewNameSingular = ViewNamePlural = PH.TargetAnalyzer.ViewNameTargetProfile;
            
        }
        protected override Expression<Func<Target, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TargetId == id.Value) : (Expression<Func<Target, bool>>)(x => 1 == 0);
        }
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_TargetProfile>(UserId);
            ActionService = new TargetService(UserId);
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.TimeDefs = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);

        }
        public FileResult DownloadTemplate(int timdefId, int countryId,int? teamId) 
        {
            var fileName = string.Format("Profile Target_{0}.xlsx", timdefId);
            var table = (ActionService as TargetService).ExportProfileProductTerritory(timdefId, countryId,teamId);

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "ProfileTarget")
                , MimeMapping.GetMimeMapping(fileName))
            {
                FileDownloadName = fileName
            };
        }
        //public ActionResult Create() {
        //    return View(new VMTargetProfile { TargetTimeDefId = new BaseService<TimeDefinition>().GetOne(w=> w.FromDate<= DateTime.Now  && DateTime.Now >= w.ToDate).TimeDefinitionId });
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMTargetProfile model)
        {
            FillViewBag(true);
            var err = "";
            if (ModelState.IsValid && model.File.ContentLength > 0)
            {
                //get data
                //pass it to service
                var result = (ActionService as TargetService)
                       .ImportProfileProductTerritoryTarget(model.ExtractTimeDefId, model.TargetTimeDefId, model.CountryId, FileHelper.GetDateSetFromExcel(model.File.InputStream));
                //if succeeded return to list
                //save file
                if (result.Succeeded)
                {
                    model.File.SaveTargetProfileFile(model.TargetTimeDefId, UserId);
                    return RedirectToAction("Index");
                }
                else
                    err = string.Join("<br/>", result.ErrorList);
            }
            err += GetModelStateErrors(ModelState);
            ShowError(err);
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            FillViewBag(false);
            return View(ActionService.GetOne(w => w.TargetId == id));
        }
        public ActionResult ReadSupSummaryList([DataSourceRequest] DataSourceRequest request, int year, int countryId) => Json((ActionService as TargetService).GetTargetProfile_SupervisorSummary(year, countryId).ToDataSourceResult(request));
    }
}