﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using static Services.Helper.EnumHelpers;

namespace Web.App.Controllers
{
    public class BulkTargetAdjustmentController : BaseController
    {
        public BulkTargetAdjustmentController() : base()
        {
            ViewNamePlural = PH.BulkTargetAdjustment.ViewNamePlural;
            ViewNameSingular = PH.BulkTargetAdjustment.ViewNameSingular;
        }
    

        private void FillViewBag()
        {
            ViewBag.Title = ViewNamePlural; 
            ViewBag.Products = LookupHelper.GetMyProducts(this);
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
        }

        public override ActionResult Index()
        {
            FillViewBag();
            ViewBag.Layout = Layout;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(vw_TargetAdjustment target)
        {
            try
            {
                target.TypeId = (int)TargetAdjustmentType.TargetSales;
                RunResult result = new TargetAdjustmentService(UserId).AdjustTarget(target);
                if (result != null && result.Succeeded == true)
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Success, "Success", result.SuccessDesc, true);
                else
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", result.ErrorDesc, true);
            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", e.Message, true);
            }
            return RedirectToAction("Index");
        }

    }
}