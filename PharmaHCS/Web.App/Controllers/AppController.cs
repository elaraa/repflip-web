﻿using Services.Business;
using System.Web.Mvc;
using Web.App.Models;

namespace Web.App.Controllers
{
    public class AppController : Controller
    {
        // GET: App
        public ActionResult Index(string redirectUrl)
        {
            return View(new VMLogin { RedirectUrl = redirectUrl });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(VMLogin model)
        {
            if (ModelState.IsValid)
            {
                var res = LoginService.Authenticate(model.LoginName, model.Password);
                if (res.Result == LoginService.AuthenticationResult.UserFound)
                {
                    Session["SystemUserId"] = res.UserId;
                    Session["UserFullName"] = res.FullName;
                    Session["UserFirstName"] = res.FirstName;
                    Session["UserLastName"] = res.LastName;
                    Session["pic"] = res.Picture;

                    return !string.IsNullOrEmpty(model.RedirectUrl)
                        ? Redirect(model.RedirectUrl)
                        : (ActionResult)(!LoginService.IsRep(res.UserId) ? RedirectToAction("Index", "Home") : RedirectToAction("Index", "RepHome"));
                }
            }
            ViewBag.Message = new ViewBagMessage
            {
                MessageType = ViewBagMessage.ViewBagMessageType.Error,
                MessageTitle = PH.Message.Error,
                Message =PH.Message.message
            };
            return View(model);
        }
        public ActionResult UnAuthorized() { return View(); }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToActionPermanent("Index");
        }
        public ActionResult NotFound(HandleErrorInfo exception)
        {
            return View(exception);
        }
        public ActionResult GenericError(HandleErrorInfo exception)
        {
            return RedirectToAction("NotFound", new { exception });
        }
    }
}