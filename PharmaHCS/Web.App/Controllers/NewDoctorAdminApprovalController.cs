﻿using AutoMapper;
using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using Services.Business.CRM.NewDoctorRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.ProfileTargetNewDoctorApproval;

namespace Web.App.Controllers
{
    public class NewDoctorAdminApprovalController : BaseControllerList<vw_ProfiletargetNewDoctor, ProfileTargetNewDoctorApproval, int>
    {
        public NewDoctorAdminApprovalController() : base()
        {
            ViewNamePlural = PH.NewDoctorRequest.ViewNamePlural;
            ViewNameSingular = PH.NewDoctorRequest.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<ProfileTargetNewDoctorApproval, object>>[]
            {
                    x => x.ApprovalStatus
            };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new AdminService(UserId); 
            ListService = new BaseService<vw_ProfiletargetNewDoctor>(UserId);
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.Potentials = LookupHelper.GetMyPotentials(this);
            ViewBag.Specialties = LookupHelper.GetMySpecialties(this);
            ViewBag.Genders = LookupHelper.GetGenders(this);
            ViewBag.AccountTypes = LookupHelper.GetMyAccountTypes(this);
            ViewBag.AccountCategories = LookupHelper.GetMyAccountCategories(this);
            base.FillViewBag(isCreate);
        }
        public override ActionResult ReadList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(ListService.Get(null,true).ToList().Where(a => a.ApprovalStatus.ToLower()== Services.Helper.EnumHelpers.DescriptionAttr(Services.Helper.EnumHelpers.NewDoctorSataus.Approved) 
            && (a.AdminApprove.ToLower()!= Services.Helper.EnumHelpers.DescriptionAttr(Services.Helper.EnumHelpers.NewDoctorSataus.Rejected) 
            && a.AdminApprove.ToLower() != Services.Helper.EnumHelpers.DescriptionAttr(Services.Helper.EnumHelpers.NewDoctorSataus.Approved))).ToDataSourceResult(request));
        }

        public ActionResult CheckRequest(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FillViewBag(false);
            ViewBag.TimeDif = LookupHelper.GetAdminTimeDefinitions(this,id.Value);

            //return EditChildEntity(id, id.HasValue ? (x => x.TargetDoctorApprovalId == id.Value) : (Expression<Func<ProfileTargetNewDoctorApproval, bool>>)(x => 1 == 0), "CheckRequest", new BaseService<ProfileTargetNewDoctorApproval>(UserId));
            return View((ActionService as AdminService).GetDoctorRequest(id.Value));

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(ProfileTargetNewDoctorApprovalDto model) => ApproveReject(model, true);


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reject(ProfileTargetNewDoctorApprovalDto model) => ApproveReject(model, false);
        private ActionResult ApproveReject(ProfileTargetNewDoctorApprovalDto model, bool approve)
        {
            if ((ActionService as AdminService).SaveModel(model, approve).Succeeded)
            {
                ShowSuccess(string.Format("{0} {1}!",PH.NewDoctorRequest.Doctor ,approve ? PH.SWAL.Approved : PH.SWAL.Rejected), true);
                return RedirectToAction("index");
            }
            ShowError(string.Format("{0} {1}",PH.NewDoctorRequest.Failedto ,approve ? PH.SWAL.Approve : PH.SWAL.Reject));
            FillViewBag(false);
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult ApproveRequest(int? id)
        {
            if (!id.HasValue)
                return View(false);
            return Json((ActionService as AdminService).ApproveReject(id.Value, true), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RejectRequest(int? id)
        {
            if (!id.HasValue)
                return View(false);
            return Json((ActionService as AdminService).ApproveReject(id.Value, false), JsonRequestBehavior.AllowGet);
        }

    }
}
