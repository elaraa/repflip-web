﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using Web.App.Models;

namespace Web.App.Controllers
{
    public class RepTargetVisitController : BaseControllerList<vw_ProfileTargetVisit, ProfileTargetVisit, int>
    {
        public RepTargetVisitController() : base()
        {
            ViewNamePlural =PH.RepTargetVisit.ViewNamePlural ;
            ViewNameSingular = PH.RepTargetVisit.ViewNameSingular;
            ApplySecurity = false;
        }
        public override ActionResult Create()
        {
            FillViewBag(true);
            return View();
        }
        protected override void InitTemplateServices()
        {
            base.InitTemplateServices();
            ActionService = new RepTarget(UserId);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMRepTarget model)
        {
            FillViewBag(true);
            if (ModelState.IsValid)
            {
                var res = (ActionService as RepTarget).CreateTarget(model.TimeDefinitionId, (RepTargetCreateOption)model.CreateOption);
                if (res.Success)
                {
                    ShowSuccess(PH.RepTargetVisit.Targetcreratedsuccessfully, true);
                    return RedirectToAction("Edit", new { id = 1 });
                }
                else
                {
                    ShowError(
                        res.ResultId == -1 ?
                        PH.RepTargetVisit.TargetCreateForthisMonth : PH.RepTargetVisit.Donthaveprofile);
                    return View(model);
                }
            }
            else
            {
                ShowError(GetModelStateErrors(ModelState));
                return View(model);
            }
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            if (isCreate)
            {
                var createOption = new List<SelectListItem>
                {
                    new SelectListItem { Text = PH.RepTargetVisit.CopyFromPreviousPeriod, Value = "1" },
                    new SelectListItem { Text = PH.RepTargetVisit.Empty, Value = "2" }
                };
                ViewBag.CreateOption = createOption;
                ViewBag.TimeDef = LookupHelper.GetTimeDefinitions(this);
            }
            else
            {
                ViewBag.potentials = new BaseService<Potential>(UserId).Get(null, true);
            }
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FillViewBag(false);
            var profileTargetVisit = new RepTarget(UserId).GetProfileTargetVisit(id.Value);
            return View(profileTargetVisit);
           // return EditChildEntity(id, id.HasValue ? (x => x.ProfileTargetId == id.Value) : (Expression<Func<vw_ProfileTargetVisit, bool>>)(x => 1 == 0), "Edit", new BaseService<vw_ProfileTargetVisit>(UserId));
        }
        public ActionResult ReadUniverseList([DataSourceRequest] DataSourceRequest request, int targetId) => Json(new RepTarget(UserId).GetTargetUniverse(targetId).ToDataSourceResult(request));
        public ActionResult ReadDetailsList([DataSourceRequest] DataSourceRequest request, int targetId) => Json(new RepTarget(UserId).GetTargetVisitDetails(targetId).ToDataSourceResult(request));
        [HttpPost]
        public ActionResult ReadAccounts(int targetId)
        { 
            var Accounts = new RepTarget(UserId).GetAccounts(targetId);
            return Json(Accounts);
        }
        [HttpPost]
        public JsonResult AddAccount(int accountId, int? doctorId, int targetId) => Json(new RepTarget(UserId).AddAccount(targetId, accountId, doctorId));
        [HttpPost]
        public JsonResult DeleteAccount(int? id)
        {
            if (id.HasValue)
                return Json(new RepTarget(UserId).DeleteAccount(id.Value));
            else return Json(false);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDetailsList([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<vw_ProfileTargetVisitDetails> items)
        {
            var srv = new RepTarget(UserId);
            if (items != null && ModelState.IsValid)
            {
                foreach (var item in items)
                {
                    srv.UpdateAccount(item.ProfileTargetDetailId, item.PotentialId, item.Frequency);
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public JsonResult Add(int targetId,List<AccountDoctorModel> list) => Json(new RepTarget(UserId).Add(targetId,list));

        public JsonResult ReloadModel(int? id)
        {
            var profileTargetVisit = new RepTarget(UserId).GetProfileTargetVisit(id.Value);
            return Json(profileTargetVisit, JsonRequestBehavior.AllowGet);
        }
    }
}