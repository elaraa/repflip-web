﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using Services.Business.Sales.MovementLogDTo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class TierController : BaseControllerTemplate<Tier, Tier, int>
    {
        public TierController() : base()
        {
            ViewNamePlural =PH.Tier.ViewNamePlural;
            ViewNameSingular = PH.Tier.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Tier, object>>[]
            {
                    x=>x.TierName
            };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new TierService(UserId);
            ListService = new BaseService<Tier>(UserId);
        }
        protected override Expression<Func<Tier, bool>> GetOneExpression(int? id) =>
            id.HasValue ? (x => x.TierId == id.Value) : (Expression<Func<Tier, bool>>)(x => 1 == 0);
       
        

        [HttpPost]
        public ActionResult SaveTier(Tier model)
        {

            FillViewBag(true);
            return model.TierId == 0 ?
                base.CreateChildEntity<Tier>(model, "Index", null, "Index", ActionService) :
                    base.EditChildEntity<Tier>(model, PropertiesToUpdate, "Index", null, "Index", ActionService);
        }

        [HttpPost]
        public JsonResult DeleteTier(Tier model)
        {
            FillViewBag(false);
            if (model == null)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));

            return Json((ActionService as TierService).DeleteTier(model.TierId), JsonRequestBehavior.AllowGet);
        }
    }

}