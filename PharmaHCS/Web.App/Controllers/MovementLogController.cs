﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business.Sales;
using Services.Business.Sales.MovementLogDTo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class MovementLogController : BaseController
    {
        public MovementLogController() : base()
        {
            ViewNamePlural = PH.MovementLog.ViewNamePlural;
            ViewNameSingular = PH.MovementLog.ViewNameSingular;
        }
        public override ActionResult Index()
        {
            ViewBag.MedicalRep = LookupHelper.GetMyMedicalRep(this);

            return View(new MovementDto { Date = DateTime.Now });
        }

        [HttpPost]
        public ActionResult mapVisits(int? employeeId = null, DateTime? date = null)
        {
            var Visits = new MovementLogService(UserId).findVisits(employeeId,date);

            return Json(Visits);
        } 

        public ActionResult ReadEmployeeVisits([DataSourceRequest] DataSourceRequest request, int? employeeId = null, DateTime? date = null)
        { 
            return Json(new MovementLogService(UserId).getVisits(employeeId, date).ToDataSourceResult(request));
        }
    }

}