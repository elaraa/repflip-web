﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class DistributorController : BaseControllerTemplate<vw_Distributor, Distributor, int>
    {


        public DistributorController() : base()
        {
            ViewNamePlural = PH.Distributor.ViewNamePlural;
            ViewNameSingular = PH.Distributor.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Distributor, object>>[]
                {
                    x => x.DistributorCode,
                    x =>x.DistributorName,
                    x => x.SalesPluginId,
                    x => x.CountryId

                };
        }

        protected override Expression<Func<Distributor, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.DistributorId == id.Value) : (Expression<Func<Distributor, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);

            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.SalesPlugins = LookupHelper.GetMySalesPlugins(this);

        }






        #region Distributor Product Mapping

        //List
        public ActionResult ReadDistributorProductMappingList([DataSourceRequest] DataSourceRequest request, int DistributorId)
        {
            return Json(new BaseService<vw_DistributorProductMapping>(UserId).Get(w => w.DistributorId == DistributorId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_DistributorProductMapping(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Distributor Product Mapping", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Distributor Product Mapping";


            ViewBag.Distributors = LookupHelper.GetMyDistributors(this);
            ViewBag.DosageForms = LookupHelper.GetMyDosageForms(this);


        }
        //create
        public ActionResult CreateDistributorProductMapping(int distributorId)
        {
            FillViewBag_DistributorProductMapping(true);
            var model = new DistributorProductMapping { DistributorId = distributorId };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDistributorProductMapping(DistributorProductMapping distributorProductMapping)
        {
            FillViewBag_DistributorProductMapping(true);
            return CreateChildEntity<DistributorProductMapping>(distributorProductMapping, "Edit", new { id = distributorProductMapping.DistributorId }
            , "CreateDistributorProductMapping");
        }
        //edit
        public ActionResult EditDistributorProductMapping(int id)
        {
            FillViewBag_DistributorProductMapping(false);
            return EditChildEntity<int, DistributorProductMapping>(id, x => x.DistributorProductMappingId == id, "EditDistributorProductMapping");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDistributorProductMapping(DistributorProductMapping distributorProductMapping)
        {
            FillViewBag_DistributorProductMapping(false);
            return EditChildEntity<DistributorProductMapping>(distributorProductMapping, new Expression<Func<DistributorProductMapping, object>>[]
                {
                    x => x.DistributorId,
                     x => x.DosageFormId,
                      x => x.DosageFormCode,
                       x => x.DosageFormName,

                }
            , "Edit", new { id = distributorProductMapping.DistributorId }, "EditDistributorProductMapping");
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteDistributorProductMapping(int? id)
        {
            return DeleteChildEntity<int, DistributorProductMapping>(id, x => x.DistributorProductMappingId == id);
        }

        #endregion




        #region Distributor Account Mapping

        //List
        public ActionResult ReadDistributorAccountMappingList([DataSourceRequest] DataSourceRequest request, int DistributorId)
        {
            return Json(new BaseService<vw_DistributorAccountMapping>(UserId).Get(w => w.DistributorId == DistributorId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_DistributorAccountMapping(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Distributor Account Mapping", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Distributor Account Mapping";


            ViewBag.Distributors = LookupHelper.GetMyDistributors(this);
            //ViewBag.Accounts = LookupHelper.GetMyAccounts(this);


        }
        //create
        public ActionResult CreateDistributorAccountMapping(int distributorId)
        {
            FillViewBag_DistributorAccountMapping(true);
            var model = new DistributorAccountMapping { DistributorId = distributorId };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDistributorAccountMapping(DistributorAccountMapping distributorAccountMapping)
        {
            FillViewBag_DistributorAccountMapping(true);
            return CreateChildEntity<DistributorAccountMapping>(distributorAccountMapping, "Edit", new { id = distributorAccountMapping.DistributorId }
            , "CreateDistributorAccountMapping");
        }
        //edit
        public ActionResult EditDistributorAccountMapping(int id)
        {
            FillViewBag_DistributorAccountMapping(false);
            return EditChildEntity<int, DistributorAccountMapping>(id, x => x.DistributorAccountMappingId == id, "EditDistributorAccountMapping");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDistributorAccountMapping(DistributorAccountMapping distributorAccountMapping)
        {
            FillViewBag_DistributorAccountMapping(false);
            return EditChildEntity<DistributorAccountMapping>(distributorAccountMapping, new Expression<Func<DistributorAccountMapping, object>>[]
                {
                    x => x.DistributorId,
                    x => x.AccountId,
                    x => x.AccountCode,
                    x => x.AccountName,
                    x => x.AccountAddress,
                    x => x.AccountPhone,

                }
            , "Edit", new { id = distributorAccountMapping.DistributorId }, "EditDistributorAccountMapping");
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteDistributorAccountMapping(int? id)
        {
            return DeleteChildEntity<int, DistributorAccountMapping>(id, x => x.DistributorAccountMappingId == id);
        }

        #endregion




        #region Distributor Branch 

        //List
        public ActionResult ReadDistributorBranchList([DataSourceRequest] DataSourceRequest request, int DistributorId)
        {
            return Json(new BaseService<vw_DistributorBranch>(UserId).Get(w => w.DistributorId == DistributorId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_DistributorBranch(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Distributor Branch", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Distributor Branch";


            ViewBag.Distributors = LookupHelper.GetMyDistributors(this);

        }
        //create
        public ActionResult CreateDistributorBranch(int distributorId)
        {
            FillViewBag_DistributorBranch(true);
            var model = new DistributorBranch { DistributorId = distributorId };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDistributorBranch(DistributorBranch distributorBranch)
        {
            FillViewBag_DistributorAccountMapping(true);
            return CreateChildEntity<DistributorBranch>(distributorBranch, "Edit", new { id = distributorBranch.DistributorId }
            , "CreateDistributorBranch");
        }
        //edit
        public ActionResult EditDistributorBranch(int id)
        {
            FillViewBag_DistributorBranch(false);
            return EditChildEntity<int, DistributorBranch>(id, x => x.BranchId == id, "EditDistributorBranch");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDistributorBranch(DistributorBranch distributorBranch)
        {
            FillViewBag_DistributorBranch(false);
            return EditChildEntity<DistributorBranch>(distributorBranch, new Expression<Func<DistributorBranch, object>>[]
                {
                    x => x.DistributorId,
                    x => x.BranchName,
                    x => x.BranchCode,

                }
            , "Edit", new { id = distributorBranch.DistributorId }, "EditDistributorBranch");
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteDistributorBranch(int? id)
        {
            return DeleteChildEntity<int, DistributorBranch>(id, x => x.BranchId == id);
        }

        #endregion


    }
}