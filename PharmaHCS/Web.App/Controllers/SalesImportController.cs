﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;
using ImportedFileStatus = Services.Business.Sales.ImportedFileStatus;

namespace Web.App.Controllers
{
    public class SalesImportController : BaseControllerList<vw_ImportedFile, ImportedFile, int>
    {
        public SalesImportController() : base()
        {
            ViewNamePlural = PH.SalesImport.ViewNamePlural;
            ViewNameSingular = PH.SalesImport.ViewNamePlural;
        }

        protected override Expression<Func<ImportedFile, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.FileId == id.Value) : (Expression<Func<ImportedFile, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            if (isCreate)
            {
                ViewBag.Distributors = LookupHelper.GetMyDistributors(this);
                ViewBag.TimeDef = LookupHelper.GetTimeDefinitions(this);
            }
            else
            {
                ViewBag.Territories = LookupHelper.GetMyTerritories(this);
                ViewBag.AccountCategories = LookupHelper.GetMyAccountCategories(this);
                ViewBag.AccountTypes = LookupHelper.GetMyAccountTypes(this);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(VMSalesImport model)
        {
            FillViewBag(true);
            if (!ModelState.IsValid)
                return View(model);

            var fileId = new SalesImportEngine(UserId, Request.PhysicalApplicationPath)
                .Import(model.DistributorId, model.TimeDefinitionId, model.Files.GetInput(), model.ImportMode);
            if (fileId < 1)
                return View(model);
            model.Files.SaveSalesImport(fileId);//save files into proper folder
            return RedirectToAction("Edit", new { id = fileId });
        }
        public ActionResult Edit(int id)
        {
            FillViewBag(false);
            var details = new SalesImportService(UserId, id);
            var model = details.FileHeaderDetails;
            ViewBag.Summary = details.FileSummary.ToList();
            ViewBag.UnmappedAccounts = ((List<fn_GetImportedFileSummary>)ViewBag.Summary).FirstOrDefault(w => w.Factor == "Unmapped Accounts").Current;
            ViewBag.UnmappedProducts = ((List<fn_GetImportedFileSummary>)ViewBag.Summary).FirstOrDefault(w => w.Factor == "Unmapped Products").Current;
            ViewBag.ShowDownload = ValidateFilesExist(id);
            ViewBag.Products = (int)ViewBag.UnmappedProducts > 0 ? new BaseService<vw_Product>(UserId).Get() : null;
            ViewBag.Actions = SalesImportService.GetActions(details.FileHeaderDetails.StatusId);

            ViewBag.CanMapAccount = SalesImportService.CanMapAccount(details.FileHeaderDetails.StatusId);
            ViewBag.CanMapProduct = SalesImportService.CanMapProduct(details.FileHeaderDetails.StatusId);

            switch ((ImportedFileStatus)model.StatusId)
            {
                case ImportedFileStatus.UnderValidation:
                    ViewBag.StatusColor = "default";
                    break;
                case ImportedFileStatus.SavedbyTerritoryOnly:
                    ViewBag.StatusColor = "primary";
                    break;
                case ImportedFileStatus.Saved:
                    ViewBag.StatusColor = "success";
                    break;
                case ImportedFileStatus.Deleted_Full:
                case ImportedFileStatus.Deleted_withoutMappings:
                    ViewBag.StatusColor = "warning";
                    break;
                case ImportedFileStatus.Failed:
                default:
                    ViewBag.StatusColor = "danger";
                    break;
            }
            return View(model);
        }
        bool ValidateFilesExist(int id)
        {
            ///check if the folder for the fileId exists
            if (!Directory.Exists(HttpContext.Request.PhysicalApplicationPath + @"\Uploads\Sales\Imports\" + id)) return false;
            ///get all files inside the folder and pass it to return
            var filePaths = Directory.GetFiles(HttpContext.Request.PhysicalApplicationPath + @"\Uploads\Sales\Imports\" + id);
            if (filePaths.Length < 1) return false;

            return true;
        }
        public FileResult DownloadFiles(int id)
        {
            ///check if the folder for the fileId exists
            if (!Directory.Exists(HttpContext.Request.PhysicalApplicationPath + @"\Uploads\Sales\Imports\" + id)) return null;
            ///get all files inside the folder and pass it to return
            var filePaths = Directory.GetFiles(HttpContext.Request.PhysicalApplicationPath + @"\Uploads\Sales\Imports\" + id);
            if (filePaths.Length < 1) return null;

            using (var memStream = new MemoryStream())
            {
                using (var zip = new ZipArchive(memStream, ZipArchiveMode.Create, true))
                {
                    foreach (var fPath in filePaths)
                        zip.CreateEntryFromFile(fPath, Path.GetFileName(fPath));
                }
                return File(memStream.ToArray(), "application/zip", "Attachments.zip");
            }
        }
        public ActionResult ReadMappedProductsList([DataSourceRequest] DataSourceRequest request, int fileId) => Json(new SalesImportService(UserId, fileId).MappedProducts.ToList().ToDataSourceResult(request));
        public ActionResult ReadUnmappedAccountsList([DataSourceRequest] DataSourceRequest request, int fileId) => Json(new SalesImportService(UserId, fileId).UnmappedAccounts.ToDataSourceResult(request));
        public ActionResult ReadUnmappedAccountsToCreateList([DataSourceRequest] DataSourceRequest request, int fileId) => Json(new SalesImportService(UserId, fileId).UnmappedAccountsToCreate.ToDataSourceResult(request));
        public ActionResult ReadUnmappedProductsList([DataSourceRequest] DataSourceRequest request, int fileId) => Json(new SalesImportService(UserId, fileId).UnmappedProducts.ToDataSourceResult(request));

        public ActionResult ReadMappedAccountsList([DataSourceRequest] DataSourceRequest request, int fileId)
        {
            return Json(new SalesImportService(UserId, fileId).MappedAccounts.ToDataSourceResult(request));
        }

        public ActionResult ReadAccountsList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new BaseService<vw_Account>(UserId).Get(null, true).ToDataSourceResult(request));
        }

        public ActionResult ReadDosagesList([DataSourceRequest] DataSourceRequest request) => Json(new BaseService<vw_DosageForm>(UserId).Get(null, true).ToDataSourceResult(request));
        public JsonResult MapAccount(int fileId, int recordId, int accountId) => Json(new SalesImportService(UserId, fileId).MapAccount(recordId, accountId));
        public JsonResult MapProduct(int fileId, int recordId, int dosageId) => Json(new SalesImportService(UserId, fileId).MapProduct(recordId, dosageId));
        public JsonResult MarkAccountforCreation(int recordId, int fileId) => Json(new SalesImportService(UserId, fileId).MarkAccountForCreation(recordId));
        public JsonResult UndoProductMapping(int recordId, int fileId) => Json(new SalesImportService(UserId, fileId).UndoProductMap(recordId));
        public JsonResult UndoAccountMapping(int recordId, int fileId) => Json(new SalesImportService(UserId, fileId).UndoAccountMap(recordId));
        public JsonResult UndoAccountCreation(int recordId, int fileId) => Json(new SalesImportService(UserId, fileId).UndoAccountCreation(recordId));
        public JsonResult SubmitSalesImport(int id, int type) => Json(new SalesImportService(UserId, id).Submit((SalesImportService.ImportAction)type));

        public ActionResult UploadAccounts(int fileId)
        {
            var details = new SalesImportService(UserId, fileId);
            return View(details.FileHeaderDetails);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAccounts(int fileId, HttpPostedFileBase File)
        {
            var err = "";
            if (File.ContentLength > 0)
            {
                var data = FileHelper.GetDateSetFromExcel(File.InputStream);
                if (data.Tables.Count > 0)
                {
                    MasterDataUpload.UploadStatusResult result = new MasterDataUpload(UserId).UploadFile(
                        data.Tables[0]
                        , MasterDataType.Account_ImportedFile
                        , fileId.ToString()
                        );
                    if (result.Status == MasterDataUpload.UploadStatus.Success)
                        return RedirectToAction("Edit", new { id = fileId });

                    switch (result.Status)
                    {
                        case MasterDataUpload.UploadStatus.FileIsEmpty:
                            err = PH.SalesImport.FileIsEmpty;
                            break;
                        case MasterDataUpload.UploadStatus.InvalidFileHeader:
                            err = PH.SalesImport.InvalidFileHeader;
                            break;
                        case MasterDataUpload.UploadStatus.dbError_FailedtoImportData:
                            err = PH.SalesImport.dbErrorFailedtoImportData;
                            break;
                        case MasterDataUpload.UploadStatus.DataContainsErrors:
                            err = PH.SalesImport.DataContainsErrors;
                            var errList = new List<string>();
                            foreach (System.Data.DataRow row in result.failedReasons.Rows)
                                errList.Add(string.Format("Row {0}: {1}", row[0], row[1]));
                            err += "<br/>" + string.Join("<br/>", errList);
                            break;

                    }
                }
                else err = PH.SalesImport.ErrReadingFile;
                ShowError(err);
                var det = new SalesImportService(UserId, fileId);
                return View(det.FileHeaderDetails);
            }
            ShowError(PH.SalesImport.NoFileSelected);
            var details = new SalesImportService(UserId, fileId);
            return View(details.FileHeaderDetails);
        }
        public ActionResult ReadFileSummary([DataSourceRequest] DataSourceRequest request, int fileId) {
            return Json(new SalesImportService(UserId, fileId).FileSummary.ToDataSourceResult(request));
        }
        public ActionResult CreateAccount(int fileId, int recordId, string accountCode,string accountName, string accountAddress,int accountTerr, int accountCategory,int accountType) {
            return Json(new SalesImportService(UserId, fileId).CreateAndMapAccount(recordId, accountCode, accountName, accountAddress, accountTerr, accountCategory, accountType));
        }
    }
    //public static class HttpFileHelper
    //{

    //    #region PluginInput Helper
    //    public static Sales.Core.ImportFile.PluginInput GetInput(this HttpPostedFileBase file)
    //    {
    //        return new Sales.Core.ImportFile.PluginInput
    //        {
    //            FileStream = file.InputStream,
    //            FileName = file.FileName,
    //            FileContentType = file.ContentType
    //        };
    //    }
    //    public static Sales.Core.ImportFile.PluginInput[] GetInput(this HttpPostedFileBase[] files)
    //    {
    //        var inputs = new List<Sales.Core.ImportFile.PluginInput>();
    //        foreach (var item in files)
    //            inputs.Add(item.GetInput());
    //        return inputs.ToArray();

    //    }
    //    public static bool SaveSalesImport(this HttpPostedFileBase[] files, int fileId)
    //    {
    //        string rootFolder = string.Format(@"{0}\uploads\sales\imports", HttpContext.Current.Request.PhysicalApplicationPath);
    //        string _template = rootFolder + @"\{0}\{1}";
    //        try
    //        {
    //            if (!System.IO.Directory.Exists(rootFolder))
    //                System.IO.Directory.CreateDirectory(rootFolder);

    //            if (!System.IO.Directory.Exists(rootFolder + "\\" + fileId))
    //                System.IO.Directory.CreateDirectory(rootFolder + "\\" + fileId);

    //            foreach (var item in files)
    //                item.SaveAs(string.Format(_template, fileId, item.FileName));
    //            return true;
    //        }
    //        catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e); return false; }

    //    }
    //    #endregion
    //}
}