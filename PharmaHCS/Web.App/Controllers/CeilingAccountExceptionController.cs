﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class CeilingAccountExceptionController : BaseControllerTemplate<vw_CeilingAccount, CeilingAccountException, int>
    {
         
        public CeilingAccountExceptionController() : base()
        {
            ViewNamePlural = PH.AccountCeiling.ViewNamePlural;
            ViewNameSingular = PH.AccountCeiling.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<CeilingAccountException, object>>[]
                {
                    x=>x.AccountId,
                    x => x.ProductId,
                    x => x.FromTimeId,
                    x => x.ToTimeId

                };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new AccountCeilingService(UserId);
            ListService = new BaseService<vw_CeilingAccount>(UserId);
        }
        protected override Expression<Func<CeilingAccountException, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.CeilingAccountExceptionId == id.Value) : (Expression<Func<CeilingAccountException, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.TimeDef = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Products = LookupHelper.GetMyProducts(this);
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(CeilingAccountException model)
        {
            FillViewBag(true);
            var result = (ActionService as AccountCeilingService).IsOverlapped(model);
            return AddOrUpdateModel(model, null, "Index", null, "Create", result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(CeilingAccountException model)
        {
            FillViewBag(false);
            var result = (ActionService as AccountCeilingService).IsOverlapped(model);
            return AddOrUpdateModel(model, PropertiesToUpdate, "Index", null, "Edit", result);
        }



      


    }
}