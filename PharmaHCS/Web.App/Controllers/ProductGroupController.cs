﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ProductGroupController : BaseControllerTemplate<vw_ProductGroup, ProductGroup, int>
    {

        public ProductGroupController() : base()
        { 
            ViewNamePlural = PH.Product.ViewNamePluralProductGroup;
            ViewNameSingular = PH.Product.ViewNameSingularProductGroup;
            
            PropertiesToUpdate = new Expression<Func<ProductGroup, object>>[]
                {
                    x=>x.GroupName,
                    x => x.GroupCode,
                    x => x.ParentGroupId,
                    x => x.CountryId,
                };
        }

        protected override void InitTemplateServices()
        {
            ActionService = new ProductGroupService(UserId);
            ListService = new BaseService<vw_ProductGroup>(UserId);
        }
        protected override Expression<Func<ProductGroup, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.GroupId == id.Value) : (Expression<Func<ProductGroup, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            
            ViewBag.ProductGroups = LookupHelper.GetMyProductGroups(this);
            ViewBag.Country = LookupHelper.GetMyCountries(this);

        }
        public override ActionResult Edit(int? id)
        {
            ViewBag.ProductGroups = LookupHelper.GetMyProductGroups(this,id);
            ViewBag.Country = LookupHelper.GetMyCountries(this);

            return EditChildEntity(id, id.HasValue ? (x => x.GroupId == id.Value) : (Expression<Func<ProductGroup, bool>>)(x => 1 == 0), "Edit");
        }
        

        [HttpPost] 
        public ActionResult DeleteGroup(int? id)
        {
            var result = (ActionService as ProductGroupService).deleteGroup(id.Value);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}