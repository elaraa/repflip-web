﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class RepHomeController : BaseController
    {
        public RepHomeController()
        {
            ApplySecurity = false;
        }
        
        public ActionResult _GetRepCoverage_Table([DataSourceRequest] DataSourceRequest request) => Json(new BaseService<DB.ORM.DB.VisitsCoverage>(UserId).Get(w => w.EmployeeId == UserId, true).ToDataSourceResult(request));
        public ActionResult _GetRepCallRate_Table([DataSourceRequest] DataSourceRequest request) => Json(new BaseService<DB.ORM.DB.VisitsCallRate>(UserId).Get(w => w.EmployeeId == UserId, true).ToDataSourceResult(request));

    }
}