﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class PotentialController : BaseControllerTemplate<vw_Potential, Potential, int>
    {
        public PotentialController() : base()
        {
            ViewNamePlural =PH.Potential.ViewNamePlural;
            ViewNameSingular = PH.Potential.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Potential, object>>[]
                {
                    x=>x.PotentialCode,
                    x => x.PotentialName,
                    x => x.CountryId
                };
        }

        protected override Expression<Func<Potential, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.PotentialId == id.Value) : (Expression<Func<Potential, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            base.FillViewBag(isCreate);
        }
    }
}
