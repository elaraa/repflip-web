﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;

namespace Web.App.Controllers
{
    public class TargetProductController : BaseControllerList<vw_TargetProduct, Target, int>
    {
        public TargetProductController()
        {
            ViewNameSingular = ViewNamePlural = PH.TargetAnalyzer.ViewNameProduct;
        }
        protected override Expression<Func<Target, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TargetId == id.Value) : (Expression<Func<Target, bool>>)(x => 1 == 0);
        }
        protected override void InitTemplateServices()
        {
            ListService = new Services.Business.BaseService<vw_TargetProduct>(UserId);
            ActionService = new Services.Business.TargetService(UserId);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMMarketShareImport model)
        {
            FillViewBag(true);
            if (ModelState.IsValid && model.File.ContentLength > 0)
            {
                //get data
                //pass it to service
                var result = (ActionService as TargetService)
                       .ImportProductTarget(model.Year, model.CountryId, FileHelper.GetDateSetFromExcel(model.File.InputStream));
                //if succeeded return to list
                //save file
                if (result.Succeeded)
                {
                    model.File.SaveTargetProductFile(model.Year);
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public FileResult DownloadTemplate(int year, int countryId) {
            var date = DateTime.Parse(string.Format("{0}-{1}-{2}",year, DateTime.Now.Month, DateTime.Now.Day));
            var fileName = string.Format("Product Target_{0}.xlsx", year);
            var table = (ActionService as TargetService).ExportProductTarget(date, countryId);

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "ProductTarget")
                , MimeMapping.GetMimeMapping(fileName))
                 {
                     FileDownloadName = fileName
                };
        }
        public ActionResult Edit(int id)
        {
            FillViewBag(false);
            return View(ActionService.GetOne(w=>w.TargetId == id));
        }
        public ActionResult ReadDetailsList([DataSourceRequest] DataSourceRequest request, int year, int countryId)
        {
            return Json(new BaseService<vw_TargetProduct_Details>(UserId)
                .Get(w => w.Year== year && w.CountryId == countryId, true).ToDataSourceResult(request));
        }
    }
}