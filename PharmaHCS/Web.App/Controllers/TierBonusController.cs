﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class TierBonusController : BaseControllerTemplate<vw_TierBonus, TierBonu, int>
    {
        public TierBonusController() : base()
        {
            ViewNamePlural = PH.Tier.TierBonusViewNamePlural;
            ViewNameSingular = PH.Tier.TierBonusViewNameSingular;

            PropertiesToUpdate = new Expression<Func<TierBonu, object>>[]
            {
                    x =>x.ProductId,
                    x => x.TierId,
                    x => x.BonusQty,
                    x => x.SalesQty,
                    x=>x.FromPeriod,
                    x=>x.ToPeriod
            };
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.Products= LookupHelper.GetMyProducts(this);
            ViewBag.Tiers = LookupHelper.GetMyTiers(this);

        }
        protected override Expression<Func<TierBonu, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TierBonusId == id.Value) : (Expression<Func<TierBonu, bool>>)(x => 1 == 0);
        }

      

    }
}