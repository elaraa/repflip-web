﻿using AutoMapper;
using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.AccountDoctor;
using static DB.ORM.DB.Doctor;
using static DB.ORM.DB.LinkedAccountSale;

namespace Web.App.Controllers
{
    public class DoctorController : BaseControllerTemplate<vw_Doctors, Doctor, int>
    {
        public DoctorController() : base()
        {
            ViewNamePlural = PH.Doctor.ViewNamePlural;
            ViewNameSingular = PH.Doctor.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Doctor, object>>[]
                {
                    x=>x.DoctorName,
                    x => x.PotentialId,
                    x => x.CountryId,
                    x => x.BirthDate,
                    x => x.Gender,
                    x => x.SpecialtyId,
                    x => x.Mobile,
                    x => x.Approved
                };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new DoctorService(UserId);
            ListService = new BaseService<vw_Doctors>(UserId);
        }
        protected override Expression<Func<Doctor, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.DoctorId == id.Value) : (Expression<Func<Doctor, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.Potentials = LookupHelper.GetMyPotentials(this);
            ViewBag.Specialties = LookupHelper.GetMySpecialties(this);
            ViewBag.Genders = LookupHelper.GetGenders(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.AccountCategories = LookupHelper.GetMyAccountCategories(this);
            ViewBag.AccountTypes = LookupHelper.GetMyAccountTypes(this);


            base.FillViewBag(isCreate);
        }

        public override ActionResult Create()
        {
            FillViewBag(true);
            return View(new DoctorDto());
        }
        public override ActionResult Edit(int? id)
        {
            FillViewBag(true);
            
            return View((ActionService as DoctorService).GetDoctor(id.Value));

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(DoctorDto model)
        {

            if (ModelState.IsValid)
            {
                
                var result = (ActionService as DoctorService).saveDoctorModel(model);
                if (result != null && result.Succeeded == true)
                {
                    ShowSuccess(result.SuccessDesc == "Create" ? PH.Message.SuccessCode : PH.Message.SuccessEditCode, true);
                    return RedirectToAction("index");
                }
                else
                {
                    ShowError(PH.Message.CodeError);
                    return View(model);
                }
            }
            else
            {
                ViewModelStateErrors(ModelState);
                return View(model);
            }
        }

        #region Doctor Account

        //List
        public ActionResult ReadDoctorAccount([DataSourceRequest] DataSourceRequest request, int DoctorId)
        {
            return Json(new BaseService<vw_AccountDoctor>(UserId).Get(w => w.DoctorId == DoctorId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_EmployeeProfile(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", PH.Doctor.DoctorAccount, isCreate ? "Create" : "Edit");
            ViewBag.EntityName = PH.Doctor.DoctorAccount;
            // ViewBag.Doctors = LookupHelper.GetMyDoctors(this);
        }
        //create
        public ActionResult LinkDoctorAccount(int DoctorId)
        {
            FillViewBag_EmployeeProfile(true);
            var model = new AccountDoctorDto { DoctorId = DoctorId };
            return View(model);
        } 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkDoctorAccount(AccountDoctorDto accountDoctor)
        {
            if (ModelState.IsValid)
            {
                FillViewBag_EmployeeProfile(true);
                var result = (ActionService as DoctorService).saveModel(accountDoctor);
                if (result != null && result.Succeeded == true)
                {
                    ShowSuccess(PH.Message.SuccessCode,true);
                    return RedirectToAction("Edit", new { id = accountDoctor.DoctorId });

                }
                else
                {
                    ShowError(PH.Message.CodeError);
                    return View(accountDoctor);
                }
            }
            else
            {
                ViewModelStateErrors(ModelState);
                return View(accountDoctor);
            }
              
        }


        //delete
        [HttpPost]
        public virtual JsonResult DeleteDoctorAccount(int? id)
        {
            return DeleteChildEntity<int, AccountDoctor>(id, x => x.AccountId == id);
        }

        #endregion

        #region Link Accounts
        //List
        public ActionResult ReadAccounts([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new BaseService<vw_LinkedAccountSales>(UserId).Get(null, true).ToDataSourceResult(request));
        }

        private void FillViewBag_LinkAccounts(bool isCreate)
        { 
            ViewBag.Title = string.Format("{1} {0}", PH.Doctor.LinkAccounts, isCreate ? "Create" : "Edit");
            ViewBag.EntityName = PH.Doctor.LinkAccounts;
        }
        //create
        public ActionResult LinkAccounts(int doctorId)
        {
            FillViewBag_LinkAccounts(true);
            ViewBag.DoctorAccounts = (ActionService as DoctorService).getMyAccount(doctorId).ToDropDownList(a => new SelectListItem { Text = a.AccountName, Value = a.AccountId.ToString() });
            ViewBag.Accounts = (ActionService as DoctorService).getAccounts(doctorId).ToDropDownList(a => new SelectListItem { Text = a.AccountName, Value = a.AccountId.ToString() });
            var model = new LinkedAccountSalesDto { DoctorId=doctorId};
            return View(model);
        }
        [HttpPost] 
        [ValidateAntiForgeryToken]
        public ActionResult LinkAccounts(LinkedAccountSalesDto linkedAccount)
        {
            if (ModelState.IsValid)
            {
                FillViewBag_EmployeeProfile(true);
                var result = (ActionService as DoctorService).saveAccountModel(linkedAccount);
                if (result != null && result.Succeeded == true)
                {
                    ShowSuccess(PH.Message.SuccessCode, true);
                    return RedirectToAction("Edit", new { id = linkedAccount.DoctorId });

                }
                else
                {
                    ShowError(PH.Message.CodeError);
                    return View(linkedAccount);
                }
            }
            else
            {
                ViewModelStateErrors(ModelState);
                return View(linkedAccount);
            }
        }


        //delete 
        [HttpPost]
        public virtual JsonResult DeleteAccount(int? id)
        {
            return DeleteChildEntity<int, LinkedAccountSale>(id, x => x.LinkedAccountId == id);
        }
        #endregion
    }
}
