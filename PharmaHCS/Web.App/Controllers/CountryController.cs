﻿using DB.ORM.DB;
using System;
using System.Linq.Expressions;


namespace Web.App.Controllers
{
    public class CountryController : BaseControllerTemplate<vw_Country, Country, int>
    {
        public CountryController() : base()
        {
            ViewNamePlural = PH.Country.ViewNamePlural;
            ViewNameSingular = PH.Country.ViewNamePlural;

            PropertiesToUpdate = new Expression<Func<Country, object>>[]
                {
                    x=>x.CountryCode,
                    x => x.CountryName
                };
        }

        protected override Expression<Func<Country, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.CountryId == id.Value) : (Expression<Func<Country, bool>>)(x => 1 == 0);
        }
    }
}