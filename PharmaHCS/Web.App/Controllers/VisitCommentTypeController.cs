﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class VisitCommentTypeController : BaseControllerTemplate<vw_VisitCommentType, VisitCommentType, int>
    {
        public VisitCommentTypeController() : base()
        {
            ViewNamePlural = PH.VisitCommentType.ViewCommentNamePlural;
            ViewNameSingular = PH.VisitCommentType.ViewCommentNameSingular;

            PropertiesToUpdate = new Expression<Func<VisitCommentType, object>>[]
                {
                    x=>x.CommentType,
                    x => x.Active
            
                };
        }

        protected override Expression<Func<VisitCommentType, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.CommentTypeId == id.Value) : (Expression<Func<VisitCommentType, bool>>)(x => 1 == 0);
        }
    }
}