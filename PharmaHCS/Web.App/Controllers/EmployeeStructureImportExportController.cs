﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using Services.Business.Sales.TemplatesService;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using Web.App.Models;
using Web.Helpers.File;
using ImportedFileStatus = Services.Business.Sales.ImportedFileStatus;

namespace Web.App.Controllers
{
    public class EmployeeStructureImportExportController : BaseControllerList<vw_ProfileAccount, EmployeeHistory, int>
    {
        public EmployeeStructureImportExportController() : base() 
        {
            ViewNamePlural = PH.TemplatesImprtExport.ViewNamePlural;
            ViewNameSingular = PH.TemplatesImprtExport.ViewNameSingular;
        }

        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_ProfileAccount>(UserId);
            ActionService = new TemplatesService(UserId);
        }
        protected override void FillViewBag(bool isCreate)
        {
            //base.FillViewBag(isCreate);
            //
            ViewBag.Title = ViewNamePlural;
            ViewBag.types = LookupHelper.GetTemplateStructureTypes();
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.TimeDef = LookupHelper.GetTimeDefinitions(this);
            ViewBag.RecordperMinute = (ActionService as TemplatesService).GetRecordsperMinute;

        }
        public override ActionResult Index()
        {
            FillViewBag(true);
            var model = new TemplatViewModel();
            return View(model);
        }
        public ActionResult Submit(TemplatViewModel model)
        {
            FillViewBag(true);
            if (model.File == null || model.File.ContentLength < 1)
            {
                ShowError(PH.Message.Nofileselected);
                return View("Index");
            }
            var data = (ActionService as TemplatesService).uploadFiles(model);
            if (data != null && data.Tables.Count > 0)
            {
                var datatype = new MasterDataType();
                if (model.TypeId == (int)EnumHelpers.TemplateStructureType.EmployeeStructure)
                    datatype = MasterDataType.EmployeeHistoryProfile;
                else if (model.TypeId == (int)EnumHelpers.TemplateStructureType.ProfileAccount) 
                    datatype = MasterDataType.ProfileAccount;
                else
                    datatype = MasterDataType.ProfileTerritory;
                MasterDataUpload.UploadStatusResult result = new MasterDataUpload(UserId).UploadFile(
                    data.Tables[0]
                    , datatype
                    , model.TimeId.ToString()
                    );

                switch (result.Status)
                {
                    case MasterDataUpload.UploadStatus.Success:
                        ShowSuccess(PH.Message.ImportedSuccessfully);
                        break;
                    case MasterDataUpload.UploadStatus.FileIsEmpty:
                        ShowError(PH.Message.FileEmpty);
                        break;
                    case MasterDataUpload.UploadStatus.InvalidFileHeader:
                        ShowError(PH.Message.IncorrectStructureFile);
                        break;
                    case MasterDataUpload.UploadStatus.dbError_FailedtoImportData:
                        ShowError(PH.Message.Failedtoupload);
                        break;
                    case MasterDataUpload.UploadStatus.DataContainsErrors:
                        ShowError(PH.Message.Cannotupload);
                        break;
                    default:
                        break;
                }
                if (result.Status==MasterDataUpload.UploadStatus.DataContainsErrors)
                {
                    var list = new List<TemplateValidation>();
                    if (model.TypeId == (int)EnumHelpers.TemplateStructureType.ProfileAccount)
                        list = (ActionService as TemplatesService).ProfileAccountFailedReasons(result.failedReasons);
                    else if (model.TypeId == (int)EnumHelpers.TemplateStructureType.ProfileTerritory)
                    {
                        list = (ActionService as TemplatesService).ProfileTerritoryFailedReasons(result.failedReasons);
                    }
                    else
                        list = (ActionService as TemplatesService).EmployeeHistoryProfileFailedReasons(result.failedReasons);
                     
                    ViewBag.list = list;
                    return View("Index");
                }
                else
                    return View("Index");
            }
            else
            {
                ShowError(PH.Message.IncorrectStructureFile);
                return View("Index");
            }

        }

        public ActionResult StartProcessing(ref DataTable table, string fileName, int? type, int? team, int? time, bool allData = false, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                table = (ActionService as TemplatesService).ExportTemplate(type, team, time, allData);

                //return new FileContentResult(FileHelper.DatatableToBytes(table, "TemplateSheet")
                // , MimeMapping.GetMimeMapping(fileName)){FileDownloadName = fileName};
                var document = new XmlDocument();

                // Uncomment the following line to
                // simulate a noticeable latency.
                //Thread.Sleep(5000);

                // Replace this file name with a valid file name.
                document.Load(@"http://www.tailspintoys.com/sample.xml");
                //return RedirectToAction("DownloadAsyncFile", new {table = table, fileName = fileName });
            }
            catch (Exception ex)
            {
                ProcessCancellation(ref table, type, team, time, allData);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }

        private void ProcessCancellation(ref DataTable table, int? type, int? team, int? time, bool allData = false)
        {
            Thread.Sleep(60000);
            table = (ActionService as TemplatesService).ExportTemplate(type, team, time, allData);
        }

        public ActionResult DownloadTemplate(int? type, int? team, int? time,bool allData = false)
        {
            DataTable table = new DataTable();
            var fileNameWithMandatoryFields = (ActionService as TemplatesService).FileName(type.Value);
            string fileName = fileNameWithMandatoryFields.FileName;
            // HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => StartProcessing(ref table, fileName, type, team, time, allData, cancellationToken));
            table = (ActionService as TemplatesService).ExportTemplate(type, team, time, allData);
            return new FileContentResult(FileHelper.DatatableToBytes(table, "TemplateSheet", fileNameWithMandatoryFields.MandatoryFields)
              , MimeMapping.GetMimeMapping(fileName))
               {
                   FileDownloadName = fileName
               };
        }

        public FileResult DownloadAsyncFile(DataTable table = null, string fileName = "")
        {
            return new FileContentResult(FileHelper.DatatableToBytes(table, "TemplateSheet")
                , MimeMapping.GetMimeMapping(fileName))
            {
                FileDownloadName = fileName
            };
        }

        public FileResult DownloadEmptyTemplate(int? type)
        {
            var fileNameWithMandatoryFields = (ActionService as TemplatesService).FileName(type.Value);
            string fileName = fileNameWithMandatoryFields.FileName;
            
            var table = (ActionService as TemplatesService).ExportEmptyTemplate(type);

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "TemplateSheet", fileNameWithMandatoryFields.MandatoryFields) 
                , MimeMapping.GetMimeMapping(fileName))
                   {
                     FileDownloadName = fileName
                   };
        }

        [HttpPost]
        public ActionResult GetCount(int? type, int? team, int? time)
        {
            var Count = (ActionService as TemplatesService).GetCount(type, team, time);
            return Json(Count, JsonRequestBehavior.AllowGet);
        }
    } 
}