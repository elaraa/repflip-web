﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportRepAchProductController : BaseReportController
    {

        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNameRepAchievementProduct;

            return base.Index();
        }
        public ReportRepAchProductController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNameRepAchievementProduct;
            //ReportName = "BU+Achievement";

        }


    }
}