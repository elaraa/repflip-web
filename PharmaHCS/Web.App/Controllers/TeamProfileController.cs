﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class TeamProfileController : BaseControllerTemplate<vw_TeamProfile, TeamProfile, int>
    {

        public TeamProfileController() : base()
        {
            ViewNamePlural = PH.Team.ViewNameProfilesPlural;
            ViewNameSingular = PH.Team.ViewNameProfileSingular;

            PropertiesToUpdate = new Expression<Func<TeamProfile, object>>[]
                {
                    x=>x.ProfileCode,
                    x => x.ProfileName,
                    x => x.TeamId,
                    x => x.Active

                };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new TeamProfileService(UserId);
            ListService = new BaseService<vw_TeamProfile>(UserId);
        }
        protected override Expression<Func<TeamProfile, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.ProfileId == id.Value) : (Expression<Func<TeamProfile, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
        
        }






        #region Profile Account

        //List
        public ActionResult ReadProfileAccountList([DataSourceRequest] DataSourceRequest request, int ProfileId)
        {
            return Json(new BaseService<vw_ProfileAccount>(UserId).Get(w => w.ProfileId == ProfileId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_ProfileAccount(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Profile Account", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Profile Account";

            ViewBag.Profiles = LookupHelper.GetMyProfiles(this);
            ViewBag.Products = LookupHelper.GetMyProducts(this);
            ViewBag.DosageFroms = LookupHelper.GetMyDosageForms(this);

        }
        //create
        public ActionResult CreateProfileAccount(int profileId)
        {
            FillViewBag_ProfileAccount(true);
            var model = new ProfileAccount1 { ProfileId = profileId, FromDate = DateTime.Now };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProfileAccount(ProfileAccount1 profileAccount)
        {
            FillViewBag_ProfileAccount(true);
            //return CreateChildEntity<ProfileAccount1>(profileAccount , "Edit", new { id = profileAccount.ProfileId }
            //, "CreateProfileAccount");
            var result = (ActionService as TeamProfileService).ValidateProfileAccount(profileAccount);
            return AddOrUpdateModel(profileAccount, null, "Edit", new { id = profileAccount.ProfileId }, "CreateProfileAccount", result);
        }
        //edit
        public ActionResult EditProfileAccount(int id)
        {
            FillViewBag_ProfileAccount(false);
            return EditChildEntity<int, ProfileAccount1>(id, x => x.ProfileAccountId == id, "EditProfileAccount");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfileAccount(ProfileAccount1 profileAccount)
        {
            FillViewBag_ProfileAccount(false);
            //return EditChildEntity<ProfileAccount1>(profileAccount, new Expression<Func<ProfileAccount1, object>>[]
            //    {
            //        x => x.ProfileId,
            //         x => x.AccountId,
            //         x => x.Perccentage,
            //         x => x.ProductId,
            //         x => x.DosageFromId,
            //          x => x.FromDate,
            //           x => x.ToDate,

            //    }
            //, "Edit", new { id = profileAccount.ProfileId }, "EditProfileAccount");
            var result = (ActionService as TeamProfileService).ValidateProfileAccount(profileAccount);
            return AddOrUpdateModel(profileAccount, new Expression<Func<ProfileAccount1, object>>[]
                {
                    x => x.ProfileId,
                     x => x.AccountId,
                     x => x.Perccentage,
                     x => x.ProductId,
                     x => x.DosageFromId,
                      x => x.FromDate,
                       x => x.ToDate,

                }, "Edit", new { id = profileAccount.ProfileId }, "EditProfileAccount", result);
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteProfileAccount(int? id)
        {
            return DeleteChildEntity<int, ProfileAccount1>(id, x => x.ProfileAccountId == id);
        }

        #endregion


        #region Profile Territory

        //List
        public ActionResult ReadProfileTerritoryList([DataSourceRequest] DataSourceRequest request, int ProfileId)
        {
            return Json(new BaseService<vw_ProfileTerritory>(UserId).Get(w => w.ProfileId == ProfileId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_ProfileTerritory(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", "Profile Territory", isCreate ? "Create" : "Edit");
            ViewBag.EntityName = "Profile Territory";

            ViewBag.Profiles = LookupHelper.GetMyProfiles(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
        }
        //create
        public ActionResult CreateProfileTerritory(int profileId)
        {
            FillViewBag_ProfileTerritory(true);
            var model = new ProfileTerritory1 { ProfileId = profileId, FromDate = DateTime.Now };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProfileTerritory(ProfileTerritory1 profileTerritory)
        {
            FillViewBag_ProfileTerritory(true);
           // return CreateChildEntity<ProfileTerritory1>(profileTerritory, "Edit", new { id = profileTerritory.ProfileId }
            //, "CreateProfileAccount");
            var result = (ActionService as TeamProfileService).ValidateProfileTerritory(profileTerritory);
            return AddOrUpdateModel(profileTerritory, null, "Edit", new { id = profileTerritory.ProfileId }, "CreateProfileTerritory", result);
        }
        //edit
        public ActionResult EditProfileTerritory(int id)
        {
            FillViewBag_ProfileTerritory(false);
            return EditChildEntity<int, ProfileTerritory1>(id, x => x.ProfileTerritoryId == id, "EditProfileTerritory");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfileTerritory(ProfileTerritory1 profileTerritory)
        {
            FillViewBag_ProfileTerritory(false);
            //return EditChildEntity<ProfileTerritory1>(profileTerritory, new Expression<Func<ProfileTerritory1, object>>[]
            //   {
            //        x => x.ProfileId,
            //         x => x.TerritoryId,
            //         x => x.Percentage,
            //         c=>c.CreationDate,
            //         c=>c.ToDate
            //   }
            //, "Edit", new { id = profileTerritory.ProfileId }, "EditProfileTerritory");

           var result = (ActionService as TeamProfileService).ValidateProfileTerritory(profileTerritory);
            return AddOrUpdateModel(profileTerritory, new Expression<Func<ProfileTerritory1, object>>[]
               {
                    x => x.ProfileId,
                     x => x.TerritoryId,
                     x => x.Percentage,
                     c=>c.CreationDate,
                     c=>c.ToDate
               }, "Edit", new { id = profileTerritory.ProfileId }, "EditProfileTerritory", result);
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteProfileTerritory(int? id)
        {
            return DeleteChildEntity<int, ProfileTerritory1>(id, x => x.ProfileTerritoryId == id);
        }

        #endregion


    }
}