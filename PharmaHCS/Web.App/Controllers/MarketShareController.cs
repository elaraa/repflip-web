﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;

namespace Web.App.Controllers
{
    public class MarketShareController : BaseControllerList<vw_MarketShare, MarketShare, int>
    {
        public MarketShareController() : base()
        {
            ViewNamePlural = PH.Marketshare.ViewNamePlural;
            ViewNameSingular = PH.Marketshare.ViewNameSingular;
        }
        
        protected override Expression<Func<MarketShare, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.MarketShareId == id.Value) : (Expression<Func<MarketShare, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.Levels = LookupHelper.GetTerritoryLevels(this);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMMarketShareImport model) {
            FillViewBag(true);
            var err = "";
            if (ModelState.IsValid && model.File.ContentLength > 0)
            {
                //get data
                //pass it to service
                var result = (ActionService as MarketShareService)
                       .Import(model.Year, model.Level, model.CountryId, FileHelper.GetDateSetFromExcel(model.File.InputStream));
                //if succeeded return to list
                //save file
                if (result.Succeeded)
                {
                    model.File.SaveMarketShareFile(result.ResultId);
                    return RedirectToAction("Index");
                }
                err = string.Join("<br/>",result.ErrorList);
            }
            err += GetModelStateErrors(ModelState);
            if (!string.IsNullOrEmpty(err))
                ShowError(err);
            return View(model);
        }
        public FileResult DownloadTemplate(int year, int level, int countryId)
        {
            var fileName = string.Format("IMS Market Share_{0}.xlsx", year);
            var table = (ActionService as MarketShareService).Export(year,level, countryId);

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "IMS_market_share")
                , MimeMapping.GetMimeMapping(fileName))
            {
                FileDownloadName = fileName
            };
        }
        public ActionResult Edit(int id) {
            FillViewBag(false);
            return EditChildEntity<int,MarketShare>(id, GetOneExpression(id), "Edit", ActionService);
        }
        protected override void InitTemplateServices()
        {
            ActionService = new MarketShareService(UserId);
            ListService = new BaseService<vw_MarketShare>(UserId);
        }
        public ActionResult ReadMarketShareTerritoryList([DataSourceRequest] DataSourceRequest request, int MarketShareId)
        {
            return Json(new BaseService<vw_MarketShareTerritory>(UserId).Get(w => w.MarketShareId == MarketShareId, true).ToDataSourceResult(request));
        }
    }
}