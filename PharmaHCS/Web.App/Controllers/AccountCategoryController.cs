﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class AccountCategoryController : BaseControllerTemplate<vw_AccountCategory, AccountCategory, int>
    {
        public AccountCategoryController() : base()
        {
            ViewNamePlural = PH.AccountCategory.ViewNamePlural;
            ViewNameSingular = PH.AccountCategory.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<AccountCategory, object>>[]
                {
                    x=>x.CategoryCode,
                    x => x.CategoryName,
                    x => x.ParentCategoryId,
                    x => x.CountryId

                };
        }

        protected override Expression<Func<AccountCategory, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.CategoryId == id.Value) : (Expression<Func<AccountCategory, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //
            ViewBag.AccountCategories = LookupHelper.GetMyAccountCategories(this);
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }




    }
}