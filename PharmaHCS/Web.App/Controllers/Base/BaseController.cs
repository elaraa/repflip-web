﻿using Services.Business;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Web.App.Controllers
{
    public class BaseController : Controller
    {
        protected string Layout => IsRep ? "~/Views/Shared/_RepLayout.cshtml" : "~/Views/Shared/_Layout.cshtml";
        protected bool IsRep => LoginService.IsRep(UserId);
        protected bool ApplySecurity { get; set; }
        protected string ViewNamePlural { get; set; }
        protected string ViewNameSingular { get; set; }
        public int UserId
        {
            get
            {
                try
                {
                    return Convert.ToInt32(Session["SystemUserId"]);
                }
                catch
                {
                    // Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                }

                return int.MinValue;
            }
        }
        protected bool IsUserLoggedIn => UserId != int.MinValue;
        public BaseController()
        {
            ApplySecurity = true;

            if (!IsUserLoggedIn)
                RedirectToAction("Index", "App");
        }
        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            if (UserId == int.MinValue || UserId == 0)
            {
                filterContext.Result = RedirectToAction("Index", "App", new { redirectUrl = Request.Url });
            }
            else
            {
                var controllerName = filterContext.Controller.ControllerContext.RouteData.Values["controller"].ToString();
                //if (controllerName != "Lookup" && controllerName != "BaseReport" & controllerName != "My")
                if (IsRep && controllerName == "Home")
                    filterContext.Result = RedirectToAction("Index", "RepHome");
                else if (ApplySecurity)
                    //validate user auth for controller
                    if (!new MenuAccessService(UserId).CanAccess(controllerName))
                        filterContext.Result = RedirectToAction("UnAuthorized", "App");
            }
            base.OnAuthentication(filterContext);
        }

        //protected override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    int x = 0;
        //    base.OnAuthorization(filterContext);
        //}

        // GET: Product
        public virtual ActionResult Index()
        {
            ViewBag.Title = ViewNamePlural;
            ViewBag.Layout = Layout;
            return View();
        }


        #region Create/Edit Helpers
        protected virtual void FillViewBag(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", ViewNameSingular, isCreate ? "Create" : "Edit");
            ViewBag.EntityName = ViewNameSingular;
            ViewBag.Layout = Layout;
        }
        protected virtual string GetModelStateErrors(ModelStateDictionary modelState, string separator = "<br/>")
        {
            var errors = new List<string>();
            foreach (var ms in modelState.Values)
                foreach (var modelError in ms.Errors)
                    errors.Add(modelError.ErrorMessage);

            return string.Join(separator, errors);
        }

        public ActionResult AddOrUpdateModel<TModel>(TModel model, Expression<Func<TModel, object>>[] propertiesToUpdate = null
          , string successView = "", object successRouteId = null, string failView = "", RunResult result = null, BaseService<TModel> service = null)
             where TModel : class, new()
        {
            try
            {
                if (result != null && result.Succeeded != true)
                {
                   // ShowError(string.Format(PH.Message.CodeError, result.ErrorDesc), true);
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", result.ErrorDesc);
                    return View(failView, model);
                }
                else
                {
                    if (propertiesToUpdate != null)
                       return EditChildEntity(model, propertiesToUpdate, successView, successRouteId, failView, service, result);
                    else
                     return CreateChildEntity(model, successView, successRouteId, failView, service,result);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(e));
            }

            return View(failView, model);
        }

        public virtual ActionResult CreateChildEntity<TModel>(TModel model
             , string successView = "Edit", object successRouteId = null, string failView = ""
            , BaseService<TModel> service = null, RunResult result = null)
             where TModel : class, new()
        {
            FillViewBag(true);
           
            if (ModelState.IsValid)
            {
                if (service == null) service = new BaseService<TModel>(UserId);
                try
                {
                    service.Add(model);
                  // var action= ControllerContext.RouteData.Values["action"].ToString();
                    if (result != null && result.Succeeded == true)
                        ShowMessage(ViewBagMessage.ViewBagMessageType.Success, "Success", result.SuccessDesc,successView=="Edit" ?true:false);
                    else
                        ShowSuccess(PH.Message.SuccessCode, successView=="Edit" ?true:false);
                    return RedirectToAction(successView, successRouteId);
                }
                catch (Exception e)
                {
                    Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(e));
                    return View(failView, model);
                }
            }

            ViewModelStateErrors(ModelState);
            return View(failView, model);
        }
        public  void ViewModelStateErrors(ModelStateDictionary modelState)
        {
            if (modelState.IsValid) return;
            var msgsList = new List<string>();
            foreach (var item in ModelState.Values)
                foreach (var error in item.Errors)
                    msgsList.Add(error.ErrorMessage);
            ShowError(string.Join("<br/>", msgsList.ToArray()));
        }
        public virtual ActionResult EditChildEntity<ChildIDType, TModel>(ChildIDType? id,
            Expression<Func<TModel, bool>> expression
            , string viewName = "", BaseService<TModel> service = null)
             where ChildIDType : struct
            where TModel : class, new()
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (service == null) service = new BaseService<TModel>(UserId);
            TModel model = service.GetOne(expression);
            if (model == null)
                return new HttpNotFoundResult();
            return View(viewName, model);
        }
        public virtual ActionResult EditChildEntity<TModel>(TModel model, Expression<Func<TModel, object>>[] propertiesToUpdate
            , string successView = "Edit", object successRouteId = null, string failView = ""
            , BaseService<TModel> service = null, RunResult result = null)
            where TModel : class, new()
        {
            if (service == null) service = new BaseService<TModel>(UserId);
            if (ModelState.IsValid)
            {
                if (service.Update(model, propertiesToUpdate))
                {
                    // get action from request 
                    if (result != null && result.Succeeded == true)
                        ShowMessage(ViewBagMessage.ViewBagMessageType.Success, "Success", result.SuccessDesc, successView == "Edit" ? true : false);
                    else
                        ShowSuccess(PH.Message.SuccessEditCode, successView == "Edit" ? true : false);

                    return RedirectToAction(successView, successRouteId);
                }
            }
            ViewModelStateErrors(ModelState);
            return View(failView, model);
        }
        public virtual JsonResult DeleteChildEntity<ChildIDType, TModel>(ChildIDType? id, Expression<Func<TModel, bool>> expression, BaseService<TModel> service = null)
            where TModel : class, new()
            where ChildIDType : struct
        {
            if (id == null)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));

            if (service == null) service = new BaseService<TModel>(UserId);
            TModel model = service.GetOne(expression);
            return model == null ? Json(HttpNotFound()) : Json(service.Delete(expression) > 0);
        }
        #endregion

        protected void ShowError(string message, bool persistOverActions = false) => ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", message, persistOverActions);
        protected void ShowSuccess(string message, bool persistOverActions = false) => ShowMessage(ViewBagMessage.ViewBagMessageType.Success, "Success", message, persistOverActions);
        protected void ShowMessage(ViewBagMessage.ViewBagMessageType type, string title, string message, bool persistOverActions = false)
        {
            var msg = new ViewBagMessage
            {
                MessageType = type,
                MessageTitle = title,
                Message = message
            };
            if (!persistOverActions)
                ViewBag.Message = msg;
            else
                TempData["Message"] = msg;
        }

    }
}