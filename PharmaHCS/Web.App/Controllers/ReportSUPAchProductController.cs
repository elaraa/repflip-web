﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportSUPAchProductController : BaseReportController
    {

        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNameSUPAchievementProduct;

            return base.Index();
        }
        public ReportSUPAchProductController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNameSUPAchievementProduct;
            //ReportName = "BU+Achievement";

        }


    }
}