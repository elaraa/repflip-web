﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class IndividualTargetAdjustmentController : BaseController
    {
        public IndividualTargetAdjustmentController() : base()
        {
            ViewNamePlural = PH.TargetAdjustment.ViewNamePlural;
            ViewNameSingular = PH.TargetAdjustment.ViewNameSingular;
        }
    

        private void FillViewBag()
        {
            ViewBag.Title = ViewNamePlural;
            ViewBag.Products = LookupHelper.GetMyProducts(this);
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Profiles = LookupHelper.GetMyProfiles(this);
           // ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.types = LookupHelper.GetTargetTypes();
        }

        public override ActionResult Index()
        {
            FillViewBag();
            ViewBag.Layout = Layout;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(vw_TargetAdjustment target)
        {
            try
            {
                RunResult result = new TargetAdjustmentService(UserId).AdjustTarget(target);
                if (result != null && result.Succeeded == true)
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Success, "Success", result.SuccessDesc, true);
                else
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", result.ErrorDesc, true);
            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", e.Message, true);
            }
            return RedirectToAction("Index");
        }

        public JsonResult GetMyTerritories(int? profileId,int? timeId)
        {
            var res = LookupHelper.GetMyTerritories(this,profileId,timeId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}