﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportBUAchProductController : BaseReportController
    {
        // GET: ReportBUAchProduct
        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNameBUAchievementProduct;

            return base.Index();
        }

        public ReportBUAchProductController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNameBUAchievementProduct;
            //ReportName = "BU+Achievement";

        }
    }
}