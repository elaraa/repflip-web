﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class LookupController : BaseController
    {
        public LookupController():base()
        {
            ApplySecurity = false;
        }
        #region Account Dropdownlist Virtual

        public ActionResult ReadAccountsList([DataSourceRequest] DataSourceRequest request, int? categoryId, int? typeId)
        {
            return Json(GetAccounts(categoryId, typeId).ToDataSourceResult(request));
        }
        private IQueryable<vw_Account> GetAccounts(int? categoryId, int? typeId)
        {
            Expression<Func<vw_Account, bool>> whereClause = null;
            int catId = categoryId ?? -1;
            int tId = typeId ?? -1;

            if (categoryId.HasValue && typeId.HasValue)
                whereClause = w => w.CategoryId == catId && w.AccountTypeId == tId;
            else if (categoryId.HasValue)
                whereClause = w => w.CategoryId == catId;
            else if(typeId.HasValue)
                whereClause = w => w.AccountTypeId == tId;

            return new BaseService<vw_Account>(UserId).Get(whereClause, true);//.OrderBy(o=>o.AccountName);
        }
        public ActionResult Accounts_ValueMapper(int[] values, int? categoryId, int? typeId)
        {
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;

                foreach (var account in GetAccounts(categoryId, typeId))
                {
                    if (values.Contains(account.AccountId))
                        indices.Add(index);

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Doctor Dropdown List
        public ActionResult ReadDoctorsList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetDoctors().ToDataSourceResult(request));
        }
        private IQueryable<vw_Doctors> GetDoctors()
        {
            Expression<Func<vw_Doctors, bool>> whereClause = null;
            return new BaseService<vw_Doctors>(UserId).Get(whereClause, true);//.OrderBy(o=>o.AccountName);
        }
        public ActionResult Doctors_ValueMapper(int[] values)
        {
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;

                foreach (var doctor in GetDoctors())
                {
                    if (values.Contains(doctor.DoctorId))
                        indices.Add(index);

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReadMyAccountList([DataSourceRequest] DataSourceRequest request, string id = "")
        {
            return Json(GetMyAccountList(id).ToDataSourceResult(request));
        }
        private IQueryable<vw_Account> GetMyAccountList(string id = "")
        {
            Expression<Func<vw_Account, bool>> whereClause = null;
            if (id == "")
                return new BaseService<vw_Account>(UserId).Get(whereClause, true);
            else
            {
                int ID = int.Parse(id);
                var myAccountIds = new BaseService<vw_AccountDoctor>(UserId).Get<vw_AccountDoctor>(a => a.DoctorId == ID).Select(a => a.AccountId);
                var accounts = new BaseService<vw_Account>(UserId).Get<vw_Account>(a => !myAccountIds.Contains(a.AccountId));/*.Except(exceptAccounts);*/
                return accounts;
            }
        }



        #endregion

        #region My Accounts 
        public ActionResult ReadNotMyAccountList([DataSourceRequest] DataSourceRequest request, string id="")
        { 
            return Json(GetnotMyAccounts(id).ToDataSourceResult(request));
        }
        private IQueryable<vw_Account> GetnotMyAccounts(string id="")
        {
            Expression<Func<vw_Account, bool>> whereClause = null;
            if (id == "")
                return new BaseService<vw_Account>(UserId).Get(whereClause, true);
            else
            {
                int ID = int.Parse(id);
                var myAccountIds =new BaseService<vw_AccountDoctor>(UserId).Get<vw_AccountDoctor>(a => a.DoctorId == ID).Select(a => a.AccountId);
                var accounts = new BaseService<vw_Account>(UserId).Get<vw_Account>(a => a.AccountTypeName.Contains("ph")&& !myAccountIds.Contains(a.AccountId));/*.Except(exceptAccounts);*/
                return accounts;
            }
        }
        public ActionResult MyAccounts_ValueMapper(int[] values, string id = "")
        { 
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;

                foreach (var account in GetnotMyAccounts(id))
                {
                    if (values.Contains(account.AccountId))
                        indices.Add(index);

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}