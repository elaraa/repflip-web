﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportSUPAchController : BaseReportController
    {

        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = PH.Reports.ViewNameSUPAchievement;

            return base.Index();
        }
        public ReportSUPAchController()
        {
            ViewNamePlural = ViewNameSingular = PH.Reports.ViewNameSUPAchievement;
            //ReportName = "BU+Achievement";

        }


    }
}