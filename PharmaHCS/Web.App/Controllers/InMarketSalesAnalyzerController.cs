﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class InMarketSalesAnalyzerController : BaseController
    {
        public InMarketSalesAnalyzerController()
        {
            ViewNameSingular = ViewNamePlural = PH.InMarketSalesAnalyzer.ViewNamePlural;
        }
        // GET: InMarketSalesAnalyzer
        public override ActionResult Index()
        {
            ViewBag.PageType = "Dashboard";
            ViewBag.Title = "In Market Sales Analyzer";
            return View(new InMarketSalesAnalyzer(UserId).GetSummary());
        }
        public ActionResult _GetDistributorAnalysis_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetDistributorSales().ToDataSourceResult(request));
        }
        public ActionResult _GetDistributorAnalysisChart()
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetDistributorSales());
        }
        public ActionResult _GetSalesTrend_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetSalesTrend().ToDataSourceResult(request));
        }
        public ActionResult _GetSalesTrendChart()
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetSalesTrend());
        }
        public ActionResult _GetSalesByAccountCategory_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetSalesByAccountCategory().ToDataSourceResult(request));
        }
        public ActionResult _SalesByAccountCategoryChart()
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetSalesByAccountCategory());
        }
        public ActionResult _GetSalesByTerritory_Table([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetSalesByTerritory().ToDataSourceResult(request));
        }
        public ActionResult _GetSalesByTerritoryChart()
        {
            return Json(new InMarketSalesAnalyzer(UserId).GetSalesByTerritory());
        }
    }
}