﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class AccountTypeController : BaseControllerTemplate<vw_AccountType, AccountType, int>
    {
        public AccountTypeController() : base()
        {
            ViewNamePlural =PH.Account.ViewNamePlural;
            ViewNameSingular = PH.Account.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<AccountType, object>>[]
                {
                    x=>x.AccountTypeName,
                    x => x.AccountTypeCode,
                    x => x.MustHaveDoctor,
                    x => x.TargetAndPlanWithDoctor,
                    x => x.IncludeInSales,
                    x => x.CountryId

                };
        }

        protected override Expression<Func<AccountType, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.AccountTypeId == id.Value) : (Expression<Func<AccountType, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);
            //

            ViewBag.Countries = LookupHelper.GetMyCountries(this);
        }


    }
}