﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using Services.Business.Sales.BonusTracking;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class BonusTrackingController : BaseController
    {
        public BonusTrackingController() : base()
        {
            ViewNamePlural = "Bonus Tracking";
            ViewNameSingular = "Bonus Tracking";
        }
     

        private void FillViewBag()
        {
            ViewBag.Title = ViewNamePlural;
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            ViewBag.Teams = LookupHelper.GetMyTeams(this);

        }

        public override ActionResult Index()
        {
            FillViewBag();
            ViewBag.Layout = Layout;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit( BonusTrackingDto target)
        {
            try
            {
                RunResult result = new BonusTrackingService(UserId).Tracking(target);
                if (result != null && result.Succeeded == true)
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Success, "Success", result.SuccessDesc, true);
                else
                    ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", result.ErrorDesc, true);
            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                ShowMessage(ViewBagMessage.ViewBagMessageType.Error, "Error", e.Message, true);
            }
            return RedirectToAction("Index"); 
        }

        public JsonResult GetMyProducts(int? TeamId, int? timeId)
        {
            var res = LookupHelper.GetMyProducts(this, TeamId, timeId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }


    }
  
}