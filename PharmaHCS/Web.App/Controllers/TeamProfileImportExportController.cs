﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using Services.Business.Sales.TemplatesService;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;
using ImportedFileStatus = Services.Business.Sales.ImportedFileStatus;

namespace Web.App.Controllers
{
    public class TeamProfileImportExportController : BaseControllerList<vw_TeamProfile, TeamProfile, int>
    {
        public TeamProfileImportExportController() : base() 
        {
            ViewNamePlural = PH.TemplatesImprtExport.ViewNamePlural;
            ViewNameSingular = PH.TemplatesImprtExport.ViewNameSingular;
        }

        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_TeamProfile>(UserId);
            ActionService = new TeamProfileTemplatesService(UserId);
        }
        protected override void FillViewBag(bool isCreate)
        {
            //base.FillViewBag(isCreate);
            //
            ViewBag.Title = ViewNamePlural;
            ViewBag.Teams = LookupHelper.GetMyTeams(this);
        }
        public override ActionResult Index()
        {
            FillViewBag(true);
            var model = new TeamProfileTemplatViewModel();
            return View(model);
        }
        public ActionResult Submit(TeamProfileTemplatViewModel model)
        {
            FillViewBag(true);
            if (model.File == null || model.File.ContentLength < 1)
            { 
                ShowError(PH.Message.Nofileselected);
                return View("Index");
            }
            var data = (ActionService as TeamProfileTemplatesService).uploadFiles(model);
            if (data != null && data.Tables.Count > 0)
            {
          
                MasterDataUpload.UploadStatusResult result = new MasterDataUpload(UserId).UploadFile(
                    data.Tables[0]
                    , MasterDataType.TeamProfile 
                    );

                 switch (result.Status)
                {
                    case MasterDataUpload.UploadStatus.Success:
                        ShowSuccess(PH.Message.ImportedSuccessfully);
                        break;
                    case MasterDataUpload.UploadStatus.FileIsEmpty:
                        ShowError(PH.Message.FileEmpty);
                        break;
                    case MasterDataUpload.UploadStatus.InvalidFileHeader:
                        ShowError(PH.Message.IncorrectStructureFile);
                        break;
                    case MasterDataUpload.UploadStatus.dbError_FailedtoImportData:
                        ShowError(PH.Message.Failedtoupload);
                        break;
                    case MasterDataUpload.UploadStatus.DataContainsErrors:
                        ShowError(PH.Message.Cannotupload);
                        break;
                    default:
                        break;
                 }
                if (result.Status==MasterDataUpload.UploadStatus.DataContainsErrors)
                {
                    var list = new List<TemplateValidation>();
                    list = (ActionService as TeamProfileTemplatesService).TeamProfileFailedReasons(result.failedReasons);
                   
                    ViewBag.list = list;
                    return View("Index");
                }
                else
                    return View("Index");
            }
            else
            {
                ShowError(PH.Message.IncorrectStructureFile);
                return View("Index");
            }

        }

        public FileResult DownloadTemplate(int? team)
        {
           var fileName = string.Format("{0}.xlsx","TeamProfile");


            var table =(ActionService as TeamProfileTemplatesService).ExportTemplate(team);

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "TemplateSheet")
                , MimeMapping.GetMimeMapping(fileName))
                   {
                      FileDownloadName = fileName
                   };
        }
        
        public FileResult DownloadEmptyTemplate()
        {
            var fileName = string.Format("{0}.xlsx", "TeamProfile");
            var table = (ActionService as TeamProfileTemplatesService).ExportEmptyTemplate();

            return new FileContentResult(
                 FileHelper.DatatableToBytes(table, "TemplateSheet")
                , MimeMapping.GetMimeMapping(fileName))
                   {
                     FileDownloadName = fileName
                   };
        }
    } 
}