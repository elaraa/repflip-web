﻿using AutoMapper;
using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.CRM;
using Services.Business.CRM.NewDoctorRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.ProfileTargetNewDoctorApproval;

namespace Web.App.Controllers
{
    public class NewDoctorRequestController : BaseControllerTemplate<vw_ProfiletargetNewDoctor, ProfileTargetNewDoctorApproval, int>
    {
        public NewDoctorRequestController() : base()
        {
            ViewNamePlural = PH.NewDoctorRequest.ViewNamePlural;
            ViewNameSingular = PH.NewDoctorRequest.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<ProfileTargetNewDoctorApproval, object>>[]
                {
                    x=>x.DoctorName,
                    x => x.PotentialId,
                    x => x.ProfileId,
                    x => x.Gender,
                    x => x.SpecialtyId,
                    x => x.Mobile,
                    x => x.AccountName,
                    x => x.AccountCode,
                    x => x.Address,
                    x => x.AccountTypeId,
                    x => x.TerritoryId,
                    x => x.Frequency,
                    x => x.Latitude,
                    x => x.Longitude,
                    x => x.ApprovalStatus
            };
        }
        protected override void InitTemplateServices()
        {
            ActionService = new NewDoctorRequestService(UserId);
            ListService = new BaseService<vw_ProfiletargetNewDoctor>(UserId);
        }
        public override ActionResult ReadList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(ListService.Get(a=>a.EmployeeId==UserId, true).ToDataSourceResult(request));
        }
        protected override Expression<Func<ProfileTargetNewDoctorApproval, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.TargetDoctorApprovalId == id.Value) : (Expression<Func<ProfileTargetNewDoctorApproval, bool>>)(x => 1 == 0);
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Countries = LookupHelper.GetMyCountries(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.Potentials = LookupHelper.GetMyPotentials(this);
            ViewBag.Specialties = LookupHelper.GetMySpecialties(this);
            ViewBag.Genders = LookupHelper.GetGenders(this);
            ViewBag.AccountTypes = LookupHelper.GetMyAccountTypes(this);
            ViewBag.AccountCategories = LookupHelper.GetMyAccountCategories(this);

            base.FillViewBag(isCreate);
        }

       

        public override ActionResult Create()
        {
            FillViewBag(true);
            decimal lat = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLat"]);
            decimal lng = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLng"]);
            return View(new DoctorAccountVM { Latitude = lat, Longitude = lng });
        }
        public override ActionResult Edit(int? id)
        {
            FillViewBag(true);
            decimal lat = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLat"]);
            decimal lng = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLng"]);

            return View((ActionService as NewDoctorRequestService).GetDoctorRequest(id.Value,lat,lng));

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult Submit(DoctorAccountVM model)
        {
            if (ModelState.IsValid)
            {
                var result = (ActionService as NewDoctorRequestService).SaveModel(model);
                if (result != null && result.Succeeded == true)
                {
                    ShowSuccess(result.SuccessDesc=="Create" ?PH.Message.SuccessCode: PH.Message.SuccessEditCode, true);
                    return View("Index");

                }
                else
                {
                    ShowError(PH.Message.CodeError);
                    return View(model);
                }
            }
            else
                return View(model);
            
        }
        
        [HttpPost]
        public ActionResult SendDocotrRequest(int? id)
        {
            return Json((ActionService as NewDoctorRequestService).SendDocotrRequest(id.Value), JsonRequestBehavior.AllowGet);
        }
    }
}
