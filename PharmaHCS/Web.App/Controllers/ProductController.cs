﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Business;
using Services.Business.Sales;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ProductController : BaseControllerTemplate<vw_Product, Product, int>
    {
        public ProductController() : base()
        {
            ViewNamePlural = PH.Product.ViewNamePlural;
            ViewNameSingular = PH.Product.ViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Product, object>>[]
                {
                    x=>x.ProductCode,
                    x => x.ProductName,
                    x => x.Contraindications,
                    x => x.CountryId,
                    x => x.Indications,
                    x => x.SideEffects,
                    x => x.Storage,
                    x => x.ParentProductId,
                    x => x.Active,
                    a=>a.GroupId
                };
        }
        //public override ActionResult Create()
        //{
        //    FillViewBag(true);
        //    return View(new Product { ProductId = 0});
        //}
        protected override Expression<Func<Product, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.ProductId == id.Value) : (Expression<Func<Product, bool>>)(x => 1 == 0);
        }

        protected override void FillViewBag(bool isCreate)
        {
            base.FillViewBag(isCreate);

            ViewBag.ProductGroups = LookupHelper.GetMyProductGroups(this);
            ViewBag.Country = LookupHelper.GetMyCountries(this);

        }



        #region Dosage Form

        //List
        public ActionResult ReadDosageFormList([DataSourceRequest] DataSourceRequest request, int ProductId)
        {
            return Json(new BaseService<vw_DosageForm>(UserId).Get(w => w.ProductId == ProductId, true).ToDataSourceResult(request));
        }
        private void FillViewBag_DosageForm(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", PH.Product.DosageForm,isCreate? PH.Product.Create : PH.Product.Edit);
            ViewBag.EntityName = PH.Product.DosageForm;

            ViewBag.Products = LookupHelper.GetMyProducts(this);
            ViewBag.Units = LookupHelper.GetMyUnits(this);
        }
        //create
        public ActionResult CreateDosageForm(int productId)
        {
            FillViewBag_DosageForm(true);
            var model = new ProductDosageForm { ProductId = productId };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDosageForm(ProductDosageForm dosageForm)
        {
            FillViewBag_DosageForm(true);
            return CreateChildEntity<ProductDosageForm>(dosageForm, "Edit", new { id = dosageForm.ProductId }
            , "CreateDosageForm");
        }
        //edit
        public ActionResult EditDosageForm(int id)
        {
            FillViewBag_DosageForm(false);
            return EditChildEntity<int,ProductDosageForm>(id, x=>x.DosageFormId == id, "EditDosageForm");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDosageForm(ProductDosageForm dosageForm)
        {
            FillViewBag_DosageForm(false);
            return EditChildEntity<ProductDosageForm>(dosageForm, new Expression<Func<ProductDosageForm, object>>[]
                {
                    x=>x.DosageFormCode,
                    x => x.DosageFormName,
                     x => x.ProductId,
                      x => x.Size,
                       x => x.UnitId,
                        x => x.PackFactor,
                         x => x.CountryId,

                }
            , "Edit", new { id = dosageForm .ProductId}, "EditDosageForm");
        }
        //delete
        [HttpPost]
        public JsonResult DeleteDosageForm(int? id)
        {
            return DeleteChildEntity<int, ProductDosageForm>(id, x => x.DosageFormId == id);
        }

        #endregion






        #region Product Price

        //List
        public ActionResult ReadProductPriceList([DataSourceRequest] DataSourceRequest request, int ProductId)
        {
            return Json(new ProductPriceService(UserId).GetProductPrices(ProductId).ToDataSourceResult(request));
        }
        private void FillViewBag_ProductPrice(bool isCreate)
        {
            ViewBag.Title = string.Format("{1} {0}", PH.Product.ProductPrice, isCreate ? PH.Product.Create : PH.Product.Edit);
            ViewBag.EntityName = PH.Product.ProductPrice;

            ViewBag.Products = LookupHelper.GetMyProducts(this);
            ViewBag.DosageForms = LookupHelper.GetMyDosageForms(this);
            ViewBag.Distributors = LookupHelper.GetMyDistributors(this);
            ViewBag.Territories = LookupHelper.GetMyTerritories(this);
            ViewBag.AccountCategory = LookupHelper.GetMyAccountCategories(this);
        }
        //create
        public ActionResult CreateProductPrice(int productId)
        {
            FillViewBag_ProductPrice(true);
            var model = new ProductPrice { ProductId = productId, FromDate = DateTime.Now };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProductPrice(ProductPrice ProductPrice)
        {
            FillViewBag_ProductPrice(true);
            return CreateChildEntity<ProductPrice>(ProductPrice, "Edit", new { id = ProductPrice.ProductId }
            , "CreateProductPrice");
        }
        //edit
        public ActionResult EditProductPrice(int id)
        {
            FillViewBag_ProductPrice(false);
            return EditChildEntity<int, ProductPrice>(id, x => x.ProductPriceId == id, "EditProductPrice");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProductPrice(ProductPrice ProductPrice)
        {
            FillViewBag_ProductPrice(false);
            return EditChildEntity<ProductPrice>(ProductPrice, new Expression<Func<ProductPrice, object>>[]
                {
                    x=>x.ProductId,
                    x => x.DosageFormId,
                    x => x.DistributorId,
                    x => x.AccountCategoryId,
                    x => x.TerritoryId,
                    x => x.AccountId,
                    x => x.FromDate,
                    x => x.ToDate,
                    x => x.Price,
                }
            , "Edit", new { id = ProductPrice.ProductId }, "EditProductPrice");
        }
        //delete
        [HttpPost]
        public virtual JsonResult DeleteProductPrice(int? id)
        {
            return DeleteChildEntity<int, ProductPrice>(id, x => x.ProductPriceId == id);
        }

        #endregion



    }
}