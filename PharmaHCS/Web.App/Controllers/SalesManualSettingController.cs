﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class SalesManualSettingController : BaseController
    {
        public SalesManualSettingController()
        {
            ViewNamePlural = ViewNameSingular = PH.SalesManualSetting.ViewNamePlural;
        }
        public override ActionResult Index()
        {
            ViewBag.Time = LookupHelper.GetTimeDefinitions(this);

            return base.Index();
        }
        [HttpPost]
        public ActionResult EmployeeStructureRun()
        {
            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => StartProcessing(cancellationToken));
            //if (result.Succeeded)
            //    ShowSuccess(PH.SalesManualSetting.SuccessfullCode, true);
            //else
            //    ShowError(string.Format(PH.SalesManualSetting.ErrorCode, result.ErrorDesc), true);

            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult CeilingCalcRun(int timeId) {
            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => StartProcessing(cancellationToken, timeId, true));
            //if (result.Succeeded)
            //    ShowSuccess(PH.SalesManualSetting.SuccessfullCode, true);
            //else
            //    ShowError(string.Format(PH.SalesManualSetting.ErrorCode, result.ErrorDesc), true);

            return RedirectToAction("Index");
        }

        public void StartProcessing(CancellationToken cancellationToken = default(CancellationToken), int timeId = 0, bool isCeiling = false)
        {
            var actionService = new Services.Business.Sales.AdminManualRun(UserId);
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                if(isCeiling)
                   actionService.ExecuteCeilingCalc(timeId);
                else
                   actionService.ExecuteEmpStructure();
            }
            catch (Exception ex)
            {
                ProcessCancellation(timeId, isCeiling);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        private void ProcessCancellation(int timeId, bool isCeiling = false)
        {
            Thread.Sleep(60000);

            if (isCeiling)
                new Services.Business.Sales.AdminManualRun(UserId).ExecuteCeilingCalc(timeId);
            else
                new Services.Business.Sales.AdminManualRun(UserId).ExecuteEmpStructure();
        }

    }
}