﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Core.ImportFile
{
    public class PluginInput
    {
        public string FileName { get; set; }
        public string FileContentType { get; set; }
        public Stream FileStream { get; set; }
        public static string GetFilesNames(PluginInput[] inputs) {
            if (inputs == null || inputs.Length < 1) return string.Empty;

            return string.Join(",", inputs.Select(s => s.FileName).ToArray());
        }
        
    }
}
