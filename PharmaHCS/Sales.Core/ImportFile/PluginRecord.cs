﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Core.ImportFile
{
    public class PluginRecord
    {
        public string RawRecord { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerTerritory { get; set; }
        public string DosageFormCode { get; set; }
        public string DosageFormName { get; set; }
        public string FileQuantity { get; set; }
        public string FileAmount { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? SalesDate { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? BonusQty { get; set; }
        public bool InvalidQty { get; set; }
        public bool InvalidRow { get; set; }
        public string DistributorBranchCode { get; set; }

        public string DistributorBranchName { get; set; }
    }
}
