﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Core.ImportFile
{
    public enum ValidationStatus
    {
        Succeeded,
        InvalidFormat,
        WrongFileType
    }
}
