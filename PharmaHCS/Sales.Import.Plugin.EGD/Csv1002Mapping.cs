﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser.Mapping;

namespace Sales.Import.Plugin.EGD
{
    public class Csv1002Mapping: CsvMapping<C1002File>
    {
        public Csv1002Mapping()
             : base()
        {
            MapProperty(0, x => x.DATE_SWITCH);
            MapProperty(1, x => x.ITEM_CODE);
            MapProperty(2, x => x.ITEM_NAME);
            MapProperty(3, x => x.STATUS);
            MapProperty(4, x => x.VENDOR_CODE);
            MapProperty(5, x => x.DESTINATION_BRANCH_CODE);
            MapProperty(6, x => x.NAME);
            MapProperty(7, x => x.NAME1);
            MapProperty(8, x => x.compute_0009);
            MapProperty(9, x => x.QUANTIY);
            MapProperty(10, x => x.compute_0011);
            MapProperty(11, x => x.DESTINATION_BRANCH_CODE1);
            MapProperty(12, x => x.NAME2);
            MapProperty(13, x => x.ADDRESS);
            MapProperty(14, x => x.INVOICE_NO);


        }
    }
    public class C1002File
    {
        public string DATE_SWITCH { get; set; }
        public string ITEM_CODE { get; set; }
        public string ITEM_NAME { get; set; }
        public string STATUS { get; set; }
        public string VENDOR_CODE { get; set; }
        public string DESTINATION_BRANCH_CODE { get; set; }
        public string NAME { get; set; }
        public string NAME1 { get; set; }
        public string compute_0009 { get; set; }
        public string QUANTIY { get; set; }
        public string compute_0011 { get; set; }
        public string DESTINATION_BRANCH_CODE1 { get; set; }
        public string NAME2 { get; set; }
        public string ADDRESS { get; set; }
        public string INVOICE_NO { get; set; }

    }
}
