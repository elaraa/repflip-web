﻿using Sales.Core.ImportFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser;

namespace Sales.Import.Plugin.LTD
{
    public class LTDFile : PluginBase
    {
        public override void ProcessData(PluginInput[] files)
        {
            foreach (var file in files)
            {
                try
                {
                    CsvParserOptions csvParserOptions = new CsvParserOptions(false, ',');
                    CsvPersonMapping csvMapper = new CsvPersonMapping();
                    CsvParser<LTDCsvFile> csvParser = new CsvParser<LTDCsvFile>(csvParserOptions, csvMapper);
                    Encoding enc = Encoding.GetEncoding("windows-1256");
                    var result = csvParser
                        .ReadFromStream(file.FileStream, enc)
                        .ToList();

                    foreach (var item in result)
                    {
                        decimal? qty = null;
                        decimal? bonusQty = null;
                        if (!string.IsNullOrEmpty(item.Result.Qty) && !string.IsNullOrWhiteSpace(item.Result.Qty))
                        {
                            if (double.TryParse(item.Result.custCode, out double custCode))
                            {
                                /// remove sales Deficit & increase  and Sales LTD TO EGD
                                if (custCode < 10201001 || new double[] { 11201001, 11201002, 11201003 }.Contains(custCode))
                                    continue;
                            }


                            qty = Decimal.TryParse(item.Result.Qty, out decimal q) ? q : (decimal?)null;
                            bonusQty = Decimal.TryParse(item.Result.Bouns, out decimal b) ? b : (decimal?)null;
                            if ((bonusQty.HasValue && bonusQty.Value != 0) || (qty.HasValue && qty.Value != 0))
                            {
                                FileRecords.Add(new PluginRecord
                                {
                                    CustomerCode = item.Result.custCode,
                                    CustomerName = item.Result.custName,
                                    CustomerAddress = item.Result.custAddress,
                                    DosageFormCode = item.Result.ProdCode,
                                    DosageFormName = item.Result.ProdName,
                                    FileQuantity = bonusQty.HasValue && bonusQty.Value != 0 ? item.Result.Bouns : item.Result.Qty,
                                    BonusQty = (item.Result.Status == "1" ? -1 : 1) * bonusQty,  // ask araby
                                    Quantity = (item.Result.Status == "1" ? -1 : 1) * qty, /*1 return, 0 sales*/
                                    InvalidQty = !qty.HasValue
                                });
                            }
                        }
                    }
                }
                catch (Exception)
                {

                    Status = ValidationStatus.InvalidFormat;
                }
            }
        }
        private ValidationStatus Status { get; set; }
        public override ValidationStatus ValidateFiles()
        {
            return Status;
        }
    }
}
