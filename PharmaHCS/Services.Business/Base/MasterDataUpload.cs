﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Services.Business
{
    public class MasterDataUpload
    {
        public enum UploadStatus
        {
            Success,
            FileIsEmpty,
            InvalidFileHeader,
            dbError_FailedtoImportData,
            DataContainsErrors
        }
        public class UploadStatusResult
        {
            public UploadStatus Status { get; set; }
            public DataTable failedReasons { get; set; }
        }
        private int UserId { get; set; }

        protected MasterDataUploadDefinition MasterDataDefinition { get; set; }
        public MasterDataUpload(int userId)
        {
            UserId = userId;
        }
        public UploadStatusResult UploadFile(DataTable data, MasterDataType type, params string[] inputs)
        {

            MasterDataDefinition = MasterDataDefinitionFactory.GetType(type);
            if (data == null || data.Rows.Count < 1) return new UploadStatusResult { Status = UploadStatus.FileIsEmpty };
            if (data.Columns.Count - 1 != MasterDataDefinition.TemplateColumns.Count) return new UploadStatusResult { Status = UploadStatus.InvalidFileHeader };

            if (!ValidColumns(data.Columns)) return new UploadStatusResult { Status = UploadStatus.InvalidFileHeader };
            string tableName = string.Empty;
            if (!CreateTempTable(data, out tableName)) return new UploadStatusResult { Status = UploadStatus.dbError_FailedtoImportData };

            inputs = WrapInputsWithQuotes(inputs);
            try
            {

                DataTable failedresults = CommitData(tableName, inputs);
                if (failedresults != null && failedresults.Rows.Count > 0) return new UploadStatusResult { Status = UploadStatus.DataContainsErrors, failedReasons = failedresults };

            }
            catch (Exception e)
            {
                return new UploadStatusResult { Status = UploadStatus.dbError_FailedtoImportData };
            }
            return new UploadStatusResult { Status = UploadStatus.Success };
        }

        private string[] WrapInputsWithQuotes(string[] inputs)
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i] = string.Format("'{0}'", inputs[i]);
            }
            return inputs;
        }

        private DataTable CommitData(string tableName, params string[] inputs)
        {
            try
            {
                ///TODO: make the procedure syntax in the definition and change parameter forceUpdate to List of object parameters
                ///to be passed to stored procedure
                DataSet ds = new DataSet();
                var sa = new SqlDataAdapter(
                    //string.Format("exec {0} '{1}', '{2}'", MasterDataDefinition.StoredProcName, MasterDataDefinition.TableName, UserId),
                    MasterDataDefinition.GetExecuteStoredString(UserId, tableName, inputs),
                    connectionString);
                sa.SelectCommand.CommandTimeout = 0;
                sa.Fill(ds);
                return ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static System.Data.DataTable ToDataTable<T>(List<T> data, List<string> excludedColumns)
            where T : class
        {
            System.ComponentModel.PropertyDescriptorCollection props =
                System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));
            System.Data.DataTable table = new System.Data.DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                System.ComponentModel.PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name);
            }
            object[] values = new object[props.Count];
            if (data != null)
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }
                    table.Rows.Add(values);
                }
            if (excludedColumns != null)
                foreach (string exclude in excludedColumns)
                {
                    if (table.Columns.Contains(exclude))
                        table.Columns.RemoveAt(table.Columns.IndexOf(exclude));
                }

            return table;
        }
        private string connectionString => new DBModel().Database.Connection.ConnectionString;
        public bool CreateTempTable(DataTable data, out string tableName)
        {
            bool ret = true;

            List<string> cols = new List<string>();
            foreach (string col in MasterDataDefinition.TemplateColumns)
                cols.Add(string.Format("[{0}][nvarchar] (max) NULL", col));

            tableName = string.Format("{0}_{1}_{2}", MasterDataDefinition.TableName, UserId, Guid.NewGuid().ToString().Replace("-", ""));
            string createScript = string.Format(@"CREATE TABLE [Temp].[{0}](AutoID int, {1})", tableName, string.Join(",", cols.ToArray()));

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = createScript;
                cmd.ExecuteNonQuery();

                //Open bulkcopy connection.
                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(connection))
                {
                    //Set destination table name
                    //to table previously created.
                    bulkcopy.DestinationTableName = string.Format("[Temp].[{0}]", tableName);
                    try
                    {
                        bulkcopy.WriteToServer(data);
                    }
                    catch (Exception e)
                    {
                        ret = false;
                    }

                    connection.Close();
                }
            }
            return ret;

        }

        private bool ValidColumns(DataColumnCollection columns)
        {
            foreach (string item in MasterDataDefinition.TemplateColumns)
                if (!columns.Contains(item))
                    return false;

            return true;
        }
    }

    public struct MasterDataUploadDefinition
    {
        public string TableName { get; set; }
        public string StoredProcName { get; set; }
        public List<string> TemplateColumns { get; set; }

        public string GetExecuteStoredString(int userId, string tableName, params string[] inputs)
        {
            return inputs != null && inputs.Length > 0 ?
                string.Format("exec {0} '{3}','{1}', {2}"
                    , StoredProcName
                    , userId
                    , string.Join(",", inputs)
                    , tableName
                    )
                    :
                    string.Format("exec {0} '{2}','{1}'"
                    , StoredProcName
                    , userId
                    , tableName
                    );
        }
    }

    public enum MasterDataType
    {
        Account,
        Employee,
        Product,
        DosageForm,
        Taregt,
        Plan,
        Holiday,
        Team,
        Target_ProfileTerritoryProduct,
        Account_ImportedFile,
        ProfileAccount,
        ProfileTerritory,
        EmployeeHistoryProfile,
        TeamProfile,
        ImportedFileRecord
    }
    public static class MasterDataDefinitionFactory
    {
        public static MasterDataUploadDefinition GetType(MasterDataType type)
        {
            MasterDataUploadDefinition mdDef = new MasterDataUploadDefinition();
            switch (type)
            {
                case MasterDataType.Account:
                    mdDef.TableName = "Account";
                    mdDef.StoredProcName = "sp_Import_Customer";
                    mdDef.TemplateColumns = new List<string>() { "CustomerCode", "CustomerName", "TypeCode", "CustomerType", "SpecialtyCode", "SpecialtyName"
    ,"PotentialName","PersonalPhone", "Mobile", "EMail", "BirthDate", "Gender", "Active"
    , "AffiliationType", "TerritoryCode", "TerritoryName","Address", "AffiliationPhone", "Primary", "Customer2Code", "Customer2Name"};
                    break;
                case MasterDataType.Employee:
                    mdDef.TableName = "Employee";
                    mdDef.StoredProcName = "sp_Import_Employee";
                    mdDef.TemplateColumns = new List<string>() { "EmployeeCode", "EmployeeName", "InsuranceNumber", "EMail", "Phone", "BirthDate", "Gender", "HiringDate", "ResignationDate", "Active"
    ,"IsTrainer", "TitleCode", "TitleName", "ManagerCode", "ManagerName"
    ,"TeamCode", "TeamName","FromDate", "ToDate","ResponsibilityCode"    ,"LogonName", "RoleName"};
                    break;
                case MasterDataType.ImportedFileRecord:
                    mdDef.TableName = "ImportedFileRecord";
                    mdDef.StoredProcName = "[Sales].[sp_Import_ImportedFileRecord]";
                    mdDef.TemplateColumns = new List<string>()
                    { "BQuantity", "FileId", "DistributorBranchId","RawRecord", "CustomerCode","CustomerName","CustomerAddress", "CustomerTerritory",
                      "DosageFormCode", "DosageFormName","FileQuantity", "FileAmount","InvoiceNumber" ,"SalesDate", "AccountId",
                      "DosageFormId","Quantity", "Amount", "DistributorBranchCode", "DistributorBranchName", "CustomerMapped",
                      "ProductMapped", "DuplicateRow", "InvalidQty", "InvalidRow", "AccountToBeCreated", "CreationDate",
                      "LastModifiedDate", "CreatedById", "LastModifiedById", "IsBonus", "Employee", "Employee1",
                      "DistributorBranch", "ImportedFile", "Account", "ProductDosageForm"};
                    break;
                case MasterDataType.Product:
                    mdDef.TableName = "Product";
                    mdDef.StoredProcName = "sp_Import_Product";
                    mdDef.TemplateColumns = new List<string>() { "ProductCode", "ProductName", "Indications", "Contraindications", "SideEffects", "Storage", "Active"
    ,"SpecialtyCode", "SpecialtyName"};
                    break;
                case MasterDataType.DosageForm:
                    mdDef.TableName = "PDosageForms";
                    mdDef.StoredProcName = "sp_Import_ProductDosageForm";
                    mdDef.TemplateColumns = new List<string>() { "ProductCode", "ProductName", "DosageFormCode", "DosageFormName" };
                    break;
                case MasterDataType.Taregt:
                    mdDef.TableName = "Target";
                    mdDef.StoredProcName = "sp_Import_Target";
                    mdDef.TemplateColumns = new List<string>() { "EmployeeCode", "EmployeeName","TeamCode", "TeamName", "PeriodName", "PeriodYear","Status"
    , "CustomerCode", "CustomerName", "TypeName", "AffiliationId","Address", "TerritoryCode", "TerritoryName"};
                    break;
                case MasterDataType.Plan:
                    mdDef.TableName = "Plan";
                    mdDef.StoredProcName = "sp_Import_Plan";
                    mdDef.TemplateColumns = new List<string>() { "EmployeeCode", "EmployeeName","TeamCode", "TeamName", "PeriodName", "PeriodYear", "FromDate", "ToDate" ,"Status"
    ,"AppointmentDate" , "CustomerCode", "CustomerName", "AffiliationType", "AffiliationId","Address", "TerritoryCode", "TerritoryName" };
                    break;
                case MasterDataType.Holiday:
                    mdDef.TableName = "Holiday";
                    mdDef.StoredProcName = "sp_Import_Holiday";
                    mdDef.TemplateColumns = new List<string>() { "HolidayDescription", "FromDate", "ToDate" };
                    break;
                case MasterDataType.Target_ProfileTerritoryProduct:
                    mdDef.TableName = "Target_ProfileTerritoryProduct";
                    mdDef.StoredProcName = "Sales.sp_Target_ImportProfileTerritoryProduct";
                    mdDef.TemplateColumns = new List<string>() { "ProductId", "ProfileId", "tgt_qty" };
                    break;
                case MasterDataType.Account_ImportedFile:
                    mdDef.TableName = "Account_ImportedFile";
                    mdDef.StoredProcName = "Sales.sp_ImportedFile_ImportAndMapAccounts";
                    mdDef.TemplateColumns = new List<string>() { "Customer Code", "Customer Name", "Customer Address", "Customer Territory", "Territory", "Account Type", "Category", "RecordId" };
                    break;
                case MasterDataType.ProfileAccount:
                    mdDef.TableName = "ProfileAccount";
                    mdDef.StoredProcName = "Sales.sp_CommitProfileAccount";
                    mdDef.TemplateColumns = new List<string>() { "Profile Code", "Account Code", "Percentage", "Product Code", "DosageForm Code"};
                    break;
                case MasterDataType.ProfileTerritory:
                    mdDef.TableName = "ProfileTerritory";
                    mdDef.StoredProcName = "Sales.sp_CommitProfileTerritory";
                    mdDef.TemplateColumns = new List<string>() { "Profile Code", "Territory Code", "Percentage" };
                    break;
                case MasterDataType.EmployeeHistoryProfile:
                    mdDef.TableName = "EmployeeHistoryProfile";
                    mdDef.StoredProcName = "Sales.sp_CommitEmployeeHistoryProfile";
                    mdDef.TemplateColumns = new List<string>() { "Employee Code", "Employee Name", "NationalID", "Department","Manager Code", "Can Visit",
                    "Job Title","Team","Profile Code","Profile Name"};
                    break;
                case MasterDataType.TeamProfile:
                    mdDef.TableName = "TeamProfile";
                    mdDef.StoredProcName = "Sales.sp_CommitTeamProfile";
                    mdDef.TemplateColumns = new List<string>() { "Profile Code", "Team", "AvgCallRate", "ListTargetDoctorsCount" };
                    break;
            }
            return mdDef;
        }
    }
}
