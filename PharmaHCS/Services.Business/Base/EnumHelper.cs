﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Services.Helper
{
    public static class EnumHelpers
    {
        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        public static string DisplayName(this Enum value)
        {
            Type enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);
            MemberInfo member = enumType.GetMember(enumValue)[0];

            var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            var outString = ((DisplayAttribute)attrs[0]).Name;

            if (((DisplayAttribute)attrs[0]).ResourceType != null)
            {
                outString = ((DisplayAttribute)attrs[0]).GetName();
            }

            return outString;
        }

        public enum ProductPriceCritria
        {
            [Display(Name = "DosageForm", ResourceType = typeof(PH.ProductPrice))]
            DosageForm = 1000,

            [Display(Name = "Distributor", ResourceType = typeof(PH.ProductPrice))]
            Distributor = 0100,

            [Display(Name = "Territory", ResourceType = typeof(PH.ProductPrice))]
            Territory = 0010,

            [Display(Name = "Account", ResourceType = typeof(PH.ProductPrice))]
            Account = 0001,

            [Display(Name = "DosageDistributor", ResourceType = typeof(PH.ProductPrice))]
            DosageDistributor = 1100,

            [Display(Name = "DosageTerritory", ResourceType = typeof(PH.ProductPrice))]
            DosageTerritory = 1010,

            [Display(Name = "DosageAccount", ResourceType = typeof(PH.ProductPrice))]
            DosageAccount = 1001,

            [Display(Name = "DistributorTerritory", ResourceType = typeof(PH.ProductPrice))]
            DistributorTerritory = 0110,

            [Display(Name = "DistributorAccount", ResourceType = typeof(PH.ProductPrice))]
            DistributorAccount = 0101,

            [Display(Name = "TerritoryAccount", ResourceType = typeof(PH.ProductPrice))]
            TerritoryAccount = 0011,

            [Display(Name = "DosageDistributorTerritory", ResourceType = typeof(PH.ProductPrice))]
            DosageDistributorTerritory = 1110,

            [Display(Name = "DosageDistributorAccount", ResourceType = typeof(PH.ProductPrice))]
            DosageDistributorAccount = 1101,

            [Display(Name = "DosageAccountTerritory", ResourceType = typeof(PH.ProductPrice))]
            DosageAccountTerritory = 1011,

            [Display(Name = "DistributorTerritoryAccount", ResourceType = typeof(PH.ProductPrice))]
            DistributorTerritoryAccount = 0111,

            [Display(Name = "DosageDistributorAccountTerritory", ResourceType = typeof(PH.ProductPrice))]
            DosageDistributorAccountTerritory = 1111

        }

        public enum TemplateStructureType
        {
            [Display(Name = "EmployeeStructure", ResourceType = typeof(PH.EmployeeStructureImport))]
            EmployeeStructure=1,

            [Display(Name = "ProfileAccount", ResourceType = typeof(PH.EmployeeStructureImport))]
            ProfileAccount = 2,

            [Display(Name = "ProfileTerritory", ResourceType = typeof(PH.EmployeeStructureImport))]
            ProfileTerritory = 3,
        }
        public enum TemplateMandatoryFields
        {
            [Description("A1,D1,G1,I1")]
            EmployeeStructure = 0,

            [Description("A1,B1,C1")]
            ProfileAccount,

            [Description("A1,B1,C1")]
            ProfileTerritory,

            [Description("A1")]
            TeamProfile
        }

        public enum TargetAdjustmentType
        {
            [Display(Name = "TargetBiggerSales", ResourceType = typeof(PH.TargetAdjustment))]
            TargetBiggerSales=1,

            [Display(Name = "TargetSales", ResourceType = typeof(PH.TargetAdjustment))]

            TargetSales = 2,

            [Display(Name = "TargetZero", ResourceType = typeof(PH.TargetAdjustment))]

            TargetZero = 3,

            [Display(Name = "TargetValue", ResourceType = typeof(PH.TargetAdjustment))]

            TargetValue = 4,
        }
        public enum NewDoctorSataus
        {
            [Description("d")]
            Draft = 0,

            [Description("p")]
            Pending,

            [Description("a")]
            Approved,

            [Description("r")]
            Rejected
        }
        public enum SettingApp
        {
            [Description("RecordperMinute")]
            RecordperMinute
             
        } 
     
    }
}
