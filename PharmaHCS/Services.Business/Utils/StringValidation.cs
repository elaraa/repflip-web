﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public static class StringValidation
    {
        public static bool IsEmpty(this string v) {

            return string.IsNullOrEmpty(v) || string.IsNullOrWhiteSpace(v);
        }
    }
}
