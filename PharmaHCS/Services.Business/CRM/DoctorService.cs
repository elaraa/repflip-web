﻿using AutoMapper;
using DB.ORM.DB;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DB.ORM.DB.AccountDoctor;
using static DB.ORM.DB.Doctor;
using static DB.ORM.DB.LinkedAccountSale;

namespace Services.Business.CRM
{ 
    public class DoctorService : BaseService<Doctor>
    {
        public DoctorService(int userId) : base(userId)
        {

        }
        public IQueryable<vw_AccountDoctor> getMyAccount(int id)
        {
            return Get<vw_AccountDoctor>(a => a.DoctorId == id);//ToDropDownList(a => new SelectListItem { Text = a.AccountName, Value = a.AccountId.ToString() }); ;
        }

        public IQueryable<vw_Account> getAccounts(int id)
        {
            var myAccountIds = Get<vw_AccountDoctor>(a => a.DoctorId == id).Select(a => a.AccountId);
            var exceptAccounts = Get<vw_Account>(a => myAccountIds.Contains(a.AccountId));
            var accounts = Get<vw_Account>(a => a.AccountTypeId == 9 && a.AccountTypeName.Contains("ph")).Except(exceptAccounts);
            return accounts;
        }

        public RunResult saveDoctorModel(DoctorDto model)
        {
            try
            {
                if (model != null)
                {
                    if (model.DoctorId>0)
                    {
                        AddOrUpdate(Mapper.Map<Doctor>(model));
                        return new RunResult { Succeeded = true };
                    }
                    else
                    {
                        var doctor = Mapper.Map<Doctor>(model);
                        AddOrUpdate(doctor);
                        
                        if (model.AccountId != null)
                        {
                            var accountDoctor = new AccountDoctor
                            {
                                DoctorId = doctor.DoctorId,
                                AccountId = model.AccountId.Value,
                                IsPrimary = true
                            };
                            AddOrUpdate<AccountDoctor>(accountDoctor);
                        }
                        else
                        {
                            var account = Mapper.Map<Account>(model);
                            AddOrUpdate<Account>(account);

                            var accountDoctor = new AccountDoctor
                            {
                                DoctorId = doctor.DoctorId,
                                AccountId = account.AccountId,
                                IsPrimary = true
                            };
                            AddOrUpdate<AccountDoctor>(accountDoctor);
                        }
                        return new RunResult { Succeeded = true ,SuccessDesc="Create"};
                    }
                    
                }
                return new RunResult { Succeeded = false };
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false };
            }
           
        }
        public DoctorDto GetDoctor(int id)
        {
            try 
            {
                var doctor = GetOne<Doctor>(a => a.DoctorId == id);
                return Mapper.Map<DoctorDto>(doctor);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }





        public RunResult saveModel(AccountDoctorDto model)
        {
            if (model != null)
            {
                foreach (var item in model.AccountIds)
                {
                    var entity = new AccountDoctor
                    {
                        AccountId = item,
                        DoctorId = model.DoctorId,
                        IsPrimary = true
                    };
                    AddOrUpdate<AccountDoctor>(entity);
                }
                return new RunResult { Succeeded = true };
            }
            else
                return new RunResult { Succeeded = false };
        }

        public RunResult saveAccountModel(LinkedAccountSalesDto model) 
        {
            if (model != null)
            {
                foreach (var item in model.LinkedAccountIds)
                {
                    var entity = new LinkedAccountSale
                    {
                        LinkedAccountId = item,
                        AccountId = model.AccountId,
                        IsPrimary = true
                    };
                    AddOrUpdate<LinkedAccountSale>(entity);
                }
                return new RunResult { Succeeded = true };
            }
            else
                return new RunResult { Succeeded = false };
        }
    }
}
