﻿using DB.ORM.DB;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Services.Business.CRM
{
    public class RepVisit : BaseService<vw_VisitsPerDate>
    {
        public RepVisit(int userId) : base(userId) { }
        public override IQueryable<vw_VisitsPerDate> Get(Expression<Func<vw_VisitsPerDate, bool>> whereClause = null, bool safe = false, params string[] childrenToInclude)
        {
            var predicate = PredicateBuilder.New<vw_VisitsPerDate>(true);
            if (whereClause != null)
                predicate = predicate.And(whereClause);

            predicate.And(w => w.EmployeeId == UserId);

            return base.Get(predicate, safe, childrenToInclude);
        }

        public List<Product> GetProducts() => db.crm_fnGetRepProducts(UserId, DateTime.Now).ToList();

        public vw_VisitsPerDate GetByDate(DateTime date) => GetOne(w => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(w.VisitDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(date) && w.EmployeeId == UserId);

        public List<ProductPresentation> GetPresentations() => db.crm_fnGetRepPresentations(UserId, DateTime.Now).ToList();
        public List<Account> GetAccounts() => db.crm_fnGetRepAccounts(UserId, DateTime.Now).ToList();
        public List<Doctor> GetAccountDoctors(int accountId) => db.crm_fnGetRepAccountDoctors(UserId, DateTime.Now, accountId).ToList();
        public List<Employee> GetCoaches() => db.crm_fnGetRepCoaches(UserId, DateTime.Now).ToList();

        public bool Add(Visit model)
        {
            model.EmployeeId = UserId;
            model.CountryId = db.Employees.FirstOrDefault(w=>w.EmployeeId == UserId).CountryId.Value;
            //fill visit Products
            var plist = model.VisitPresentations.Select(s => s.PresentationId).ToList();
            var prodList = db.ProductPresentations.Where(w => plist.Contains(w.PresentationId))
                .Select(s=>s.ProductId).Distinct().ToList()
                .Select(s => new VisitProduct { VisitId = model.VisitId, ProductId = s }).ToList();
            model.VisitProducts = prodList;
            //save
            db.Visits.Add(model);
            return db.SaveChanges() > 0;
        }
        public IQueryable<vw_VisitsList> ReadVisitsList(DateTime date) {
            return db.vw_VisitsList.Where(w => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(w.VisitDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(date) && w.EmployeeId == UserId);
        }

        public bool DeleteVisit(Guid id)
        {
            var v = db.Visits.FirstOrDefault(w=>w.VisitId == id && w.EmployeeId == UserId && w.IsReviewed == false && w.IsFalseVisit == false);
            if (v != null && !v.IsReviewed && !v.IsFalseVisit) {
                db.VisitCoaches.Where(w => w.VisitId == id).DeleteFromQuery();
                db.VisitComments.Where(w => w.VisitId == id).DeleteFromQuery();
                db.VisitDoctors.Where(w => w.VisitId == id).DeleteFromQuery();
                db.VisitProducts.Where(w => w.VisitId == id).DeleteFromQuery();
                db.VisitRequests.Where(w => w.VisitId == id).DeleteFromQuery();
                db.VisitPresentations.Where(w => w.VisitId == id).DeleteFromQuery();
                db.Visits.Where(w => w.VisitId == id).DeleteFromQuery();

                return db.SaveChanges() > 0;
            }
            return false;
        }
    }
}
