﻿using DB.ORM.DB;
using Services.Business.Sales;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DB.ORM.DB.AccountDoctor;
using static DB.ORM.DB.LinkedAccountSale;

namespace Services.Business.CRM
{ 
    public class SupervisorService : BaseService<ProfileTargetNewDoctorApproval>
    {
        public SupervisorService(int userId) : base(userId)
        {

        }
        
        public bool ApproveReject(int id, bool approved)
        { 
            try
            {
                var entity = GetOne(a => a.TargetDoctorApprovalId == id);
                if (entity != null)
                {
                    entity.ApprovalStatus = approved ? EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Approved) : EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Rejected);
                    entity.AdminApprove = approved ? EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Pending) : null;
                    entity.SupervisorId = UserId;
                    AddOrUpdate(entity);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

       public IEnumerable<vw_ProfiletargetNewDoctor>GetList()
       {
            try
            {
                var ids = db.fn_Security_GetEmployeesForEmployee(UserId, DateTime.Now).ToList();
                var res = Get<vw_ProfiletargetNewDoctor>(a => ids.Contains(a.EmployeeId.Value), true);
                return res != null ? res.ToList().Where(a => a.ApprovalStatus == EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Pending)) : res;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
       }
    }
}
