﻿using AutoMapper;
using DB.ORM.DB;
using Services.Business.Sales;
using Services.Helper;
using System;
using static DB.ORM.DB.ProfileTargetNewDoctorApproval;

namespace Services.Business.CRM
{ 
    public class AdminService : BaseService<ProfileTargetNewDoctorApproval>
    {
        public AdminService(int userId) : base(userId)
        {

        }

        public RunResult SaveModel(ProfileTargetNewDoctorApprovalDto model, bool approved)
        {
            try
            {
                if (model != null)
                {
                    if (!approved)
                    {
                        model.AdminApprove = EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Rejected);
                        AddOrUpdate(Mapper.Map<ProfileTargetNewDoctorApproval>(model));
                        return new RunResult { Succeeded = true };
                    }
                    else
                    {
                        model.AdminApprove = EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Approved);
                        AddOrUpdate(Mapper.Map<ProfileTargetNewDoctorApproval>(model));
                        Account account = new Account();
                        Doctor doctor = new Doctor();

                        if (model.DoctorId == null && model.AccountId == null)
                        {
                            doctor = Mapper.Map<Doctor>(model);
                            doctor.Approved = true;
                          
                            account = Mapper.Map<Account>(model);
                            account.Active = true;
                            account.Approved = true;
                          
                            AddOrUpdate<Doctor>(doctor);
                            AddOrUpdate<Account>(account);
                            var accountDoctor = GetNewAccountDoctor(account.AccountId,doctor.DoctorId);
                          
                            AddOrUpdate<AccountDoctor>(accountDoctor);

                        }
                        else if (model.AccountId == null)
                        {
                            account = Mapper.Map<Account>(model);
                            account.Active = true;
                            account.Approved = true;
                            
                            AddOrUpdate<Account>(account);

                            var accountDoctor = GetNewAccountDoctor(account.AccountId,model.DoctorId.Value);
                            AddOrUpdate<AccountDoctor>(accountDoctor);
                        }
                        else if (model.DoctorId == null)
                        {
                            doctor = Mapper.Map<Doctor>(model);
                            doctor.Approved = true;
                           
                            AddOrUpdate<Doctor>(doctor);

                            var accountDoctor = GetNewAccountDoctor(model.AccountId.Value, doctor.DoctorId);
                            AddOrUpdate<AccountDoctor>(accountDoctor);
                        }
                        else
                            return new RunResult { Succeeded = true };

                        var profileTargetVisit = GetOne<ProfileTargetVisit>(a => a.ProfileId == model.ProfileId && (model.TimeId >= a.FromTimeDefinitionId && a.ToTimeDefinitionId.HasValue ? model.TimeId <= a.ToTimeDefinitionId : true));
                        if (profileTargetVisit.ToTimeDefinitionId.HasValue)
                        {
                            var ExistOverLappedPeriod = GetOne<ProfileTargetVisit>(a => a.ProfileId == model.ProfileId && (model.TimeId >= a.FromTimeDefinitionId && a.ToTimeDefinitionId.HasValue ? model.TimeId <= a.ToTimeDefinitionId : true));

                            if (ExistOverLappedPeriod != null)
                            {
                                var targetVisitDetails = new ProfileTargetVisitDetail
                                {
                                    ProfileTargetId = ExistOverLappedPeriod.ProfileTargetId,
                                    AccountId = account.AccountId,
                                    DoctorId = doctor.DoctorId,
                                    PotentialId = model.PotentialId,
                                    Frequency = model.Frequency
                                };
                                AddOrUpdate<ProfileTargetVisitDetail>(targetVisitDetails);
                            }
                            else
                            {
                                var targetVisit = new ProfileTargetVisit
                                {
                                    FromTimeDefinitionId = model.TimeId,
                                    CountryId = model.CountryId,
                                    ProfileId = model.ProfileId.Value,
                                };
                                AddOrUpdate<ProfileTargetVisit>(targetVisit);
                                var targetVisitDetails = new ProfileTargetVisitDetail
                                {
                                    ProfileTargetId = targetVisit.ProfileTargetId,
                                    AccountId = account.AccountId,
                                    DoctorId = doctor.DoctorId,
                                    PotentialId = model.PotentialId,
                                    Frequency = model.Frequency
                                };
                                AddOrUpdate<ProfileTargetVisitDetail>(targetVisitDetails);
                            }
                        }
                        else
                        {
                            if (profileTargetVisit.FromTimeDefinitionId<model.TimeId)
                            {
                                profileTargetVisit.ToTimeDefinitionId = model.TimeId - 1;
                                AddOrUpdate<ProfileTargetVisit>(profileTargetVisit);
                                var targetVisit = new ProfileTargetVisit
                                {
                                    FromTimeDefinitionId = model.TimeId,
                                    CountryId = model.CountryId,
                                    ProfileId = model.ProfileId.Value,
                                    ManagerResponse="d"
                                };
                                AddOrUpdate<ProfileTargetVisit>(targetVisit);
                                var targetVisitDetails = new ProfileTargetVisitDetail
                                {
                                    ProfileTargetId = targetVisit.ProfileTargetId,
                                    AccountId = account.AccountId,
                                    DoctorId = doctor.DoctorId,
                                    PotentialId = model.PotentialId,
                                    Frequency = model.Frequency
                                };
                                AddOrUpdate<ProfileTargetVisitDetail>(targetVisitDetails);
                            }
                            else
                            {
                                var targetVisitDetails = new ProfileTargetVisitDetail
                                {
                                    ProfileTargetId = profileTargetVisit.ProfileTargetId,
                                    AccountId = account.AccountId,
                                    DoctorId = doctor.DoctorId,
                                    PotentialId = model.PotentialId,
                                    Frequency = model.Frequency
                                };
                                AddOrUpdate<ProfileTargetVisitDetail>(targetVisitDetails);
                            }
                        }
                        

                        return new RunResult { Succeeded = true };
                    }

                }
                else
                    return new RunResult { Succeeded = false };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false };
            }

        }

        public bool ApproveReject(int id, bool approved)
        {
            try
            {
                var entity = GetOne(a => a.TargetDoctorApprovalId == id);
               var res= SaveModel(Mapper.Map<ProfileTargetNewDoctorApprovalDto>(entity), approved);

                return res.Succeeded ;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public ProfileTargetNewDoctorApprovalDto GetDoctorRequest(int id)
        {
            try
            {
                var doctor = GetOne<ProfileTargetNewDoctorApproval>(a => a.TargetDoctorApprovalId == id);
               
                return Mapper.Map<ProfileTargetNewDoctorApprovalDto>(doctor);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }

        private AccountDoctor GetNewAccountDoctor(int accountId,int doctorId)
        { 
               return new AccountDoctor
               {
                    AccountId = accountId,
                    DoctorId = doctorId,
                    IsPrimary = true,
                    CreatedById = UserId
               };
        }
    }
}
