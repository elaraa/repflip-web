﻿using AutoMapper;
using DB.ORM.DB;
using Services.Business.CRM.NewDoctorRequest;
using Services.Business.Sales;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DB.ORM.DB.AccountDoctor;
using static DB.ORM.DB.LinkedAccountSale;
using static DB.ORM.DB.ProfileTargetNewDoctorApproval;

namespace Services.Business.CRM
{  
    public class NewDoctorRequestService : BaseService<ProfileTargetNewDoctorApproval>
    {
        public NewDoctorRequestService(int userId) : base(userId)
        {

        }
        

        public RunResult SaveModel(DoctorAccountVM model)
        {
            try
            {
                if (model != null)
                {
                    var territory = GetOne<Territory>(a => a.TerritoryId == model.TerritoryId);
                    var country = GetOne<Country>(a => a.CountryId == model.CountryId);
                    model.Address = string.Format("{0},{1},{2}", territory != null ? territory.TerritoryName : string.Empty, model.City ?? string.Empty, country != null ? country.CountryName : string.Empty);

                    if (model.TargetDoctorApprovalId == 0)
                    {
                        var profile = GetOne<EmployeeProfile>(a => a.EmployeeId == UserId && a.ToDate == null);
                        if (profile != null)
                            model.ProfileId = profile.ProfileId;
                       
                        model.ApprovalStatus = EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Draft);
                        model.CreatedById = UserId;
                        
                        AddOrUpdate(Mapper.Map<ProfileTargetNewDoctorApproval>(model));
                        return new RunResult { Succeeded = true, SuccessDesc = "Create" };
                    }
                    else
                    {
                        
                        model.LastModifiedById = UserId;
                        AddOrUpdate(Mapper.Map<ProfileTargetNewDoctorApproval>(model));
                        return new RunResult { Succeeded = true, SuccessDesc = "Edit" };
                    }
                }
                else
                    return new RunResult { Succeeded = false };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false };
            }
        }


        public bool SendDocotrRequest(int id)
        {
            try
            {
                var newDoctor = GetOne(x => x.TargetDoctorApprovalId == id);
                if (newDoctor != null && newDoctor.ApprovalStatus.ToLower() == EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Draft))
                {
                    newDoctor.ApprovalStatus = EnumHelpers.DescriptionAttr(EnumHelpers.NewDoctorSataus.Pending);
                    AddOrUpdate(newDoctor);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public DoctorAccountVM GetDoctorRequest(int id, decimal lat, decimal lng)
        {
            try
            {
                var doctor = GetOne<ProfileTargetNewDoctorApproval>(a => a.TargetDoctorApprovalId == id);
                doctor.Latitude = doctor.Latitude != null ? doctor.Latitude : lat;
                doctor.Longitude = doctor.Longitude != null ? doctor.Longitude : lng;
                return Mapper.Map<DoctorAccountVM>(doctor);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
            
        }
    }
}
