﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.CRM.NewDoctorRequest
{
    public class DoctorAccountVM : vw_ProfiletargetNewDoctor
    {
        [Required]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }
        
        public string City { get; set; }
       

    }
}
