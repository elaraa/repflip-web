﻿using AutoMapper;
using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DB.ORM.DB.vw_ProfileTargetVisit;

namespace Services.Business.CRM
{
    public enum RepTargetCreateOption
    {
        CopyFromPreviousPeriod = 1,
        Empty
    }
    public class ReptargetResult
    {
        public bool Success { get; set; }
        public int ResultId { get; set; }
    }
    public class RepTarget: BaseService<ProfileTargetVisit>
    {
        public RepTarget(int userId): base(userId){}
        public ReptargetResult CreateTarget(int timeDefinitionId, RepTargetCreateOption createOption){
            var res = db.sp_RepTargetCreate(UserId, timeDefinitionId, (int)createOption);
            return new ReptargetResult { Success = res > 0, ResultId = res };
        }
       // public IQueryable<crm_fn_Target_Universe> GetTargetUniverse(int targetId) => db.crm_fn_Target_Universe(targetId);
        public IQueryable<vw_ProfileTargetVisitDetails> GetTargetVisitDetails(int targetId) => db.vw_ProfileTargetVisitDetails.Where(w => w.ProfileTargetId == targetId);
        public bool DeleteAccount(int targetDetailId) {
            return (from d in db.ProfileTargetVisitDetails
                    join t in db.ProfileTargetVisits on d.ProfileTargetId equals t.ProfileTargetId
                    where d.ProfileTargetDetailId == targetDetailId
                        && ( t.ManagerResponse == null || t.ManagerResponse.ToLower() == "r")
                    select d
                ).DeleteFromQuery()>0;
        }
        public bool UpdateAccount(int targetDetailId, int potentialId, int frequency) {
            return db.ProfileTargetVisitDetails
                .Where(w => w.ProfileTargetDetailId == targetDetailId)
                .UpdateFromQuery(u=>new ProfileTargetVisitDetail { Frequency = frequency,  PotentialId = potentialId } ) > 0;
        }
        public bool AddAccount(int targetId, int accountId, int? doctorId)
        {
            var potId = (doctorId.HasValue ? db.Doctors.FirstOrDefault(w => w.DoctorId == doctorId).PotentialId : db.Accounts.FirstOrDefault(w => w.AccountId == accountId).PotentialId) ?? db.Potentials.FirstOrDefault().PotentialId;

            db.ProfileTargetVisitDetails.Add(new ProfileTargetVisitDetail {
                ProfileTargetId = targetId,
                AccountId = accountId,
                DoctorId = doctorId,
                PotentialId = potId,
                Frequency = 1
            });
            return db.SaveChanges() > 0;
        }

        public bool Add(int targetId, List<AccountDoctorModel> list)
        {
            if (list != null)
            {
                foreach (var item in list)
                {
                    var potId = (item.doctorId.HasValue ? db.Doctors.FirstOrDefault(w => w.DoctorId == item.doctorId).PotentialId : db.Accounts.FirstOrDefault(w => w.AccountId == item.accountId).PotentialId) ?? db.Potentials.FirstOrDefault().PotentialId;

                    db.ProfileTargetVisitDetails.Add(new ProfileTargetVisitDetail
                    {
                        ProfileTargetId = targetId,
                        AccountId = item.accountId,
                        DoctorId = item.doctorId,
                        PotentialId = potId,
                        Frequency = 1
                    });
                }
                return db.SaveChanges() > 0;
            }
            else
                return false;

        }
        public vw_ProfileTargetVisitDto GetProfileTargetVisit(int id)
        {
            try
            {
                if (id > 0)
                {
                    
                    var profileTarget = GetOne<vw_ProfileTargetVisit>(a => a.ProfileTargetId == id);
                    if (profileTarget!=null)
                    {
                        var doctorCount = Get<ProfileTargetVisitDetail>(a => a.ProfileTargetId == profileTarget.ProfileTargetId).Select(a => a.DoctorId).Distinct().Count();
                        //var UniverseTargetDoctorCount = db.crm_fn_Target_Universe(profileTarget.ProfileTargetId) != null ? db.crm_fn_Target_Universe(profileTarget.ProfileTargetId).Count() : 0;
                        var UniverseTargetDoctorCount = GetTargetUniverse(profileTarget.ProfileTargetId)!=null? GetTargetUniverse(profileTarget.ProfileTargetId).Count():0;
                        var workingDays = db.fn_GetWorkingDays(profileTarget.ProfileId, profileTarget.FromTimeDefinitionId, profileTarget.ToTimeDefinitionId);

                        var profileTargetDto = Mapper.Map<vw_ProfileTargetVisitDto>(profileTarget);
                        profileTargetDto.TotalTargetVisit = profileTarget.AvgCallRate * workingDays;
                        profileTargetDto.UniverseTargetDoctor = UniverseTargetDoctorCount;
                        profileTargetDto.ActualCallRate = (decimal)profileTarget.TargetVisits / workingDays;
                        profileTargetDto.ActualTargetDoctor = profileTarget.TargetDoctorsCount;

                        //var profileTargetDto = new vw_ProfileTargetVisitDto
                        //{
                        //    ProfileTargetId= profileTarget.ProfileTargetId,
                        //    ProfileId = profileTarget.ProfileId,
                        //    ProfileCode= profileTarget.ProfileCode,
                        //    PeriodName = profileTarget.PeriodName,
                        //    TargetStatus= profileTarget.TargetStatus,
                        //    AvgCallRate= profileTarget.AvgCallRate,
                        //    TargetVisits= profileTarget.TargetVisits,
                        //    TargetAccountsCount=profileTarget.TargetAccountsCount,

                        //    TotalTargetVisit = profileTarget.AvgCallRate * workingDays,
                        //    UniverseTargetDoctor = UniverseTargetDoctorCount,
                        //    ActualTargetDoctor = profileTarget.TargetDoctorsCount,
                        //    ActualCallRate = (decimal)profileTarget.TargetVisits / workingDays
                        //};
                        return profileTargetDto;
                    }
                    return null;
                }
                else
                    return null;
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }
        public List<AccountMapDto> GetAccounts(int? id) 
        {
            var Accounts = Get<vw_ProfileTargetVisitDetails>(a => a.ProfileTargetId == id).ToList().Where(c => c.Latitude.HasValue &&
                 c.Longitude.HasValue).Select(c => new AccountMapDto
                 {
                     latlng = GetLatLng(c.Latitude.Value, c.Longitude.Value),
                     Lat = c.Latitude.Value,
                     Lng = c.Longitude.Value,
                     AccountName = c.AccountName
                 }).ToList();

            return Accounts;
        }
        private List<decimal> GetLatLng(decimal lat, decimal lng)
        {
            var latlng = new List<decimal>();
            latlng.Add(lat);
            latlng.Add(lng);

            return latlng;
        }

        public IQueryable<vw_Target_Universe> GetTargetUniverse(int targetId, int? potentialId = null)
        {
            var target = GetOne<ProfileTargetVisit>(c => c.ProfileTargetId == targetId);
            return db.vw_Target_Universe.Where(c => c.ProfileId == target.ProfileId &&
              (target.FromTimeDefinitionId >= c.FromTimeDefinitionId.Value &&
              (!c.ToTimeDefinitionId.HasValue || target.ToTimeDefinitionId <= c.ToTimeDefinitionId.Value))
              && (!potentialId.HasValue || c.PotentialId == potentialId.Value));
        }
    }
    public class AccountMapDto
    {
        public string AccountName { get; set; }
        public List<decimal> latlng { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }

    }
    public class AccountDoctorModel
    {
        public int accountId { get; set; }
        public int? doctorId { get; set; } 
    }
}
