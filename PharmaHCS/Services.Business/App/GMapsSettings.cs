﻿using DB.ORM.DB;

namespace Services.Business
{
    public class GMapsSettings : BaseService<Setting>
    {
        public string GoogleMapsKey { get; set; }

        public GMapsSettings(int userId) : base(userId)
        {
            var key = GetOne(w => w.SettingId == 4);
            if (key != null)
                GoogleMapsKey = key.SettingValue;
        }
    }
}
