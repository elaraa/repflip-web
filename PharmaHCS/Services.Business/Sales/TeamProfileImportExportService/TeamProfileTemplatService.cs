﻿using DB.ORM.DB;
using Services.Business.Sales;
using Services.Business.Sales.TeamProfileImportExportService;
using Services.Business.Sales.TemplatesService;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Helpers.File;

namespace Services.Business
{
    public class TeamProfileTemplatesService : BaseService<TeamProfile>
    {

        public TeamProfileTemplatesService(int userId) : base(userId)
        {
        }

        public DataTable ExportTemplate(int? teamId)
        {
             var table = new DataTable();
             table.Columns.Add("Profile Code"); table.Columns.Add("Team"); table.Columns.Add("AvgCallRate");
             table.Columns.Add("ListTargetDoctorsCount");
            var query = Get<vw_TeamProfile>(a => (teamId != null) ? a.TeamId == teamId : true ).Where(a=>a.Active != false).ToList();
             var teamProfile = query.Select(a => new
                {
                    id = a.ProfileId,
                    a.ProfileCode,
                    a.Team,
                    a.AvgCallRate,
                    a.ListTargetDoctorsCount
                    
                }).Distinct();
              foreach (var item in teamProfile)
              {
                    DataRow row = table.NewRow();
                    row[0] = item.ProfileCode;
                    row[1] = item.Team;
                    row[2] = item.AvgCallRate;
                    row[3] = item.ListTargetDoctorsCount;
                    table.Rows.Add(row);
              }
             return table;

        }
         
        public DataSet uploadFiles(TeamProfileTemplatViewModel model)
        {
            System.Data.DataSet data = FileHelper.GetDataSetFromExcel(
               FileHelper.UploadFile(model.File)
               );

            return data;
        }

        public List<TemplateValidation> TeamProfileFailedReasons(DataTable data)
        {
            var list = (from DataRow dr in data.Rows 
                        select new TemplateTeamProfile()
                        {
                            RowIndex = dr["RowIndex"].ToString(),
                            InvalidProfileCode = dr["InvalidProfileCode"].ToString(),
                            IsDuplicated = dr["IsDuplicated"].ToString(),
                            InvalidTeam = dr["InvalidTeam"].ToString(),
                            InvalidAvgCallRate = dr["InvalidAvgCallRate"].ToString(),
                            InvalidListTargetDoctorsCount = dr["InvalidListTargetDoctorsCount"].ToString()
                        }).ToList();
            var res = list.Select(a => new TemplateValidation
            {
                Index = a.RowIndex,
                Comment = PH.TemplatesImprtExport.RowIssues + (a.InvalidTeam == "False" ? "" : ",Invalid Team ") + (a.InvalidProfileCode == "False" ? "" : ",Invalid Profile Code  ") + (a.IsDuplicated == "False" ? "" : ",Is Duplicated ") +
                           (a.InvalidAvgCallRate == "False" ? "" : ",Invalid AvgCallRate") + (a.InvalidListTargetDoctorsCount == "False" ? "" : ",Invalid List Target Doctors Count ")
            }).ToList();
            return res;
        }

        public DataTable ExportEmptyTemplate()
        {
            var table = new DataTable();
            table.Columns.Add("Profile Code"); table.Columns.Add("Team"); table.Columns.Add("AvgCallRate");
            table.Columns.Add("ListTargetDoctorsCount");
            return table;
        }

     
    }
}
