﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales.TeamProfileImportExportService
{
    public class TemplateTeamProfile
    {
        public string RowIndex { get; set; }

        public string ProfileCode { get; set; }

        public string Team { get; set; }

        public decimal AvgCallRate { get; set; }

        public int ListTargetDoctorsCount { get; set; }
         
        public string InvalidProfileCode { get; set; }
         
        public string InvalidTeam { get; set; }

        public string InvalidAvgCallRate { get; set; } 

        public string InvalidListTargetDoctorsCount { get; set; }

        public string IsDuplicated { get; set; }



    }
}
