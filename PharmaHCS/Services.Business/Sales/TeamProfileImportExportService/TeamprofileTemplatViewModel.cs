﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Business.Sales
{
    public class TeamProfileTemplatViewModel
    {
        
        public int TeamId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredFile", ErrorMessageResourceType = typeof(PH.TemplatesImprtExport))]
        public HttpPostedFileBase File { get; set; } 
    } 
}
