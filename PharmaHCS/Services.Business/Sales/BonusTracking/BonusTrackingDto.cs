﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales.BonusTracking
{
    public class BonusTrackingDto
    {
        public int TimeId { get; set; }
        public int TeamId { get; set; }
        public int ProductId { get; set; }
    }
}
