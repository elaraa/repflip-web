﻿using DB.ORM.DB;
using Services.Business.Sales.BonusTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class BonusTrackingService : BaseService<vw_BonusTracking>
    {
        public BonusTrackingService(int userId) 
        {
            UserId = userId;
        }

        public RunResult Tracking(BonusTrackingDto model)
        {
            try
            {
                var res = Get();
                return new RunResult { Succeeded=true};
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RunResult { Succeeded = false };
            }
        }
    }
  
}
