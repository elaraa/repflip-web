﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class TargetAnalyzer
    {
        private int UserId { get; set; }
        public TargetAnalyzer(int userId)
        {
            UserId = userId;
        }
        public sp_Target_Analysis_GetSummary GetSummary(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null) {
            var res = new sp_Target_Analysis_GetSummary();
            using (var db = new DBModel())
                res= db.Sp_Target_Analysis_GetSummary(1, year??DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).FirstOrDefault();

            res.TargetGrowth = res.TargetGrowth ?? 1;
            res.ProductTargetGrowth = res.ProductTargetGrowth ?? 1;
            res.ProfileTargetGrowth = res.ProfileTargetGrowth ?? 1;
            res.TerritoryTargetGrowth = res.TerritoryTargetGrowth ?? 1;            

            return res;
        }
        public List<fn_Target_ProductAnalysis> GetProductAnalysis(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
            using (var db = new DBModel())
                return db.fn_Target_ProductAnalysis(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();
        }
        public List<sp_Target_TerritoryAnalysis> GetTerritoryAnalysis(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null)
        {
            using (var db = new DBModel())
                return db.sp_Target_TerritoryAnalysis(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();
        }
        public List<sp_Target_Analysis_ProfileTarget> GetProfileAnalysis(int? year = null, int? teamId = null, int? productId = null, int? TerritoryId = null, int? employeeId = null) {
            using (var db = new DBModel())
                return db.sp_Target_Analysis_ProfileTarget(1, year ?? DateTime.Now.Year, UserId, teamId, productId, TerritoryId, employeeId).ToList();
        }
    }
}
