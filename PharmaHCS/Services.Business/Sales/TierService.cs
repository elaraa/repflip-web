﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class TierService : BaseService<Tier>
    {
        public TierService(int userId) : base(userId)
        {
        }
        
        public bool DeleteTier(int id)
        {
            try
            {
                var tier = GetOne(a => a.TierId == id);
                if (tier!=null)
                {
                    if (!Any<TierBonu>(a=>a.TierId==tier.TierId))
                    {
                        Delete(a => a.TierId == tier.TierId);
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        
    }
}
