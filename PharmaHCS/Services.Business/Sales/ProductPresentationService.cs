﻿using AutoMapper;
using DB.ORM.DB;
using Services.Business.Base;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Helpers.File;
using static DB.ORM.DB.ProductPresentation;
using static Services.Helper.EnumHelpers;

namespace Services.Business.Sales
{
    public class ProductPresentationService : BaseService<ProductPresentationDto>
    {
        public ProductPresentationService(int userId) : base(userId)
        { 
        }

        public bool toogleActive(int id)
        {
            try
            {
                var presentation = GetOne<ProductPresentation>(x => x.PresentationId == id);
                presentation.Active = presentation.Active == true ? false : true;
                AddOrUpdate<ProductPresentation>(presentation);
                return true; 
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
        public bool deletePresentation(int id)
        {
            try
            {
                var presentation = GetOne<ProductPresentation>(x => x.PresentationId == id);
                if (presentation != null)
                {
                    if (!Any<VisitPresentation>(a => a.PresentationId == presentation.PresentationId))
                    {
                        Delete<ProductPresentation>(a => a.PresentationId == id);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
            return false;
        }

        public override RunResult AddOrUpdate(ProductPresentationDto productPresentationDto, bool isCreate)
        {
            try 
            {
                 var probuctPresentationDb = AddOrUpdate(Mapper.Map<ProductPresentation>(productPresentationDto));
                 if (probuctPresentationDb != null)
                 {
                     var baseUploadUrl = "Uploads/Presentation" + '/' + probuctPresentationDb.PresentationId.ToString() + '/';

                     if (productPresentationDto.UploadFile != null && productPresentationDto.UploadFile.ContentLength > 0)
                     {
                         var isUploaded = FileHelper.saveFile(productPresentationDto.UploadFile, "Presentation", probuctPresentationDb.PresentationId);
                         if (isUploaded)
                         {
                             probuctPresentationDb.FilePath = baseUploadUrl + productPresentationDto.UploadFile.FileName;
                             probuctPresentationDb.Ext = System.IO.Path.GetExtension(productPresentationDto.UploadFile.FileName).Replace(".", "");
                             AddOrUpdate(probuctPresentationDb);
                         }
                     }
                     return MessageService.ShowSuccessMessage(isCreate);
                 }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = PH.Message.CodeError };
        } 
         
        public string GetPath(int id)
        {
            return GetOne<ProductPresentation>(a => a.PresentationId == id).FilePath;
        }
    }
}
