﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales.MovementLogDTo
{
    public class VisitDto
    {
        public string AccountName { get; set; }
       // public List<decimal> latlng { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
         
    }
}
