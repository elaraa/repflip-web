﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales.MovementLogDTo
{
    public class MovementDto
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
    }
}
