﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Services.Helper.EnumHelpers;

namespace Services.Business.Sales
{
    public class TargetAdjustmentService : BaseService<Target>
    {
        public TargetAdjustmentService(int userId) : base(userId)
        {
        }
        public RunResult AdjustTarget(vw_TargetAdjustment target)
        {
            try
            {
                int count = db.Sp_SalesAdjustmentTarget(target.TimeId, target.ProductId, target.TerritoryId
                    , target.ProfileId, target.TargetValue, target.TypeId == (int)TargetAdjustmentType.TargetSales|| target.TypeId == (int)TargetAdjustmentType.TargetBiggerSales
                    , target.TypeId == (int)TargetAdjustmentType.TargetSales);
               
                if(count > 0)
                     return new RunResult { Succeeded = true, SuccessDesc = count > 1 ? count + PH.TargetAdjustment.AdjustedTargets : count + PH.TargetAdjustment.AdjustedTargets};
                else
                     return new RunResult { Succeeded = false, ErrorDesc = PH.TargetAdjustment.NoMatchedTarget};
            }
            catch (Exception e)
            {
                return new RunResult { Succeeded = false, ErrorDesc = e.Message };
            }
        }
    }
}
