﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Services.Business.Sales
{
    public class AdminManualRun : BaseService<Setting>
    {
        public AdminManualRun(int userId) : base(userId)
        {
        }
        public RunResult ExecuteEmpStructure()
        {
            try
            {
                db.Sp_EmpStructure();
                return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                return new RunResult { Succeeded = false, ErrorDesc= e.Message};
            }
        }

        public RunResult ExecuteCeilingCalc(int timeId)
        {
            try
            {
                //HostingEnvironment.QueueBackgroundWorkItem(a=> db.Sp_SalesCrunch_FCS_Ceiling_2020_Per_Period(timeId));
                db.Sp_SalesCrunch_FCS_Ceiling_2020_Per_Period(timeId);
                return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                return new RunResult { Succeeded = false, ErrorDesc = e.Message };
            }
        }
    }
    public class RunResult
    {
        public bool Succeeded { get; set; }
        public string ErrorDesc { get; set; }
        public string SuccessDesc { get; set; }  
    }
}
