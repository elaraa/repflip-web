﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class ImportResult {
        public int ResultId { get; set; }
        public bool Succeeded { get; set; }
        public List<string> ErrorList { get; set; }
        public ImportResult()
        {
            ResultId = int.MinValue;
            Succeeded = false;
            ErrorList = new List<string>();
        }
    }
    public class MarketShareService : BaseService<MarketShare>
    {
        public MarketShareService(int userId): base(userId)
        {
        }

        public ImportResult Import(int year, int level, int countryId, DataSet dataSet)
        {
            var result = new ImportResult();
            ///from dataset read the 2 columns ["IMS Brick", "other"] => dataset will have 3 columns for "AutoID"
            if (dataSet.Tables.Count < 1) {result.ErrorList.Add("Invalid Format, file does not contain valid columns");  return result; }
            if (dataSet.Tables[0].Columns.Contains("IMS BRICK") && dataSet.Tables[0].Columns.Count == 3 ){
                if (dataSet.Tables[0].Rows.Count < 1) { result.ErrorList.Add("File does not contain records"); return result; }

                var msTerritory = new List<MarketShareTerritory>();
                var terr = db.fn_Territory_GetParentLevelForLeaf(countryId, level).ToList();

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    ///map brick to territory
                    var brickName = row["IMS BRICK"].ToString().Trim().ToLower();
                    var mshareString = row["GEO SH. V."].ToString().Trim();
                    if (string.IsNullOrEmpty(brickName) && string.IsNullOrEmpty(mshareString)) { continue; }
                    var foundTerr = terr.FirstOrDefault(w => w.RootTerritoryName.ToLower() == brickName);
                    if (foundTerr != null && decimal.TryParse(mshareString, out decimal msshare))
                    {
                        var tLeafs = terr.Where(w => w.RootTerritoryId == foundTerr.RootTerritoryId && w.TerritoryId != foundTerr.RootTerritoryId);
                        foreach (var item in tLeafs)
                            msTerritory.Add(new MarketShareTerritory { TerritoryId = foundTerr.TerritoryId, MarketShare = msshare/tLeafs.Count() });
                        
                    }
                    else {
                        if (foundTerr == null) result.ErrorList.Add(string.Format("Row {1}: Territory '{0}' does not exist", brickName, row[0]));
                        if (!decimal.TryParse(mshareString, out msshare)) result.ErrorList.Add(string.Format("Row {1}: Share value is not number '{0}'", mshareString, row[0]));
                    }
                    ///make sure bricks are related to leafs ??
                    ///validate share column to be number, and not overflow
                }
                ///save it to marketshare, and marketshareterritory
                if (result.ErrorList.Count > 0) return result;

                var ms = db.MarketShares.FirstOrDefault(w => w.Year == year && w.CountryId == countryId);
                ms = ms ?? new MarketShare { Year = year, CountryId = countryId };
                db.MarketShareTerritories.Where(w => w.MarketShareId == ms.MarketShareId).DeleteFromQuery();

                foreach (var item in msTerritory)
                    ms.MarketShareTerritories.Add(item);

                if (ms.MarketShareId < 1) db.MarketShares.Add(ms);

                result.Succeeded =  db.SaveChanges()>0;
                result.ResultId = ms.MarketShareId;
            }
            else { result.ErrorList.Add("Invalid Format, column names does not match"); }
            
            return result;
        }
        public override int Delete(Expression<Func<MarketShare, bool>> predicate)
        {
            var ms = db.MarketShares.Where(predicate);
            (from mst in db.MarketShareTerritories 
                        join m in ms on mst.MarketShareId equals m.MarketShareId
                      select mst).DeleteFromQuery();
            db.MarketShares.Where(predicate).DeleteFromQuery();
            return db.SaveChanges();
            
        }
        public DataTable Export(int year, int level, int countryId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("IMS BRICK"); table.Columns.Add("GEO SH. V.");

            var existingData = db.fn_MarketShare_GetTemplate(year, level    , countryId).ToList();
            foreach (var item in existingData)
                table.Rows.Add(item.TerritoryName, item.MarketShare);
            return table;
        }
    }
}
