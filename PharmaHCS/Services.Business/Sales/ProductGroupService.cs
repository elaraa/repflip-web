﻿using AutoMapper;
using DB.ORM.DB;
using Services.Business.Base;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Helpers.File;
using static DB.ORM.DB.ProductPresentation;
using static Services.Helper.EnumHelpers;

namespace Services.Business.Sales
{
    public class ProductGroupService : BaseService<ProductGroup>
    {
        public ProductGroupService(int userId) : base(userId)
        { 
        }

       
        public bool deleteGroup(int id)
        {
            try
            {
                var group = GetOne<ProductGroup>(x => x.GroupId == id);
                if (group != null)
                {
                    
                        Delete<ProductGroup>(a => a.GroupId == id);
                        return true;
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
            return false;
        }

       
    }
}
