﻿using DB.ORM.DB;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Services.Helper.EnumHelpers;

namespace Services.Business.Sales
{
    public class ProductPriceService : BaseService<ProductPrice>
    {
        public ProductPriceService(int userId) : base(userId)
        {
        }
        public RunResult IsOverlapped(ProductPrice productPrice) {
            try
            {
                if (Get<ProductPrice>(c => c.ProductPriceId != productPrice.ProductPriceId 
                      && c.ProductId == productPrice.ProductId 
                      && (productPrice.DosageFormId.HasValue ? c.DosageFormId.Value == productPrice.DosageFormId : true)
                      && (productPrice.DistributorId.HasValue ? c.DistributorId.Value == productPrice.DistributorId : true)
                      && (productPrice.TerritoryId.HasValue ? c.TerritoryId.Value == productPrice.TerritoryId : true)
                      && (productPrice.AccountId.HasValue ? c.AccountId.Value == productPrice.AccountId : true)

                      && ((c.ToDate.HasValue ? productPrice.FromDate <= c.ToDate.Value : true) &&
                         (productPrice.ToDate.HasValue ? productPrice.ToDate.Value >= c.FromDate : true))).Any())

                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DateOverlapping };
                else
                    return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                return new RunResult { Succeeded = false, ErrorDesc = e.Message };
            }
        }

        public List<vw_ProductPrice> GetProductPrices(int? productId = null) {
            var prices = Get<vw_ProductPrice>(c => productId.HasValue ? c.ProductId == productId : true, true).ToList();
            foreach (var price in prices)
            {
                var priceType = "";
                priceType += price.DosageFormId == null ? '0' : '1'; priceType += price.DistributorId == null ? '0' : '1';
                priceType += price.TerritoryId == null ? '0' : '1'; priceType += price.AccountId == null ? '0' : '1';
                int type = int.Parse(priceType);
                price.PriceType = type !=0 ? ((ProductPriceCritria)type).DisplayName() : "";
            }

            return prices;
        }
    }
}
