﻿using DB.ORM.DB;
using Services.Business.Sales.MovementLogDTo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales
{
    public class MovementLogService : BaseService<vw_Movement>
    {
        public MovementLogService(int userId) : base(userId)
        {

        }

        public IQueryable<vw_Movement> getVisits(int? id, DateTime? date)
        {
            return Get(a => a.EmployeeId == id && a.VisitDate == date.Value);
        }


        public List<VisitDto> findVisits(int? id, DateTime? date = null)
        {
            var Visits = Get(a => a.EmployeeId == id).ToList().Where(c => c.Latitude.HasValue &&
                 c.Longitude.HasValue && date != null ? c.VisitDate.Date == date.Value.Date : true).Select(c => new VisitDto
                 {
                    // latlng = GetLatLng(c.Latitude.Value, c.Longitude.Value),
                     Lat= c.Latitude.Value,
                     Lng= c.Longitude.Value,
                     AccountName = c.AccountName+ "<br><br>" + c.AccountTypeName+ "<br>" + (c.DoctorName == "" ? "" : c.DoctorName )+ "<br>" + c.VisitDate.ToShortDateString()
                 }).ToList();

            return Visits;
        }


        //private List<decimal> GetLatLng(decimal lat, decimal lng)
        //{
        //    var latlng = new List<decimal>();
        //    latlng.Add(lat);
        //    latlng.Add(lng);

        //    return latlng;
        //}
    }
}
