﻿using DB.ORM.DB;
using Services.Business.Sales.EmployeeStructureImportExportervice;
using Services.Business.Sales.TemplatesService;
using Services.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using Web.Helpers.File;
using static Services.Helper.EnumHelpers;

namespace Services.Business
{
    public class TemplatesService : BaseService<EmployeeHistory>
    {

        public TemplatesService(int userId) : base(userId)
        {
        }

        public  DataTable ExportTemplate(int? type, int? teamId, int? timeId, bool allData = false)
        {
            int recordsPerMinute = GetRecordsperMinute;

            var table = new DataTable();
            var dateOfPreviousMonth = DateTime.Now.AddMonths(-1).AddDays(-DateTime.Now.Day + 1);

            var time2 = GetOne<TimeDefinition>(a => (timeId!=null)?a.TimeDefinitionId==timeId : System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(a.FromDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(dateOfPreviousMonth));
            if (type == (int)EnumHelpers.TemplateStructureType.EmployeeStructure)
            {
                table=ExportEmptyTemplate(type);

                    var time = GetOne<TimeDefinition>(a => a.TimeDefinitionId == timeId);
                    var query = allData ? db.fn_GetProfilesForEmployeeTemplate(UserId, time != null ? time.FromDate : DateTime.Now).Where(a =>(teamId.HasValue)? a.TeamId == teamId:true).ToList()
                        : db.fn_GetProfilesForEmployeeTemplate(UserId, time != null ? time.FromDate : DateTime.Now).Where(a => (teamId.HasValue) ? a.TeamId == teamId : true).ToList().Take(recordsPerMinute);
                    var employees = query.Select(a => new
                    {
                        id = a.EmployeeHistoryId,
                        a.EmployeeCode,
                        a.Department,
                        a.JobTitle,
                        a.Team,
                        a.CanVisit,
                        a.Employee,
                        a.NationalIDNumber,
                        a.ProfileCode,
                        a.ProfileName,
                        a.ManagerCode,
                    }).Distinct();
                    foreach (var item in employees)
                    {
                        DataRow row = table.NewRow();
                        row[0] = item.EmployeeCode;
                        row[1] = item.Employee;
                        row[2] = item.NationalIDNumber;
                        row[3] = item.Department;
                        row[4] = item.ManagerCode;
                        row[5] = item.CanVisit;
                        row[6] = item.JobTitle;
                        row[7] = item.Team;
                        row[8] = item.ProfileCode;
                        row[9] = item.ProfileName;
                        table.Rows.Add(row);
                    }
                    return table;
            
            }
            else if (type == (int)EnumHelpers.TemplateStructureType.ProfileAccount)
            {
                table = ExportEmptyTemplate(type);

                var query = allData ? Get<vw_ProfileAccount>(a => (teamId != null) ? a.Team == teamId : true).Where(a => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(a.FromDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(time2.FromDate)).ToList()
                                  : Get<vw_ProfileAccount>(a => (teamId != null) ? a.Team == teamId : true).Where(a => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(a.FromDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(time2.FromDate)).ToList().Take(recordsPerMinute);

                var profileAccounts = query.Select(a => new
                    {
                        id = a.ProfileAccountId,
                        a.ProfileCode,
                        a.AccountCode,
                        a.Perccentage,
                        a.ProductCode,
                        a.DosageFormCode
                    }).Distinct();
                    foreach (var item in profileAccounts)
                    {
                        DataRow row = table.NewRow();
                        row[0] = item.ProfileCode;
                        row[1] = item.AccountCode;
                        row[2] = item.Perccentage;
                        row[3] = item.ProductCode;
                        row[4] = item.DosageFormCode;
                        table.Rows.Add(row);
                    }
                    return table;
            }
            else
            {
                 table = ExportEmptyTemplate(type);
                 var query = allData ? Get<vw_ProfileTerritory>(a => (teamId != null) ? a.TeamId == teamId : true ).Where(a => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(a.FromDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(time2.FromDate)).ToList()
                             : Get<vw_ProfileTerritory>(a => (teamId != null) ? a.TeamId == teamId : true).Where(a => System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(a.FromDate) == System.Data.Entity.Core.Objects.EntityFunctions.TruncateTime(time2.FromDate)).ToList().Take(recordsPerMinute);
                    var profileTerritory = query.Select(a => new
                    {
                        id = a.ProfileTerritoryId,
                        a.ProfileCode,
                        a.TerritoryCode,
                        a.Percentage
                    }).Distinct();
                    foreach (var item in profileTerritory)
                    {
                        DataRow row = table.NewRow();
                        row[0] = item.ProfileCode;
                        row[1] = item.TerritoryCode;
                        row[2] = item.Percentage;
                        table.Rows.Add(row);
                    }
                    return table;
            }

        }

        public DataSet uploadFiles(TemplatViewModel model)
        {
            System.Data.DataSet data = FileHelper.GetDataSetFromExcel(
               FileHelper.UploadFile(model.File)
               );

            return data;
        }
        
        public List<TemplateValidation> EmployeeHistoryProfileFailedReasons(DataTable data)
        {
            var list = (from DataRow dr in data.Rows
                        select new TemplateEmployeeStructure()
                        {
                            RowIndex = dr["RowIndex"].ToString(),
                            InvalidDepartment = dr["InvalidDepartment"].ToString(),
                            InvalidJobTitle = dr["InvalidJobTitle"].ToString(),
                            InvalidTeam = dr["InvalidTeam"].ToString(),
                            InvalidManagerCode = dr["InvalidManagerCode"].ToString(),
                            IsDuplicated = dr["IsDuplicated"].ToString(),
                            IsOverlappedEmployeeHistory = dr["IsOverlappedEmployeeHistory"].ToString(),
                            IsOverlappedEmployeeProfile = dr["IsOverlappedEmployeeProfile"].ToString(),
                            RequiredTeamForProfile = dr["RequiredTeamForProfile"].ToString()
                        }).ToList();
            var res = list.Select(a => new TemplateValidation
            {
                Index = a.RowIndex,
                Comment = PH.TemplatesImprtExport.RowIssues + (a.InvalidDepartment == "False" ? "" : ",Invalid Department ") + (a.InvalidJobTitle == "False" ? "" : ",Invalid Job Title") +
                           (a.InvalidTeam == "False" ? "" : ",Invalid Team ") + (a.InvalidManagerCode == "False" ? "" : ",Invalid Manager Code ") + (a.IsDuplicated == "False" ? "" : ",Is Duplicated ") +
                           (a.IsOverlappedEmployeeHistory == "False" ? "" : ",Is Overlapped Employee History ") + (a.IsOverlappedEmployeeProfile == "False" ? "" : ",Is Overlapped Employee Profile  ") + (a.RequiredTeamForProfile == "False" ? "" : "Required Team For Profile ")
            }).ToList();
            return res;
        }

        public List<TemplateValidation> ProfileAccountFailedReasons(DataTable data)
        {
            var list = (from DataRow dr in data.Rows
                        select new TemplateProfileAccount()
                        {
                            RowIndex = dr["RowIndex"].ToString(),
                            InvalidAccountCode = dr["InvalidAccountCode"].ToString(),
                            InvalidProductCode = dr["InvalidProductCode"].ToString(),
                            InvalidDosageFormCode = dr["InvalidDosageFormCode"].ToString(),
                            InvalidProfileCode = dr["InvalidProfileCode"].ToString(),
                            IsDuplicated = dr["IsDuplicated"].ToString(),
                            IsOverlapped = dr["IsOverlapped"].ToString(),
                            IsExceedPercentage = dr["IsExceedPercentage"].ToString(),
                            ExceededPercentage = dr["ExceededPercentage"].ToString()
                        }).ToList();
            var res = list.Select(a => new TemplateValidation
            {
                Index = a.RowIndex,
                Comment = "This Row has Following Issues " + (a.InvalidAccountCode == "False" ? "" : ",Invalid Account Code ") + (a.InvalidDosageFormCode == "False" ? "" : ",Invalid DosageForm Code ") +
                           (a.InvalidProductCode == "False" ? "" : ",Invalid Product Code ") + (a.InvalidProfileCode == "False" ? "" : ",Invalid Profile Code , ") + (a.IsDuplicated == "False" ? "" : ",Is Duplicated ") +
                           (a.IsOverlapped == "False" ? "" : ",Is Overlapped ") + (a.IsExceedPercentage == "False" ? "" : ",Is Exceed Percentage  ") + (a.ExceededPercentage == "NULL" ? "" : a.ExceededPercentage)
            }).ToList();
            return res;
        }

        public List<TemplateValidation> ProfileTerritoryFailedReasons(DataTable data)
        {
            var list = (from DataRow dr in data.Rows
                        select new TemplateProfileTerritory()
                        {
                            RowIndex = dr["RowIndex"].ToString(),
                            InvalidTerritoryCode = dr["InvalidTerritoryCode"].ToString(),
                            InvalidProfileCode = dr["InvalidProfileCode"].ToString(),
                            IsDuplicated = dr["IsDuplicated"].ToString(),
                            IsOverlapped = dr["IsOverlapped"].ToString(),
                            IsExceedPercentage = dr["IsExceedPercentage"].ToString(),
                            ExceededPercentage = dr["ExceededPercentage"].ToString()
                        }).ToList();
            var res = list.Select(a => new TemplateValidation
            {
                Index = a.RowIndex,
                Comment = "This Row has Following Issues " + (a.InvalidTerritoryCode == "False" ? "" : ",Invalid Territory Code ") + (a.InvalidProfileCode == "False" ? "" : ",Invalid Profile Code  ") + (a.IsDuplicated == "False" ? "" : ",Is Duplicated ") +
                           (a.IsOverlapped == "False" ? "" : ",Is Overlapped ") + (a.IsExceedPercentage == "False" ? "" : ",Is Exceed Percentage  ") + (a.ExceededPercentage == "NULL" ? "" : a.ExceededPercentage)
            }).ToList();
            return res;
        }

        public DataTable ExportEmptyTemplate(int? type)
        {
            var table = new DataTable();
            if (type == (int)EnumHelpers.TemplateStructureType.EmployeeStructure)
            {
                table.Columns.Add("Employee Code"); table.Columns.Add("Employee Name"); table.Columns.Add("NationalID"); table.Columns.Add("Department");
                table.Columns.Add("Manager Code"); table.Columns.Add("Can Visit"); table.Columns.Add("Job Title");
                table.Columns.Add("Team"); table.Columns.Add("Profile Code"); table.Columns.Add("Profile Name");
                return table;
            }
            else if (type == (int)EnumHelpers.TemplateStructureType.ProfileAccount)
            {
                table.Columns.Add("Profile Code"); table.Columns.Add("Account Code"); table.Columns.Add("Percentage");
                table.Columns.Add("Product Code"); table.Columns.Add("DosageForm Code");
                return table;
            }
            else
            {
                table.Columns.Add("Profile Code"); table.Columns.Add("Territory Code"); table.Columns.Add("Percentage");
                return table;
            }
        }

        public FileNameWithMandatoryFields FileName(int type)
        { 
            var fileMandatory = new FileNameWithMandatoryFields();
            if (type == (int)EnumHelpers.TemplateStructureType.EmployeeStructure)
            {
                fileMandatory.FileName = string.Format("{0}.xlsx", EnumHelpers.TemplateStructureType.EmployeeStructure.DescriptionAttr());
                fileMandatory.MandatoryFields = TemplateMandatoryFields.EmployeeStructure.DescriptionAttr();
            }

            else if (type == (int)EnumHelpers.TemplateStructureType.ProfileAccount)
            {
                fileMandatory.FileName = string.Format("{0}.xlsx", EnumHelpers.TemplateStructureType.ProfileAccount.DescriptionAttr());
                fileMandatory.MandatoryFields = TemplateMandatoryFields.ProfileAccount.DescriptionAttr();
            }
            else
            {
                fileMandatory.FileName = string.Format("{0}.xlsx", EnumHelpers.TemplateStructureType.ProfileTerritory.DescriptionAttr());
                fileMandatory.MandatoryFields = TemplateMandatoryFields.ProfileTerritory.DescriptionAttr();
            }

            return fileMandatory;
        }

        public int GetCount(int? type, int? teamId, int? timeId)
        {
            try
            {
                var dateOfPreviousMonth = DateTime.Now.AddMonths(-1).AddDays(-DateTime.Now.Day + 1);

                var time2 = GetOne<TimeDefinition>(a => (timeId != null) ? a.TimeDefinitionId == timeId :DbFunctions.TruncateTime(a.FromDate) == DbFunctions.TruncateTime(dateOfPreviousMonth));
                if (type == (int)TemplateStructureType.EmployeeStructure)
                {
                    var time = GetOne<TimeDefinition>(a => a.TimeDefinitionId == timeId);
                    return db.fn_GetProfilesForEmployeeTemplate(UserId, time != null ? time.FromDate : DateTime.Now).Where(a => (teamId.HasValue) ? a.TeamId == teamId : true).Count();
                }
                else if (type == (int)TemplateStructureType.ProfileAccount)
                    // return Count<vw_ProfileAccount>(a => (teamId != null) ? a.Team == teamId : true && (DbFunctions.TruncateTime(a.FromDate) == DbFunctions.TruncateTime(time2.FromDate)));
                    return Get<vw_ProfileAccount>(a => (teamId != null) ? a.Team == teamId : true).Where(a => DbFunctions.TruncateTime(a.FromDate) == DbFunctions.TruncateTime(time2.FromDate)).Count();
                else
                    //  return Count<vw_ProfileTerritory>(a => (teamId != null) ? a.TeamId == teamId : true && (DbFunctions.TruncateTime(a.FromDate) == DbFunctions.TruncateTime(time2.FromDate)));
                    return Get<vw_ProfileTerritory>(a => (teamId != null) ? a.TeamId == teamId : true).Where(a=>DbFunctions.TruncateTime(a.FromDate) == DbFunctions.TruncateTime(time2.FromDate)).Count();
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return 0;
            }
        }

        public int GetRecordsperMinute
        { 
            get
            {
                try
                {
                    int value = int.Parse(Get<Setting>().AsEnumerable().FirstOrDefault(c => c.SettingLabel == SettingApp.RecordperMinute.DescriptionAttr()).SettingValue);
                    return value;
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    return -1;
                }
            }
        }
    }
    public class FileMandatoryFields
    {
        public string  FileName { get; set; }
        public string Type { get; set; } 

    }
}
