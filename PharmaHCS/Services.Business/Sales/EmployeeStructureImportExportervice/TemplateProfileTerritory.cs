﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Business.Sales.TemplatesService
{
    public class TemplateProfileTerritory
    {

        public string RowIndex { get; set; }

        public string ProfileCode { get; set; }

        public string TerritoryCode { get; set; }

        public decimal Percentage { get; set; }

        public string InvalidProfileCode { get; set; } 

        public string InvalidTerritoryCode { get; set; } 

        public string IsDuplicated { get; set; } 

        public string IsOverlapped { get; set; } 

        public string IsExceedPercentage { get; set; } 

        public string ExceededPercentage { get; set; }

    }
}
