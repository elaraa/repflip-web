﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Sales.EmployeeStructureImportExportervice
{
    public class FileNameWithMandatoryFields
    {
        public string FileName { get; set; }
        public string MandatoryFields { get; set; }
    } 
}
