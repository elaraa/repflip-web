﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Business.Sales.TemplatesService
{
    public class TemplateProfileAccount 
    {

        public string RowIndex { get; set; }

        public string ProfileCode { get; set; }

        public string AccountCode { get; set; }

        public decimal Perccentage { get; set; }

        public string ProductCode { get; set; }

        public string DosageFormCode { get; set; }

        public string InvalidProfileCode { get; set; }

        public string InvalidAccountCode { get; set; } 

        public string InvalidProductCode { get; set; }

        public string InvalidDosageFormCode { get; set; } 

        public string IsDuplicated { get; set; } 

        public string IsOverlapped { get; set; } 

        public string IsExceedPercentage { get; set; } 

        public string ExceededPercentage { get; set; }

    } 
}
