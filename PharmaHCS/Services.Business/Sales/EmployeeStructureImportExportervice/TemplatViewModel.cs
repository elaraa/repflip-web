﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Business.Sales.TemplatesService
{
    public class TemplatViewModel
    {
        [Required(ErrorMessageResourceName = "RequiredType", ErrorMessageResourceType = typeof(PH.TemplatesImprtExport))]
        public int TypeId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredTime", ErrorMessageResourceType = typeof(PH.TemplatesImprtExport))]
        public int TimeId { get; set; }

        public int? TeamId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredFile", ErrorMessageResourceType = typeof(PH.TemplatesImprtExport))]
        public HttpPostedFileBase File { get; set; } 
    } 
}
