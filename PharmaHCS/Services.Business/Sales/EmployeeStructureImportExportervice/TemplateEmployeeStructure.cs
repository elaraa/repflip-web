﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Business.Sales.TemplatesService
{
    public class TemplateEmployeeStructure
    {
            public string RowIndex { get; set; }

            public string EmployeeName { get; set; }

            public string EmployeeCode { get; set; }

            public string ManagerCode { get; set; }

            public string Department { get; set; }

            public string Team { get; set; }

            public string JobTitle { get; set; }

            public bool? CanVisit { get; set; }

            public string ProfileCode { get; set; }

            public string InvalidDepartment { get; set; } 

            public string InvalidJobTitle { get; set; } 

            public string InvalidTeam { get; set; } 

            public string InvalidManagerCode { get; set; }

            public string IsDuplicated { get; set; } 

            public string IsOverlappedEmployeeHistory { get; set; }

            public string IsOverlappedEmployeeProfile { get; set; } 

            public string RequiredTeamForProfile { get; set; }
       
    } 
}
