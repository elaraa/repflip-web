﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class MProfile
    {
        public string Pic { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime HiringDate { get; set; }
        public string JobTitle { get; set; }
        public string DepartmentName { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        [MinLength(11)]
        public string Mobile { get; set; }
        public string Country { get; set; }
        public string ManagerName { get; set; }
        public string Password { get; set; }
        public string MaritalStatusName { get; set; }
        public string GenderText { get; set; }
    }
}
