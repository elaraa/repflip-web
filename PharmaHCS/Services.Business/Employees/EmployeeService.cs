﻿using DB.ORM.DB;
using Services.Business.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business.Employees
{
    public class EmployeeService : BaseService<Employee>
    {
        public EmployeeService(int userId) : base(userId)
        {

        }
        public bool edit(Employee model)
        {
            if (model != null)
            {

                if (AddOrUpdate(model) != null)
                {
                    if (!model.Active)
                    {
                        var employeeHistory = Get<EmployeeHistory>(a => a.EmployeeId == model.EmployeeId).AsEnumerable().LastOrDefault();
                        employeeHistory.ToDate = employeeHistory.ToDate == null ? employeeHistory.ToDate = DateTime.Now : employeeHistory.ToDate;
                        AddOrUpdate<EmployeeHistory>(employeeHistory);

                    }
                    return true;
                }

            }
            return false;

        }

        public bool create(Employee model)
        {
            if (model != null)
            {
                if (Add(model))
                {
                    EmployeeHistory employeeHistory = new EmployeeHistory
                    {
                        EmployeeId = model.EmployeeId,
                        JobTitleId = model.JobTitleId,
                        DepartmentId = model.DepartmentId.Value,
                        ManagerId = UserId,
                        OrganizationLevel = model.OrganizationLevel,
                        FromDate = model.HireDate,
                        CountryId = model.CountryId,
                    };
                    AddOrUpdate<EmployeeHistory>(employeeHistory);
                    return true;
                }
               
            }
            return true;
   
        
        }


        public RunResult IsOverlapped(EmployeeHistory history)
        {
            try
            {
                if(history.EmployeeId==history.ManagerId)
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.EmployeeHistoryError };
                else if (history.ToDate.HasValue ? history.ToDate < history.FromDate : true)
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DatesError };
                else if (Get<EmployeeHistory>(c=>c.EmployeeHistoryId!=history.EmployeeHistoryId && c.FromDate==history.FromDate
                        &&c.ToDate == history.ToDate).Any())
                    return new RunResult { Succeeded = false, ErrorDesc = PH.Message.DateOverlapping };
                else
                    return new RunResult { Succeeded = true };
            }
            catch (Exception e)
            {
                return new RunResult { Succeeded = false, ErrorDesc = e.Message };
            }
        }

        //public IEnumerable<TeamProfile> getTeams(int id)
        //{
        //    var pro = GetOne<EmployeeProfile>(a => a.EmployeeId == id).ProfileId ;
        //   var profiles =Get<TeamProfile>(p => p.ProfileId == pro);
        //    return profiles;
        //}
    }
}
